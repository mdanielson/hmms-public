#!/usr/bin/env python

# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import os.path
import sys
from pkg_resources import parse_version
from distutils.command.clean import clean as Clean
from Cython.Build import cythonize

VERSION = '0.0.1'
SCIPY_MIN_VERSION = '0.17.0'
NUMPY_MIN_VERSION = '1.11.0'

def configuration(parent_package='', top_path=None):
    if os.path.exists('MANIFEST'):
        os.remove('MANIFEST')

    from numpy.distutils.misc_util import Configuration

    config = Configuration(None, parent_package, top_path)

    # Avoid non-useful msg:
    # "Ignoring attempt to set 'name' (from ... "
    config.set_options(ignore_setup_xxx_py=True,
                       assume_default_configuration=True,
                       delegate_options_to_subpackages=True,
                       quiet=True)

    config.add_subpackage('hmm')
#    # Append a few comiler directives
#    for module in config.ext_modules:
#        module.define_macros.append(("CYTHON_TRACE", 1))
    compiler_directives = {
        "warn.unused": True,
        "warn.undeclared": True,
        "warn.unused_result": True,
        "warn.unused_arg": True,
        "embedsignature": True,
        "unraisable_tracebacks": True
    }
    import numpy as np
    config.ext_modules = cythonize(config.ext_modules, annotate=True, compiler_directives=compiler_directives)
    # Sadly, this part needs to be done manually.
    for ext in config.ext_modules:
        for k, v in np.distutils.misc_util.get_info("npymath").items():
            setattr(ext, k, v)
        ext.include_dirs = [np.get_include()]
        ext.extra_compile_args = ["-O3", ]


    return config

class CleanCommand(Clean):
    """
    From scikit-learn
    """
    description = "Remove build artifacts from the source tree"

    def run(self):
        Clean.run(self)
        # Remove c files if we are not within a sdist package
        cwd = os.path.abspath(os.path.dirname(__file__))
        remove_c_files = not os.path.exists(os.path.join(cwd, 'PKG-INFO'))
        if remove_c_files:
            print('Will remove generated .c files')
        if os.path.exists('build'):
            shutil.rmtree('build')
        for dirpath, dirnames, filenames in os.walk('sklearn'):
            for filename in filenames:
                if any(filename.endswith(suffix) for suffix in
                       (".so", ".pyd", ".dll", ".pyc")):
                    os.unlink(os.path.join(dirpath, filename))
                    continue
                extension = os.path.splitext(filename)[1]
                if remove_c_files and extension in ['.c', '.cpp']:
                    pyx_file = str.replace(filename, extension, '.pyx')
                    if os.path.exists(os.path.join(dirpath, pyx_file)):
                        os.unlink(os.path.join(dirpath, filename))
            for dirname in dirnames:
                if dirname == '__pycache__':
                    shutil.rmtree(os.path.join(dirpath, dirname))

def get_numpy_status():
    """
    Returns a dictionary containing a boolean specifying whether NumPy
    is up-to-date, along with the version string (empty string if
    not installed).
    """
    numpy_status = {}
    try:
        import numpy
        numpy_version = numpy.__version__
        numpy_status['up_to_date'] = parse_version(
            numpy_version) >= parse_version(NUMPY_MIN_VERSION)
        numpy_status['version'] = numpy_version
    except ImportError:
        traceback.print_exc()
        numpy_status['up_to_date'] = False
        numpy_status['version'] = ""
    return numpy_status


cmdclass = {'clean': CleanCommand}


def setup_package():
    metadata = dict(name="hmm",
                    maintainer="mdanielson",
                    maintainer_email="mgd5@st-andrews.ac.uk",
                    description="",
                    license="new BSD",
                    url="",
                    download_url="",
                    version=VERSION,
                    long_description="",
                    classifiers=['Intended Audience :: Science/Research',
                                 'Intended Audience :: Developers',
                                 'License :: OSI Approved',
                                 'Programming Language :: C',
                                 'Programming Language :: Python',
                                 'Topic :: Software Development',
                                 'Topic :: Scientific/Engineering',
                                 'Operating System :: Microsoft :: Windows',
                                 'Operating System :: POSIX',
                                 'Operating System :: Unix',
                                 'Operating System :: MacOS',
                                 'Programming Language :: Python :: 3.7',
                                 ('Programming Language :: Python :: '
                                  'Implementation :: CPython'),
                                 ('Programming Language :: Python :: '
                                  'Implementation :: PyPy')
                                 ],
                    cmdclass=cmdclass,
                    install_requires=[
                        'numpy>={0}'.format(NUMPY_MIN_VERSION),
                        'scipy>={0}'.format(SCIPY_MIN_VERSION)
                    ],
                    )

    if len(sys.argv) == 1 or (
            len(sys.argv) >= 2 and ('--help' in sys.argv[1:] or
                                    sys.argv[1] in ('--help-commands',
                                                    'egg_info',
                                                    '--version',
                                                    'clean'))):
        # For these actions, NumPy is not required
        #
        # They are required to succeed without Numpy for example when
        # pip is used to install Scikit-learn when Numpy is not yet present in
        # the system.
        try:
            from setuptools import setup
        except ImportError:
            from distutils.core import setup

        metadata['version'] = VERSION
    else:
        numpy_status = get_numpy_status()
        numpy_req_str = "hmm requires NumPy >= {0}.\n".format(
            NUMPY_MIN_VERSION)

        instructions = ("Installation instructions are available on the "
                        "scikit-learn website: "
                        "http://scikit-learn.org/stable/install.html\n")

        if numpy_status['up_to_date'] is False:
            if numpy_status['version']:
                raise ImportError("Your installation of Numerical Python "
                                  "(NumPy) {0} is out-of-date.\n{1}{2}"
                                  .format(numpy_status['version'],
                                          numpy_req_str, instructions))
            else:
                raise ImportError("Numerical Python (NumPy) is not "
                                  "installed.\n{0}{1}"
                                  .format(numpy_req_str, instructions))

        from numpy.distutils.core import setup

        metadata['configuration'] = configuration

    setup(**metadata)


if __name__ == "__main__":
    setup_package()


#if __name__ == "__main__":
    #from numpy.distutils.core import setup
    ##print(configuration(top_path='').todict())
    #setup(configuration=configuration)
