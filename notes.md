## Notes
### HMMLearn
 * The implementation of fit() in hmmlearn appears broken - especially with regards to finding the initial state probabilities.
 * Defining an HMM, sampling from the hmm, and then using these observations to learn a new HMM, results in the HMM having crummy transition probabilities.
 * When only one observation is available, it may do the right thing.

### Our Implementation
#### TODO
 * implement sklearn-like API
 * factor out base-class of the python and cython implementations
 * implement save() and load()
 * Investigate "smart" seeds for initialization of parameters
 * Do one more test of the math by hand.
 * Rabiner:
   - Model Comparison
   - Autoregressive?
   - Descriminative Training: ML, MMI, MDI, and MCE Comparison for classification
   - Initialization parameters
 * Other methods for training an HMM
 * Fix sample() methods

## Implementation Notes
 * https://sklearn.org/developers/utilities.html#developers-utils
 * https://sklearn.org/developers/performance.html#performance-howto


