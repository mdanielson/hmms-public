PYTHONVERSION=3.8

all: build

tox:
	tox-${PYTHONVERSION} -r
autopep8:
	# whitespace, comments, newlines
	autopep8 -r hmm --in-place --select E302,E303,E305,E271,E261,E265,E231,E266,W391,E222,E202,E201,E203,E187,E225,I100,I201,E221,E124,E127,E251

flake8:
	flake8 hmm

profile_enable:
	find . -name "*.pyx" -type f | xargs sed -i 's|# cython: profile=.*|# cython: profile=True|'

profile_disable:
	find . -name "*.pyx" -type f | xargs sed -i 's|# cython: profile=.*|# cython: profile=False|'

test: build
	PYTHONPATH=. py.test-${PYTHONVERSION} tests

examples: build
	PYTHONPATH=. py.test-${PYTHONVERSION} examples

build:
	python${PYTHONVERSION} setup.py build_ext --inplace -j4

PYX=$(shell find . -name "*.pyx")
GEN_C=$(patsubst %.pyx, %.c, ${PYX}) $(patsubst %.pyx, %.html, ${PYX})

clean:
	rm -rf .tox
	rm -rf build dist hmm.egg-info
	find hmm -name "*.pyc"|xargs rm -rf
	find hmm -name "*,cover"|xargs rm -rf
	find hmm -name "*.html"|xargs rm -rf
	find hmm -name "*.c"|xargs rm -rf
	find hmm -name "*.so"|xargs rm -rf
	rm -f ${GEN_C}
	find . -type d -name __pycache__ |while read i; do  xargs rm -rf  $$i; done
	rm -rf .pytest_cache

doc:
	PYTHONPATH=${PWD}  make -C docs html

.PHONY: build test flake8 doc
