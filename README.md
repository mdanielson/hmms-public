

HMMS
===

An under development open source library for learning HMMs with EM and Variational Inference.


Contact: Matt Danielson <mgd5@st-andrews.ac.uk>

Notes
=====
 * set ```OMP_NUM_THREADS=1``` to tell the underlying linear algebra libraries we don't want threads


