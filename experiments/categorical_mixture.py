# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import argparse
import logging
import os.path
import shutil

import matplotlib.pyplot as plt
import pandas as pd
import sklearn.utils
from hmm import CategoricalHMM, CategoricalMHMM,CategoricalVariationalHMM, CategoricalVariationalMHMM, plots
from hmm.datasets import synthetic

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--n-components", type=int, default=3)
    parser.add_argument("--n-mixture-components", type=int, default=3)
    parser.add_argument("--n-explorations", type=int, default=10)
    parser.add_argument("--tol", type=float, default=1e-6)
    parser.add_argument("--n-iterations", type=int, default=5000)
    parser.add_argument("--results-dir", type=str, default="categorical-results")
    parser.add_argument("--verbose", default="ERROR", choices=["INFO", "DEBUG", "ERROR"])

    args = parser.parse_args()
    if args.verbose == "ERROR":
        args.verbose = logging.ERROR
    elif args.verbose == "INFO":
        args.verbose = logging.INFO
    elif args.verbose == "DEBUG":
        args.verbose = logging.DEBUG
    else:
        assert False
    #run_em(args)
    run_variational(args)

def run_em(args):
    name = "em-mixture"
    results = os.path.join(args.results_dir, name)
    if os.path.exists(results):
        shutil.rmtree(results)
    os.makedirs(results)
    random_state = sklearn.utils.check_random_state(None)
    models = []
    data, labels = synthetic.get_categorical_beal(random_state)
    best = None
    lls = []
    for i in range(args.n_explorations):
        mixture = CategoricalMHMM.CategoricalMHMM(
            n_mixture_components=args.n_mixture_components,
            n_components=args.n_components,
            n_iterations=args.n_iterations,
            random_state=random_state,
            n_inits=1,
            n_jobs=1,
            verbose=args.verbose,
            tol=args.tol,
            smoothing=("k", 1e-6)
        )
        mixture.fit(data)
        models.append(mixture)
        lls.append(mixture.loglikelihoods_)
        pd.Series(mixture.loglikelihoods_).to_csv(os.path.join(results, "lls-{}.csv".format(i)))
        if best is None or best.loglikelihoods_[-1] < mixture.loglikelihoods_[-1]:
            best = mixture
        print("{} Model {} has {:.4f}".format(name, i, lls[-1][-1]))

    for i in range(best.n_mixture_components):
        print("=="*10)
        print(i)
        print(pd.Series(best.pi_[i]))
        print(pd.DataFrame(best.A_[i]))
        print(pd.DataFrame(best.B_[i]))

    for i, m in enumerate(models):
        fig = plots.categorical_mixture_hinton(
            m.pi_,
            m.A_,
            m.B_,
            vmin=0,
            vmax=1,
            b_vals=["a", "b", "c"]
        )
        fig.savefig(os.path.join("{}/hinton-{}.png".format(results, i)))
        plt.close(fig)

    fig = plots.multiple_learning_plot(lls)
    fig.savefig("{}/learning-plot.png".format(results))
    plt.close(fig)

def run_variational(args):
    name = "variational-categorical-mixture"
    results = os.path.join(args.results_dir, name)
    if os.path.exists(results):
        shutil.rmtree(results)
    os.makedirs(results)
    random_state = sklearn.utils.check_random_state(None)
    models = []
    data, labels = synthetic.get_categorical_beal(random_state)
    best = None
    energies = []
    for i in range(args.n_explorations):
        mixture = CategoricalVariationalMHMM.CategoricalVariationalMHMM(
            n_mixture_components=args.n_mixture_components,
            n_components=args.n_components,
            n_iterations=args.n_iterations,
            random_state=random_state,
            n_inits=1,
            n_jobs=1,
            verbose=args.verbose,
            tol=args.tol
        )
        mixture.fit(data)
        models.append(mixture)
        energies.append(mixture.lower_bound_)
        pd.Series(mixture.lower_bound_).to_csv(os.path.join(results, "energy-{}.csv".format(i)))
        verbose_lower_bound = pd.DataFrame(mixture.verbose_lower_bound_).set_index("iteration")
        verbose_lower_bound.to_csv(os.path.join(results, "verbose-energy-{}.csv".format(i)))
        if best is None or best.lower_bound_[-1] < mixture.lower_bound_[-1]:
            best = mixture
        print("Model {} has {:.4f}".format(i, energies[-1][-1]))
        print("{} Model {} has {:.4f}".format(name, i, energies[-1][-1]))

    for i in range(best.n_mixture_components):
        print("=="*10)
        print(i)
        print(pd.Series(best.pi_posterior_[i]))
        print(pd.DataFrame(best.A_posterior_[i]))
        print(pd.DataFrame(best.B_posterior_[i]))

    for i, m in enumerate(models):
        fig = plots.categorical_mixture_hinton(
            m.mixture_weights_normalized_,
            m.pi_normalized_,
            m.A_normalized_,
            m.B_normalized_,
            vmin=0,
            vmax=1,
            b_vals=["a", "b", "c"]
        )
        fig.savefig(os.path.join("{}/hinton-{}.png".format(results, i)))
        plt.close(fig)

        fig = plots.plot_mixture_energies(m.verbose_lower_bound_)
        fig.savefig(os.path.join("{}/verbose-energy-{}.png".format(results, i)))
        plt.close(fig)

    fig = plots.multiple_learning_plot(energies)
    fig.savefig("{}/learning-plot.png".format(results))
    plt.close(fig)
if __name__ == "__main__":
    main()
