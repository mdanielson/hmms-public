# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import matplotlib.gridspec
import matplotlib.pyplot as plt

import numpy as np


def symmetric_model_distance(model_0, model_1, n=500, random_state=None):
    """
    Compute the symmetric model distance between two HMMS.

    From: A Probabilistic Distance Measure for Hidden Markov Models, Juang/Rabiner 1985

    Parameters
    ----------
    model_0 : a HMM
    model_1 : a HMM
    n : number of samples to draw from from each model

    Returns
    -------
    float : symmetric distance metric
    """
    observed_0, hidden_0, lengths_0 = model_0.sample(1, n, random_state=random_state)
    observed_1, hidden_1, lengths_1 = model_1.sample(1, n, random_state=random_state)

    d_0 = model_distance(model_0, model_1, observed_0, lengths_0)
    d_1 = model_distance(model_1, model_0, observed_1, lengths_1)
    return (d_0 + d_1) / 2


def model_distance(model_0, model_1, X_1, lengths_1):
    """
    1/T log P(O_0|model_0) - log P(O_0|model_1)

    Parameters
    ----------

    X_1 : np.ndarray
                     Samples from model_0
    model_0 : a HMM
    model_1 : a HMM

    Returns
    -------
    float
    """
    t = X_1.shape[0]
    return 1/t * (model_0.score_samples(X_1, lengths_1)[0] - model_1.score_samples(X_1, lengths_1)[0])


def explore_model_distances(model_0, model_1, n=1000, random_state=None, verbose=logging.ERROR):
    """
    Compare how two models are dissimilar as the length of a sequence increases

    Parameters
    ----------
    model_0 : A HMM
    model_1 : A HMM
    n : int

    Returns
    -------
    tuple : distances - model distances by number of samples
            symmetric_distances - symmetric model distances by number of samples
            model_0_logliks - model_0 loglikelihoods
            model_1_logliks - model_1 loglikelihoods

    """
    distances = []
    symmetric_distances = []
    model_0_logliks = []
    model_1_logliks = []

    old_model_0_verbose = model_0.verbose
    old_model_1_verbose = model_1.verbose

    model_0.verbose = verbose
    model_1.verbose = verbose

    for i in range(1, n):
        observed_0, hidden_0, lengths_0 = model_0.sample(1, i, random_state=random_state)
        observed_1, hidden_1, lengths_1 = model_1.sample(1, i, random_state=random_state)
        model_0_logliks.append(model_0.score_samples(observed_0, lengths_0)[0])
        model_1_logliks.append(model_1.score_samples(observed_0, lengths_0)[0])
        distances.append(model_distance(model_0, model_1, observed_0, lengths_0))
        symmetric_distances.append(symmetric_model_distance(model_0, model_1))

    model_0.verbose = old_model_0_verbose
    model_1.verbose = old_model_1_verbose
    return distances, symmetric_distances, model_0_logliks, model_1_logliks


def run_model_distance_tests(model_0, model_1, n=1000, random_state=None, figsize=(15, 5), verbose=logging.ERROR):
    """
    Run a series of tests and produce charts similar to those from the Juang/Rabiner paper

    Parameters
    ----------
    model_0 : A HMM
    model_1 : A HMM
    n_samples : int
    figsize : tuple

    Returns
    -------
    matplotlib.Figure
    """
    distances, symmetric_distances, model_0_logliks, model_1_logliks = explore_model_distances(model_0, model_1, n, random_state=random_state, verbose=verbose)
    f = plt.figure(figsize=figsize)
    gs = matplotlib.gridspec.GridSpec(1, 3)
    ax = f.add_subplot(gs[0, 0])

    ax.set_title("Model LogLikelihood Scores\nSequences from model_0")
    ax.plot(np.arange(1, n), model_0_logliks, label="model_0")
    ax.plot(np.arange(1, n), model_1_logliks, label="model_1")

    ax.legend(loc="best")
    ax.set_ylabel("Log Probability")
    ax.set_xlabel("Sequence Length")

    ax = f.add_subplot(gs[0, 1])
    ax.set_title("Model Distance Metric")
    ax.plot(np.arange(1, n), distances)
    ax.set_xlabel("Sequence Length")
    ax.set_ylabel("Distance")

    ax = f.add_subplot(gs[0, 2])
    ax.set_title("Symmetric Model Distance Metric")
    ax.plot(np.arange(1, n), symmetric_distances)
    ax.set_xlabel("Sequence Length")
    ax.set_ylabel("Symmetric Distance")
    return f
