# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import sklearn.base
import sklearn.cluster
from sklearn.utils import check_array, check_random_state
from sklearn.utils.validation import check_is_fitted

from . import EMMHMMBase, confidence_interval
from ._hmm.em import new_mixture_gaussian


class GaussianMHMM(EMMHMMBase.EMMHMMBase):
    """
    A Hidden Markov Model with Univariate Gaussian Emissions.

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    init_pi : bool
    init_A : bool
    init_emissions : bool

    """

    def __init__(self, n_mixture_components=1, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, init_mixture_weights="uniform", init_pi="random", init_A="random", init_emissions="kmeans",
                 implementation="scaling", allowed_to_use_log=True, variance_regularization=1e-6, random_state=None, verbose=0):
        super(GaussianMHMM, self).__init__(n_mixture_components=n_mixture_components, n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                           n_jobs=n_jobs, tol=tol, init_mixture_weights=init_mixture_weights, init_pi=init_pi, init_A=init_A,
                                           implementation=implementation, allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.init_emissions = init_emissions
        self.variance_regularization = variance_regularization

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        GaussianMHMM
        """
        raise NotImplementedError("TODO")

    def _init_emissions(self, X, lengths):
        """
        Parameters
        ----------
        X : np.ndarray

        """

        rs = check_random_state(self.random_state)
        if self.init_emissions == None:
            pass
        elif self.init_emissions == "kmeans":
            self.means_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
            self.variances_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
            data = np.asarray(X).ravel()[:, None]
            for mixture_component in range(self.n_mixture_components):
                kmeans = sklearn.cluster.KMeans(n_clusters=self.n_components, random_state=rs)
                labels = kmeans.fit(data).labels_
                responsibilities = np.zeros((data.shape[0], self.n_components))
                responsibilities[np.arange(data.shape[0]), labels] = 1
                mixture_sums = responsibilities.sum(axis=0)
                self.means_[mixture_component] = kmeans.cluster_centers_.ravel()
                variances = np.zeros(self.n_components, float)
                for i in range(self.n_components):
                    val = np.dot(responsibilities[:, i] * (data - self.means_[mixture_component][i]).T,  data - self.means_[mixture_component][i])
                    val /= mixture_sums[i]
                    if not np.any(np.isnan(val)) and not np.any(np.isinf(val)):
                        variances[i] += val
                self.variances_[mixture_component] = variances[i]
        elif self.init_emissions == "random":
            raise NotImplementedError("TODO")
        else:
            if not isinstance(self.init_emissions, tuple) and not isinstance(self.init_emissions, list):
                raise ValueError("init_emissions parameter should be tuple or a list, got {}".format(type(self.init_emissions)))

            means, variances = self.init_emissions
            self.means_ = np.asarray(means)
            self.variances_ = np.asarray(variances)

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super()._prepare_for_fit()
        self.means_ = np.asarray(self.means_, dtype=float)
        self.variances_ = np.asarray(self.variances_, dtype=float)
        assert self.A_.shape[1] == self.means_.shape[1]
        assert self.A_.shape[1] == self.variances_.shape[1]

    def _assert_is_fitted(self):
        super()._assert_is_fitted()
        check_is_fitted(self, ["means_", "variances_"])

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_mixture_gaussian(
            self.mixture_weights_,
            self.pi_,
            self.A_,
            self.means_,
            self.variances_,
            self.variance_regularization,
            impl=self.implementation,
            allowed_to_use_log=self.allowed_to_use_log,
            log_level=self.verbose)

    def _update_after_fit(self, trainer):
        super()._update_after_fit(trainer)
        for i in range(self.n_mixture_components):
            self.means_[i] = np.asarray(trainer.emissions[i].means)
            self.variances_[i] = np.asarray(trainer.emissions[i].variances)

    def _emissions_params(self):
        """
        The number of free parameters in a Gaussian HMM
        """
        return 2

    def _emissions_to_dict(self):
        means = None
        variances = None
        if hasattr(self, "means_"):
            means = np.asarray(self.means_).tolist()
        if hasattr(self, "variances_"):
            variances = np.asarray(self.variances_).tolist()
        return means, variances

    def _emissions_from_dict(self, emissions):
        means, variances = emissions
        if means is not None:
            self.means_ = np.asarray(means)
        if variances is not None:
            self.variances_ = np.asarray(variances)
