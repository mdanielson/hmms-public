# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import base64
import pickle
from abc import abstractmethod

import numpy as np

import sklearn.base
import sklearn.utils
from sklearn.preprocessing import normalize
from sklearn.utils.validation import check_is_fitted

from . import base


class EMMHMMBase(base.EMBase, base.MHMMMixin):

    def __init__(self, n_mixture_components, n_components, n_iterations, n_inits, n_jobs, tol,
                 init_mixture_weights, init_pi, init_A, implementation,
                 allowed_to_use_log=True, random_state=None, verbose=0):
        """

        Parameters
        ----------
        n_components : int, defaults to 1
            The number of hidden states for the HMM

        n_iterations : int, defaults to 100
            The number of iterations to run for EM

        n_inits : int, defaults to 1
            The number of random initializations to run. The best results are kept

        """
        self.n_components = n_components
        self.n_iterations = n_iterations
        self.n_inits = n_inits
        self.n_jobs = n_jobs
        self.tol = tol
        self.init_pi = init_pi
        self.init_A = init_A
        self.implementation = implementation
        self.allowed_to_use_log = allowed_to_use_log
        self.random_state = random_state
        self.verbose = verbose
        self.n_mixture_components = n_mixture_components
        self.init_mixture_weights = init_mixture_weights

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
         * Ensure all matrices are properly normalized
         * Ensure all matrices have the correct shape
        """
        self.A_ = np.asarray(self.A_, dtype=float)
        self.pi_ = np.asarray(self.pi_, dtype=float)
        for mixture_component in range(self.n_mixture_components):
            self.A_[mixture_component] = normalize(self.A_[mixture_component], norm="l1", axis=1)
            self.pi_[mixture_component] /= self.pi_[mixture_component].sum()

        assert self.pi_.shape[0] == self.A_.shape[0], self.pi_
        assert self.A_.shape[1] == self.A_.shape[2], self.A_

    def _assert_is_fitted(self):
        check_is_fitted(self, ["mixture_weights_", "pi_", "A_", ])

    def n_parameters(self):
        """
        Matrix rows must sum to 1, so final value is dependenct on other values
        """
        mixture_params = self.mixture_weights_.shape[0]
        pi_params = 0
        A_params = 0
        B_params = 0

        for mixture_component in range(mixture_params):
            pi_count = self.pi_[mixture_component].shape[0]
            pi_params += pi_count

            A = self.A_[mixture_component]
            for hidden_state in range(A.shape[0]):
                A_row = A[hidden_state].shape[0]
                A_params += A_row
                B_params += self._emissions_params()
        return mixture_params + pi_params + A_params + B_params

    @abstractmethod
    def _emissions_params(self):
        """
        Return the number of free parameters in this HMM.
        """
        raise NotImplementedError()

    def _init_for_fit(self, X, lengths):
        """
        Parameters
        ----------
        X : np.ndarray

        Initialize pi, A, and B.

        Christopher Bishop, Pattern Recognition and Machine Learning
            Page 617 - pi/A select random values subject to normalization and non-negativity

        Rabiner paper:
            Initialize pi/A to uniform
        """
        self._init_mixture_weights(X, lengths)
        self._init_pi(X, lengths)
        self._init_A(X, lengths)
        self._init_emissions(X, lengths)
        self._prepare_for_fit()

    def _init_mixture_weights(self, X, lengths):
        rs = sklearn.utils.check_random_state(self.random_state)
        if self.init_mixture_weights is None:
            pass
        elif self.init_mixture_weights == "random":
            self.mixture_weights_ = rs.rand(self.n_mixture_components)
            self.mixture_weights_ /= self.mixture_weights_.sum()
        elif self.init_mixture_weights == "uniform":
            self.mixture_weights_ = np.zeros((self.n_mixture_components,)) + 1 / self.n_mixture_components
        else:
            if not isinstance(self.init_mixture_weights, np.ndarray) and not isinstance(self.init_mixture_weights, list):
                raise ValueError("init_mixture_weights parameter should be an array or a list, got {}".format(type(self.init_mixture_weights)))
            self.mixture_weights_ = np.asarray(self.init_mixture_weights)

    def _init_pi(self, X, lengths):
        rs = sklearn.utils.check_random_state(self.random_state)
        if self.init_pi is None:
            pass
        elif self.init_pi == "random":
            self.pi_ = normalize(
                rs.rand(self.n_mixture_components, self.n_components),
                norm="l1",
                axis=1
            )
        elif self.init_pi == "uniform":
            self.pi_ = np.full((self.n_mixture_components, self.n_components), 1. / self.n_components)
        else:
            if not isinstance(self.init_pi, np.ndarray) and not isinstance(self.init_pi, list):
                raise ValueError("init_pi parameter should be an array or a list, got {}".format(type(self.init_pi)))
            self.pi_ = np.asarray(self.init_pi)

    def _init_A(self, X, lengths):
        rs = sklearn.utils.check_random_state(self.random_state)
        if self.init_A is None:
            pass
        elif self.init_A == "random":
            self.A_ = rs.rand(self.n_mixture_components, self.n_components, self.n_components)
            for mixture_component in range(self.n_mixture_components):
                self.A_[mixture_component] = normalize(self.A_[mixture_component], norm="l1", axis=1)
        elif self.init_A == "uniform":
            self.A_ = np.full((self.n_mixture_components, self.n_components, self.n_components), 1. / self.n_components)
        elif self.init_A == "diagonal":
            self.A_ = np.full((self.n_mixture_components, self.n_components, self.n_components), (.5 / (self.n_components - 1)))
            indices = np.arange(self.n_components)
            self.A_[:, indices, indices] = .5
        elif self.init_A == "left-to-right":
            assert False, "Not implemented"

        else:
            if not isinstance(self.init_A, np.ndarray) and not isinstance(self.init_A, list):
                raise ValueError("init_A parameter should be an array or a list, got {}".format(type(self.init_A)))
            self.A_ = np.asarray(self.init_A)

    def _update_after_fit(self, trainer):
        self.mixture_weights_ = np.asarray(trainer.mixture_weights)
        self.pi_ = np.asarray(trainer.pi)
        self.A_ = np.asarray(trainer.A)

    def to_dict(self):
        """
        Serialize models... ignores random state
        """
        mixture_weights = None
        pi = None
        A = None
        if hasattr(self, "mixture_weights_"):
            mixture_weights = np.asarray(self.mixture_weights_).tolist()
        if hasattr(self, "pi_"):
            pi = np.asarray(self.pi_).tolist()
        if hasattr(self, "A_"):
            A = np.asarray(self.A_).tolist()

        params = self.get_params()
        params["random_state"] = base64.b64encode(pickle.dumps(params["random_state"])).decode("ascii")
        return {
            "params": params,
            "mixture_weights": mixture_weights,
            "pi": pi,
            "A": A,
            "emissions": self._emissions_to_dict(),
        }

    @classmethod
    def from_dict(cls, blob):
        instance = cls()
        instance.set_params(**blob["params"])
        instance.random_state = pickle.loads(base64.b64decode(instance.random_state))
        if blob["mixture_weights"] is not None:
            instance.mixture_weights_ = np.asarray(blob["mixture_weights"])
        if blob["pi"] is not None:
            instance.pi_ = np.asarray(blob["pi"])
        if blob["A"] is not None:
            instance.A_ = np.asarray(blob["A"])
        instance._emissions_from_dict(blob["emissions"])
        return instance
