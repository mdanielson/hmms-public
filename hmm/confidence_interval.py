# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
Compute Confidence Intervals for model parameters using Bootstrap Sampling.

I. Visser, M. E. J. Raijmakers, and P. C. M. Molenaar, “Confidence intervals for hidden Markov model parameters,”
British Journal of Mathematical and Statistical Psychology, vol. 53, no. 2, pp. 317–327, Nov. 2000.
"""


import collections
import warnings

import numpy as np
import sklearn.base


def generate_sample_models(mle_model, n_samples=1, length=2000, n_iterations=500):
    """
    Parameters
    ----------
    n_samples : int, The number of samples to draw from each HMM
    length : int, The length of each sample to drawn
    n_iterations : int, The number of separate models to run
    """
    models = []
    for b in range(n_iterations):
        a_observations, a_hiddens, a_lengths = mle_model.sample(n_samples=n_samples, length=length)
        new = sklearn.base.clone(mle_model)
        # Only fit one model
        new.n_inits = 1
        new.fit(a_observations, a_lengths)
        models.append(new)

    return models


def compute_common_with_means_and_covariances(mle_model, n_samples=1, length=200, n_iterations=500, ci=90):
    raise NotImplementedError("Not yet")


def compute_common_with_means_and_variances(mle_model, n_samples=1, length=200, n_iterations=500, ci=90):
    assert ci <= 100

    mle_order = np.argsort(mle_model.means_)
    mle_pi = mle_model.pi_[mle_order]
    mle_means = mle_model.means_[mle_order]
    mle_A = mle_model.A_[mle_order, :][:, mle_order]

    models = generate_sample_models(mle_model, n_samples=n_samples, length=length, n_iterations=n_iterations)
    mle_variances = None
    mle_variances = mle_model.variances_[mle_order]

    pis = []
    As = []
    means = []
    variances = []
    for m in models:
        # Organize models by the means
        order = np.argsort(m.means_)
        means.append(m.means_[order])
        pis.append(m.pi_[order])
        As.append(m.A_[order, :][:, order])
        variances.append(m.variances_[order])

    pis = np.dstack(pis)
    As = np.dstack(As)
    means = np.dstack(means)
    variances = np.dstack(variances)

    result = collections.defaultdict(list)
    lower_percentile = (100 - ci) // 2
    upper_percentile = 100 - lower_percentile
    limit_key = "{} limits".format(ci)
    for i in range(As.shape[0]):
        result["pi"].append(
            {
                "i": i,
                "mle": mle_pi[i],
                limit_key: (
                    np.percentile(pis[0, i], lower_percentile),
                    np.percentile(pis[0, i], upper_percentile)
                )
            }
        )
        result["means"].append(
            {
                "i": i,
                "mle": mle_means[i],
                limit_key: (
                    np.percentile(means[0, i], lower_percentile),
                    np.percentile(means[0, i], upper_percentile)
                )
            }
        )
        result["variances"].append(
            {
                "i": i,
                "mle": mle_variances[i],
                limit_key: (
                    np.percentile(variances[0, i], lower_percentile),
                    np.percentile(variances[0, i], upper_percentile)
                )
            }
        )

        for j in range(As.shape[1]):
            result["A"].append(
                {
                    "i": i,
                    "j": j,
                    "mle": mle_A[i, j],
                    limit_key: (
                        np.percentile(As[i, j], lower_percentile),
                        np.percentile(As[i, j], upper_percentile)
                    )
                }
            )

    return result


def compute_common_with_rates(mle_model, n_samples=1, length=200, n_iterations=500, ci=90):
    assert ci <= 100

    mle_order = np.argsort(mle_model.rates_)
    mle_pi = mle_model.pi_[mle_order]
    mle_rates = mle_model.rates_[mle_order]
    mle_A = mle_model.A_[mle_order, :][:, mle_order]

    models = generate_sample_models(mle_model, n_samples=n_samples, length=length, n_iterations=n_iterations)

    pis = []
    As = []
    rates = []
    for m in models:
        # Organize models by the rates
        order = np.argsort(m.rates_)
        rates.append(m.rates_[order])
        pis.append(m.pi_[order])
        As.append(m.A_[order, :][:, order])

    pis = np.dstack(pis)
    As = np.dstack(As)
    rates = np.dstack(rates)

    result = collections.defaultdict(list)
    lower_percentile = (100 - ci) // 2
    upper_percentile = 100 - lower_percentile
    limit_key = "{} limits".format(ci)
    for i in range(As.shape[0]):
        result["pi"].append(
            {
                "i": i,
                "mle": mle_pi[i],
                limit_key: (
                    np.percentile(pis[0, i], lower_percentile),
                    np.percentile(pis[0, i], upper_percentile)
                )
            }
        )
        result["rates"].append(
            {
                "i": i,
                "mle": mle_rates[i],
                limit_key: (
                    np.percentile(rates[0, i], lower_percentile),
                    np.percentile(rates[0, i], upper_percentile)
                )
            }
        )
        for j in range(As.shape[1]):
            result["A"].append(
                {
                    "i": i,
                    "j": j,
                    "mle": mle_A[i, j],
                    limit_key: (
                        np.percentile(As[i, j], lower_percentile),
                        np.percentile(As[i, j], upper_percentile)
                    )
                }
            )

    return result


def compute_categorical_confidence_interval(mle_model, n_samples=1, length=200, n_iterations=500, ci=90):
    warnings.warn("Dangerous...")
    assert ci <= 100

    mle_order = np.argsort(mle_model.B_[:, 0])
    mle_pi = mle_model.pi_[mle_order]
    mle_A = mle_model.A_[mle_order, :][:, mle_order]
    mle_B = mle_model.B_[mle_order, :]

    models = generate_sample_models(mle_model, n_samples=n_samples, length=length, n_iterations=n_iterations)

    pis = []
    As = []
    Bs = []

    for m in models:
        # Organize models by categorical emissions probabilities
        order = np.argsort(m.B_[:, 0])
        Bs.append(m.B_[order])
        pis.append(m.pi_[order])
        As.append(m.A_[order, :][:, order])

    pis = np.dstack(pis)
    As = np.dstack(As)
    Bs = np.dstack(Bs)

    result = collections.defaultdict(list)
    lower_percentile = (100 - ci) // 2
    upper_percentile = 100 - lower_percentile
    limit_key = "{} limits".format(ci)
    for i in range(As.shape[0]):
        result["pi"].append(
            {
                "i": i,
                "mle": mle_pi[i],
                limit_key: (
                    np.percentile(pis[0, i], lower_percentile),
                    np.percentile(pis[0, i], upper_percentile)
                )
            }
        )
        for j in range(Bs.shape[1]):
            result["B"].append(
                {
                    "i": i,
                    "j": j,
                    "mle": mle_B[i, j],
                    limit_key: (
                        np.percentile(Bs[i, j], lower_percentile),
                        np.percentile(Bs[i, j], upper_percentile)
                    )
                }
            )

        for j in range(As.shape[1]):
            result["A"].append(
                {
                    "i": i,
                    "j": j,
                    "mle": mle_A[i, j],
                    limit_key: (
                        np.percentile(As[i, j], lower_percentile),
                        np.percentile(As[i, j], upper_percentile)
                    )
                }
            )

    return result
