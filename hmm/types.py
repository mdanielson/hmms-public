
from typing import Any, Dict, List, Optional, Union
import numpy as np

ObservationType = Union[List, np.ndarray]
StatesType = np.ndarray
LengthType = Union[List, np.ndarray]
