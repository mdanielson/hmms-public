# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numbers

import numpy as np

import sklearn.utils

from . import VariationalHMMBase, util
from ._hmm import new_variational_categorical, reestimate
from .distributions import dirichlet


class CategoricalVariationalHMM(VariationalHMMBase.VariationalHMMBase):
    """
    A Hidden Markov Model with Categorical Emissions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    """

    B_ = property(lambda x: x.B_normalized_)

    def __init__(self, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, pi_prior=None, pi_posterior="sample", A_prior=None, A_posterior="sample",
                 B_prior=None, B_posterior="sample", n_features=None, implementation="scaling", allowed_to_use_log=True, random_state=None, verbose=0):
        super(CategoricalVariationalHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                                        n_jobs=n_jobs, tol=tol,
                                                        pi_prior=pi_prior, pi_posterior=pi_posterior, A_prior=A_prior, A_posterior=A_posterior,
                                                        implementation=implementation, allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.B_prior = B_prior
        self.B_posterior = B_posterior
        self.n_features = n_features

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, lengths, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        CategoricalVariationalHMM
        """
        observed_states, lengths = util.check_arguments(observed_states, lengths)
        observed_states = observed_states.astype(int)
        pi, A, B = reestimate.reestimate_categorical(observed_states, hidden_states, lengths)
        instance = cls(
            n_components=pi.shape[0],
            n_iterations=0,
            random_state=random_state,
        )

        state_counts = np.bincount(hidden_states.ravel())
        instance._reestimate_with_prior(pi, A, state_counts, lengths)
        instance.B_posterior_ = np.zeros_like(B)
        for i in range(instance.n_components):
            instance.B_posterior_[i] = B[i] * state_counts[i]

        instance.B_prior_ = B
        instance.B_normalized_ = B
        instance.B_subnormalized_ = B

        return instance

    def _init_emissions(self, X, lengths):
        random_state = sklearn.utils.check_random_state(self.random_state)
        # This  guess can go bad
        if self.n_features is None:
            symbols = np.unique(X)
            self.n_features_ = int(max(symbols.max()+1, symbols.shape[0]))
        else:
            self.n_features_ = self.n_features
        B_prior = self.B_prior
        # Setup prior estimate..
        if B_prior is None or isinstance(B_prior, np.ndarray) \
           or isinstance(B_prior, numbers.Number):
            if B_prior is None:
                B_prior = 1 / self.n_features_
            self.B_prior_ = np.zeros((self.n_components, self.n_features_), dtype=float) + B_prior
        else:
            raise ValueError("Unknown value for B_prior")

        if self.B_posterior == "sample":
            self.B_posterior_ = dirichlet.setup_dirichlet_posterior(self.B_prior_, lengths.sum(), random_state)
        elif self.B_posterior == "uniform":
            self.B_posterior = np.copy(self.B_prior_) * lengths.sum()
        else:
            raise ValueError("Unknown value for A_posterior:{}".format(self.pi_prior))

    def _update_after_fit(self, trainer):
        super(CategoricalVariationalHMM, self)._update_after_fit(trainer)
        self.B_posterior_ = trainer.emissions.B_posterior
        self.B_normalized_ = trainer.emissions.B_normalized
        self.B_subnormalized_ = trainer.emissions.B_subnormalized

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(CategoricalVariationalHMM, self)._prepare_for_fit()

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_variational_categorical(
                self.pi_posterior_,
                self.pi_prior_,
                self.A_posterior_,
                self.A_prior_,
                self.B_posterior_,
                self.B_prior_,
                impl=self.implementation,
                log_level=self.verbose
            )

    def _emissions_params(self):
        counts = self.B_posterior_[hidden_state]
        prior = self.B_prior_[hidden_state]
        vals = counts[(counts - prior) > self.prior_threshold_for_num_free_params]
        return max(0, vals.shape[0] - 1)

    def _emissions_to_dict(self):
        if hasattr(self, "B_posterior_"):
            params = {}
            for field in ["B_prior_", "B_posterior_", "B_subnormalized_", "B_normalized_"]:
                params[field] = np.asarray(getattr(self, field)).tolist()
            return params
        return None

    def _emissions_from_dict(self, params):
        if params is None:
            return
        for field in ["B_prior_", "B_posterior_", "B_subnormalized_", "B_normalized_"]:
            setattr(self, field, np.asarray(params[field]))
