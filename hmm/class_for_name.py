

# EM
from .CategoricalHMM import CategoricalHMM  # noqa: I100
from .GMMHMM import GMMHMM  # noqa: I100
from .GaussianHMM import GaussianHMM  # noqa: I100
from .MultivariateGaussianHMM import MultivariateGaussianHMM  # noqa: I100
from .PoissonHMM import PoissonHMM
# Variationl
from .CategoricalVariationalHMM import CategoricalVariationalHMM  # noqa: I100
from .GMMVariationalHMM import GMMVariationalHMM  # noqa: I100
from .GaussianVariationalHMM import GaussianVariationalHMM  # noqa: I100
from .MultivariateGaussianVariationalHMM import MultivariateGaussianVariationalHMM  # noqa: I100
from .PoissonVariationalHMM import PoissonVariationalHMM  # noqa: I100


def load(name: str):
    """
    Find the class for a given HMM
    """
    to_load = [
        CategoricalHMM,
        GMMHMM,
        GaussianHMM,
        MultivariateGaussianVariationalHMM,
        PoissonHMM,
        # Variational
        CategoricalVariationalHMM,
        GMMVariationalHMM,
        GaussianVariationalHMM,
        MultivariateGaussianHMM,
        PoissonVariationalHMM,
    ]
    _hmms = {}
    for item in to_load:
        _hmms[item.__name__] = item

    try:
        return _hmms[name]
    except KeyError:
        raise ValueError("Can't determin class type for {}".format(name))
