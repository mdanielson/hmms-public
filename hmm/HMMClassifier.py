# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import base64
import pickle
import warnings

import joblib

import numpy as np

from scipy.special import logsumexp

import sklearn.base
from sklearn.preprocessing import LabelBinarizer
from sklearn.utils import check_array

from . import CategoricalHMM, class_for_name, util


def train_one(classifier, train_X, train_lengths, train_y, test_X, test_lengths, test_y):
    classifier = classifier.fit(train_X, train_lengths, train_y)
    probs = classifier.predict_proba(test_X, test_lengths)
    return classifier, sklearn.metrics.roc_auc_score(test_y, probs[:, 1])


class HMMClassifier(sklearn.base.BaseEstimator):
    """
    Train several HMMS, one for each class, and make predictions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    init_pi : bool
    init_A : bool
    init_emissions : bool
    log_level : logging.log_level

    """

    def __init__(self, estimators={}, default_estimator=CategoricalHMM.CategoricalHMM(),
                 estimator_params=None, predict_method="map", class_log_prior=None,
                 n_inits=1, scoring="roc_auc", n_jobs=-1, random_state=None, verbose=False):
        self.estimators = estimators
        self.default_estimator = default_estimator
        self.estimator_params = estimator_params
        self.predict_method = predict_method
        if self.predict_method not in ("mlc", "map"):
            raise ValueError("predict_method: {} is not supported".format(self.predict_method))

        self.class_log_prior = class_log_prior
        self.n_inits = n_inits
        self.scoring = scoring
        self.n_jobs = n_jobs
        self.random_state = random_state
        self.verbose = verbose

    def fit(self, X, lengths, y, test_X=None, test_lengths=None, test_y=None):
        """
        Parameters
        ----------
        X : np.ndarray
        y : np.ndarray
            class labels
        """

        self.trained_estimators_ = {}
        self.classes_ = None
        self.class_log_prior_ = None
        if self.n_inits > 1 and (test_X is None or test_lengths is None or test_y is None):
            raise ValueError("With n_inits > 1 test_X, test_lengths, and test_Y may not be None")

        y = check_array(y, dtype=None, ensure_2d=False)
        self._set_classes(X, lengths, y)

        if self.n_inits > 1:
            # We are training multiple classifiers.
            # Override setting n_inits for individual estimators to 1
            if self.default_estimator is not None:
                self.default_estimator.n_inits = 1
            for estimator in self.estimators.values():
                estimator.n_inits = 1

            classifiers = []
            for _ in range(self.n_inits):
                clone = sklearn.base.clone(self)
                if self.estimator_params is not None:
                    clone.set_params(**self.estimator_params)
                clone.n_inits = 1
                classifiers.append(clone)

            with joblib.Parallel(n_jobs=self.n_jobs, verbose=self.verbose) as parallel:
                results = parallel(joblib.delayed(train_one)(
                    classifier,
                    X,
                    lengths,
                    y,
                    test_X,
                    test_lengths,
                    test_y
                ) for classifier in classifiers)

            models = [r[0] for r in results]
            scores = [r[1] for r in results]
            best = np.argmax(scores)
            self.trained_estimators_ = models[best].trained_estimators_
            self.best_score_ = scores[best]
            self.scores_ = scores

        else:
            self.trained_estimators_ = self._fit_one(X, lengths, y)
            self.best_score_ = None

        assert set(self.trained_estimators_) == set(self.classes_)
        return self

    def _fit_one(self, X, lengths, y):
        models = {}
        groups = {}
        for label in self.classes_:
            groups[label] = np.where(y == label)[0]

        grouped_data, grouped_lengths = util.partition_sequences(X, lengths, groups)

        # Setup random state
        rs = sklearn.utils.check_random_state(self.random_state)

        for label in grouped_data:
            estimator = self.estimators.get(label)
            if estimator is None:
                estimator = sklearn.base.clone(self.default_estimator)
            if self.estimator_params is not None:
                estimator.set_params(**self.estimator_params)
            estimator.random_state = rs

            models[label] = estimator.fit(grouped_data[label], grouped_lengths[label])
        return models

    def _set_classes(self, X, lengths, y):
        y = check_array(y, dtype=None, ensure_2d=False)

        # Translate labels
        label_bin = LabelBinarizer()
        Y = label_bin.fit_transform(y)
        if Y.shape[1] == 1:
            Y = np.concatenate((1 - Y, Y), axis=1)
        #  Get Class Labels
        self.classes_ = label_bin.classes_
        if self.class_log_prior is None:
            class_counts = Y.sum(axis=0)
            # Normalized class_log_prior
            self.class_log_prior_ = np.log(class_counts) - np.log(class_counts.sum())
        else:
            self.class_log_prior_ = np.asarray([self.class_log_prior[label] for label in self.classes_])

        class_counts = Y.sum(axis=0)
        self.class_log_prior_ = np.log(class_counts) - np.log(class_counts.sum())

    def partial_fit(self, X, lengths, y, n_iterations):
        y = check_array(y, dtype=None, ensure_2d=False)
        X = check_array(X, dtype=float)
        if not hasattr(self, "classes_"):
            raise ValueError("Please call fit() first")

        groups = {}
        for label in self.classes_:
            groups[label] = np.where(y == label)[0]

        grouped_data, grouped_lengths = util.partition_sequences(X, lengths, groups)

        for label in grouped_data:
            estimator = self.trained_estimators_[label]
            self.trained_estimators_[label] = estimator.partial_fit(grouped_data[label], grouped_lengths[label], n_iterations=n_iterations)
        return self

    def predict_proba(self, X, lengths):
        return np.exp(self.predict_log_proba(X, lengths))

    def _joint_log_likelihood(self, X, lengths):
        scores = np.zeros((lengths.shape[0], len(self.classes_)))
        for label, model in self.trained_estimators_.items():
            label_idx = (self.classes_ == label).argmax()
            scores[:, label_idx] = model.score_samples(X, lengths)

        if self.predict_method == "map":
            return scores + self.class_log_prior_
        return scores

    def predict_log_proba(self, X, lengths):
        X = check_array(X)
        jll = self._joint_log_likelihood(X, lengths)
        log_prob_x = logsumexp(jll, axis=1)
        return jll - np.atleast_2d(log_prob_x).T

    def predict(self, X, lengths):
        """
        Perform classification on an array of test vectors X.

        Parameters
        ----------
        X : array-like, shape = [n_samples, n_features]

        Returns
        -------
        C : array, shape = [n_samples]
            Predicted target values for X
        """
        jll = self._joint_log_likelihood(X, lengths)
        return self.classes_[np.argmax(jll, axis=1)]

    def to_dict(self):
        params = self.get_params()

        params["random_state"] = base64.b64encode(pickle.dumps(params["random_state"])).decode("ascii")
        del params["estimators"]
        estimator_class = self.default_estimator.__class__.__name__
        # Handle this separately
        default_estimator_params = params["default_estimator"].to_dict()
        del params["default_estimator"]
        # All of the default_estimator_params will get added back later
        defaults = [k for k in params if k.startswith("default_estimator")]
        for d in defaults:
            del params[d]

        trained_estimators = []
        estimators = []
        for key, val in self.estimators.items():
            if val.__class__.__name__ != self.default_estimator.__class__.__name__:
                warnings.warn("default_estimator class does not match class of estimator for class {}".format(key))

            estimators.append(
                {
                    "key": key,
                    "class": val.__class__.__name__,
                    "params": val.to_dict(),
                }
            )

        class_log_prior = None
        scores = None
        best_score = None
        if hasattr(self, "trained_estimators_"):
            for key, val in self.trained_estimators_.items():
                if val.__class__.__name__ != self.default_estimator.__class__.__name__:
                    warnings.warn("default_estimator class does not match class of estimator for class {}".format(key))
                trained_estimators.append(
                    {
                        "key": key,
                        "class": val.__class__.__name__,
                        "params": val.to_dict()
                    }
                )
        classes_ = None
        if hasattr(self, "classes_"):
            classes_ = self.classes_.tolist()

        best_score = class_log_prior = None
        if hasattr(self, "class_log_prior_"):
            class_log_prior = self.class_log_prior_.tolist()
        if hasattr(self, "best_score_"):
            best_score = self.best_score_

        return {
            "params": params,
            "estimator_class": estimator_class,
            "estimators": estimators,
            "default_estimator_params": default_estimator_params,
            "classes_": classes_,
            "class_log_prior_": class_log_prior,
            "trained_estimators": trained_estimators,
            "best_score": best_score,
            "scores": scores,
        }

    @classmethod
    def from_dict(cls, blob):
        instance = cls()

        instance.set_params(**blob["params"])
        instance.random_state = pickle.loads(base64.b64decode(instance.random_state))
        estimator_class = class_for_name.load(blob["estimator_class"])
        instance.default_estimator = estimator_class.from_dict(blob["default_estimator_params"])
        instance.estimators = {}
        for definition in blob["estimators"]:
            key = definition["key"]
            impl_class = class_for_name.load(definition["class"])
            instance.estimators[key] = impl_class.from_dict(definition["params"])

        if blob["trained_estimators"] is not None:
            instance.trained_estimators_ = {}
            for definition in blob["trained_estimators"]:
                key = definition["key"]
                impl_class = class_for_name.load(definition["class"])
                instance.trained_estimators_[key] = impl_class.from_dict(definition["params"])

        if blob["classes_"] is not None:
            instance.classes_ = np.asarray(blob["classes_"])
        if blob["class_log_prior_"] is not None:
            instance.class_log_prior_ = np.asarray(blob["class_log_prior_"])
        if blob["best_score"] is not None:
            instance.best_score_ = blob["best_score"]
        if blob["scores"] is not None:
            instance.scores_ = blob["scores"]

        return instance
