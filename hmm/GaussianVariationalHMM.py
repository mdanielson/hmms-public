# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging
import warnings

import numpy as np
import sklearn.cluster

from . import VariationalHMMBase, util, base
from .types import Any, ObservationType, LengthType
from ._hmm import new_variational_gaussian, reestimate


class GaussianVariationalHMM(VariationalHMMBase.VariationalHMMBase):
    """
    A Hidden Markov Model with Categorical Emissions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    """

    def __init__(self, n_components: int = 1, n_iterations: int = 100, n_inits: int = 1, n_jobs: int = -1, tol: float = 1e-6,
                 pi_prior=None, pi_posterior="sample", A_prior=None, A_posterior="sample", means_prior=None,
                 means_posterior="kmeans", variances_prior=None, variances_posterior="kmeans", implementation="scaling", allowed_to_use_log=True, random_state=None, verbose: int = 0):
        super(GaussianVariationalHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                                     n_jobs=n_jobs, tol=tol, pi_prior=pi_prior, pi_posterior=pi_posterior, A_prior=A_prior, A_posterior=A_posterior,
                                                     implementation=implementation, allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.means_prior = means_prior
        self.means_posterior = means_posterior
        self.variances_prior = variances_prior
        self.variances_posterior = variances_posterior

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, lengths, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        GaussianVariationalHMM
        """
        observed_states, lengths = util.check_arguments(observed_states, lengths)
        pi, A, means, variances = reestimate.reestimate_gaussian(observed_states, hidden_states, lengths)

        instance = cls(
            n_components=pi.shape[0],
            n_iterations=0,
            random_state=random_state,
            verbose=logging.INFO
        )

        state_counts = np.bincount(hidden_states.ravel()).astype(float)
        instance._reestimate_with_prior(pi, A, state_counts, lengths)

        instance.means_prior_ = means
        instance.means_posterior_ = means.copy()

        instance.beta_prior_ = state_counts
        instance.beta_posterior_ = state_counts.copy()

        instance.a_prior_ = state_counts
        instance.a_posterior_ = state_counts

        instance.b_prior_ = state_counts * variances
        instance.b_posterior_ = state_counts * variances

        #instance.variances_prior_ = variances
        instance.variances_posterior_ = variances

        return instance

    def _init_emissions(self, X, lengths):
        """
        https://www.fil.ion.ucl.ac.uk/~wpenny/publications/

        """
        random_state = sklearn.utils.check_random_state(self.random_state)
        data = np.asarray(X).ravel()[:, None]
        # first setup priors
        if isinstance(self.means_prior, tuple):
            # Format is (a, b)
            self.means_prior_ = np.zeros((self.n_components,)) + self.means_prior[0]
            self.beta_prior_ = np.zeros((self.n_components,)) + self.means_prior[1]
        elif self.means_prior is None or self.means_prior == "uninformed":
            self.means_prior_ = np.full((self.n_components, ), data.mean())
            # similar to sklearn
            self.beta_prior_ = np.zeros_like(self.means_prior_) + 1
        elif self.means_prior == "data":
            warnings.warn("This will be deprecated soon")
            self.means_prior_ = np.full((self.n_components, ), data.mean())
            # similar to sklearn
            self.beta_prior_ = np.zeros_like(self.means_prior_) + data.shape[0]
        else:
            assert False, "Not an option: {}".format(self.means_prior)

        if isinstance(self.variances_prior, tuple):
            # Format is (a, b)
            self.a_prior_ = np.zeros_like(self.means_prior_) + self.variances_prior[0]
            self.b_prior_ = np.zeros_like(self.means_prior_) + self.variances_prior[1]
        elif self.variances_prior is None or self.variances_prior == "uninformed":
            # From ~wpenny publication
            self.a_prior_ = np.zeros_like(self.means_prior_) + 1
            self.b_prior_ = np.zeros_like(self.means_prior_) + 1e-3
        elif self.variances_prior == "data":
            warnings.warn("This will be deprecated soon")
            self.a_prior_ = np.zeros_like(self.means_prior_) + data.shape[0]/2
            self.b_prior_ = np.zeros_like(self.means_prior_) + data.shape[0] / (2/data.var())
        else:
            raise ValueError("Invalid option for variances_prior:{}".format(self.variances_prior))

        # Initialize the posterior estimates from kmeans estimates
        kmeans = sklearn.cluster.KMeans(n_clusters=self.n_components, random_state=random_state)
        kmeans.fit(data)
        assignments = kmeans.predict(data)
        variances = np.zeros(kmeans.n_clusters)
        counts = np.zeros(kmeans.n_clusters)
        for i in sorted(set(assignments)):
            these = assignments == i
            counts[i] = these.sum()
            if counts[i] > 1:
                variances[i] = np.var(data[these])
            else:
                variances[i] = data.var()

        if self.variances_posterior == "kmeans":
            self.a_posterior_ = counts
            self.b_posterior_ = np.full((variances.shape[0],), variances) * counts
        else:
            raise ValueError("Invalid option for variances_posterior:{}".format(self.variances_posterior))

        if self.means_posterior == "kmeans":
            self.means_posterior_ = kmeans.cluster_centers_.ravel()
            self.beta_posterior_ = np.zeros_like(self.means_prior_) + counts
        else:
            raise ValueError("Invalid option for means_posterior:{}".format(self.means_posterior))

    def _update_after_fit(self, trainer):
        super(GaussianVariationalHMM, self)._update_after_fit(trainer)
        self.means_posterior_ = np.asarray(trainer.emissions._means_posterior)
        self.beta_posterior_ = np.asarray(trainer.emissions._beta_posterior)
        self.a_posterior_ = np.asarray(trainer.emissions._a_posterior)
        self.b_posterior_ = np.asarray(trainer.emissions._b_posterior)
        self.variances_posterior_ = np.asarray(trainer.emissions._variances_posterior)

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(GaussianVariationalHMM, self)._prepare_for_fit()
        self.means_posterior_ = np.asarray(self.means_posterior_, dtype=float)
        self.beta_posterior_ = np.asarray(self.beta_posterior_, dtype=float)
        self.a_posterior_ = np.asarray(self.a_posterior_, dtype=float)
        self.b_posterior_ = np.asarray(self.b_posterior_, dtype=float)
        assert self.A_posterior_.shape[0] == self.means_posterior_.shape[0]
        assert self.A_posterior_.shape[0] == self.beta_posterior_.shape[0]

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_variational_gaussian(
                self.pi_prior_,
                self.pi_posterior_,
                self.A_prior_,
                self.A_posterior_,
                means_prior=self.means_prior_,
                means_posterior=self.means_posterior_,
                beta_prior=self.beta_prior_,
                beta_posterior=self.beta_posterior_,
                a_prior=self.a_prior_,
                a_posterior=self.a_posterior_,
                b_prior=self.b_prior_,
                b_posterior=self.b_posterior_,
                impl=self.implementation,
                log_level=self.verbose
            )

    def _emissions_params(self):
        return 4

    def _emissions_to_dict(self):

        if hasattr(self, "means_prior_"):
            params = {}
            for field in ["means_prior_", "means_posterior_", "beta_prior_", "beta_posterior_", "a_prior_", "a_posterior_", "b_prior_", "b_posterior_", ]:
                params[field] = np.asarray(getattr(self, field)).tolist()
            return params
        return None

    def _emissions_from_dict(self, params):
        if params is None:
            return
        for field in ["means_prior_", "means_posterior_", "beta_prior_", "beta_posterior_", "a_prior_", "a_posterior_", "b_prior_", "b_posterior_", ]:
            setattr(self, field,  np.asarray(params[field]))

    def verbose_dic(self, X: ObservationType, lengths: LengthType):

        trainer = self._new_trainer()
        X, lengths = util.check_arguments(X, lengths)
        pds, dics = trainer.deviance_information_criterion(X, lengths)
        dic = dics.sum()
        if np.isinf(dic):
            raise ValueError("DIC is not supported")
        return pds, dics

    def dic(self, X: ObservationType, lengths: LengthType):
        X, lengths = util.check_arguments(X, lengths)
        dic = self.verbose_dic(X, lengths)[1].sum()
        if np.isinf(dic):
            raise ValueError("DIC is not supported")
        return dic

