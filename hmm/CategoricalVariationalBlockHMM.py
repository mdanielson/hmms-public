import numpy as np
import scipy.linalg

import sklearn.utils
import sklearn.preprocessing
from . import CategoricalVariationalHMM

class CategoricalVariationalBlockHMM(CategoricalVariationalHMM.CategoricalVariationalHMM):
    def __init__(self, n_components=1, n_iterations=100, block_size=2, n_inits=1, n_jobs=-1, tol=1e-6, pi_prior=None, pi_posterior="sample",
                 A_prior=None, A_posterior="sample", B_prior=None, B_posterior="sample",
                 n_features=None, implementation="scaling", allowed_to_use_log=True, random_state=None, verbose=0):
        super(CategoricalVariationalBlockHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                                             n_jobs=n_jobs, tol=tol, pi_prior=pi_prior, pi_posterior=pi_posterior,
                                                             A_prior=A_prior, A_posterior=A_posterior, B_prior=B_prior, B_posterior=B_posterior,
                                                             allowed_to_use_log=allowed_to_use_log,
                                                             implementation=implementation, random_state=random_state, verbose=verbose)
        self.block_size = block_size
        if self.n_components % self.block_size != 0:
            raise ValueError("Invalid block_size for n_components: {} {}".format(self.block_size, n_components))

    def _init_A(self, X, lengths):
        rs = sklearn.utils.check_random_state(self.random_state)
        blocks = []
        n_blocks = self.n_components // self.block_size
        for i in range(n_blocks):
            blocks.append(
                sklearn.preprocessing.normalize(
                    rs.rand(self.block_size, self.block_size),
                    norm="l1",
                    axis=1)
            )


        self.A_prior_ = scipy.linalg.block_diag(*blocks)
        self.A_prior_[self.A_prior_==0] = 1e-9
        self.A_prior_ = sklearn.preprocessing.normalize(self.A_prior_, norm="l1", axis=1)
        self.A_posterior_ = self.A_prior_ * lengths.shape[0]

if __name__ == "__main__":
    from hmm.datasets import synthetic
    import pandas as pd
    from . import scoring
    train_data = synthetic.get_categorical_beal(None)[0]
    vb_block = CategoricalVariationalBlockHMM(block_size=3, n_components=9, n_iterations=500, n_inits=10, n_jobs=-1, tol=1e-10)
    vb_block.fit(train_data)
    print(pd.DataFrame(vb_block.A_posterior_))
    print(pd.DataFrame(vb_block.B_posterior_))
    print(vb_block.lower_bound_[-1])
    print(vb_block.score(train_data))
    print(vb_block.aic(train_data))
    print(vb_block.bic(train_data))
    print(scoring.perplexity(vb_block, train_data))
