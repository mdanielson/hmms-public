# cython: language_level=3

import numpy as np
cimport numpy as np

from .types cimport DOUBLE_DTYPE_t


cdef class Normal:

    cdef public DOUBLE_DTYPE_t mean
    cdef public DOUBLE_DTYPE_t variance
    cdef DOUBLE_DTYPE_t divisor1
    cdef DOUBLE_DTYPE_t divisor2
    cdef DOUBLE_DTYPE_t log_divisor2

    cdef object random_state

    cpdef DOUBLE_DTYPE_t pdf(self, DOUBLE_DTYPE_t x)
    cpdef DOUBLE_DTYPE_t logpdf(self, DOUBLE_DTYPE_t x)
    cpdef DOUBLE_DTYPE_t rvs(self)

cdef class MultivariateNormal:

    cdef public const DOUBLE_DTYPE_t[:] mean
    cdef public const DOUBLE_DTYPE_t[:, :] covariance
    cdef public DOUBLE_DTYPE_t[:, :] precision
    cdef public DOUBLE_DTYPE_t two_pi_k_det
    cdef public Py_ssize_t n_variates

    cdef public object dist
    cdef object random_state

    cpdef DOUBLE_DTYPE_t pdf(self, const DOUBLE_DTYPE_t[:] x)
    cpdef DOUBLE_DTYPE_t logpdf(self, const DOUBLE_DTYPE_t[:] x)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] rvs(self)
