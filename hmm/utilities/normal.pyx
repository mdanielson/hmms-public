# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: language_level=3
# cython: profile=False
cimport cython
cimport libc.math
import numpy as np
cimport numpy as np
from numpy.math cimport logl, isinf, expl
import sklearn.utils
import scipy.stats

from .types cimport DOUBLE_DTYPE_t

cdef class Normal:
    """
    An efficient Normal Distribution mimmicking the scipy.stats interface
    """
    def __init__(self, DOUBLE_DTYPE_t mean, DOUBLE_DTYPE_t variance, random_state = None):
        self.mean = mean
        assert variance > 0, "zero variance is not allowed: {}".format(variance)
        self.variance = variance
        self.random_state = sklearn.utils.check_random_state(random_state)
        self.divisor1 = 2.* self.variance
        self.divisor2 = 1. / libc.math.sqrt(libc.math.pi * self.divisor1)
        self.log_divisor2 = logl(1.) - logl(libc.math.sqrt(libc.math.pi * self.divisor1))

    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t pdf(self, DOUBLE_DTYPE_t x):
        return self.divisor2 * libc.math.exp( - ((x - self.mean)**2)/self.divisor1)

    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t logpdf(self, DOUBLE_DTYPE_t x):
        return self.log_divisor2 - ((x - self.mean)**2)/self.divisor1


    cpdef DOUBLE_DTYPE_t rvs(self):
        # TODO
        return self.random_state.normal(self.mean, libc.math.sqrt(self.variance))

cdef class MultivariateNormal:

    def __init__(self, const DOUBLE_DTYPE_t[:] mean, const DOUBLE_DTYPE_t[:, :] covariance, random_state = None):
        self.mean = mean
        self.covariance = covariance
        self.precision = np.linalg.inv(self.covariance)
        self.random_state = sklearn.utils.check_random_state(random_state)
        #self.dist = scipy.stats.multivariate_normal(self.mean, self.covariance)
        assert self.mean.shape[0] == self.covariance.shape[0]
        assert self.mean.shape[0] == self.covariance.shape[1]
        self.n_variates = self.mean.shape[0]
        self.two_pi_k_det = np.sqrt((2.0 * np.pi) ** self.mean.shape[0] * np.linalg.det(self.covariance))

    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef DOUBLE_DTYPE_t pdf(self, const DOUBLE_DTYPE_t[:] x):
        cdef :
            DOUBLE_DTYPE_t[:] tmp = np.zeros(self.n_variates)
            DOUBLE_DTYPE_t d = 0
            Py_ssize_t i, j
        #print("D2",np.dot(np.dot(diff, self.precision), diff))
        # First outer product
        for i in range(self.n_variates):
            for j in range(self.n_variates):
                tmp[i] += (x[j] - self.mean[j]) * self.precision[j, i]
        # Second outer product
        for i in range(self.n_variates):
            d += tmp[i] * (x[i] - self.mean[i])

        num = libc.math.exp( -.5 * d)
        return num / self.two_pi_k_det

    cpdef DOUBLE_DTYPE_t logpdf(self, const DOUBLE_DTYPE_t[:] x):
        return logl(self.pdf(x))

    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] rvs(self):
        return self.random_state.multivariate_normal(self.mean, self.covariance)
