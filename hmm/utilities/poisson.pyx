# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
cimport cython
cimport libc.math
import sklearn.utils


cdef class Poisson:
    """
    An efficient Poisson Distribution mimmicking the scipy.stats interface
    """
    def __init__(self, DOUBLE_DTYPE_t mean, random_state = None):
        self.mean = mean
        self.mean_exp = libc.math.exp(-self.mean)
        self.random_state = sklearn.utils.check_random_state(random_state)

    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t pmf(self, DOUBLE_DTYPE_t x):
        cdef:
            DOUBLE_DTYPE_t d = self.mean_exp
            Py_ssize_t i
            Py_ssize_t j = <Py_ssize_t> x + 1

        for i in range(1, j):
            d *= self.mean
            d /= i
        return d

    cpdef DOUBLE_DTYPE_t rvs(self):
        # TODO
        return self.random_state.poisson(self.mean)

