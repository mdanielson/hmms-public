cimport cython
from hmm.utilities.types cimport DOUBLE_DTYPE_t


@cython.boundscheck(False)
@cython.initializedcheck(False)
cdef void do_vector_matrix_dot(const DOUBLE_DTYPE_t[:] v1, const DOUBLE_DTYPE_t[:, :] m1, DOUBLE_DTYPE_t[:] out):
    """
    Efficiently Multiply a vector by a matrix.
    """
    cdef:
        Py_ssize_t i, j

    assert v1.shape[0] == m1.shape[0]
    assert v1.shape[0] == m1.shape[1]
    assert v1.shape[0] == out.shape[0]
    # For each column
    for j in range(m1.shape[1]):
        out[j] = 0
        # sum/Multiply the column
        for i in range(m1.shape[0]):
            out[j] += v1[i] * m1[i, j]


@cython.boundscheck(False)
@cython.initializedcheck(False)
cdef void do_outer_product(const DOUBLE_DTYPE_t[:] v1, const DOUBLE_DTYPE_t[:] v2, DOUBLE_DTYPE_t[:,:] out):
    """
    Efficient outer product of two vectors
    """
    cdef:
        Py_ssize_t i, j

    assert v1.shape[0] == v2.shape[0]
    assert v1.shape[0] == out.shape[0]
    assert v1.shape[0] == out.shape[1]
    for i in range(v1.shape[0]):
        for j in range(v1.shape[0]):
            out[i, j] = v1[i] * v2[j]

cdef DOUBLE_DTYPE_t do_dot(const DOUBLE_DTYPE_t[:] v1, const DOUBLE_DTYPE_t[:] v2):
    """
    Efficient Dot Product of two vectors
    """
    cdef:
        Py_ssize_t i
        DOUBLE_DTYPE_t tmp = 0

    assert v1.shape[0] == v2.shape[0]
    for i in range(v1.shape[0]):
        tmp += v1[i] * v2[i]
    return tmp



