
cimport cython
cimport numpy as np
import numpy as np
from libc.math cimport isinf, isnan, exp, log, M_PI
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE
from scipy.linalg.cython_lapack cimport dgetrf, dgetri


cdef void inverse_matrix(DOUBLE_DTYPE_t* A, int n, int info)
cpdef DOUBLE_DTYPE_t[::1,:] inverse_with_copy(DOUBLE_DTYPE_t[::1, :] A)
cpdef DOUBLE_DTYPE_t[::1,:] inverse(DOUBLE_DTYPE_t[::1, :] A)

