
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE

cdef void do_vector_matrix_dot(const DOUBLE_DTYPE_t[:] v1, const DOUBLE_DTYPE_t[:, :] m1, DOUBLE_DTYPE_t[:] out)
cdef void do_outer_product(const DOUBLE_DTYPE_t[:] v1, const DOUBLE_DTYPE_t[:] v2, DOUBLE_DTYPE_t[:,:] out)
cdef DOUBLE_DTYPE_t do_dot(const DOUBLE_DTYPE_t[:] v1, const DOUBLE_DTYPE_t[:] v2)
