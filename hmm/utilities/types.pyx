# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import cython
import numpy as np
cimport numpy as np

DOUBLE_DTYPE = np.double
LONG_DOUBLE_DTYPE = np.longdouble
INT_DTYPE = np.int
