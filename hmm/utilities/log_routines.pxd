# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: language_level=3
# cython: wraparound=False
# cython: boundscheck=False
cimport cython
from libc.math cimport isinf, exp, log, fabs, log1p, INFINITY


from hmm.utilities.types cimport DOUBLE_DTYPE_t

## From hmmlearn
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline int _argmax(DOUBLE_DTYPE_t[:] X) nogil:
    cdef:
        DOUBLE_DTYPE_t X_max = -INFINITY
        Py_ssize_t pos = 0
        Py_ssize_t i

    for i in range(X.shape[0]):
        if X[i] > X_max:
            X_max = X[i]
            pos = i
    return pos


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline DOUBLE_DTYPE_t _max(DOUBLE_DTYPE_t[:] X) nogil:
    return X[_argmax(X)]


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline DOUBLE_DTYPE_t _logsumexp(DOUBLE_DTYPE_t[:] X) nogil:
    cdef:
        DOUBLE_DTYPE_t X_max = _max(X)
        Py_ssize_t i
        DOUBLE_DTYPE_t acc = 0

    if isinf(X_max):
        return -INFINITY

    for i in range(X.shape[0]):
        if not isinf(X[i]):
            acc += exp(X[i] - X_max)

    return log(acc) + X_max


@cython.boundscheck(False)
cdef inline DOUBLE_DTYPE_t _logaddexp(DOUBLE_DTYPE_t a, DOUBLE_DTYPE_t b) nogil:
    if isinf(a) and a < 0:
        return b
    elif isinf(b) and b < 0:
        return a
    else:
        return max(a, b) + log1p(exp(-fabs(a - b)))




