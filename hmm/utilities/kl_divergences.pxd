# cython: profile=False
# cython: language_level=3

cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t

cpdef DOUBLE_DTYPE_t kl_categorical_distribution(DOUBLE_DTYPE_t[:] vec_a, DOUBLE_DTYPE_t[:] vec_b)
cpdef DOUBLE_DTYPE_t kl_dirichlet_distribution(DOUBLE_DTYPE_t[:] buff_a, DOUBLE_DTYPE_t[:] buff_b)
cpdef DOUBLE_DTYPE_t kl_normal_distribution(DOUBLE_DTYPE_t mean_q, DOUBLE_DTYPE_t variance_q, DOUBLE_DTYPE_t mean_p, DOUBLE_DTYPE_t variance_p)
cpdef DOUBLE_DTYPE_t kl_gamma_distribution(DOUBLE_DTYPE_t a_1, DOUBLE_DTYPE_t b_1, DOUBLE_DTYPE_t a_2, DOUBLE_DTYPE_t b_2)
cpdef DOUBLE_DTYPE_t kl_multivariate_normal(DOUBLE_DTYPE_t[:] mean_q, DOUBLE_DTYPE_t[:, :] covariance_q, DOUBLE_DTYPE_t[:]  mean_p, DOUBLE_DTYPE_t[:, :] covariance_p)
cpdef DOUBLE_DTYPE_t kl_wishart(DOUBLE_DTYPE_t dof_q, DOUBLE_DTYPE_t[:, :] scale_q, DOUBLE_DTYPE_t  dof_p, DOUBLE_DTYPE_t[:, :] scale_p)
