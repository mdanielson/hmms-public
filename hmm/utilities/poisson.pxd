cimport numpy as np
import numpy as np

from .types cimport DOUBLE_DTYPE_t

cdef class Poisson:

    cdef public DOUBLE_DTYPE_t mean
    cdef public DOUBLE_DTYPE_t mean_exp

    cdef object random_state

    cpdef DOUBLE_DTYPE_t pmf(self, DOUBLE_DTYPE_t x)
    cpdef DOUBLE_DTYPE_t rvs(self)
