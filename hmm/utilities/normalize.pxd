cimport cython


from hmm.utilities.types cimport DOUBLE_DTYPE_t

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
@cython.initializedcheck(False)
cdef inline void normalize_1d(DOUBLE_DTYPE_t[:] source, DOUBLE_DTYPE_t[:] destination):
    cdef:
        DOUBLE_DTYPE_t tmp = 0
        Py_ssize_t i

    for i in range(source.shape[0]):
        tmp += source[i]

    for i in range(destination.shape[0]):
        destination[i] = source[i] / tmp


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
@cython.initializedcheck(False)
cdef inline void normalize_2d(DOUBLE_DTYPE_t[:, :] source, DOUBLE_DTYPE_t[:, :] destination):
    cdef:
        DOUBLE_DTYPE_t tmp
        Py_ssize_t i

    # normalize A and B
    for i in range(source.shape[0]):
        tmp = 0
        for j in range(source.shape[1]):
            tmp += source[i, j]
        for j in range(destination.shape[1]):
            destination[i, j] = source[i, j] / tmp




