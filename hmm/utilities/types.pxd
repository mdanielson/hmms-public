cimport cython
import cython
cimport numpy as np
import numpy as np

ctypedef np.double_t DOUBLE_DTYPE_t
ctypedef np.longdouble_t LONG_DOUBLE_DTYPE_t
ctypedef np.int_t INT_DTYPE_t

