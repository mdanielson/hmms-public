# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

# cython: profile=False
# cython: language_level=3
cimport numpy as np
import sys
import numpy as np
import scipy.special
from libc.math cimport log, INFINITY, isinf, M_PI

from hmm.utilities.types cimport LONG_DOUBLE_DTYPE_t, DOUBLE_DTYPE_t, INT_DTYPE_t

cdef bint DEBUG = False

cdef extern from "<math.h>" nogil:
    long double powl(long double x, long double y)

cpdef DOUBLE_DTYPE_t kl_categorical_distribution(DOUBLE_DTYPE_t[:] vec_a, DOUBLE_DTYPE_t[:] vec_b):
    """
    KL Divergence between two categorical distributions

    KL(q || p) = sum q_i log (q_i/p_i)

    """
    cdef:
        Py_ssize_t i
        DOUBLE_DTYPE_t result = 0.0

    for i in range(vec_a.shape[0]):
        if vec_a[i] == 0:
            continue
        result += vec_a[i] * log(vec_a[i] / vec_b[i])
    return result

cpdef DOUBLE_DTYPE_t kl_dirichlet_distribution(DOUBLE_DTYPE_t[:] buff_a, DOUBLE_DTYPE_t[:] buff_b):
    """
    KL Divergence between two dirichlet distributions

    KL(q || p) = ln [gamma(q)/gamma(p)] - sum [ ln [gamma(q_j)/gamma(p_j)] - (q_j - p_j) (digamma(q_j) - digamma(p_j)]

    """
    cdef:
        np.ndarray[DOUBLE_DTYPE_t, ndim=1] vec_a = np.asarray(buff_a)
        np.ndarray[DOUBLE_DTYPE_t, ndim=1] vec_b = np.asarray(buff_b)

    alphaA = vec_a.sum()
    alphaB = vec_b.sum()
    delta = vec_a - vec_b
    return scipy.special.gammaln(alphaA) - scipy.special.gammaln(alphaB) \
            - np.sum(scipy.special.gammaln(vec_a) - scipy.special.gammaln(vec_b))\
            + np.sum( np.dot(delta, scipy.special.digamma(vec_a) - scipy.special.digamma(alphaA)))

cpdef DOUBLE_DTYPE_t kl_normal_distribution(DOUBLE_DTYPE_t mean_q, DOUBLE_DTYPE_t variance_q, DOUBLE_DTYPE_t mean_p, DOUBLE_DTYPE_t variance_p):
    """
    KL Divergence between two normal distributions
    http://www.fil.ion.ucl.ac.uk/~wpenny/publications/densities.ps
    KL (q || P)  = .5 log (variance_p / variance_q) + (mean_q**2 + mean_p**2 + variance_q - 2 * mean_q * mean_p) / (2 * variance_p) - .5

    """
    cdef:
        DOUBLE_DTYPE_t result
    result = (np.log(variance_p / variance_q)) / 2 + (mean_q**2 + mean_p**2 + variance_q - 2 * mean_q * mean_p) / (2 * variance_p) - 1/2
    if result <= 0:
        if result > -1e-10:
            result = 0
        else:
            print("OMG-normal", mean_q, variance_q, mean_p, variance_p, fd=sys.stderr)
    assert result >=0
    return result

cpdef DOUBLE_DTYPE_t kl_gamma_distribution(DOUBLE_DTYPE_t a_1, DOUBLE_DTYPE_t b_1, DOUBLE_DTYPE_t a_2, DOUBLE_DTYPE_t b_2):
    """
    KL Divergence between two gamma distributions
    https://arxiv.org/pdf/1611.01437.pdf

    """
    cdef:
        DOUBLE_DTYPE_t result
    ##
    # https://stats.stackexchange.com/questions/11646/kullback-leibler-divergence-between-two-gamma-distributions?rq=1
    ## #
    # `b` and `d` are Gamma shape parameters and
    # `a` and `c` are scale parameters.
    # (All, therefore, must be positive.)
    #
    ## KL = function(a,b,c,d){
    #  ((a-c)/c)*b + lgamma(d) - lgamma(b) + d*log(c) - b*log(a) + (b-d)*(log(a) + digamma(b))
    #  }

    result = (a_1 - a_2) * scipy.special.digamma(a_1) - scipy.special.gammaln(a_1) + scipy.special.gammaln(a_2) + a_2 * (log(b_1) - log(b_2)) + a_1 * (b_2-b_1)  / b_1
    #print("gammaln(a_1)", a_1, scipy.special.gammaln(a_1))
    #print("gammaln(a_2)", a_2, scipy.special.gammaln(a_2))
    #print("term23",a_1 * (b_2-b_1)  / b_1)
    if result <= 0:
        if result > -1e-10:
            result = 0
        else:
            print("OMG-GAMMA", a_1, b_1, a_2, b_2, result, fd=sys.stderr)
    assert result >= 0
    return result


cpdef DOUBLE_DTYPE_t kl_multivariate_normal(DOUBLE_DTYPE_t[:] mean_q, DOUBLE_DTYPE_t[:, :] covariance_q, DOUBLE_DTYPE_t[:]  mean_p, DOUBLE_DTYPE_t[:, :] covariance_p):
    """
    www.fil.ion.ucl.ac.uk/~wpenny/publications/densities.ps
    """

    cdef:
        DOUBLE_DTYPE_t tmp1, tmp2, tmp3, tmp4
        DOUBLE_DTYPE_t D = mean_q.shape[0]
        DOUBLE_DTYPE_t[:] diff

    tmp1 = .5 * log (np.linalg.det(covariance_p)/np.linalg.det(covariance_q))
    if DEBUG:
        print("tmp1", tmp1)
    tmp2 = 0.5 * np.trace(np.dot(np.linalg.inv(covariance_p), covariance_q))
    if DEBUG:
        print("tmp2", tmp2)
    diff = np.asarray(mean_q) - np.asarray(mean_p)
    tmp3 = .5 * np.dot(np.dot(diff, np.linalg.inv(covariance_p)), diff)
    if DEBUG:
        print("tmp3", tmp3)
    tmp4 = D/2
    if DEBUG:
        print("tmp4", tmp4)
    return tmp1 + tmp2 + tmp3 - tmp4


cpdef DOUBLE_DTYPE_t kl_wishart(DOUBLE_DTYPE_t dof_q, DOUBLE_DTYPE_t[:, :] scale_q, DOUBLE_DTYPE_t  dof_p, DOUBLE_DTYPE_t[:, :] scale_p):
    """
    www.fil.ion.ucl.ac.uk/~wpenny/publications/densities.ps
    """

    cdef:
        DOUBLE_DTYPE_t term1, term2, term3, term4, term5
        Py_ssize_t D = scale_p.shape[0]

    term1 = (dof_q - dof_p)/2 * elogr(dof_q, scale_q)
    #print("term1", term1)
    term2 = -D * dof_q / 2
    #print("term2", term2)
    term3 = dof_q/2 * np.trace(np.dot(scale_p, np.linalg.inv(scale_q)))
    #print("term3", term3)
    #print("term23", term2 + term3)
    term4 = logZ(dof_p, scale_p)
    #print("term4", term4)
    term5 = -logZ(dof_q, scale_q)
    #print("term5", term5)
    return term1 + term2 + term3 + term4 + term5

cdef elogr(DOUBLE_DTYPE_t dof, DOUBLE_DTYPE_t[:, :] scale):
    cdef:
        DOUBLE_DTYPE_t term1, term2
        Py_ssize_t i
    term1 = -log(np.linalg.det(np.asarray(scale)/2))
    #print("elogrterm1", term1)
    term2 = 0
    for i in range(1, scale.shape[0]+1):
        term2 += scipy.special.digamma((dof + 1 - i)/2)
    #print("elogrterm2", term2)
    return term1 + term2


cdef DOUBLE_DTYPE_t logZ(DOUBLE_DTYPE_t dof, DOUBLE_DTYPE_t[:, :] scale):
    cdef:
        Py_ssize_t D = scale.shape[0]
        DOUBLE_DTYPE_t term1, term2, term3

    term1 = (D * (D-1)/4) * log(M_PI)
    #print("logZ term1", term1)
    term2 = -dof/2 * np.log(np.linalg.det(np.asarray(scale)/2))
    #print("logZ term2", term2)
    term3 = 0
    for i in range(1, scale.shape[0]+1):
        term3 += scipy.special.gammaln((dof - i + 1)/2)
    #print("logZ term3", dof, term3)
    return term1 + term2 + term3

