
cimport cython
cimport numpy as np
import numpy as np
from libc.math cimport isinf, isnan, exp, log, M_PI
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE
from scipy.linalg.cython_lapack cimport dgetrf, dgetri


cdef void inverse_matrix(DOUBLE_DTYPE_t* A, int n, int info):
     cdef:
         int[:] ipiv = np.empty(n, dtype=np.int32)
         DOUBLE_DTYPE_t[:] work = np.empty(n, dtype=np.float64)

     dgetrf(&n, &n, A, &n, &ipiv[0], &info)
     dgetri(&n, A, &n, &ipiv[0], &work[0], &n, &info)


cpdef DOUBLE_DTYPE_t[::1,:] inverse_with_copy(DOUBLE_DTYPE_t[::1, :] A):
    cdef:
        int N = A.shape[0]
        int info
        DOUBLE_DTYPE_t[::1, :] out = np.empty((A.shape[0], A.shape[1]), dtype=DOUBLE_DTYPE, order='F')

    out[:] = A

    if (out.shape[0] != out.shape[1]):
       raise TypeError("The input array must be a square matrix")
    out = np.require(out, requirements=['F'])
    inverse_matrix(&out[0,0], N, info)

    if info < 0:
        raise ValueError('illegal value in %d-th argument of internal getrf' % -info)
    elif info > 0:
        raise ValueError("Diagonal number %d is exactly zero. Singular matrix." % -info)
    return out


cpdef DOUBLE_DTYPE_t[::1,:] inverse(DOUBLE_DTYPE_t[::1, :] A):
      if (A.shape[0] != A.shape[1]):
         raise TypeError("The input array must be a square matrix")
      cdef int N = A.shape[0]
      cdef int info
      A = np.require(A, requirements=['F'])
      inverse_matrix(&A[0,0], N, info)

      if info < 0:
          raise ValueError('illegal value in %d-th argument of internal getrf' % -info)
      elif info > 0:
          raise ValueError("Diagonal number %d is exactly zero. Singular matrix." % -info)
      return A
