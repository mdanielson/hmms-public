# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import collections

import numpy as np
import pandas as pd
import sklearn.base
from sklearn.utils import check_array, check_random_state

from . import base, util


def yield_sequences(X, lengths):
    seq_start = 0
    for o_i, length in enumerate(lengths):
        seq_end = seq_start + length
        sequence = X[seq_start:seq_end]
        yield sequence
        seq_start = seq_end


class MarkovModel(sklearn.base.BaseEstimator, base.EMModelSelectionMixin):

    def __init__(self, order=1, random_state=None):
        self.transition_probabilities_ = None
        self.initial_probabilities_ = None
        self.loglikelihoods_ = None
        self.num_states_ = 0
        self.order = order
        self.random_state = check_random_state(random_state)
        self.start_symbol_ = list(util.ngrams([1], n=self.order+1, pad=True))[0][:-1]

    def transition_probabilities_frame(self):
        return pd.DataFrame(
            self.transition_probabilities_,
        ).fillna(0).T

    def initial_probabilities_frame(self):
        return pd.Series(
            self.initial_probabilities_,
        ).fillna(0)

    def fit(self, X, lengths):
        # Estimate initial probabilities and transition probabilities
        transitions = collections.defaultdict(collections.Counter)
        states = set()
        for sequence in yield_sequences(X, lengths):
            for i, gram in enumerate(util.ngrams(sequence, n=self.order+1, pad=True)):
                history = gram[:-1]
                current = gram[-1]
                print(current)
                states.add(current)
                transitions[history][current] += 1

        for j, row in transitions.items():
            row_sum = sum(row.values())
            for k, item in row.items():
                row[k] = item/row_sum

        self.num_states_ = len(states)
        self.transition_probabilities_ = transitions
        self.initial_probabilities_ = self.transition_probabilities_[self.start_symbol_]
        self.loglikelihoods_ = [np.sum(self.score(X, lengths))]

    def score_samples(self, X, lengths):
        """
        return log probability of all sequences
        """
        transitions = self.transition_probabilities_
        result = []
        for sequence in yield_sequences(X, lengths):
            prob = 0
            for i, gram in enumerate(util.ngrams(sequence, n=self.order+1, pad=True)):
                history = gram[:-1]
                current = gram[-1]
                prob += np.log(transitions[history][current])
            result.append(prob)
        return np.asarray(result)

    def score(self, X, lengths):
        return self.score_samples(X, lengths).mean()

    def unconditional_state_probabilities(self, t):
        """
        Parameters
        ==========

        t: int
           number of time steps
        """

        initials = np.asarray(self.initial_probabilities_)
        transitions = np.asarray(self.transition_probabilities_)
        for i in range(t):
            # Matrix multiplications
            initials = initials @ transitions
        return initials

    def predict(self, X, lengths, random_state=None):
        """
        Predict next state
        """
        if random_state is None:
            random_state = self.random_state

        random_state = check_random_state(random_state)
        result = []
        transition_cdf = np.cumsum(self.transition_probabilities_, axis=1)
        for sequence in yield_sequences(X, lengths):
            rand = random_state.rand()
            currstate = sequence[-1]
            currstate = (transition_cdf[currstate] > rand).argmax()
            result.append(currstate)
        return result

    def sample(self, n_samples=1, length=10, random_state=None):

        if random_state is None:
            random_state = self.random_state
        random_state = check_random_state(random_state)

        result = []
        lengths = []
        #initial_pdf = self.initial_probabilities_.cumsum()
        #transition_cdf = np.cumsum(self.transition_probabilities_, axis=1)
        for i in range(n_samples):
            sequence = []
            currstate = None
            state = self.start_symbol_
            for j in range(length):
                currstate = random_state.choice(list(self.transition_probabilities_[state].keys()), p=list(self.transition_probabilities_[state].values()))
                #currstate = (transition_cdf[currstate] > rand).argmax()
                state = tuple(list(state[1:]) + [currstate])
                sequence.append(currstate)
            result.extend(sequence)
            lengths.append(len(sequence))
        return np.asarray(result)

    def n_parameters(self):
        """
        Hidden Markov Models for TimeSeries
        """
        num_states = self.num_states_
        order = self.order
        return (num_states**order) * (num_states-1)
