# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging
import multiprocessing
import os
import sys
import warnings

import psutil

__logger = None


def init_logging(level=logging.ERROR, handler=None):
    warnings.warn("Calling Deprecated Function init_logging")
    global __logger
    return create_logger(__name__, level)


def set_level(level, name=__name__):
    warnings.warn("Calling Deprecated Function set_level")
    logger = logging.getLogger(name)
    logger.setLevel(level)
    return logger


def get_formatter():
    return logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(memory)skb - %(proc_name)s/%(pid)s %(message)s')


class MemoryFilter(logging.Filter):
    """
    Decorate log messages with memory, process, and pid information
    """

    def filter(self, record):
        pid = os.getpid()
        record.memory = psutil.Process(pid).memory_info().rss / 1024.
        record.proc_name = multiprocessing.current_process().name
        record.pid = pid
        return True


def create_logger(name, level):
    """
    New logging api.. call create_logger(__name__, log_level)
    Automatically adds process info into log message
    """
    logger = logging.getLogger(name)
    if not len(logger.handlers):
        # create console handler and set level to debug
        handler = logging.StreamHandler(sys.stdout)
        # Specifies lowest level message that the handler will process
        # create formatter
        # add formatter to handler
        handler.setFormatter(get_formatter())

        # add handler to logger
        logger.addHandler(handler)
        logger.addFilter(MemoryFilter())
    logger.setLevel(level)
    logger.debug("Logger Created")
    return logger


class LoggerMixin(object):

    logger = property(lambda self: self._logger)

    def __init__(self, name, level):
        self._logger = create_logger(name + "." + type(self).__name__, level)

    def log_debug(self, msg, *args):
        return self._logger.debug(msg, *args)

    def log_info(self, msg, *args):
        return self._logger.info(msg, *args)

    def log_error(self, msg, *args):
        return self._logger.error(msg, *args)

    def log_warning(self, msg, *args):
        return self._logger.warning(msg, *args)

    def log_exception(self, msg, *args):
        return self._logger.exception(msg, *args)

    def _set_level(self, level):
        return self._logger.setLevel(level)
