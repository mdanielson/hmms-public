# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

import numpy as np

from sklearn.preprocessing import normalize
from sklearn.utils import check_random_state
from sklearn.utils.validation import check_is_fitted

from . import EMHMMBase, confidence_interval, util
from ._hmm import new_categorical, reestimate

from .types import Any, Optional, Dict, ObservationType, LengthType, StatesType


class CategoricalHMM(EMHMMBase.EMHMMBase):
    """
    A Hidden Markov Model with Categorical Emissions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    init_pi : bool
    init_A : bool
    init_emissions : bool

    """

    def __init__(self, n_components: int = 1, n_iterations: int = 100, n_inits: int = 1, n_jobs: int = -1, tol: float = 1e-6,
                 init_pi: Optional[str] = "random", init_A: Optional[str] = "random", init_emissions: Optional[str] = "random",
                 n_features: Optional[int] = None, smoothing: Optional[str] = None, smoothing_params: Optional[Any] = None,
                 implementation: str = "scaling", allowed_to_use_log: bool = True, random_state: Optional[Any] = None, verbose: int = 0):
        super(CategoricalHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                             n_jobs=n_jobs, tol=tol, init_pi=init_pi, init_A=init_A,
                                             implementation=implementation, allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.init_emissions = init_emissions
        self.n_features = n_features
        self.smoothing = smoothing
        self.smoothing_params = smoothing_params

    @classmethod
    def reestimate_from_sequences(cls, observed_states: ObservationType, hidden_states: StatesType, lengths: LengthType, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        CategoricalHMM
        """
        observed_states, lengths = util.check_arguments(observed_states, lengths)
        observed_states = observed_states.astype(int)
        hidden_states = np.asarray(hidden_states)
        pi, A, B = reestimate.reestimate_categorical(observed_states, hidden_states, lengths)
        instance = cls(
            n_iterations=0,
            init_pi=None,
            init_A=None,
            init_emissions=None,
            random_state=random_state,
        )
        instance.pi_ = pi
        instance.A_ = A
        instance.B_ = B
        return instance

    def _init_emissions(self, X: ObservationType, lengths: LengthType):
        if self.n_features is None:
            symbols = np.unique(X)
            self.n_features_ = int(max(symbols.max()+1, symbols.shape[0]))
        else:
            self.n_features_ = self.n_features

        rs = check_random_state(self.random_state)
        if self.init_emissions is None:
            pass
        elif self.init_emissions == "random":
            self.B_ = normalize(
                rs.rand(self.n_components, self.n_features_),
                norm="l1",
                axis=1
            )
        elif self.init_emissions == "uniform":

            self.B_ = np.full((self.n_components, self.n_features_), 1. / self.n_features_)
        else:
            if not isinstance(self.init_emissions, np.ndarray) and not isinstance(self.init_emissions, list):
                raise ValueError("init_emissions parameter should be an array or a list, got {}".format(type(self.init_emissions)))
            self.B_ = np.asarray(self.init_emissions)

    def _update_after_fit(self, trainer):
        super()._update_after_fit(trainer)
        self.B_ = np.asarray(trainer.emissions.B)

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(CategoricalHMM, self)._prepare_for_fit()
        self.B_ = np.asarray(self.B_)
        self.B_ = normalize(self.B_, norm="l1", axis=1)
        assert self.A_.shape[0] == self.B_.shape[0]

    def _assert_is_fitted(self):
        super(CategoricalHMM, self)._assert_is_fitted()
        check_is_fitted(self, ["B_"])

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_categorical(
            self.pi_,
            self.A_,
            self.B_,
            smoothing=self.smoothing,
            smoothing_params=self.smoothing_params,
            impl=self.implementation,
            allowed_to_use_log=self.allowed_to_use_log,
            log_level=self.verbose
        )

    def _emissions_params(self) -> int:
        """
        Matrix rows must sum to 1, so final value is dependenct on other values
        """
        B_params = max(1, (self.B_.shape[0] * (self.B_.shape[1]-1)))
        B_params -= (np.abs(self.B_) < util.EPSILON).sum()
        return B_params

    def _compute_confidence_intervals(self, n_samples, length, n_iterations, ci):
        """
        Call implementation specific confidence interval code
        """
        return confidence_interval.compute_categorical_confidence_interval(self, n_samples=n_samples, length=length, n_iterations=n_iterations, ci=ci)

    def _emissions_to_dict(self) -> Optional[Dict]:
        if hasattr(self, "B_"):
            return np.asarray(self.B_).tolist()
        return None

    def _emissions_from_dict(self, B):
        if B is not None:
            self.B_ = np.asarray(B)
