# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import collections
import datetime

import joblib
import numpy as np
import sklearn.base
import sklearn.model_selection
import sklearn.utils


def msg(msg):
    print("{} - {}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), msg))


def clone_with_random_state(estimator, random_state):
    """
    Sample and set the estimator's random state
    """
    new_estimator = sklearn.base.clone(estimator)
    new_estimator.random_state = random_state.randint(1, 2**32-1)
    return new_estimator


def fit_parallel_variational(estimator, X, lengths):
    """
    Train multiple copies of an estimator, keeping the estimator with the best lower_bound_

    Parameters
    ----------

    estimator : HMMBase-derived class

    X : array-like, shape (n_samples, length)
        The input data array

    Returns
    -------
    estimator : HMMBase-derived class
        The best fit estimator
    """
    n_inits = estimator.n_inits
    estimator.set_params(n_inits=1)
    random_state = sklearn.utils.check_random_state(estimator.random_state)
    with joblib.Parallel(n_jobs=estimator.n_jobs, verbose=estimator.verbose) as parallel:
        models = parallel(joblib.delayed(train_one)(clone_with_random_state(estimator, random_state), X, lengths) for i in range(n_inits))

    free_energies = [m.lower_bound_[-1] for m in models]
    best_idx = np.argmax(free_energies)
    best_estimator = models[best_idx]
    best_estimator.set_params(n_inits=n_inits, random_state=estimator.random_state)

    estimator.__dict__.update(best_estimator.__dict__)
    estimator.explored_lower_bounds_ = [m.lower_bound_ for m in models]
    return estimator


def fit_parallel(estimator, X, lengths):
    """
    Train multiple copies of an estimator, keeping the estimator with the best loglikelihoods_

    Parameters
    ----------

    estimator : HMMMixtureBase-derived class

    X : array-like, shape (n_samples, length)
        The input data array

    Returns
    -------
    estimator : HMMMixtureBase-derived class
        The best fit estimator
    """
    n_inits = estimator.n_inits
    estimator.set_params(n_inits=1)

    random_state = sklearn.utils.check_random_state(estimator.random_state)
    with joblib.Parallel(n_jobs=estimator.n_jobs, verbose=estimator.verbose) as parallel:
        models = parallel(joblib.delayed(train_one)(clone_with_random_state(estimator, random_state), X, lengths) for i in range(n_inits))

    lls = [m.loglikelihoods_[-1] for m in models]
    best_idx = np.argmax(lls)
    best_estimator = models[best_idx]
    best_estimator.set_params(n_inits=n_inits, random_state=estimator.random_state)

    estimator.__dict__.update(best_estimator.__dict__)
    estimator.explored_loglikelihoods_ = [m.loglikelihoods_ for m in models]
    return estimator


def train_one(model, *args):
    """
    Call Fit on a model given data. Used for Parallel training
    """
    model.fit(*args)
    return model


EPSILON = 1e-9


def ngrams(sequence, n=1, pad=False, lpad=None, ):
    sequence = list(sequence)
    if pad:
        gram = [lpad] * n
        for item in sequence:
            gram.append(item)
            gram = gram[1:]
            yield tuple(gram)
    else:
        gram = sequence[:n]
        for item in sequence[n:]:
            yield tuple(gram)
            gram.append(item)
            gram = gram[1:]
        yield tuple(gram)


def valid_covariance(covariance_type):
    return covariance_type in ("full", "tied", "diagonal", "spherical")


def sample_and_split(models, n_samples=100, sample_lengths=200, test_size=.7, random_state=None):
    random_state = sklearn.utils.check_random_state(random_state)
    observations = []
    labels = []
    lengths = []
    if isinstance(n_samples, int):
        new_n_samples = [n_samples] * len(models)
        n_samples = new_n_samples

    for i, (m, n) in enumerate(zip(models, n_samples)):
        labels += [i] * n
        observed, _, lens = m.sample(n, sample_lengths, random_state=random_state)
        observations.append(observed)
        lengths += lens.tolist()
    observations = np.concatenate(observations)
    cumlengths = np.cumsum(lengths)

    train_idx, test_idx = sklearn.model_selection.train_test_split(np.arange(cumlengths.shape[0]), test_size=test_size, stratify=labels, random_state=random_state)
    cumlengths = [0] + cumlengths.tolist()
    test_labels = np.asarray([labels[i] for i in test_idx])
    train_labels = np.asarray([labels[i] for i in train_idx])
    partitioned_X, partitioned_lengths = partition_sequences(observations, lengths, {"train": train_idx, "test":test_idx})
    return partitioned_X["train"], partitioned_X["test"], train_labels, test_labels, partitioned_lengths["train"], partitioned_lengths["test"]


def iter_sequences(X, lengths):
    """
    Yield an iterator over individual sequences in X
    """
    seq_start = 0
    for o_i in range(len(lengths)):
        seq_end = seq_start + lengths[o_i]
        yield X[seq_start:seq_end]
        seq_start = seq_end


def partition_train_test(X, lengths, train_indices, test_indices):
    partitioned_X, partitioned_lengths = partition_sequences(X, lengths, {"train": train_indices, "test":test_indices})
    return partitioned_X["train"], partitioned_lengths["train"], \
            partitioned_X["test"], partitioned_lengths["test"]


def partition_sequences(X, lengths, partition_indices):
    """
    Given a set of sequences specified by X and lengths, separate this into disparate groups based on the indices in partition_indices
    """
    cum_lengths = [0] + np.cumsum(lengths).tolist()
    partitioned_X = collections.defaultdict(list)
    partitioned_lengths = collections.defaultdict(list)
    for partition, indices in partition_indices.items():
        for i in indices:
            partitioned_X[partition].append(X[cum_lengths[i]:cum_lengths[i+1]])
            partitioned_lengths[partition].append(lengths[i])

    for key, val in partitioned_X.items():
        partitioned_X[key] = np.concatenate(val)
        partitioned_lengths[key] = np.asarray(partitioned_lengths[key])
    return partitioned_X, partitioned_lengths



def partition_sequences_with_groups(X, lengths, partition_indices, groups=None):
    """
    Given a set of sequences specified by X and lengths, separate this into disparate groups based on the indices in partition_indices
    """
    cum_lengths = [0] + np.cumsum(lengths).tolist()
    partitioned_X = collections.defaultdict(list)
    partitioned_lengths = collections.defaultdict(list)
    partitioned_groups = collections.defaultdict(list)
    for partition, indices in partition_indices.items():
        for i in indices:
            partitioned_X[partition].append(X[cum_lengths[i]:cum_lengths[i+1]])
            partitioned_lengths[partition].append(lengths[i])
            if groups:
                partitioned_groups[partition].append(groups[i])

    for key, val in partitioned_X.items():
        partitioned_X[key] = np.concatenate(val)
        partitioned_lengths[key] = np.asarray(partitioned_lengths[key])
        if groups:
            partitioned_groups[key] = np.asarray(partitioned_groups[key])
    return partitioned_X, partitioned_lengths, partitioned_groups


def check_arguments(X, lengths):
    X = sklearn.utils.check_array(X, dtype=np.double)
    lengths = np.asarray(lengths, dtype=int)
    assert min(lengths) > 0, "A sequence of length 0 makes no sense"
    assert X.shape[0] == lengths.sum(), "X.shape[0] != lengths.sum() ({} != {})".format(X.shape[0], lengths.sum())
    return X, lengths


def take_first(n, X, lengths, groups=None):
    sequences_by_label = {}
    sequences_by_label["keep"] = np.arange(n)

    partitioned_X, partitioned_lengths, partitioned_groups = partition_sequences_with_groups(X, lengths, sequences_by_label, groups)

    new_X = []
    new_lengths = []
    new_groups = []
    for key, X_g in partitioned_X.items():
        new_X.append(X_g)
        new_lengths.append(partitioned_lengths[key])
        if groups:
            new_groups.extend(partitioned_groups[key])

    return np.concatenate(new_X), np.concatenate(new_lengths), np.asarray(new_groups) if groups else groups


def shuffle_sequences(X, lengths, groups=None, random_state=None):

    rs = sklearn.utils.check_random_state(random_state)
    indices = rs.permutation(lengths.shape[0])

    cum_lengths = [0] + np.cumsum(lengths).tolist()

    new_X = []
    new_lengths = []
    if groups:
        new_groups = []

    for i in indices:
        new_X.append(X[cum_lengths[i]:cum_lengths[i+1]])
        new_lengths.append(lengths[i])
        if groups:
            new_groups.append(groups[i])
    if groups:
        return np.concatenate(new_X), np.asarray(new_lengths), np.asarray(new_groups)
    else:
        return np.concatenate(new_X), np.asarray(new_lengths)
