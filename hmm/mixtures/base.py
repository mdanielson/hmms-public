# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import warnings
from abc import ABC, abstractmethod

import numpy as np
import scipy.stats
import sklearn.base
import sklearn.utils
import sklearn.utils.validation
from scipy.special import logsumexp

from .. import model_selection
from ..utilities import poisson


class BaseMixtureModel(ABC, sklearn.base.BaseEstimator):

    def __init__(self, n_components=2, n_iterations=100, n_inits=1, n_jobs=1, tol=1e-6, random_state=None):
        """
        Learn a Poisson Mixture Model via EM

        Parameters
        ==========
        n_components : int
            number of independent poissons to learn
        n_iterations : int
            maximum number of iterations to perform
        tol : float
              stopping criteria
        """
        self.n_components = n_components
        self.n_iterations = n_iterations
        self.n_inits = n_inits
        self.n_jobs = n_jobs
        self.tol = tol
        self.random_state = random_state

    def fit(self, X, y=None):
        """
        Learn a mixture model from the data using EM

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        n_features must be 1

        """

        self.lower_bounds_ = []
        X = sklearn.utils.check_array(X)
        model = None
        for init in range(self.n_inits):
            self._init_distributions(X)
            converged = False
            for i in range(self.n_iterations):

                log_norm, log_probs = self._e_estep(X)
                self.lower_bounds_.append(self._do_lower_bound(log_norm, log_probs, X))
                self._m_step(X, log_probs)
                if len(self.lower_bounds_) > 1:
                    delta = self.lower_bounds_[-2] - self.lower_bounds_[-1]
                    if delta > 0:
                        for i in self.lower_bounds_:
                            print(i)
                        assert False, "Lower bound did not improve: {}".format(delta)
                # Convergence check
                if i > 0 and np.abs(self.lower_bounds_[-2] - self.lower_bounds_[-1]) <= self.tol:
                    converged = True
                    break

            if not converged:
                warnings.warn("Training did not converge! Consider running for more iterations or choosing different starting values")
            break

    def _do_lower_bound(self, log_norm, log_probs, X):
        return log_norm.sum()

    @abstractmethod
    def _init_distributions(self, X):
        pass

    @abstractmethod
    def _weighted_log_probability(self, X):
        """
        Compute the join probability that each sample comes from each mixture.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        weighted_log_prob : array, shape (n_samples, n_component)
        """
        pass

    def _log_responsibilities(self, X):
        weighted_log_prob = self._weighted_log_probability(X)
        log_norm = logsumexp(weighted_log_prob, axis=1)
        weighted_log_prob -= log_norm[:, np.newaxis]
        return log_norm, weighted_log_prob

    def _e_estep(self, X):
        log_norm, log_responsibilities = self._log_responsibilities(X)
        return log_norm, log_responsibilities

    @abstractmethod
    def _m_step(self, X, log_probs):
        pass

    def score_samples(self, X):
        """
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
        """
        self._assert_is_fitted()
        return logsumexp(self._weighted_log_probability(X), axis=1)

    def score(self, X):
        """
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
        """
        self._assert_is_fitted()
        return self.score_samples(X).mean()

    def predict(self, X):
        """
        Predict mixture class membership for each sample
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
        """

        return self.predict_proba(X).argmax(axis=1)

    def predict_proba(self, X):
        """
        Compute class membership probabilities for each sample

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        memberships, array, shape(n_samples, n_components)
        """
        self._assert_is_fitted()
        _, log_probs = self._log_responsibilities(X)
        return np.exp(log_probs)

    @abstractmethod
    def n_parameters(self):
        """
        The number of free Parameters in this model
        """
        pass

    @abstractmethod
    def sample(self, n_samples=100):
        """
        Generate samples from the mixture model
        """
        pass

    def aic(self, X):
        """
        Compute The Akaike Information Criterion for the given dataset

        AIC = -2*logLike + 2 * num_free_params

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        float
        """
        num_data = X.shape[0]
        loglik = self.score(X).mean() * num_data
        return model_selection.akaike_information_criterion(loglik, num_data, self.n_parameters())

    def bic(self, X):
        """
        Compute The Bayesian Information Criterion for the given dataset

        BIC = -2*logLike + num_free_params * log(num_of_data)

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        float
        """
        num_data = X.shape[0]
        loglik = self.score(X).mean() * num_data
        return model_selection.bayesian_information_criterion(loglik, num_data, self.n_parameters())

    def caic(self, X):
        """
        Compute The Consistend Akaike Information Criterion for the given dataset

        CAIC = -2*logLike + num_free_params * (log(num_of_data) + 1)

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        float
        """
        num_data = X.shape[0]
        loglik = self.score(X).mean() * num_data
        return model_selection.consistent_akaike_information_criterion(loglik, num_data, self.n_parameters())
