# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import functools

import numpy as np
import sklearn.base
import scipy.special
import sklearn.cluster
import sklearn.utils
import sklearn.utils.validation

from ..utilities import poisson
from hmm.utilities.kl_divergences import kl_dirichlet_distribution, kl_categorical_distribution, kl_gamma_distribution
from . import base


class CachedComputation:
    def __init__(self, func):
        self._func = func
        self._cache = {}

    def __call__(self, x):
        if x in self._cache:
            return self._cache[x]
        else:
            self._cache[x] = self._func(x)
            return self._cache[x]


class VariationalPoissonMixtureModel(base.BaseMixtureModel):

    def __init__(self, n_components=2, rates_prior="uninformed", weights_prior="uninformed", n_iterations=100, n_inits=1, n_jobs=1, tol=1e-6, random_state=None):
        """
        Learn a Poisson Mixture Model via EM

        Parameters
        ==========
        n_components : int
            number of independent poissons to learn
        init_emissions : bool
            Initialize Means (using percentiles)
        init_weights : bool
        Initialize mixture weights (randomly)
        n_iterations : int
            maximum number of iterations to perform
        tol : float
              stopping criteria
        """
        super(VariationalPoissonMixtureModel, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=1, n_jobs=1, tol=tol, random_state=random_state)
        self.weights_prior = weights_prior
        self.rates_prior = rates_prior

    def _init_distributions(self, X):

        assert X.shape[1] == 1, "Not yet supported"
        random_state = sklearn.utils.check_random_state(self.random_state)

        if self.weights_prior is None or self.weights_prior == "uninformed":
            self.weights_prior_ = np.zeros(self.n_components, dtype=np.float) + 1 / self.n_components
            self.weights_posterior_ = self.weights_prior_.copy() * X.shape[0]
        else:
            raise ValueError("Unknown value for weights_prior: {}".format(self.weights_prior))
        if self.rates_prior is None or self.rates_prior == "uninformed":
            self.alpha_prior_ = np.zeros(self.n_components, dtype=np.float) + 1
            self.beta_prior_ = np.zeros(self.n_components, dtype=np.float) + 1

        elif self.rates_prior == "data":
            self.alpha_prior_ = np.zeros(self.n_components, dtype=np.float) + X.mean()
            self.beta_prior_ = np.zeros_like(self.alpha_prior_) + 1

        else:
            raise ValueError("Unknown initialization")

        kmeans = sklearn.cluster.KMeans(n_clusters=self.n_components, random_state=sklearn.utils.check_random_state(self.random_state))
        data = np.asarray(X).ravel()[:, None]
        kmeans.fit(data)
        rates = kmeans.cluster_centers_.ravel()

        self.alpha_posterior_ = rates * self.weights_posterior_
        self.beta_posterior_ = self.weights_posterior_.copy()
        self.rates_posterior_ = self.alpha_posterior_ / self.beta_posterior_

    def _weighted_log_probability(self, X):
        """
        Compute the join probability that each sample comes from each mixture.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        weighted_log_prob : array, shape (n_samples, n_component)
        """
        probs = np.empty((X.shape[0], self.weights_posterior_.shape[0]), np.float)
        for col in range(probs.shape[1]):
            probs[:, col] = self.subnormalized_density_for(col, X)

        # Expected value of the log of the weights
        log_weights =  scipy.special.digamma(self.weights_posterior_) - scipy.special.digamma(self.weights_posterior_.sum())
        return probs + log_weights

    def subnormalized_density_for(self, component, X):

        probs = np.empty(X.shape[0], np.float)
        mean1 =  np.exp(scipy.special.digamma(self.alpha_posterior_[component]) - np.log(self.beta_posterior_[component]))
        exp_beta =  np.exp(-self.rates_posterior_[component])

        assert not np.isnan(mean1), (mean1, self.alpha_posterior_[component], self.beta_posterior_[component])
        assert not np.isinf(mean1),  (mean1, self.alpha_posterior_[component], self.beta_posterior_[component])
        assert not np.isnan(exp_beta), (exp_beta, self.alpha_posterior_[component],  self.beta_posterior_[component])
        assert not np.isinf(exp_beta), (exp_beta, self.alpha_posterior_[component],  self.beta_posterior_[component])

        def do_poisson(exp_beta, mean1, item):
            d = exp_beta
            for i in range(1, item+1):
                d *= mean1
                d /= i
            return d

        computer = CachedComputation(functools.partial(do_poisson, exp_beta, mean1))

        for idx, item in enumerate(X.ravel()):
            d = computer(item)
            probs[idx] = d
            assert not np.isnan(d) and not np.isinf(d), (component, idx, item, d)

            #probs[idx] = - self.rates_posterior_[component] + item * mean1
        return np.log(probs)

    def _do_lower_bound(self, log_norm, log_probs, X):
        probs = np.exp(log_probs)
        mixture_sums = probs.sum(axis=0)
        kl_dir =  - kl_dirichlet_distribution(self.weights_posterior_, self.weights_prior_)
        kl_gamma = 0
        for component in range(self.n_components):
            kl_gamma -= kl_gamma_distribution(self.alpha_posterior_[component], self.beta_posterior_[component], self.alpha_prior_[component], self.beta_prior_[component])
        weights_kl_term =  np.exp(scipy.special.digamma(self.weights_posterior_) - scipy.special.digamma(self.weights_posterior_.sum()))
        kl_cat = 0
        log_prob = 0

        newprobs = np.empty((X.shape[0], self.weights_posterior_.shape[0]), np.float)
        for col in range(probs.shape[1]):
            newprobs[:, col] = self.subnormalized_density_for(col, X)
        for idx in range(probs.shape[0]):
            kl_cat -= kl_categorical_distribution(probs[idx], weights_kl_term)
            log_prob += np.sum(newprobs[idx] * probs[idx])

        result = log_prob
        result += kl_dir
        result += kl_gamma
        result += kl_cat
        return result

    def _m_step(self, X, log_probs):
        probs = np.exp(log_probs)
        mixture_sums = probs.sum(axis=0)
        mixture_weighted_sums = (probs * X).sum(axis=0)

        self.weights_posterior_ = self.weights_prior_ + mixture_sums
        self.alpha_posterior_ = self.alpha_prior_ + mixture_weighted_sums
        self.beta_posterior_ = self.beta_prior_ + mixture_sums

        self.rates_posterior_ = self.alpha_posterior_ / self.beta_posterior_

    def n_parameters(self):
        sklearn.utils.validation.check_is_fitted(self, ["rates_posterior_", "weights_posterior_"])
        return self.rates_posterior_.shape[0] + (self.weights_posterior_.shape[0] - 1)

    def sample(self, n_samples=100):
        sklearn.utils.validation.check_is_fitted(self, ["rates_posterior_", "weights_posterior_"])
        random_state = sklearn.utils.check_random_state(self.random_state)
        results = np.zeros(n_samples, dtype=np.float)
        weights = self.weights_posterior_ / self.weights_posterior_.sum()
        weights = np.cumsum(weights)
        for i in range(n_samples):
            r = random_state.rand()
            idx = (weights > r).argmax()
            results[i] = random_state.poisson(self.rates_posterior_[idx])
        return results

    def _assert_is_fitted(self):
        sklearn.utils.validation.check_is_fitted(self, ["rates_posterior_", "weights_posterior_"])
