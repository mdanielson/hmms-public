# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import scipy.special
import sklearn.cluster
import sklearn.mixture

from hmm.mixtures import GaussianMixtureModel

debug = False


def make_sample_data():
    gm = GaussianMixtureModel.GaussianMixtureModel(
        n_components=3,
        init_means=False,
        init_variances=False,
        init_weights=False,
        n_iter=0,
        random_state=32
    )

    # gm.means_ = np.asarray([0, 5, 10])#, 30, 50])
    # gm.variances_ = np.asarray([1, 1, 1])#, 8, 8])
    # gm.weights_ = np.asarray([.5, .25, .25])#, .25, .25])
    # return gm.sample(200)
    gm.means_ = np.asarray([10, 40, 50])
    gm.variances_ = np.asarray([1, 1, 1])
    gm.weights_ = np.asarray([.3, .35, .35])
    return gm.sample(200)


def e_step(data, weights_estimate, means_posterior, means_variances_posterior, b_s, c_s):

    log_pi_tilde_s = scipy.special.digamma(weights_estimate) - scipy.special.digamma(weights_estimate.sum())

    log_beta_tilde_s = scipy.special.digamma(c_s) + np.log(b_s)
    beta_hat_s = b_s * c_s
    # print(1/beta_hat_s)
    print("log_beta_tilde_s", log_beta_tilde_s)
    print("beta_tilde_s", np.exp(log_beta_tilde_s))
    #print("beta_hat_s", beta_hat_s)
    #beta_tilde_s = np.exp(log_beta_tilde_s)
    print("log_pi_tilde_s", log_pi_tilde_s)
    print("pi_tilde_s", np.exp(log_pi_tilde_s))
    #pi_tilde_s = np.exp(log_pi_tilde_s)
    gammas = np.zeros((data.shape[0], weights_estimate.shape[0]))
    #print("pi_tilde_s", pi_tilde_s)
    for i in range(gammas.shape[0]):
        for j in range(gammas.shape[1]):
            inside_exp = -.5 * beta_hat_s[j] * (data[i]**2 + means_posterior[j]**2 + means_variances_posterior[j] - 2 * means_posterior[j] * data[i])

            ans = log_pi_tilde_s[j] + .5 * log_beta_tilde_s[j] + inside_exp
            gammas[i, j] = ans
    # print(gammas)
    print("ASDFASDFA")
    print(np.exp(gammas[:4]))
    gammas -= scipy.special.logsumexp(gammas, axis=1)[:, None]
    #gammas = np.zeros((data.shape[0], weights_estimate.shape[0]))
    #weights = weights_estimate/weights_estimate.sum()
    # for j in range(gammas.shape[1]):
    #    print("normal: {:.4f} {:.4f}".format(means_posterior[j], np.sqrt(1/(b_s[j]*c_s[j]))))
    #    dist = scipy.stats.norm(means_posterior[j], np.sqrt(1/(b_s[j]*c_s[j])))
    #    gammas[:, j] =  dist.logpdf(data)
    #print("gammas-logpdf", gammas[4,:])
    #gammas += np.log(weights)
    #print("gammas-weights", gammas[4,:])
    #gammas -= scipy.special.logsumexp(gammas, axis=1)[:, None]
    #print("gammas-normed", gammas[4,:])
    return np.exp(gammas)
    # return gammas


def m_step(gammas, data, weights_prior, means_prior, means_posterior, means_variances_posterior, b_0, c_0, mean_precision_prior_):
    print("gamma and data")
    print(data[:4])
    print(gammas[:4])
    N = gammas.shape[0]
    print(gammas.sum(axis=0))
    pi_hat = 1/N * gammas.sum(axis=0)
    #print("pi_hat", pi_hat)
    N_hat = N * pi_hat
    # print(N_hat)
    # print(gammas[2])
    y_hat = 1/N * np.dot(data, gammas)
    y_tilde_squared = 1/N * np.dot(data**2, gammas)
    print("y_tilde_squareD", y_tilde_squared)
    weights_estimate = N_hat + weights_prior

    sigma_squared = y_tilde_squared + pi_hat * (means_posterior**2 + means_variances_posterior) - 2 * means_posterior * y_hat

    b_inv = N / 2 * sigma_squared + 1 / b_0
    b_s = 1/b_inv
    c_s = N_hat / 2 + c_0
    beta_s = b_s * c_s
    #new_mean_variance = b_inv * 1/c_s
    if True:
        print("sigma_squared", sigma_squared)
        print("b_inv", b_inv)
        print("NHAT", N_hat)
        print("c_s", c_s)
        print("b_s", b_s)
        print("beta_s", beta_s)
        print("beta_s", weights_estimate)
        print("pi_hat", pi_hat)
        print("ASDF", sigma_squared/pi_hat)

        print(N_hat)
    #print("c_s", c_s)
    #print(b_s, c_s)
    m_data = y_hat / pi_hat
    # Force means going to zero to be zero
    m_data[np.isnan(m_data)] = 0
    tau_data = N_hat * beta_s  # 1 / new_mean_variance
    # print(m_data)
    # print(tau_data)
    tau_s = mean_precision_prior_ + tau_data
    means_posterior = (mean_precision_prior_ / tau_s) * means_prior + tau_data/tau_s * m_data
    #print(mean_precision_prior_, tau_s)
    #print("ratio0", mean_precision_prior_/tau_s)

    #print("tau_s", tau_s)
    #print("means_posterior", means_posterior)
    return weights_estimate, b_s, c_s, means_posterior, tau_s


if __name__ == "__main__":
    data = make_sample_data()
    data2d = data[:, None]
    N = data.shape[0]

    means_prior = data2d.mean().ravel()[0]
    v_0 = ((data2d.max() - data2d.min())/3)**2
    mean_precision_prior_ = 1 / v_0

    b_0 = 10**3
    c_0 = 10**-3  # Inverse of what numpy wants

    if False:
        pi_s = np.asarray([.25, .25, .25, .25])
        means_posterior = np.asarray([4, 5.1, 5.5, 6])
        variances = np.asarray([20, 20, 20, 20])
    elif False:
        pi_s = np.asarray([.333, .3333, .3333])
        means_posterior = np.asarray([4, 5, 6])
        variances = np.asarray([20, 20, 20])
    else:
        kmeans = sklearn.cluster.KMeans(n_clusters=4, random_state=43243)
        kmeans.fit(data2d)
        assignments = kmeans.predict(data2d)
        pis = np.zeros(kmeans.n_clusters)
        means = kmeans.cluster_centers_
        variances = np.zeros(kmeans.n_clusters)
        for i in sorted(set(assignments)):
            these = assignments == i
            pis[i] = these.sum()/these.shape[0]
            variances[i] = np.dot(data2d[these].ravel() - means[i], data2d[these].ravel() - means[i])/(these.sum() - 1)
            print(variances[i])
        print("ASFDDSAFDSF")
        print(variances)
        pi_s = pis.ravel()
        means_posterior = means.ravel()
        variances = variances.ravel()
    weights_prior = 1e-4

    # Means posterior
    means_variances_posterior = variances / (pi_s * N)
    tau_s = 1/means_variances_posterior
    # Variances Posterior
    b_s = 1 / variances
    c_s = np.full((variances.shape[0],), variances)

    # Mixing Posterior
    weights_estimate = pi_s * data.shape[0]
    print("mean_precision_prior_", mean_precision_prior_)
    print("b_0", b_0)
    print("c_0", c_0)
    print("pi_s", pi_s)
    print("means_posterior", means_posterior)
    print("variances", variances)
    print("means_posterior", means_posterior)
    print("means_variances_posterior", means_variances_posterior)
    print("tau_s", tau_s)
    print("b_s", b_s)
    print("c_s", c_s)

    print("lambda_a", weights_estimate)

    print("variances", variances)

    #print(means_posterior, tau_s)
    print(means_posterior)
    #print(1/(b_s* c_s))
    print(weights_estimate)
    print(b_s, c_s)
    for i in range(500):

        gammas = e_step(data, weights_estimate, means_posterior, means_variances_posterior, b_s, c_s)
        # print(gammas[12])
        weights_estimate, b_s, c_s, means_posterior, tau_s = m_step(gammas, data, weights_prior, means_prior, means_posterior, means_variances_posterior, b_0, c_0, mean_precision_prior_)
        # print(1/(b_s*c_s))
        means_variances_posterior = 1 / tau_s
        print("{}: weights_estimate: {}".format(i, weights_estimate))
        print("{}: beta_s: {}".format(i, b_s * c_s))
        #print("means_posterior", means_posterior)
        #print(1/(b_s* c_s))

        #print(means_posterior, tau_s)
        # print(means_variances_posterior)
        # print()
    print(data2d[:6])
    print("Final Means", means_posterior)
    print("Final means_variances", means_variances_posterior)
    print("Final Variances", 1/(b_s*c_s))
    print("Final Weights", weights_estimate)
    print("Final Weights", np.asarray(weights_estimate)/sum(weights_estimate))
    # print("slkear")
    #mix = sklearn.mixture.BayesianGaussianMixture(n_components=4, verbose=100, n_init=1)
    # mix.fit(data2d)
    # print(mix.means_.ravel())
    # print(mix.covariances_)
    # print(mix.weights_)
