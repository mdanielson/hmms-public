# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import sklearn.base
import sklearn.utils
import sklearn.utils.validation

from ..utilities import poisson
from . import base


class PoissonMixtureModel(base.BaseMixtureModel):

    def __init__(self, n_components=2, init_emissions=True, init_weights=True, n_iterations=100, n_inits=1, n_jobs=1, tol=1e-6, random_state=None):
        """
        Learn a Poisson Mixture Model via EM

        Parameters
        ==========
        n_components : int
            number of independent poissons to learn
        init_emissions : bool
            Initialize Means (using percentiles)
        init_weights : bool
        Initialize mixture weights (randomly)
        n_iterations : int
            maximum number of iterations to perform
        tol : float
              stopping criteria
        """
        super(PoissonMixtureModel, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=1, n_jobs=1, tol=tol, random_state=random_state)
        self.init_emissions = init_emissions
        self.init_weights = init_weights

    def _init_distributions(self, X):

        assert X.shape[1] == 1, "Not yet supported"
        random_state = sklearn.utils.check_random_state(self.random_state)

        if self.init_emissions:
            if self.init_emissions == "percentile":
                percentiles = 100 * np.arange(1, self.n_components+1) / (self.n_components + 1)
                self.rates_ = np.percentile(X.ravel(), percentiles)
            else:
                self.rates_ = random_state.random_integers(X.min(), X.max(), size=(self.n_components))
        self.rates_ = np.asarray(self.rates_)

        assert self.rates_ is not None
        if self.init_weights:
            self.weights_ = random_state.rand(self.n_components)
        else:
            self.weights_ = np.asarray(self.weights_)
        assert np.all(self.weights_ >= 0)
        # Mixture Weights must sum to 1
        self.weights_ /= self.weights_.sum()

    def _weighted_log_probability(self, X):
        """
        Compute the join probability that each sample comes from each mixture.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        weighted_log_prob : array, shape (n_samples, n_component)
        """
        probs = np.empty((X.shape[0], self.weights_.shape[0]), np.float)
        for col in range(probs.shape[1]):
            pdist = poisson.Poisson(self.rates_[col])
            for row in range(probs.shape[0]):
                xrow = X[row]
                probs[row, col] = np.log(pdist.pmf(xrow[0]))
        return probs + np.log(self.weights_)

    def _m_step(self, X, log_probs):
        probs = np.exp(log_probs)
        mixture_sums = probs.sum(axis=0)
        mixture_weighted_sums = (probs * X).sum(axis=0)

        self.weights_ = mixture_sums / X.shape[0]
        self.rates_ = mixture_weighted_sums / mixture_sums

    def n_parameters(self):
        sklearn.utils.validation.check_is_fitted(self, ["rates_", "weights_"])
        return self.rates_.shape[0] + (self.weights_.shape[0] - 1)

    def sample(self, n_samples=100):
        sklearn.utils.validation.check_is_fitted(self, ["rates_", "weights_"])
        random_state = sklearn.utils.check_random_state(self.random_state)
        results = np.zeros(n_samples, dtype=float)
        weights = np.cumsum(self.weights_)
        for i in range(n_samples):
            r = random_state.rand()
            idx = (weights > r).argmax()
            results[i] = random_state.poisson(self.rates_[idx])
        return results.astype(int)

    def _assert_is_fitted(self):
        sklearn.utils.validation.check_is_fitted(self, ["rates_", "weights_"])
