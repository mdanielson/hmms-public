# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import sklearn.cluster
from sklearn.utils import check_array, check_random_state

from . import VariationalHMMBase, util, base
from . import MultivariateGaussianHMM
from ._hmm import new_variational_gmm, reestimate


class GMMVariationalHMM(VariationalHMMBase.VariationalHMMBase, base.VariationalGaussianMixin):
    """
    A Hidden Markov Model with Categorical Emissions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    """

    B_ = property(lambda x: x.B_normalized_)

    def __init__(self, n_components=1, mixture_n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, pi_prior=None, pi_posterior="sample",
                 A_prior=None, A_posterior="sample",
                 mixture_weights_prior=None, mixture_weights_posterior="uniform",
                 mixture_means_prior=None, mixture_means_posterior="kmeans",
                 mixture_covariances_prior=None, mixture_covariances_posterior="kmeans",
                 mixture_degrees_of_freedom_prior=None, mixture_covariance_type="full", implementation="scaling", random_state=None, verbose=0):
        super(GMMVariationalHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                             n_jobs=n_jobs, tol=tol, pi_prior=pi_prior, pi_posterior=pi_posterior,
                                             A_prior=A_prior, A_posterior=A_posterior,
                                             implementation=implementation, allowed_to_use_log=True, random_state=random_state, verbose=verbose)
        self.mixture_n_components = mixture_n_components
        self.mixture_weights_prior = mixture_weights_prior
        self.mixture_weights_posterior = mixture_weights_posterior
        self.mixture_means_prior = mixture_means_prior
        self.mixture_means_posterior = mixture_means_posterior
        self.mixture_covariances_prior = mixture_covariances_prior
        self.mixture_covariances_posterior = mixture_covariances_posterior
        self.mixture_degrees_of_freedom_prior = mixture_degrees_of_freedom_prior
        self.mixture_covariance_type = mixture_covariance_type

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, lengths, n_components, mixture_covariance_type="full", random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        GMMVariational
        HMM"""

        observed_states, lengths = util.check_arguments(observed_states, lengths)
        pi, A, mixture_weights, mixture_means, mixture_covariances = reestimate.reestimate_gmm(observed_states, hidden_states, lengths, n_components, mixture_covariance_type, random_state=random_state)

        instance = cls(
            n_iterations=0,
            random_state=random_state,
            mixture_covariance_type=mixture_covariance_type
        )
        state_counts = np.bincount(hidden_states.ravel()).astype(float)
        instance._reestimate_with_prior(pi, A, state_counts, lengths)

        instance.mixture_weights_posterior_ = mixture_weights
        instance.mixture_weights_prior_ = mixture_weights
        instance.mixture_weights_ = mixture_weights
        instance.mixture_means_prior_ = mixture_means
        instance.mixture_means_posterior_ = mixture_means.copy()

        instance.mixture_beta_prior_ = mixture_weights * state_counts.copy()
        instance.mixture_beta_posterior_ = mixture_weights * state_counts.copy()
        instance.mixture_degrees_of_freedom_prior_ = mixture_weights * state_counts.copy()
        instance.mixture_degrees_of_freedom_posterior_ = mixture_weights * state_counts.copy()

        instance.mixture_scale_prior_ = mixture_covariances.copy()
        instance.mixture_scale_posterior_ = mixture_covariances.copy()
        instance.mixture_covariances_posterior_ = mixture_covariances.copy()

        for hidden_state in range(state_counts.shape[0]):
            for mixture_component in range(instance.mixture_weights_posterior_[hidden_state].shape[0]):
                instance.mixture_scale_prior_[hidden_state, mixture_component] *= mixture_weights[hidden_state, mixture_component] * state_counts[hidden_state]
                instance.mixture_scale_posterior_[hidden_state, mixture_component] *= mixture_weights[hidden_state, mixture_component] * state_counts[hidden_state]

        return instance

    def _init_emissions(self, X, lengths):
        assert self.mixture_means_prior is None
        rs = check_random_state(self.random_state)
        self.n_variates_ = X.shape[-1]
        data_mean = X.mean(axis=0)
        d = X - data_mean
        overall_variance = np.dot(d.T, d) / d.shape[0]
        if self.mixture_weights_prior is None:
            self.mixture_weights_prior_ = np.full((self.n_components, self.mixture_n_components), 1./self.mixture_n_components)
        else:
            self.mixture_weights_prior_ = np.zeros((self.n_components, self.mixture_n_components)) + mixture_weights_prior

        self.mixture_weights_posterior_ = np.zeros((self.n_components, self.mixture_n_components), dtype=float)


        self.mixture_means_prior_ = np.full((self.n_components, self.mixture_n_components, self.n_variates_), data_mean)
        if self.mixture_means_prior is None or self.mixture_means_prior == "uninformed":
            # similar to sklearn
            self.mixture_beta_prior_ = np.zeros((self.n_components, self.mixture_n_components)) + 1
        elif self.mixture_means_prior == "data":
            # similar to sklearn
            self.mixture_beta_prior_ = np.zeros(self.n_components, self.mixture_n_components) + X.shape[0]
        else:
            raise ValueError("Unknown option for means prior: {}".format(self.mixture_means_prior))
        if isinstance(self.mixture_covariances_prior, tuple):
            # Format is (gamma, delta)
            self.mixture_degrees_of_freedom_prior_ = np.zeros((self.n_components, self.mixture_n_components), dtype=float) + self.mixture_covariances_prior[0]
            self.mixture_scale_prior_ = np.zeros((self.n_components, self.mixture_n_components, self.n_variates_, self.n_variates_), dtype=float) + self.mixture_covariances_prior[1]
        else:
            if self.mixture_covariance_type == "full":
                if self.mixture_covariances_prior is None or self.mixture_covariances_prior == "uninformed":
                    # From ~wpenny publication
                    self.mixture_degrees_of_freedom_prior_ = np.zeros((self.n_components, self.mixture_n_components), dtype=float) + self.n_variates_
                    self.mixture_scale_prior_ = np.zeros((self.n_components, self.mixture_n_components, self.n_variates_, self.n_variates_), dtype=float)
                    for hidden_state in range(self.n_components):
                        for mixture_component in range(self.mixture_n_components):
                            self.mixture_scale_prior_[hidden_state, mixture_component] = np.identity(self.n_variates_) * 1e-3
                elif self.mixture_covariances_prior == "data":
                    self.mixture_degrees_of_freedom_prior_ = np.zeros((self.n_components, self.mixture_n_components), dtype=float) + len(lengths) / self.n_components
                    self.mixture_scale_prior_ = np.zeros((self.n_components, self.mixture_n_components, self.n_variates_, self.n_variates_), dtype=float) + overall_variance
                else:
                    raise ValueError("Invalid option for variances_prior:{}".format(self.variances_prior))
            elif self.mixture_covariance_type == "tied":
                if self.mixture_covariances_prior is None or self.mixture_covariances_prior == "uninformed":
                    # From ~wpenny publication
                    self.mixture_degrees_of_freedom_prior_ = np.zeros(self.n_components, dtype=float) + self.n_variates_
                    self.mixture_scale_prior_ = np.zeros((self.n_components, self.n_variates_, self.n_variates_), dtype=float)
                    for hidden_state in range(self.n_components):
                        self.mixture_scale_prior_[hidden_state] = np.identity(self.n_variates_) * 1e-3
                elif self.mixture_covariances_prior == "data":
                    assert False
                    self.mixture_degrees_of_freedom_prior_ = len(lengths) / self.n_components
                    self.mixture_scale_prior_ = np.zeros((self.n_variates_, self.n_variates_), dtype=float) + overall_variance
                else:
                    raise ValueError("Invalid option for variances_prior:{}".format(self.variances_prior))
            else:
                raise NotImplementedError("Covariance: {} is not implemented".format(self.mixture_covariance_type))

        kmeans = sklearn.cluster.KMeans(n_clusters=self.n_components, random_state=rs)
        X_ = X.reshape(-1, X.shape[-1])
        labels = kmeans.fit(X_).labels_

        self.mixture_means_posterior_ = np.zeros((self.n_components, self.mixture_n_components, self.n_variates_))
        self.mixture_beta_posterior_ = np.zeros((self.n_components, self.mixture_n_components))

        if self.mixture_covariance_type == "full":
            self.mixture_scale_posterior_ = np.zeros((self.n_components, self.mixture_n_components, self.n_variates_, self.n_variates_))
            self.mixture_degrees_of_freedom_posterior_ = np.zeros((self.n_components, self.mixture_n_components))
            self.mixture_covariances_posterior_ = np.zeros((self.n_components, self.mixture_n_components, self.n_variates_, self.n_variates_))
        elif self.mixture_covariance_type == "tied":
            self.mixture_scale_posterior_ = np.zeros((self.n_components, self.n_variates_, self.n_variates_))
            self.mixture_degrees_of_freedom_posterior_ = np.zeros((self.n_components,))
            self.mixture_covariances_posterior_ = np.zeros((self.n_components, self.n_variates_, self.n_variates_))
        else:
            raise NotImplementedError("Covariance {} is not implemented".format(self.mixture_covariance_type))

        for hidden_state in range(self.n_components):
            these = labels == hidden_state
            self.mixture_weights_posterior_[hidden_state] = self.mixture_weights_prior_[hidden_state] * these.sum()
            this_X = X_[these]
            if self.mixture_n_components > 1:
                kmeans2 = sklearn.cluster.KMeans(n_clusters=self.mixture_n_components, random_state=rs)
                these_labels = kmeans2.fit(this_X).labels_
                means = kmeans2.cluster_centers_
            else:
                these_labels = np.zeros(this_X.shape[0], dtype=int)
                means = kmeans.cluster_centers_[hidden_state]

            responsibilities = np.zeros((this_X.shape[0], self.mixture_n_components))
            responsibilities[np.arange(this_X.shape[0]), these_labels] = 1

            counts = responsibilities.sum(axis=0)
            (_, variances) = MultivariateGaussianHMM.estimate_initial_mean_and_variance(self.mixture_n_components, this_X, responsibilities, self.mixture_covariance_type)

            if self.mixture_means_posterior == "kmeans":
                self.mixture_means_posterior_[hidden_state] = means
                self.mixture_beta_posterior_[hidden_state] += counts
            else:
                raise ValueError("Invalid option for mixture_means_posterior:{}".format(self.mixture_means_posterior))

            if self.mixture_covariances_posterior == "kmeans":
                self.mixture_scale_posterior_[hidden_state] = np.copy(variances)
                if self.mixture_covariance_type == "full":
                    self.mixture_degrees_of_freedom_posterior_[hidden_state] = counts
                    for mixture_component in range(self.mixture_n_components):
                        self.mixture_scale_posterior_[hidden_state, mixture_component] *= self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component]

                elif self.mixture_covariance_type == "tied":
                    self.mixture_degrees_of_freedom_posterior_[hidden_state] = counts.sum()
                    self.mixture_scale_posterior_[hidden_state] *= self.mixture_degrees_of_freedom_posterior_[hidden_state]
                else:
                    raise NotImplementedError("Covariance: {} is not implemented".format(self.mixture_covariance_type))
            else:
                raise ValueError("Invalid option for mixture_covariances_posterior:{}".format(self.mixture_means_posterior))
        # Not needed
        # self.mixture_covariances_posterior_ = np.copy(variances)

    def _update_after_fit(self, trainer):
        super(GMMVariationalHMM, self)._update_after_fit(trainer)
        self.mixture_weights_posterior_ = np.asarray(trainer.emissions.mixture_weights_posterior_)
        self.mixture_weights_normalized_ = np.asarray(trainer.emissions.mixture_weights_normalized_)
        self.mixture_means_posterior_ = np.asarray(trainer.emissions.mixture_means_posterior_)
        self.mixture_beta_posterior_ = np.asarray(trainer.emissions.mixture_beta_posterior_)
        self.mixture_degrees_of_freedom_posterior_ = np.asarray(trainer.emissions.mixture_degrees_of_freedom_posterior_)
        self.mixture_scale_posterior_ = np.asarray(trainer.emissions.W_k_inv_)
        self.mixture_precisions_posterior_ = np.asarray(trainer.emissions.mixture_precisions_posterior_)
        self.mixture_covariances_posterior_ = np.asarray(trainer.emissions.mixture_covariances_posterior_)

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(GMMVariationalHMM, self)._prepare_for_fit()

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_variational_gmm(
            self.pi_prior_,
            self.pi_posterior_,
            self.A_prior_,
            self.A_posterior_,
            mixture_weights_prior=self.mixture_weights_prior_,
            mixture_weights_posterior=self.mixture_weights_posterior_,
            mixture_means_prior=self.mixture_means_prior_,
            mixture_means_posterior=self.mixture_means_posterior_,
            mixture_beta_prior=self.mixture_beta_prior_,
            mixture_beta_posterior=self.mixture_beta_posterior_,
            mixture_degrees_of_freedom_prior=self.mixture_degrees_of_freedom_prior_,
            mixture_degrees_of_freedom_posterior=self.mixture_degrees_of_freedom_posterior_,
            mixture_scale_prior=self.mixture_scale_prior_,
            mixture_scale_posterior=self.mixture_scale_posterior_,
            mixture_covariance_type=self.mixture_covariance_type,
            impl=self.implementation,
            log_level=self.verbose
        )

    def _emissions_params(self):
        return 43

    def _emissions_to_dict(self):

        if hasattr(self, "mixture_means_prior_"):
            params = {}
            for field in ["mixture_weights_prior_", "mixture_weights_posterior_",
                          "mixture_means_prior_", "mixture_means_posterior_", "mixture_beta_prior_", "mixture_beta_posterior_", "mixture_degrees_of_freedom_prior_",
                          "mixture_degrees_of_freedom_posterior_", "mixture_scale_prior_", "mixture_scale_posterior_", "mixture_covariances_posterior_", "mixture_precisions_posterior_"]:
                params[field] = np.asarray(getattr(self, field)).tolist()
            return params
        return None

    def _emissions_from_dict(self, params):
        if params is None:
            return
        for field in ["mixture_weights_prior_", "mixture_weights_posterior_",
                      "mixture_means_prior_", "mixture_means_posterior_", "mixture_beta_prior_", "mixture_beta_posterior_", "mixture_degrees_of_freedom_prior_",
                      "mixture_degrees_of_freedom_posterior_", "mixture_scale_prior_", "mixture_scale_posterior_", "mixture_covariances_posterior_", "mixture_precisions_posterior_"]:
            setattr(self, field,  np.asarray(params[field]))
