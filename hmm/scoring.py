# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

import numpy as np

import sklearn.metrics

def perplexity(estimator, X, lengths):
    """
    Compute the perplexity of the data for the estimator

    Perplexity is defined as the nth root of 1/P(data)

    See https://web.stanford.edu/~jurafsky/slp3/3.pdf
    """
    probs = estimator.predict_proba(X, lengths)
    for i, p in enumerate(probs):
        probs[i] = p ** (-1/lengths[i])
    return probs


def cluster_report(labels, predicted):
    methods = [
        ("Mutual Info", sklearn.metrics.mutual_info_score),
        ("Norm Mutual Info", sklearn.metrics.normalized_mutual_info_score),
        ("Rand Score", sklearn.metrics.adjusted_rand_score),
        ("Completeness", sklearn.metrics.completeness_score),
        ("V-Measure", sklearn.metrics.v_measure_score),

    ]
    for name, method in methods:
        print("{}\t{:.3f}".format(name, method(labels, predicted)))
