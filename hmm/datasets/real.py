# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import csv
import os.path

import numpy as np
from sklearn.utils import Bunch


def load_old_faithful_long_short():
    """
    Binary representation of the duration of old faithful eruptions, from:
        Azzalini, A., and A. W. Bowman. “A Look at Some Data on the Old Faithful Geyser.” Journal of the Royal Statistical Society. Series C (Applied Statistics), vol. 39, no. 3, 1990, pp. 357–365. JSTOR, www.jstor.org/stable/2347385. Accessed 11 Dec. 2020.

    """
    module_path = os.path.dirname(__file__)
    data_file = os.path.join(module_path, "data", "old_faithful_long_short.csv")
    with open(data_file) as fd:
        data = np.asarray([int(i) for i in fd]).reshape(-1, 1)

    return Bunch(data=data, lengths=np.asarray([len(data)]), index=np.arange(data.shape[0]))


def _load_old_faithful_duration_waiting():
    """
    Originally From:
        Azzalini, A., and A. W. Bowman. “A Look at Some Data on the Old Faithful Geyser.” Journal of the Royal Statistical Society. Series C (Applied Statistics), vol. 39, no. 3, 1990, pp. 357–365. JSTOR, www.jstor.org/stable/2347385. Accessed 11 Dec. 2020.

    Also available from:
        https://github.com/vincentarelbundock/Rdatasets/blob/master/csv/MASS/geyser.csv
    """
    module_path = os.path.dirname(__file__)
    data_file = os.path.join(module_path, "data", "old_faithful_duration_waiting.csv")
    with open(data_file) as fd:
        reader = csv.DictReader(fd)
        data = [item for item in reader]
    return data


def load_old_faithful_duration():
    """
    Originally From:
        Azzalini, A., and A. W. Bowman. “A Look at Some Data on the Old Faithful Geyser.” Journal of the Royal Statistical Society. Series C (Applied Statistics), vol. 39, no. 3, 1990, pp. 357–365. JSTOR, www.jstor.org/stable/2347385. Accessed 11 Dec. 2020.

    Also available from:
        https://github.com/vincentarelbundock/Rdatasets/blob/master/csv/MASS/geyser.csv
    """
    data = _load_old_faithful_duration_waiting()
    index = [i["index"] for i in data]
    duration = np.asarray([float(i["duration"]) for i in data]).reshape(-1, 1)
    return Bunch(
        data=np.asarray(duration),
        lengths=np.asarray([duration.shape[0]]),
        index=np.asarray(index)
    )


def load_old_faithful_waiting():
    """
    Originally From:
        Azzalini, A., and A. W. Bowman. “A Look at Some Data on the Old Faithful Geyser.” Journal of the Royal Statistical Society. Series C (Applied Statistics), vol. 39, no. 3, 1990, pp. 357–365. JSTOR, www.jstor.org/stable/2347385. Accessed 11 Dec. 2020.

    Also available from:
        https://github.com/vincentarelbundock/Rdatasets/blob/master/csv/MASS/geyser.csv
    """
    data = _load_old_faithful_duration_waiting()
    index = [i["index"] for i in data]
    waiting = np.asarray([int(i["waiting"]) for i in data]).reshape(-1, 1)
    return Bunch(
        data=np.asarray(waiting),
        lengths=np.asarray([waiting.shape[0]]),
        index=np.asarray(index)
    )


def load_earthquakes():
    """
    Integer Counts of severe earthquakes per year, from:

        Zucchini, W., MacDonald, I., Langrock, R. (2016). Hidden Markov Models for Time Series. New York: Chapman and Hall/CRC, https://doi.org/10.1201/b20790

    and originally from the US Geological Survey.
    """
    module_path = os.path.dirname(__file__)
    data_file = os.path.join(module_path, "data", "earthquakes.csv")
    with open(data_file) as fd:
        data = np.asarray([int(i) for i in fd]).reshape(-1, 1)

    return Bunch(data=data, lengths=np.asarray([data.shape[0]]), index=np.arange(data.shape[0]) + 1900)


def load_seizures():
    """
    The seizures dataset provides the daily count of myoclonic seizures of a single patient over a period of 204 days.

        Zucchini, W., MacDonald, I., Langrock, R. (2016). Hidden Markov Models for Time Series. New York: Chapman and Hall/CRC, https://doi.org/10.1201/b20790
    module_path = os.path.dirname(__file__)
    data_file = os.path.join(module_path, "data", "seizures.csv")
    with open(data_file) as fd:
        data = np.asarray([int(i) for i in fd])
    """
    return Bunch(data=data, lengths=np.asarray([len(data)]), index=np.arange(data.shape[0]))
