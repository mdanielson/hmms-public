import numpy as np

import sklearn.utils

from .. import CategoricalHMM, GaussianHMM


def get_gaussian_mcgrory_and_titterington_model(random_state=None):
    """
    The model described in:
        Variational Bayesian Analysis for Hidden Markov
        Models
        by C.A. McGrory and D.M. Titterington

    """
    random_state = sklearn.utils.check_random_state(random_state)
    pi = np.asarray([.25] * 4)

    A = np.asarray([
        [.2, .2, .3, .3],
        [.3, .2, .2, .3],
        [.2, .3, .3, .2],
        [.3, .3, .2, .2]
    ])
    means = np.asarray([-1.5, 0, 1.5, 3])
    variances = np.asarray([.25**2]*4)
    model = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None)
    model.pi_ = pi
    model.A_ = A
    model.means_ = means
    model.variances_ = variances
    return model


def get_rabiner_juang_categorical_distance_models():
    """
    Retrieve the two categorical models described in Rabiner/Juang's paper: A probabilistic Distance Measure for Hidden Markov Models
    """
    A_0 = np.asarray([
        [0.8, 0.15, .05, 0],
        [0.07, 0.75, 0.12, 0.06],
        [0.05, 0.14, 0.8, 0.01],
        [0.001, 0.089, 0.11, 0.8],
    ])
    pi_0 = np.asarray([0.75, 0.15, 0.05, 0.05])

    B_0 = np.asarray([
        [0.3, 0.4, 0.2, 0.1],
        [0.5, 0.3, 0.1, 0.1],
        [0.1, 0.2, 0.4, 0.3],
        [0.4, 0.3, 0.1, 0.2]
    ])

    A_1 = np.asarray([
        [0.4, 0.25, 0.15, 0.2],
        [0.27, 0.45, 0.22, 0.06],
        [0.35, 0.14, 0.4, 0.11],
        [0.111, 0.119, 0.23, 0.54],
    ])
    pi_1 = np.asarray([
        0.4, 0.25, 0.15, 0.2
    ])

    B_1 = np.asarray([
        [0.1, 0.15, 0.65, 0.1, ],
        [0.2, 0.3, 0.4, 0.1],
        [0.3, 0.3, 0.1, 0.3],
        [0.15, 0.25, 0.4, 0.2]
    ])
    model_0 = CategoricalHMM.CategoricalHMM(init_pi=None, init_A=None, init_emissions=None)
    model_0.pi_ = pi_0
    model_0.A_ = A_0
    model_0.B_ = B_0

    model_1 = CategoricalHMM.CategoricalHMM(init_pi=None, init_A=None, init_emissions=None)
    model_1.pi_ = pi_1
    model_1.A_ = A_1
    model_1.B_ = B_1

    return model_0, model_1


def get_smyth_clustering_models(random_state=None):
    pi_1 = [.5, .5]
    A_1 = [[.6, .4],
           [.4, .6]]
    means_1 = [0, 3]
    var_1 = [1, 1]

    pi_2 = pi_1
    A_2 = [[.4, .6],
           [.6, .4]]
    means_2 = means_1
    var_2 = var_1
    model_1 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_1.pi_ = pi_1
    model_1.A_ = A_1
    model_1.means_ = means_1
    model_1.variances_ = var_1
    model_2 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_2.pi_ = pi_2
    model_2.A_ = A_2
    model_2.means_ = means_2
    model_2.variances_ = var_2

    return model_1, model_2


def get_beal_categorical(random_state=None):
    pi = np.ones(3)/3.
    category1 = CategoricalHMM.CategoricalHMM(random_state=random_state)
    category1.pi_ = pi
    category1.A_ = np.asarray([[0, 1, 0], [0, 0, 1], [1, 0, 0]])
    category1.B_= [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    category2 = CategoricalHMM.CategoricalHMM(random_state=random_state)
    category2.pi_ = pi
    category2.A_ = [[0, 0, 1], [1, 0, 0], [0, 1, 0]]
    category2.B_=[[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    category3 = CategoricalHMM.CategoricalHMM(random_state=random_state)
    category3.pi_ = [1]
    category3.A_ = [[1]]
    category3.B_=[[.5, .5]]

    return (category1, category2, category3)
