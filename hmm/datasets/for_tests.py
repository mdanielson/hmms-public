import numpy as np

import sklearn.utils

from . import models
from .. import CategoricalHMM, GaussianHMM, util


def get_gaussian_two_state_different_means(random_state=None):
    pi_1 = [.5, .5]
    A_1 = [[.6, .4],
           [.4, .6]]
    means_1 = [0, 3]
    var_1 = [1, 1]

    means_2 = [-3, 3]
    var_2 = [.5, .5]

    model_1 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_1.pi_ = pi_1
    model_1.A_ = A_1
    model_1.means_ = means_1
    model_1.variances_ = var_1
    model_2 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_2.pi_ = pi_1
    model_2.A_ = A_1
    model_2.means_ = means_2
    model_2.variances_ = var_2

    return model_1, model_2


def two_cluster_categorical_clustering(random_state=1983, n_samples=[100, 100], sample_lengths=200, test_size=.7):
    random_state = sklearn.utils.check_random_state(1983)

    pi_1 = [.5, .5]
    A_1 = [[.3, .7],
           [.7, .3]]
    B_1 = [
        [.3, .5, .2],
        [.1, .1, .8]
    ]

    pi_2 = pi_1
    A_2 = [[.7, .3],
           [.3, .7]]
    B_2 = B_1

    model_1 = CategoricalHMM.CategoricalHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_1.pi_ = pi_1
    model_1.A_ = A_1
    model_1.B_ = B_1
    model_2 = CategoricalHMM.CategoricalHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_2.pi_ = pi_2
    model_2.A_ = A_2
    model_2.B_ = B_2

    return util.sample_and_split([model_1, model_2], n_samples=n_samples, sample_lengths=sample_lengths, random_state=random_state)


def three_cluster_categorical_clustering(random_state=1983):
    random_state = sklearn.utils.check_random_state(random_state)

    pi_1 = [.3, .7]
    A_1 = [[.7, .3],
           [.3, .7]]
    B_1 = [
        [.3, .5, .2],
        [.1, .1, .8]
    ]

    pi_2 = pi_1
    A_2 = [[.7, .3],
           [.3, .7]]
    B_2 = B_1

    pi_3 = pi_1
    A_3 = [[.7, .3],
           [.3, .7]]
    B_3 = [[.7, .1, .2],
           [.4, .4, .2]]

    model_1 = CategoricalHMM.CategoricalHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_1.pi_ = pi_1
    model_1.A_ = A_1
    model_1.B_ = B_1
    model_2 = CategoricalHMM.CategoricalHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_2.pi_ = pi_2
    model_2.A_ = A_2
    model_2.B_ = B_2

    model_3 = CategoricalHMM.CategoricalHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_3.pi_ = pi_3
    model_3.A_ = A_3
    model_3.B_ = B_3

    return util.sample_and_split([model_1, model_2, model_3], random_state=random_state)


def three_cluster_gaussian(random_state=1983):
    random_state = sklearn.utils.check_random_state(random_state)

    pi_1 = [.5, .5]
    A_1 = [[.6, .4],
           [.4, .6]]
    means_1 = [0, 4]
    var_1 = [2, 1]

    pi_2 = pi_1
    A_2 = [[.4, .6],
           [.6, .4]]
    means_2 = means_1
    var_2 = var_1

    pi_3 = pi_1
    A_3 = [[.2, .8],
           [.8, .2]]
    means_3 = means_1
    var_3 = var_1

    model_1 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_1.pi_ = pi_1
    model_1.A_ = A_1
    model_1.means_ = means_1
    model_1.variances_ = var_1
    model_2 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_2.pi_ = pi_2
    model_2.A_ = A_2
    model_2.means_ = means_2
    model_2.variances_ = var_2

    model_3 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_3.pi_ = pi_3
    model_3.A_ = A_3
    model_3.means_ = means_3
    model_3.variances_ = var_3

    return util.sample_and_split([model_1, model_2, model_3], random_state=random_state, n_samples=100)


def get_smyth_data(random_state, n_samples=[40, 40], sample_lengths=200, test_size=.5):
    random_state = sklearn.utils.check_random_state(1983)
    model_1, model_2 = models.get_smyth_clustering_models(random_state)

    return util.sample_and_split([model_1, model_2], n_samples, sample_lengths=sample_lengths, random_state=random_state, test_size=test_size)


def get_smyth_extended(random_state):
    random_state = sklearn.utils.check_random_state(1983)

    model_1, model_2 = models.get_smyth_clustering_models(random_state)

    model_3 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_3.pi_ = model_1.pi_
    model_3.A_ = model_1.A_
    model_3.means_ = np.asarray(model_1.means_) + 1.5
    model_3.variances_ = model_1.variances_

    return util.sample_and_split([model_1, model_2, model_3], [60, 60, 60], random_state=random_state)


def get_perturbed_mcgrory_and_titterington(random_state):
    base_model = models.get_gaussian_mcgrory_and_titterington_model(random_state)
    models = []
    for i in range(base_model.A_.shape[0]):
        A = np.roll(base_model.A_, -i, 1)
        model = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None)
        model.pi_ = base_model.pi_
        model.A_ = A
        model.means_ = base_model.means_
        model.variances_ = base_model.variances_
        models.append(model)

    return models


def get_perturbed_mcgrory_and_titterington_data(random_state, n_samples=10, sample_lengths=500):
    models = get_perturbed_mcgrory_and_titterington(random_state)

    return util.sample_and_split(models, n_samples=n_samples, sample_lengths=sample_lengths, test_size=.5)


def get_music_analysis(random_state=None):
    model1 = CategoricalHMM.CategoricalHMM(init_pi=False, init_A=False, init_emissions=False)
    model1.pi_ = [.5, .5]
    model1.A_ = [[.8, .2], [.2, .8]]
    model1.B_ = [[.85, .1, .05],
                 [.1, .85, .05]]
    model2 = CategoricalHMM.CategoricalHMM(init_pi=False, init_A=False, init_emissions=False)
    model2.pi_ = [.5, .5]
    model2.A_ = [[.2, .8], [.8, .2]]
    model2.B_ = [[.05, .1, .85],
                 [.05, .85, .1]]

    model3 = CategoricalHMM.CategoricalHMM(init_pi=False, init_A=False, init_emissions=False)
    model3.pi_ = [1/3., 1/3., 1/3.]
    model3.A_ = [[1/3., 1/3., 1/3.],
                 [1/3., 1/3., 1/3.],
                 [1/3., 1/3., 1/3.]]

    model3.B_ = [[.9, .05, .05],
                 [.05, .9, .05],
                 [.05, .05, .9]]

    return model1, model2, model3


def get_gaussian_mcgrory_and_titterington_sequences(n_samples=10, sample_size=50, random_state=None):
    """
    The model described in:
        Variational Bayesian Analysis for Hidden Markov
        Models
        by C.A. McGrory and D.M. Titterington

    """
    model = models.get_gaussian_mcgrory_and_titterington_model(random_state)
    observed_sequences, hidden_sequences, lengths = model.sample(n_samples, sample_size, random_state=random_state)
    return observed_sequences, hidden_sequences, lengths


def make_random_0_1(length, random_state):
    s = []
    for i in range(length):
        if random_state.rand() <= .5:
            s.append([0])
        else:
            s.append([1])
    return s


def get_categorical_beal(random_state, num_each=7, min_length=15, max_length=40, unique_hidden_states=False):
    """
    Simple categorical model as described in Chapter 3 of Variational Algorithms for Approximate Bayesian In, By Matthew Beal
    """

    random_state = sklearn.utils.check_random_state(random_state)
    (category1, category2, category3) = models.get_beal_categorical(random_state)
    data = []
    lengths = []
    labels = []
    hiddens = []
    for i in range(num_each):
        for l, m in [(0, category1), (1, category2), (2, category3)]:
            length = random_state.randint(min_length, max_length)
            obs, hid, length = m.sample(1, length, random_state=random_state)
            data.append(obs)
            if unique_hidden_states:
                hid += 3*l
            hiddens.append(hid)
            lengths.append(length[0])
            labels.append(l)

    return np.concatenate(data), np.asarray(lengths), np.asarray(labels), np.concatenate(hiddens)
