# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import os.path


def configuration(parent_package='', top_path=None):
    if os.path.exists('MANIFEST'):
        os.remove('MANIFEST')

    from numpy.distutils.misc_util import Configuration

    config = Configuration("datasets", parent_package, top_path)

    config.add_data_dir('data')
    config.add_data_dir('descr')

    return config


if __name__ == "__main__":
    from numpy.distutils.core import setup
    # setup(**configuration(top_path='').todict())
    setup(configuration=configuration)
