import numpy as np
import scipy.linalg

import sklearn.utils
import sklearn.preprocessing
from . import GaussianVariationalHMM

class GaussianVariationalBlockHMM(GaussianVariationalHMM.GaussianVariationalHMM):
    def __init__(self, n_components=1, n_iterations=100, block_size=2, n_inits=1, n_jobs=-1, tol=1e-6, pi_prior=None, pi_posterior="sample",
                 A_prior=None, A_posterior="sample", means_prior="uninformed",
                 variances_prior="uninformed", implementation="scaling", allowed_to_use_log=True, random_state=None, verbose=0):
        super(GaussianVariationalBlockHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                                          n_jobs=n_jobs, tol=tol, pi_prior=pi_prior, pi_posterior=pi_posterior,
                                                          A_prior=A_prior, A_posterior=A_posterior, implementation=implementation,
                                                          allowed_to_use_log=allowed_to_use_log,
                                                          random_state=random_state, verbose=verbose)

        self.block_size = block_size
        if self.n_components % self.block_size != 0:
            raise ValueError("Invalid block_size for n_components: {} {}".format(self.block_size, n_components))

    def _init_A(self, X, lengths):
        rs = sklearn.utils.check_random_state(self.random_state)
        blocks = []
        n_blocks = self.n_components // self.block_size
        for i in range(n_blocks):
            blocks.append(
                sklearn.preprocessing.normalize(
                    rs.rand(self.block_size, self.block_size),
                    norm="l1",
                    axis=1)
            )

        self.A_prior_ = scipy.linalg.block_diag(*blocks)
        self.A_prior_[self.A_prior_==0] = 1e-9
        self.A_prior_ = sklearn.preprocessing.normalize(self.A_prior_, norm="l1", axis=1)
        self.A_posterior_ = self.A_prior_ * X.shape[0] * X.shape[1] / 4


if __name__ == "__main__":
    from hmm.datasets import for_tests
    import pandas as pd
    train_data_smyth, test_data_smyth, train_label, test_label = for_tests.get_smyth_data(None, n_samples=[40, 40], test_size=.5)
    vb_block = GaussianVariationalBlockHMM(block_size=2, n_components=4, n_iterations=500, n_inits=10, n_jobs=10, tol=1e-10)
    vb_block.fit(train_data_smyth)
    print(pd.DataFrame(vb_block.A_posterior_))
    print(pd.DataFrame(vb_block.means_posterior_))
    print(vb_block.dic(train_data_smyth))
    print(vb_block.lower_bound_[-1])
    print(vb_block.score(train_data_smyth))
    print(vb_block.aic(train_data_smyth))
    print(vb_block.bic(train_data_smyth))
