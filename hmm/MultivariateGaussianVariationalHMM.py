# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import sklearn.cluster
from sklearn.utils import check_array, check_random_state

from . import VariationalHMMBase, util, base
from . import MultivariateGaussianHMM
from ._hmm import new_variational_multivariate_gaussian, reestimate


class MultivariateGaussianVariationalHMM(VariationalHMMBase.VariationalHMMBase):
    """
    A Hidden Markov Model with Categorical Emissions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    """

    B_ = property(lambda x: x.B_normalized_)

    def __init__(self, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, pi_prior=None, pi_posterior="sample",
                 A_prior=None, A_posterior="sample",
                 means_prior=None, means_posterior="kmeans",
                 covariances_prior=None, covariances_posterior="kmeans",
                 covariance_type="full", implementation="scaling", random_state=None, verbose=0):
        super(MultivariateGaussianVariationalHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                                                 n_jobs=n_jobs, tol=tol, pi_prior=pi_prior, pi_posterior=pi_posterior,
                                                                 A_prior=A_prior, A_posterior=A_posterior,
                                                                 implementation=implementation, allowed_to_use_log=True, random_state=random_state, verbose=verbose)
        self.means_prior = means_prior
        self.means_posterior = means_posterior
        self.covariances_prior = covariances_prior
        self.covariances_posterior = covariances_posterior
        self.covariance_type = covariance_type

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, lengths, covariance_type="full", random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        MultivariateGaussianVariationalHMM
        """

        observed_states, lengths = util.check_arguments(observed_states, lengths)
        pi, A, means, covariances = reestimate.reestimate_multivariate_gaussian(observed_states, hidden_states, lengths, covariance_type=covariance_type)
        instance = cls(
            n_iterations=0,
            random_state=random_state,
            covariance_type=covariance_type
        )
        state_counts = np.bincount(hidden_states.ravel()).astype(float)
        instance._reestimate_with_prior(pi, A, state_counts, lengths)

        instance.means_prior_ = means
        instance.means_posterior_ = means.copy()

        instance.beta_prior_ = state_counts.copy()
        instance.beta_posterior_ = state_counts.copy()

        instance.scale_prior_ = covariances.copy()
        instance.scale_posterior_ = covariances.copy()
        instance.covariances_posterior_ = covariances.copy()
        instance.precisions_posterior_ = []
        if instance.covariance_type == "tied":
            instance.precisions_posterior_ = np.linalg.inv(instance.covariances_posterior_)
        else:
            for item in covariances:
                instance.precisions_posterior_.append(np.linalg.inv(item))
            instance.precisions_posterior_ = np.asarray(instance.precisions_posterior_)


        for i in range(state_counts.shape[0]):
            instance.scale_prior_[i] *= state_counts[i]
            instance.scale_posterior_[i] *= state_counts[i]

        if instance.covariance_type == "tied":
            instance.degrees_of_freedom_prior_ = state_counts.copy().sum()
            instance.degrees_of_freedom_posterior_ = state_counts.copy().sum()
        else:
            instance.degrees_of_freedom_prior_ = state_counts.copy()
            instance.degrees_of_freedom_posterior_ = state_counts.copy()
        return instance

    def _init_emissions(self, X, lengths):
        assert self.means_prior is None
        rs = check_random_state(self.random_state)
        self.n_variates_ = X.shape[-1]
        data_mean = X.mean(axis=0)
        d = X - data_mean

        overall_variance = np.dot(d.T, d) / d.shape[0]
        self.means_prior_ = np.zeros((self.n_components, self.n_variates_), dtype=float) + data_mean
        if isinstance(self.means_prior, tuple):
            # Format is (gamma, delta)
            self.means_prior_ = np.zeros((self.n_components,)) + self.means_prior[0]
            self.beta_prior_ = np.zeros((self.n_components,)) + self.means_prior[1]
        elif self.means_prior is None or self.means_prior == "uninformed":
            # similar to sklearn
            self.beta_prior_ = np.zeros(self.n_components) + 1
        else:
            raise ValueError("Unknown option for means prior: {}".format(self.means_prior))
        if isinstance(self.covariances_prior, tuple):
            self.degrees_of_freedom_prior_ = np.zeros((self.n_components), dtype=float) + self.covariances_prior[0]
            if self.covariance_type == "full":
                self.scale_prior_ = np.zeros((self.n_components, self.n_variates_, self.n_variates_), dtype=float) + self.covariances_prior[1]
            elif self.covariance_type == "tied":
                self.scale_prior_ = np.zeros((self.n_variates_, self.n_variates_), dtype=float) + self.covariances_prior[1]
            elif self.covariance_type == "diagonal":
                self.scale_prior_ = np.zeros((self.n_components, self.n_variates_, self.n_variates_), dtype=float) + self.covariances_prior[1]
            elif self.covariance_type == "spherical":
                self.scale_prior_ = np.zeros((self.n_components, self.n_variates_, self.n_variates_), dtype=float) + self.covariances_prior[1]
        else:
            if self.covariance_type == "full" or self.covariance_type == "diagonal" or self.covariance_type == "spherical":
                if self.covariances_prior is None or self.covariances_prior == "uninformed":
                    # From ~wpenny publication
                    self.degrees_of_freedom_prior_ = np.zeros((self.n_components), dtype=float) + self.n_variates_
                    self.scale_prior_ = np.zeros((self.n_components, self.n_variates_, self.n_variates_), dtype=float)
                    for i in range(self.n_components):
                        self.scale_prior_[i] = np.identity(self.n_variates_) * 1e-3
                else:
                    raise ValueError("Invalid option for variances_prior:{}".format(self.variances_prior))
            elif self.covariance_type == "tied":
                if self.covariances_prior is None or self.covariances_prior == "uninformed":
                    # From ~wpenny publication
                    self.degrees_of_freedom_prior_ = self.n_variates_
                    self.scale_prior_ = np.identity(self.n_variates_) * 1e-3
                else:
                    raise ValueError("Invalid option for variances_prior:{}".format(self.variances_prior))

            else:
                raise NotImplementedError("Covariance: {} is not implemented".format(self.covariance_type))

        # Posterior Estimates
        kmeans = sklearn.cluster.KMeans(n_clusters=self.n_components, random_state=rs)
        # Flatten data into 2D
        kmeans.fit(X)
        labels = kmeans.labels_
        responsibilities = np.zeros((X.shape[0], self.n_components))
        responsibilities[np.arange(X.shape[0]), labels] = 1.
        counts = responsibilities.sum(axis=0)
        (_, variances) = MultivariateGaussianHMM.estimate_initial_mean_and_variance(self.n_components, X, responsibilities, self.covariance_type)

        if self.means_posterior == "kmeans":
            self.means_posterior_ = kmeans.cluster_centers_
            self.beta_posterior_ = np.zeros(self.n_components, dtype=float) + counts
        else:
            raise ValueError("Invalid option for means_posterior:{}".format(self.means_posterior))

        if self.covariances_posterior == "kmeans":
            self.scale_posterior_ = np.copy(variances)
            if self.covariance_type == "full" or self.covariance_type == "diagonal" or self.covariance_type == "spherical":
                self.degrees_of_freedom_posterior_ = np.zeros(self.n_components, dtype=float) + counts
                for i in range(self.n_components):
                    self.scale_posterior_[i] *= self.degrees_of_freedom_posterior_[i]

            elif self.covariance_type == "tied":
                self.degrees_of_freedom_posterior_ = counts.sum()
                self.scale_posterior_ *= self.degrees_of_freedom_posterior_
            else:
                raise NotImplementedError("Covariance: {} is not implemented".format(self.covariance_type))
        else:
            raise ValueError("Invalid option for covariances_posterior:{}".format(self.means_posterior))

        self.covariances_posterior_ = np.copy(variances)

    def _update_after_fit(self, trainer):
        super(MultivariateGaussianVariationalHMM, self)._update_after_fit(trainer)
        self.means_posterior_ = np.asarray(trainer.emissions.means_posterior_)
        self.beta_posterior_ = np.asarray(trainer.emissions.beta_posterior_)
        self.degrees_of_freedom_posterior_ = np.asarray(trainer.emissions.degrees_of_freedom_posterior_)
        self.scale_posterior_ = np.asarray(trainer.emissions.W_k_inv_)
        self.precisions_posterior_ = np.asarray(trainer.emissions.precisions_posterior_)
        self.covariances_posterior_ = np.asarray(trainer.emissions.covariances_posterior_)

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(MultivariateGaussianVariationalHMM, self)._prepare_for_fit()

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_variational_multivariate_gaussian(
            self.pi_prior_,
            self.pi_posterior_,
            self.A_prior_,
            self.A_posterior_,
            means_prior=self.means_prior_,
            means_posterior=self.means_posterior_,
            beta_prior=self.beta_prior_,
            beta_posterior=self.beta_posterior_,
            degrees_of_freedom_prior=self.degrees_of_freedom_prior_,
            degrees_of_freedom_posterior=self.degrees_of_freedom_posterior_,
            scale_prior=self.scale_prior_,
            scale_posterior=self.scale_posterior_,
            covariance_type=self.covariance_type,
            impl=self.implementation,
            log_level=self.verbose
        )

    def _emissions_params(self):
        return 43

    def _emissions_to_dict(self):

        if hasattr(self, "means_prior_"):
            params = {}
            for field in ["means_prior_", "means_posterior_", "beta_prior_", "beta_posterior_", "degrees_of_freedom_prior_",
                          "degrees_of_freedom_posterior_", "scale_prior_", "scale_posterior_", "covariances_posterior_", "precisions_posterior_"]:
                params[field] = np.asarray(getattr(self, field)).tolist()
            return params
        return None

    def _emissions_from_dict(self, params):
        if params is None:
            return
        for field in ["means_prior_", "means_posterior_", "beta_prior_", "beta_posterior_", "degrees_of_freedom_prior_",
                      "degrees_of_freedom_posterior_", "scale_prior_", "scale_posterior_", "covariances_posterior_", "precisions_posterior_"]:
            setattr(self, field,  np.asarray(params[field]))
