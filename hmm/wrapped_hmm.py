# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import collections
import logging

import numpy as np
from sklearn.utils import check_random_state

from . import CategoricalHMM, logs


class WrappedHmm(object):
    """
    Provides an easier interface for discrete HMMs, such that states and observations can be represented with strings, but underneath we can use numpy arrays
    """

    def __init__(self, pi, A, B, tol=1e-2, implementation="scaling", log_level=logging.INFO):
        """
        Parameters
        ==========

        pi: dict
        A: dict
        B: dict
        log_level: log_level
        """
        assert isinstance(pi, dict)
        assert isinstance(A, dict)
        assert isinstance(B, dict)
        self.logger = logs.create_logger(__name__, log_level)
        self.implementation = implementation

        observation_map = collections.OrderedDict()
        hidden_state_map = collections.OrderedDict()
        for i, key in enumerate(pi):
            hidden_state_map[key] = i
            if key not in B:
                continue
            for state in B[key]:
                if state not in observation_map:
                    observation_map[state] = len(observation_map)

        self._observation_map = observation_map
        self._hidden_state_map = hidden_state_map
        self._robservation_map = collections.OrderedDict((v, k) for (k, v) in self._observation_map.items())
        self._rhidden_state_map = collections.OrderedDict((v, k) for (k, v) in self._hidden_state_map.items())

        # Translate matrices
        pi_ = np.asarray([pi[key] for key in self._hidden_state_map])
        A_ = np.zeros((len(self._hidden_state_map), len(self._hidden_state_map)))
        B_ = np.zeros((len(self._hidden_state_map), len(self._observation_map)))

        for key_i, i in self._hidden_state_map.items():
            for key_j, j in self._hidden_state_map.items():
                A_[i, j] = A[key_i][key_j]

            for key_j, j in self._observation_map.items():
                if key_i in B:
                    B_[i, j] = B[key_i][key_j]
        self._hmm = CategoricalHMM.CategoricalHMM(n_components=len(pi_), init_pi=None, init_A=None, init_emissions=None, tol=tol, implementation=self.implementation, verbose=log_level)
        self._hmm.pi_ = pi_
        self._hmm.A_ = A_
        self._hmm.B_ = B_

    @classmethod
    def new_from_names(cls, hidden_states, alphabet, log_level=logging.INFO, random_state=None):
        random_state = check_random_state(random_state)
        pi = {}
        A = collections.defaultdict(dict)
        B = collections.defaultdict(dict)
        for state in hidden_states:
            pi[state] = random_state.uniform(.4, .6)
            for other in hidden_states:
                A[state][other] = random_state.uniform(.4, .6)
            for observed in alphabet:
                B[state][observed] = random_state.uniform(.4, .6)

        return cls(pi, A, B, log_level=log_level)

    @property
    def pi(self):
        """
        Retrieve the Initial State Probability matrix
        """
        pi = {}
        for key_i, i in self._hidden_state_map.items():
            pi[key_i] = self._hmm.pi_[i]
        return pi

    @property
    def B(self):
        """
        Retrieve the Emission matrix
        """
        B = collections.defaultdict(dict)
        for key_i, i in self._hidden_state_map.items():
            for key_j, j in self._observation_map.items():
                B[key_i][key_j] = self._hmm.B_[i, j]
        return B

    @property
    def A(self):
        """
        Retrieve the Internal State Transition Probability matrix
        """
        A = collections.defaultdict(dict)
        for key_i, i in self._hidden_state_map.items():
            for key_j, j in self._hidden_state_map.items():
                A[key_i][key_j] = self._hmm.A_[i, j]
        return A

    def loglik(self, observed):
        """
        Compute the log probabilities of the set of observed sequences.

        Parameters
        ==========
        observed : list, each list is a list of observed states
        """
        translated, lengths = self._translate_observed(observed)
        return self._hmm.score_samples(translated, lengths)

    def likelihood(self, observed):
        """
        Compute the probabilities of the set of observed sequences.

        Parameters
        ==========
        observed : list, each list is a list of observed states
        """
        translated, lengths = self._translate_observed(observed)
        return np.exp(self._hmm.score_samples(translated, lengths))

    def _translate_observed(self, observations):
        """
        Translate a sequence from the user provided states to the internal integer states
        """
        translated = []
        lengths = []
        for i, sequence in enumerate(observations):
            lengths.append(len(sequence))
            for j, observation in enumerate(sequence):
                translated.append([self._observation_map[observation]])
        return translated, lengths

    def _rtranslate_observed(self, observations):
        """
        Translate a sequence from the internal integer states to the provided states
        """
        result = []
        for i, length in enumerate(lengths):
            one = []
            for j, observation in enumerate(sequence):
                one.append(
                    self._robservation_map[observation]
                )
            result.append(one)
        return result

    def _rtranslate_hidden(self, hidden_states):
        result = []
        for item in hidden_states:
            result.append(
                self._rhidden_state_map[item]
            )
        return result

    def viterbi(self, observed):
        """
        Find the "best" sequence of hidden states for a set of observed states

        Parameters
        ==========

        observed : list, each list is a list of observed states
        """
        translated, lengths = self._translate_observed(observed)
        optimal = self._hmm.transform(translated, lengths)
        return self._rtranslate_hidden(optimal)

    def train(self, observed, iterations=100):
        translated, lengths = self._translate_observed(observed)
        self._hmm.n_iterations = iterations
        return self._hmm.fit(translated, lengths)

    def sample(self, n_samples=1, length=10, random_state=None):
        observed, hidden = self._hmm.sample(n_samples=n_samples, length=length, random_state=random_state)
        observed = self._rtranslate_observed(observed)
        hidden = self._rtranslate_hidden(hidden)
        return observed, hidden
