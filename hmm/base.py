# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

from abc import ABC, abstractmethod
import datetime
import warnings

import numpy as np

import scipy.special

import sklearn.base

from sklearn.utils import check_array, check_random_state

from . import util

from . import model_selection

from .types import Any, ObservationType, LengthType


class CythonBase(ABC, sklearn.base.BaseEstimator):

    @abstractmethod
    def _new_trainer(self):
        pass

    def partial_fit(self, X: ObservationType, lengths: LengthType, n_iterations: int = 1):
        """
        Perform up to `n_iterations`training iterations.

        :param X: Observation sequences to learn a model from
        :param lengths: The length of each observation sequence in `X`
        :param n_iterations: The number of training iterations to perform.

        :rtype:
        """
        X, lengths = util.check_arguments(X, lengths)
        if not self._inited():
            self._init_for_fit(X, lengths)

        self._partial_fit(X, lengths, n_iterations)
        return self

    def fit(self, X: ObservationType, lengths: LengthType):
        """
        Train a model until either we have converged, or we reach the maximum number of training iterations

        :param X: Observation sequences to learn a model from
        :param lengths: The length of each observation sequence in `X`

        :rtype:
        """
        X, lengths = util.check_arguments(X, lengths)
        if self.n_iterations == 0:
            return self
        if self.n_inits == 1:
            self._fit_single(X, lengths)
        else:
            self._fit_parallel(X, lengths)
        self._is_converged()
        return self

    def _is_converged(self):
        pass

    @abstractmethod
    def _inited(self):
        pass

    @abstractmethod
    def _fit_single(self, X, lengths):
        pass

    @abstractmethod
    def _partial_fit(self, X, lengths, n_iterations=1):
        pass

    @abstractmethod
    def _fit_parallel(self, X, lengths):
        pass

    @abstractmethod
    def _assert_is_fitted(self):
        pass

    @abstractmethod
    def _prepare_for_fit(self):
        pass

    def transform(self, X: ObservationType, lengths: LengthType):
        """
        Transform the observations in  `X` into the corresponding
        sequence of Hidden States using the Viterbi algorithm

        :param X: Observation sequences to learn a model from
        :param lengths: The length of each observation sequence in `X`
        """
        X, lengths = util.check_arguments(X, lengths)
        self._assert_is_fitted()
        trainer = self._new_trainer()
        return trainer.viterbi(X, lengths)

    def score(self, X: ObservationType, lengths: LengthType):
        """
        Return the average of all log probabilities

        :param X: Observation sequences to learn a model from
        :param lengths: The length of each observation sequence in `X`
        """
        X, lengths = util.check_arguments(X, lengths)
        ans = self.score_samples(X, lengths).mean()
        return ans

    def score_samples(self, X: ObservationType, lengths: LengthType):
        """
        Return the loglikelihood of each sequence in `X`.

        :param X: Observation sequences to learn a model from
        :param lengths: The length of each observation sequence in `X`
        """

        X, lengths = util.check_arguments(X, lengths)
        self._assert_is_fitted()
        trainer = self._new_trainer()
        return trainer.loglik(X, lengths)

    @abstractmethod
    def _init_for_fit(self, X, lengths):
        raise NotImplementedError()

    @abstractmethod
    def n_parameters(self) -> int:
        pass

    @classmethod
    @abstractmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, lengths, random_state=None):
        raise NotImplementedError("Please imlement in derived")

    @abstractmethod
    def _update_after_fit(self, X):
        raise NotImplementedError()


class EMModelSelectionMixin(object):

    def aic(self, X: ObservationType, lengths: LengthType):
        """
        Compute The Akaike Information Criterion for the given dataset

        AIC = -2*logLike + 2 * n_parameters

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        float
        """
        loglik = self.score(X, lengths).mean() * len(lengths)
        return model_selection.akaike_information_criterion(loglik, sum(lengths), self.n_parameters())

    def bic(self, X: ObservationType, lengths: LengthType):
        """
        Compute The Bayesian Information Criterion for the given dataset

        BIC = -2*logLike + n_parameters * log(num_of_data)

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        float
        """
        loglik = self.score(X, lengths).mean() * len(lengths)
        return model_selection.bayesian_information_criterion(loglik, sum(lengths), self.n_parameters())

    def caic(self, X: ObservationType, lengths: LengthType):
        """
        Compute The Consistend Akaike Information Criterion for the given dataset

        CAIC = -2*logLike + n_parameters * (log(num_of_data) + 1)

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)

        Returns
        -------
        float
        """
        loglik = self.score(X, lengths).mean() * len(lengths)
        return model_selection.consistent_akaike_information_criterion(loglik, sum(lengths), self.n_parameters())


class ConfidenceIntervalMixin(object):
    def compute_confidence_intervals(self, n_samples: int = 1, length: int = 200, n_iterations: int = 500, ci: int = 90):
        """
        Compute Confidence Intervals for model parameters using Bootstrap Sampling.

        I. Visser, M. E. J. Raijmakers, and P. C. M. Molenaar, “Confidence intervals for hidden Markov model parameters,”
        British Journal of Mathematical and Statistical Psychology, vol. 53, no. 2, pp. 317–327, Nov. 2000.

        Parameters
        ----------
        n_samples : int, The number of samples to draw from each HMM
        length : int, The length of each sample to drawn
        n_iterations : int, The number of separate models to run
        ci : int, The confidence interval to report

        Returns
        -------
        dict : mapping of named parameters to confidence intervals
        """
        return self._compute_confidence_intervals(n_samples, length, n_iterations, ci)

    def _compute_confidence_intervals(self, n_samples: int, length: int, n_iterations: int, ci: int):
        raise NotImplementedError("Please imlement in derived")


class EMBase(EMModelSelectionMixin, CythonBase):

    def _is_converged(self):
        if len(self.loglikelihoods_) == self.n_iterations:
            warnings.warn("Training did not converge! Consider running for more iterations or choosing different starting values")

    def _fit_single(self, X: ObservationType, lengths: LengthType):
        """
        Train a single model.

        Parameters
        ----------
        X : array-like, shape (n_samples, length)
        """
        self._init_for_fit(X, lengths)
        self._partial_fit(X, lengths, self.n_iterations)
        return self

    def _partial_fit(self, X: ObservationType, lengths: LengthType, n_iterations: int):
        trainer = self._new_trainer()
        start = datetime.datetime.now()
        loglikelihoods = trainer.train(X, lengths, iterations=n_iterations, tol=self.tol)
        finished = datetime.datetime.now()
        self.elapsed_ = (finished - start) / datetime.timedelta(seconds=1)
        if hasattr(self, "loglikelihoods_"):
            self.loglikelihoods_.extend(loglikelihoods)
        else:
            self.loglikelihoods_ = loglikelihoods

        self._update_after_fit(trainer)
        return self

    def _fit_parallel(self, X: ObservationType, lengths: LengthType):
        return util.fit_parallel(self, X, lengths)

    def _inited(self):
        return hasattr(self, "pi_")


class VariationalBase(CythonBase):

    def _is_converged(self):
        if len(self.lower_bound_) == self.n_iterations:
            warnings.warn("Training did not converge! Consider running for more iterations or choosing different starting values")

    def _fit_parallel(self, X: ObservationType, lengths: LengthType):
        return util.fit_parallel_variational(self, X, lengths)

    def _fit_single(self, X: ObservationType, lengths: LengthType):
        """
        Train a single model.

        Parameters
        ----------
        X : array-like, shape (n_samples, length)
        """
        self._init_for_fit(X, lengths)
        self._partial_fit(X, lengths, self.n_iterations)
        return self

    def _partial_fit(self, X: ObservationType, lengths: LengthType, n_iterations: int):
        """
        Perform (upto) a fixed number of iterations of model training
        """
        trainer = self._new_trainer()
        start = datetime.datetime.now()
        lower_bound = trainer.train(X, lengths, iterations=n_iterations, tol=self.tol)
        finished = datetime.datetime.now()
        self.elapsed_ = (finished - start) / datetime.timedelta(seconds=1)

        if hasattr(self, "verbose_lower_bound_"):
            self.verbose_lower_bound_.extend(lower_bound)
        else:
            self.verbose_lower_bound_ = lower_bound

        self.lower_bound_ = [v["total"] for v in self.verbose_lower_bound_]
        self._update_after_fit(trainer)

    def _inited(self):
        return hasattr(self, "pi_posterior_")


class MHMMMixin(object):

    @abstractmethod
    def _to_dict(self):
        raise NotImplementedError()

    @abstractmethod
    def _from_dict(self, model_dict):
        raise NotImplementedError()

    @abstractmethod
    def _emissions_to_dict(self):
        raise NotImplementedError()

    @abstractmethod
    def _from_dict(self, model_dict):
        raise NotImplementedError()

    @abstractmethod
    def _init_pi(self, X, lengths):
        raise NotImplementedError()

    @abstractmethod
    def _init_A(self, X, lengths):
        raise NotImplementedError()

    @abstractmethod
    def _init_emissions(self, X):
        raise NotImplementedError()

    def _init_mixture_weights(self, X):
        raise NotImplementedError()

    def predict_log_proba(self, X: ObservationType, lengths: LengthType):
        """
        Predict the log probability of every sequence belonging to every mixture component.

        :param X: Observation sequences to learn a model from
        :param lengths: The length of each observation sequence in `X`

        :returns: log probabilities
        :rtype: np.ndarray of shape (n_sequences, n_mixture_components)
        """
        X, lengths = util.check_arguments(X, lengths)
        self._assert_is_fitted()
        trainer = self._new_trainer()
        logliks = trainer.mixture_loglik(X, lengths)
        log_prob_x = scipy.special.logsumexp(logliks, axis=1)
        return logliks - np.atleast_2d(log_prob_x).T

    def predict_proba(self, X: ObservationType, lengths: LengthType):
        """
        Predict log probability of every sequence belonging to every mixture component.

        :param X: Observation sequences to learn a model from
        :param lengths: The length of each observation sequence in `X`

        :returns: probabilities
        :rtype: np.ndarray of shape (n_sequences, n_mixture_components)
        """
        return np.exp(self.predict_log_proba(X, lengths))

    def predict(self, X: ObservationType, lengths: LengthType):
        """
        For each sequence, determine the mixture component most likely to have generated the sequence.

        :param X: Observation sequences to learn a model from
        :param lengths: The length of each observation sequence in `X`

        :returns: probabilities
        :rtype: np.ndarray of shape (n_sequences, n_mixture_components)
        """
        return self.predict_log_proba(X, lengths).argmax(axis=1)

    def sample(self, n_samples: int = 1, length: int = 10, random_state: Any = None):
        """
        Generate sample data from this model.

        :param n_samples: The number of sequences to generate
        :param length: The length of each generated sample
        :param random_state: An optional seed for the random state

        :returns: A tuple of (mixture_components, observations, hidden_sequences, lengths)
        :rtype: tuple
        """
        self._assert_is_fitted()
        # Coerces things to the correct types
        self._prepare_for_fit()

        if random_state is None:
            random_state = self.random_state

        random_state = check_random_state(random_state)
        trainer = self._new_trainer()
        mixture_components, observed_sequences, hidden_sequences, lengths = trainer.sample(n_samples, length, random_state=random_state)
        return np.asarray(mixture_components), np.asarray(observed_sequences), np.asarray(hidden_sequences), np.asarray(lengths)


class VariationalGaussianMixin(object):
    """
    Bring this back when/if we support it for other types of Gaussians
    """
    def verbose_dic(self, X: ObservationType, lengths: LengthType):

        trainer = self._new_trainer()
        X, lengths = util.check_arguments(X, lengths)
        pds, dics = trainer.deviance_information_criterion(X, lengths)
        dic = dics.sum()
        if np.isinf(dic):
            raise ValueError("DIC is not supported")
        return pds, dics

    def dic(self, X: ObservationType, lengths: LengthType):
        X, lengths = util.check_arguments(X, lengths)
        dic = self.verbose_dic(X, lengths)[1].sum()
        if np.isinf(dic):
            raise ValueError("DIC is not supported")
        return dic


class HMMMixin(object):
    @abstractmethod
    def _init_pi(self, X, lengths):
        raise NotImplementedError()

    @abstractmethod
    def _init_A(self, X, lengths):
        raise NotImplementedError()

    @abstractmethod
    def _init_emissions(self, X):
        raise NotImplementedError()

    def _init_for_fit(self, X, lengths):
        """
        Parameters
        ----------
        X : np.ndarray

        Initialize pi, A, and B.

        Christopher Bishop, Pattern Recognition and Machine Learning
            Page 617 - pi/A select random values subject to normalization and non-negativity

        Rabiner paper:
            Initialize pi/A to uniform
        """

        self._init_pi(X, lengths)
        self._init_A(X, lengths)
        self._init_emissions(X, lengths)
        self._prepare_for_fit()

    def sample(self, n_samples: int = 1, length: int = 10, random_state: Any = None):
        """
        Generate sample data from this model.

        :param n_samples: The number of sequences to generate
        :param length: The length of each generated sample
        :param random_state: An optional seed for the random state

        :returns: A tuple of (observations, hidden_sequences, lengths)
        :rtype: tuple
        """
        self._assert_is_fitted()
        # Coerces things to the correct types
        self._prepare_for_fit()

        if random_state is None:
            random_state = self.random_state

        random_state = check_random_state(random_state)
        trainer = self._new_trainer()
        observed_sequences, hidden_sequences, lengths = trainer.sample(n_samples, length, random_state=random_state)
        return np.asarray(observed_sequences), np.asarray(hidden_sequences), np.asarray(lengths)
