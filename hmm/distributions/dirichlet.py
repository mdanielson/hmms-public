
import numpy as np

from sklearn.utils import check_random_state

def setup_dirichlet_posterior(prior_value, n_samples, random_state):
    random_state = check_random_state(random_state)
    posterior = None

    posterior = np.zeros_like(prior_value)
    n_components = posterior.shape[0]
    for i in range(n_components):
        posterior[i] = random_state.dirichlet(prior_value[i]) * n_samples / n_components
    posterior += prior_value
    return posterior
