# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import sklearn.base
import sklearn.cluster
from sklearn.utils import check_array, check_random_state
from sklearn.utils.validation import check_is_fitted

from . import EMHMMBase, util
from ._hmm import new_gaussian_mixture_model, reestimate


class GMMHMM(EMHMMBase.EMHMMBase):
    """
    A Hidden Markov Model with GaussianMixtureModelEmissions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    init_pi : bool
    init_A : bool
    init_emissions : bool

    """

    def __init__(self, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, init_pi="random", init_A="random", init_emissions="kmeans",
                 mixture_n_components=2, covariance_regularization=1e-6, mixture_covariance_type="full", implementation="scaling", allowed_to_use_log=True, random_state=None, verbose=0):
        super(GMMHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                     n_jobs=n_jobs, tol=tol, init_pi=init_pi, init_A=init_A,
                                     implementation=implementation, allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.mixture_n_components = mixture_n_components
        self.init_emissions = init_emissions
        self.covariance_regularization = covariance_regularization
        self.mixture_covariance_type = mixture_covariance_type
        assert util.valid_covariance(self.mixture_covariance_type), "Unknown Covariance type: {}".format(self.mixture_covariance_type)

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, lengths, n_components, mixture_covariance_type="full", random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        GMMHMM
        """
        observed_states, lengths = util.check_arguments(observed_states, lengths)
        pi, A, mixture_weights, mixture_means, mixture_covariances = reestimate.reestimate_gmm(observed_states, hidden_states, lengths, n_components, mixture_covariance_type, random_state=random_state)
        instance = cls(
            n_iterations=0,
            init_pi=None,
            init_A=None,
            init_emissions=None,
            random_state=random_state,
            mixture_covariance_type=mixture_covariance_type
        )
        instance.n_components = pi.shape[0]
        instance.n_iterations = 0
        instance.pi_ = pi
        instance.A_ = A
        instance.mixture_n_components = mixture_means[0].shape[0]
        instance.mixture_weights_ = mixture_weights
        instance.mixture_means_ = mixture_means
        instance.mixture_covariances_ = mixture_covariances
        return instance

    def _init_emissions(self, X, lengths):
        """
        Parameters
        ----------
        X : np.ndarray

        Initialize B.

        Christopher Bishop, Pattern Recognition and Machine Learning
            Page 617 - pi/A select random values subject to normalization and non-negativity
            Also, Rabiner paper
        """
        rs = check_random_state(self.random_state)
        if self.init_emissions is None:
            pass
        elif self.init_emissions == "kmeans":
            kmeans = sklearn.cluster.KMeans(n_clusters=self.n_components, n_init=1, random_state=rs)
            X_ = X.reshape(-1, X.shape[-1])
            # Flatten data into 2D
            labels = kmeans.fit(X_).labels_
            mixture_means = []
            mixture_covariances = []
            mixture_weights = []
            for i in range(self.n_components):
                these = labels == i
                this_X = X_[these]
                kmeans2 = sklearn.cluster.KMeans(n_clusters=self.mixture_n_components, n_init=1, random_state=rs)
                these_labels = kmeans2.fit(this_X).labels_
                responsibilities = np.zeros((this_X.shape[0], self.mixture_n_components))
                responsibilities[np.arange(this_X.shape[0]), these_labels] = 1
                these_means = kmeans2.cluster_centers_
                n_variates = these_means[0].shape[0]

                if self.mixture_covariance_type == "full":
                    these_covariances = np.zeros((self.mixture_n_components, n_variates, n_variates)) + np.cov(this_X.T)
                elif self.mixture_covariance_type == "tied":
                    these_covariances = np.cov(this_X.T)
                    pass
                else:
                    assert False, self.mixture_covariance_type

                these_weights = np.bincount(these_labels)

                mixture_weights.append(these_weights / these_weights.sum())
                mixture_means.append(these_means)
                mixture_covariances.append(these_covariances)
            self.mixture_weights_ = np.asarray(mixture_weights)
            self.mixture_means_ = np.asarray(mixture_means)
            self.mixture_covariances_ = np.asarray(mixture_covariances)
        elif self.init_emissions == "random":
            raise NotImplementedError("TODO")
        else:
            if not isinstance(self.init_emissions, tuple) and not isinstance(self.init_emissions, list):
                raise ValueError("init_emissions parameter should be tuple or a list, got {}".format(type(self.init_emissions)))

            weights, means, variances = self.init_emissions
            self.mixture_weights_ = np.asarray(weights)
            self.mixture_means_ = np.asarray(means)
            self.mixture_covariances_ = np.asarray(mixture_covariances_)

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(GMMHMM, self)._prepare_for_fit()
        self.mixture_weights_ = np.asarray(self.mixture_weights_, dtype=float)
        self.mixture_means_ = np.asarray(self.mixture_means_, dtype=float)
        self.mixture_covariances_ = np.asarray(self.mixture_covariances_, dtype=float)
        assert self.A_.shape[0] == self.mixture_means_.shape[0]
        assert self.A_.shape[0] == self.mixture_covariances_.shape[0]

    def _assert_is_fitted(self):
        super(GMMHMM, self)._assert_is_fitted()
        check_is_fitted(self, ["mixture_weights_", "mixture_means_", "mixture_covariances_"])

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_gaussian_mixture_model(
            self.pi_,
            self.A_,
            self.mixture_weights_,
            self.mixture_means_,
            self.mixture_covariances_,
            variance_regularization=self.covariance_regularization,
            covariance_type=self.mixture_covariance_type,
            impl=self.implementation,
            allowed_to_use_log=self.allowed_to_use_log,
            log_level=self.verbose
        )

    def _update_after_fit(self, trainer):
        super()._update_after_fit(trainer)
        self.mixture_weights_ = np.asarray(trainer.emissions.weights_)
        self.mixture_means_ = np.asarray(trainer.emissions.means_)
        self.mixture_covariances_ = np.asarray(trainer.emissions.covariances_)

    def _emissions_params(self):
        """
        The number of free parameters in a Gaussian HMM
        """
        # mean / variance for each mixture component for each state
        weight_params = self.n_components * (self.mixture_n_components - 1)
        mean_params = self.n_components * self.mixture_n_components
        cov_params = 0
        if self.mixture_covariance_type == "full":
            n_variates = self.mixture_means_[0].shape[-1]
            cov_params = self.n_components * self.mixture_n_components * n_variates * (n_variates + 1) / 2
        elif self.mixture_covariance_type == "tied":
            cov_params = self.n_components * self.mixture_n_components * (n_variates + 1) / 2
        else:
            assert False, self.mixture_covariance_type
        B_params = weight_params + mean_params + cov_params

        return B_params

    def _compute_confidence_intervals(self, n_samples, length, n_iterations, ci):
        """
        Call implementation specific confidence interval code
        """
        raise NotImplementedError("compute_confidence_intervals")

    def _emissions_to_dict(self):
        mixture_weights = None
        mixture_means = None
        mixture_covariances = None
        if hasattr(self, "mixture_weights_"):
            mixture_weights = np.asarray(self.mixture_weights_).tolist()
        if hasattr(self, "mixture_means_"):
            mixture_means = np.asarray(self.mixture_means_).tolist()
        if hasattr(self, "mixture_covariances_"):
            mixture_covariances = np.asarray(self.mixture_covariances_).tolist()
        return mixture_weights, mixture_means, mixture_covariances

    def _emissions_from_dict(self, emissions):
        mixture_weights, mixture_means, mixture_covariances = emissions
        if mixture_weights is not None:
            self.mixture_weights_ = np.asarray(mixture_weights)
        if mixture_means is not None:
            self.mixture_means_ = np.asarray(mixture_means)
        if mixture_covariances is not None:
            self.mixture_covariances_ = np.asarray(mixture_covariances)
