# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging
import warnings

import numpy as np
import sklearn.cluster
from sklearn.utils import check_array

from . import VariationalHMMBase, util
from ._hmm import new_variational_poisson, reestimate
from .mixtures import PoissonMixtureModel


class PoissonVariationalHMM(VariationalHMMBase.VariationalHMMBase):
    """
    A Hidden Markov Model with Categorical Emissions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    """

    def __init__(self, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, pi_prior=None, pi_posterior="sample", A_prior=None, A_posterior="sample",
                 rates_prior=None, rates_posterior="kmeans", implementation="scaling", random_state=None, verbose=0):
        super(PoissonVariationalHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                                    n_jobs=n_jobs, tol=tol, pi_prior=pi_prior, pi_posterior=pi_posterior,
                                                    A_prior=A_prior, A_posterior=A_posterior, implementation=implementation,
                                                    allowed_to_use_log=True, random_state=random_state, verbose=verbose)
        self.rates_prior = rates_prior
        self.rates_posterior = rates_posterior

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, lengths, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        PoissonVariationalHMM
        """
        observed_states, lengths = util.check_arguments(observed_states, lengths)
        observed_states = observed_states.astype(int)
        pi, A, rates = reestimate.reestimate_poisson(observed_states, hidden_states, lengths)

        instance = cls(
            n_components=pi.shape[0],
            n_iterations=0,
            random_state=random_state,
            verbose=logging.INFO
        )

        state_counts = np.bincount(hidden_states.ravel()).astype(float)
        instance._reestimate_with_prior(pi, A, state_counts, lengths)

        instance.rates_prior_ = rates.copy()
        instance.rates_posterior_ = rates.copy()

        instance.a_prior_ = rates * state_counts
        instance.a_posterior_ = rates * state_counts

        instance.b_prior_ = state_counts.copy()
        instance.b_posterior_ = state_counts.copy()

        return instance

    def _init_emissions(self, X, lengths):
        """
        https://www.fil.ion.ucl.ac.uk/~wpenny/publications/

        # TODO: support better initialization

        Right now "data" initializes based on global data mean/variance, and "uninformed"
        initializes based upon a mean centered at 0 and a flat variance (a=b=1e-3)

        """

        pmm = sklearn.cluster.KMeans(n_clusters=self.n_components, random_state=self.random_state)
        data = np.asarray(X).ravel()[:, None]
        pmm.fit(data)

        assignments = pmm.predict(data)
        #means = pmm.means_.ravel()
        rates = pmm.cluster_centers_.ravel()

        counts = np.zeros(rates.shape[0])
        for i in sorted(set(assignments)):
            these = assignments == i
            counts[i] = these.sum()
        if self.rates_prior is None or self.rates_prior == "uninformed":
            self.a_prior_ = np.zeros(self.n_components, dtype=float) + data.mean()
            self.b_prior_ = np.zeros_like(self.a_prior_, dtype=float) + 1
        elif isinstance(self.rates_prior, tuple):
            a, b = self.rates_prior
            self.a_prior_ = np.zeros(self.n_components, dtype=float) + a
            self.b_prior_ = np.zeros_like(self.a_prior_) + b
        else:
            assert False, "Not an option: {}".format(self.rates_prior)

        if self.rates_posterior == "kmeans":
            self.a_posterior_ = np.zeros(self.n_components, dtype=float) + rates * counts
            self.b_posterior_ = np.zeros_like(self.a_prior_) + counts
        else:
            raise ValueError("Invalid option for rates_posterior:{}".format(self.rates_posterior))
        self.rates_prior_ = self.a_prior_ / self.b_prior_
        self.rates_posterior_ = self.a_posterior_ / self.b_posterior_

    def _update_after_fit(self, trainer):
        super(PoissonVariationalHMM, self)._update_after_fit(trainer)
        self.rates_posterior_ = np.asarray(trainer.emissions._rates_posterior)
        self.a_posterior_ = np.asarray(trainer.emissions._a_posterior)
        self.b_posterior_ = np.asarray(trainer.emissions._b_posterior)

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(PoissonVariationalHMM, self)._prepare_for_fit()

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_variational_poisson(
                self.pi_prior_,
                self.pi_posterior_,
                self.A_prior_,
                self.A_posterior_,
                rates_prior=self.rates_prior_,
                rates_posterior=self.rates_posterior_,
                a_prior=self.a_prior_,
                a_posterior=self.a_posterior_,
                b_prior=self.b_prior_,
                b_posterior=self.b_posterior_,
                impl=self.implementation,
                log_level=self.verbose
            )

    def _emissions_params(self):
        return 2

    def _emissions_to_dict(self):

        if hasattr(self, "rates_prior_"):
            params = {}
            for field in ["rates_prior_", "rates_posterior_", "a_prior_", "a_posterior_",  "b_prior_", "b_posterior_", ]:
                params[field] = np.asarray(getattr(self, field)).tolist()
            return params
        return None

    def _emissions_from_dict(self, params):
        if params is None:
            return
        for field in ["rates_prior_", "rates_posterior_", "a_prior_", "a_posterior_", "b_prior_", "b_posterior_", ]:
            setattr(self, field,  np.asarray(params[field]))
