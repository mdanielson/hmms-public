# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import copy
import logging

from . import generic, mixture
from .emissions import (categorical, gaussian, gaussian_mixture_model,
                        multivariate_gaussian, poisson)


def _get_scaling(prefer_scaling, impl):
    if impl == "scaling":
        return True
    elif impl == "log":
        return False
    return prefer_scaling


def new_poisson(pi, A, rates, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    return generic.HMMTrainer(
        pi,
        A,
        poisson.PoissonEmissions(
            rates,
            log_level=log_level
        ),
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )


def new_gaussian(pi, A, means, variances, variance_regularization, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    return generic.HMMTrainer(
        pi,
        A,
        gaussian.GaussianEmissions(
            means,
            variances,
            variance_regularization=variance_regularization,
            log_level=log_level
        ),
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )


def new_multivariate_gaussian(pi, A, means, variances, variance_regularization, covariance_type, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    emissions = None
    if covariance_type == "full":
        emissions = multivariate_gaussian.FullGaussianEmissions(
            means,
            variances,
            variance_regularization=variance_regularization,
            log_level=log_level
        )
    elif covariance_type == "tied":
        emissions = multivariate_gaussian.TiedGaussianEmissions(
            means,
            variances,
            variance_regularization=variance_regularization,
            log_level=log_level
        )
    elif covariance_type == "diagonal":
        emissions = multivariate_gaussian.DiagonalGaussianEmissions(
            means,
            variances,
            variance_regularization=variance_regularization,
            log_level=log_level
        )
    elif covariance_type == "spherical":
        emissions = multivariate_gaussian.SphericalGaussianEmissions(
            means,
            variances,
            variance_regularization=variance_regularization,
            log_level=log_level
        )
    else:
        assert False, covariance_type
    return generic.HMMTrainer(
        pi,
        A,
        emissions=emissions,
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )


def new_gaussian_mixture_model(pi, A, weights, means, variances, variance_regularization, covariance_type, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    emissions = None
    if covariance_type == "full":
        emissions = gaussian_mixture_model.FullGaussianMixtureModel(
            weights,
            means,
            variances,
            variance_regularization=variance_regularization,
            log_level=log_level
        )
    elif covariance_type == "tied":
        emissions = gaussian_mixture_model.TiedGaussianMixtureModel(
            weights,
            means,
            variances,
            variance_regularization=variance_regularization,
            log_level=log_level
        )
    else:
        assert False, covariance_type
    return generic.HMMTrainer(
        pi,
        A,
        emissions=emissions,
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )


def new_categorical(pi, A, B, smoothing=None, smoothing_params=None, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    smoother = None
    if smoothing is None:
        smoother = categorical.NoSmoothing(B.shape[1])
    elif smoothing == "laplace":
        smoother = categorical.LaplaceSmoothing(B.shape[1])
    elif smoothing == "AddK":
        smoother = categorical.AddK(B.shape[1], smoothing_params[0])
    else:
        raise ValueError("Unknown Smoothing: {}".format(smoothing))

    return generic.HMMTrainer(
        pi,
        A,
        categorical.CategoricalEmissions(
            B,
            smoother=smoother,
            log_level=log_level
        ),
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )


def new_mixture_categorical(mixture_weights, pi, A, B, smoothing=None, smoothing_params=None, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    smoother = None
    if smoothing is None:
        smoother = categorical.NoSmoothing(B.shape[-1])
    elif smoothing == "laplace":
        smoother = categorical.LaplaceSmoothing(B.shape[-1])
    elif smoothing == "AddK":
        smoother = categorical.AddK(B.shape[1], smoothing_params[0])
    else:
        raise ValueError("Unknown Smoothing: {}".format(smoothing))

    emissions = []
    for i in range(B.shape[0]):
        emissions.append(
            categorical.CategoricalEmissions(
                B[i].copy(),
                smoother=copy.deepcopy(smoother),
                log_level=log_level
            )
        )
    return mixture.HMMTrainer(
        mixture_weights,
        pi,
        A,
        emissions,
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )


def new_mixture_gaussian(mixture_weights, pi, A, means, variances, variance_regularization, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    emissions = []
    for i in range(A.shape[0]):
        emissions.append(
            gaussian.GaussianEmissions(
                means[i],
                variances[i],
                log_level=log_level
            )
        )

    return mixture.HMMTrainer(
        mixture_weights,
        pi,
        A,
        emissions,
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )
