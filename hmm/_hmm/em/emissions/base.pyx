# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import sklearn.utils

cimport cython
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from libc.math cimport INFINITY

from hmm import logs

cdef class EmissionsBase:

    def __init__(self, Py_ssize_t N, log_level=logging.ERROR):
        self._N = N
        self._logger = logs.create_logger(__name__, log_level)

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef void _m_step(self) :
        raise ValueError("Not implemented")

    @cython.initializedcheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)

    cdef void _accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight) nogil:
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        raise ValueError("Not implemented")

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        return -INFINITY


    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        return -INFINITY

    cdef list _sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        random_state :  np.random_state
        """
        raise ValueError("Not implemented")

    def __str__(self):
        return "Not Implemented"

    cdef _log_state(self):
        raise ValueError("Not implemented")
