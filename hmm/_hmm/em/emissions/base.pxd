
# cython: language_level=3

cimport cython
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t

cdef class EmissionsBase:
    cdef Py_ssize_t _N
    cdef object _logger

    cdef void _m_step(self)
    cdef void _accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight ) nogil
    cpdef DOUBLE_DTYPE_t density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state)
    cpdef DOUBLE_DTYPE_t log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state)
    cdef list _sample(self, Py_ssize_t current_state, object random_state)
    cdef _log_state(self)
