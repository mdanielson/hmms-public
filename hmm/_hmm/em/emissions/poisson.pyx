# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3

import logging
import warnings
import numpy as np

cimport cython
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from libc.math cimport exp

from .base cimport EmissionsBase

cdef class PoissonEmissions(EmissionsBase):
    """

    """
    cdef public DOUBLE_DTYPE_t[:] rates_

    cdef DOUBLE_DTYPE_t[:] new_rates_numerator_
    cdef DOUBLE_DTYPE_t[:] new_rates_denominator_

    cdef DOUBLE_DTYPE_t[:] mean_exp_

    def __init__(self, DOUBLE_DTYPE_t[:] rates, log_level=logging.ERROR):
        super(PoissonEmissions, self).__init__(rates.shape[0], log_level)

        self.rates_ = rates
        self.mean_exp_ = np.zeros_like(rates)

        self._init_distributions()
        self.new_rates_numerator_ = np.zeros_like(rates)
        self.new_rates_denominator_ = np.zeros_like(rates)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _init_distributions(self):
        cdef:
            Py_ssize_t i

        for i in range(self._N):
            self.mean_exp_[i] = exp(-self.rates_[i])


    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _m_step(self):
        cdef:
            Py_ssize_t i

        for i in range(self._N):
            if self.new_rates_denominator_[i] > 0:
                self.rates_[i] = self.new_rates_numerator_[i] / self.new_rates_denominator_[i]
            else:
                warnings.warn("Zero denominator...[{}]".format(i))
                self.rates_[i] = 0

        self.new_rates_numerator_[:] = 0
        self.new_rates_denominator_[:] = 0
        self._init_distributions()

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef void _accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight) nogil:
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t i, t

        for t in range(observed.shape[0]):
            for i in range(self._N):
                self.new_rates_denominator_[i] += weight * digamma_1[t, i]
                self.new_rates_numerator_[i] += weight * digamma_1[t, i] * observed[t, 0]

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            DOUBLE_DTYPE_t d = self.mean_exp_[hidden_state]
            DOUBLE_DTYPE_t mean = self.rates_[hidden_state]
            Py_ssize_t j, i
        assert observed_state.shape[0] == 1

        j = <Py_ssize_t> observed_state[0] + 1
        for i in range(1, j):
            d *= mean
            d /= i
        return d

    cdef list _sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        random_state :  np.random_state
        """
        return [random_state.poisson(self.rates_[hidden_state])]

    def __str__(self):
        return str(np.asarray(self._B).tolist())

    cdef _log_state(self):
        cdef:
            Py_ssize_t i

        for i in range(self._N):
            self._logger.info("B[{}] rate={}".format(i, self.rates_[i]))
