# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import warnings
import numpy as np

cimport cython
cimport numpy as np
from libc.math cimport exp, log, sqrt, M_PI
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE
from hmm._hmm.util cimport multivariate_normal_density, log_multivariate_normal_density
from ..base cimport EmissionsBase

cdef class FullGaussianEmissions(EmissionsBase):
    """

    """
    cdef DOUBLE_DTYPE_t[:, :] _means
    cdef DOUBLE_DTYPE_t[:, :, :] _covariances
    cdef DOUBLE_DTYPE_t[:, :, :] _precisions

    cdef DOUBLE_DTYPE_t[:, :] _new_means_numerator
    cdef DOUBLE_DTYPE_t[:] _new_means_denominator

    cdef DOUBLE_DTYPE_t[:, :, :] _new_variance_obs

    cdef DOUBLE_DTYPE_t[:] _two_pi_k_det
    cdef DOUBLE_DTYPE_t[:] _log_two_pi_k_det

    cdef DOUBLE_DTYPE_t _variance_regularization

    cdef Py_ssize_t _n_variates
    cdef DOUBLE_DTYPE_t[:] _density_tmp

    def __init__(self, DOUBLE_DTYPE_t[:, :] means, DOUBLE_DTYPE_t[:, :, :] covariances, variance_regularization=1e-6, log_level=logging.ERROR):
        super(FullGaussianEmissions, self).__init__(means.shape[0], log_level)
        self._means = means
        self._n_variates = means[0].shape[0]
        self._covariances = covariances
        self._precisions = np.zeros_like(covariances)
        self._two_pi_k_det = np.zeros((self._N, ), dtype=DOUBLE_DTYPE)
        self._log_two_pi_k_det = np.zeros((self._N, ), dtype=DOUBLE_DTYPE)
        self._variance_regularization = variance_regularization

        self._new_means_numerator = np.zeros((self._N, self._n_variates), dtype=DOUBLE_DTYPE)
        self._new_means_denominator = np.zeros(self._N, dtype=DOUBLE_DTYPE)
        self._new_variance_obs = np.zeros((self._N, self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)
        self._density_tmp = np.zeros(self._n_variates)
        self._init_distributions()

    @property
    def means(self):
        return self._means

    @property
    def covariances(self):
        return self._covariances

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _init_distributions(self):
        cdef:
            Py_ssize_t i, j, k
            DOUBLE_DTYPE_t[:, :] tmp

        for i in range(self._N):
            tmp = np.linalg.inv(self._covariances[i])
            for j in range(self._n_variates):
                for k in range(self._n_variates):
                    self._precisions[i, j, k] = tmp[j, k]

            self._two_pi_k_det[i] = sqrt((2.0 * M_PI) ** self._n_variates * np.linalg.det(self._covariances[i]))
            self._log_two_pi_k_det[i] = log(self._two_pi_k_det[i])

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    cdef void _m_step(self):
        cdef:
            Py_ssize_t i, j, k
        self._covariances[:] = 0
        for i in range(self._N):
            for j in range(self._n_variates):
                self._means[i][j] = self._new_means_numerator[i, j] / self._new_means_denominator[i]

        for i in range(self._N):
            for j in range(self._n_variates):
                for k in range(self._n_variates):
                    self._covariances[i, j, k] =  self._new_variance_obs[i, j, k] \
                            - (self._new_means_numerator[i, j] * self._means[i, k]) \
                            - (self._new_means_numerator[i, k] * self._means[i, j]) \
                            + (self._new_means_denominator[i] * self._means[i,j] * self._means[i, k])
            for j in range(self._n_variates):
                for k in range(self._n_variates):
                    self._covariances[i, j, k] = self._covariances[i, j, k] / self._new_means_denominator[i]
                    # To make invertifble
                    if j == k:
                        self._covariances[i, j, k] += self._variance_regularization

        self._new_means_numerator[:] = 0
        self._new_means_denominator[:] = 0
        self._new_variance_obs[:] = 0
        self._init_distributions()

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    cdef void _accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight) nogil:
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t i, t, l, m

        for t in range(observed.shape[0]):
            for i in range(self._N):
                self._new_means_denominator[i] += weight * digamma_1[t, i]
                for l in range(self._n_variates):
                    self._new_means_numerator[i, l] += weight * digamma_1[t, i] * observed[t, l]

                for l in range(self._n_variates):
                    for m in range(self._n_variates):
                        self._new_variance_obs[i, l, m] += weight * digamma_1[t, i] * observed[t, l] * observed[t, m]

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef DOUBLE_DTYPE_t density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        return multivariate_normal_density(
            observed_state,
            self._means[hidden_state],
            self._precisions[hidden_state],
            self._two_pi_k_det[hidden_state],
            self._density_tmp
        )

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef DOUBLE_DTYPE_t log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        return log_multivariate_normal_density(
            observed_state,
            self._means[hidden_state],
            self._precisions[hidden_state],
            self._log_two_pi_k_det[hidden_state],
            self._density_tmp
        )

    cdef list _sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        random_state :  np.random_state
        """
        return random_state.multivariate_normal(self._means[hidden_state], self._covariances[hidden_state]).tolist()

    def __str__(self):
        return "Not Implemented"

    cdef _log_state(self):
        cdef:
            Py_ssize_t i

        for i in range(self._N):
            self._logger.info("emissions[{}] mean={} var={}".format(i, np.asarray(self._means[i]), np.asarray(self._covariances[i])))
