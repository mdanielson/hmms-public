# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import warnings
import numpy as np

cimport cython
cimport numpy as np
from libc.math cimport exp, log, sqrt, M_PI, isinf
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE
from hmm.utilities.log_routines cimport _logsumexp
from .base cimport EmissionsBase
from hmm._hmm.util cimport multivariate_normal_density, log_multivariate_normal_density


cdef DOUBLE_DTYPE_t min_density = np.finfo(np.float).eps


cdef class FullGaussianMixtureModel(EmissionsBase):
    """

    """
    #[N_states, n_componets]
    cdef public DOUBLE_DTYPE_t[:, :] weights_
    cdef public DOUBLE_DTYPE_t[:, :] log_weights_

    #[N states, n_componens, N_Variates]
    cdef public DOUBLE_DTYPE_t[:, :, :] means_

    #[N States, n_componens, N_Covariances, N_Variates]
    cdef public DOUBLE_DTYPE_t[:, :, :, :] covariances_
    cdef public DOUBLE_DTYPE_t[:, :, :, :] precisions_

    cdef DOUBLE_DTYPE_t[:, :] weights_reestimate_
    cdef DOUBLE_DTYPE_t[:, :, :] new_means_numerator_
    #cdef DOUBLE_DTYPE_t[:, :] new_means_denominator_

    cdef DOUBLE_DTYPE_t[:, :, :, :] new_variances_obs_T

    cdef DOUBLE_DTYPE_t[:, :] two_pi_k_det_
    cdef DOUBLE_DTYPE_t[:, :] log_two_pi_k_det_

    cdef public DOUBLE_DTYPE_t variance_regularization
    cdef public str covariance_type
    cdef Py_ssize_t _n_components
    cdef Py_ssize_t _n_variates

    # Temporary buffers for density estimation and accumulating statistics
    cdef DOUBLE_DTYPE_t[:] tmp_n_variates
    cdef DOUBLE_DTYPE_t[:] tmp_n_components


    def __init__(self, DOUBLE_DTYPE_t[:, :] weights, DOUBLE_DTYPE_t[:, :, :] means, DOUBLE_DTYPE_t[:, :, :, :] covariances, variance_regularization=1e-6, log_level=logging.ERROR):
        super(FullGaussianMixtureModel, self).__init__(means.shape[0], log_level)
        self.weights_ = weights
        self.log_weights_ = np.zeros_like(weights)
        self.means_ = means
        self._n_components = means.shape[1]
        self._n_variates = means.shape[2]
        self.covariances_ = covariances
        self.precisions_ = np.zeros_like(covariances)
        self.two_pi_k_det_ = np.zeros((self._N, self._n_components), dtype=DOUBLE_DTYPE)
        self.log_two_pi_k_det_ = np.zeros((self._N, self._n_components), dtype=DOUBLE_DTYPE)
        self.variance_regularization = variance_regularization

        self.weights_reestimate_ = np.zeros((self._N, self._n_components), dtype=DOUBLE_DTYPE)
        self.new_means_numerator_ = np.zeros((self._N, self._n_components, self._n_variates), dtype=DOUBLE_DTYPE)
        #self.new_means_denominator_ = np.zeros((self._N, self._n_components), dtype=DOUBLE_DTYPE)
        self.new_variances_obs_T = np.zeros((self._N, self._n_components, self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)

        self.tmp_n_variates = np.zeros(self._n_variates)
        self.tmp_n_components = np.zeros(self._n_components)
        self._init_distributions()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef _init_distributions(self):
        cdef:
            Py_ssize_t state, component, i, j
            DOUBLE_DTYPE_t[:, :] tmp

        for state in range(self._N):
            for component in range(self._n_components):
                self.log_weights_[state, component] = log(self.weights_[state, component])
                tmp = np.linalg.inv(self.covariances_[state][component])
                for i in range(self._n_variates):
                    for j in range(self._n_variates):
                        self.precisions_[state, component, i, j] = tmp[i, j]

                self.two_pi_k_det_[state, component] = sqrt((2.0 * M_PI) ** self._n_variates * np.linalg.det(self.covariances_[state, component]))
                self.log_two_pi_k_det_[state, component] = log(self.two_pi_k_det_[state, component])
                #self._logger.info(np.asarray(self.covariances_[state, component]))
                #self._logger.info("{} {} {} {}".format(state, component, self.two_pi_k_det_[state,component],np.linalg.det(self.covariances_[state, component])))

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    cdef void _m_step(self):
        cdef:
            Py_ssize_t hidden_state, component, k, l
            DOUBLE_DTYPE_t weights_denominator
        self.covariances_[:] = 0
        self.weights_[:] = 0
        # #self.variance_regularization
        for hidden_state in range(self._N):

            # Adjust weights
            weights_denominator = min_density
            for component in range(self._n_components):
                weights_denominator += self.weights_reestimate_[hidden_state, component]

            # adjust means
            for component in range(self._n_components):
                self.weights_[hidden_state, component] = self.weights_reestimate_[hidden_state, component] / weights_denominator
                for k in range(self._n_variates):
                    self.means_[hidden_state][component][k] = self.new_means_numerator_[hidden_state, component, k] / self.weights_reestimate_[hidden_state, component]

        # Update Covariances
        for hidden_state in range(self._N):
            for component in range(self._n_components):
                #self._logger.info("reestimate {} {} ".format(hidden_state, component))
                #self._logger.info(np.asarray(self.new_variances_obs_T[hidden_state, component]))
                for k in range(self._n_variates):
                    for l in range(self._n_variates):
                        self.covariances_[hidden_state, component, k, l] =  self.new_variances_obs_T[hidden_state, component, k, l] \
                                - (self.new_means_numerator_[hidden_state, component, k] * self.means_[hidden_state, component, l]) \
                                - (self.new_means_numerator_[hidden_state, component, l] * self.means_[hidden_state, component, k]) \
                                + (self.weights_reestimate_[hidden_state, component] * self.means_[hidden_state, component, k] * self.means_[hidden_state, component, l])

                        self.covariances_[hidden_state, component, k, l] /= self.weights_reestimate_[hidden_state, component]

                        # To make invertifble
                        if k == l:
                            self.covariances_[hidden_state, component, k, l] += self.variance_regularization

        self.weights_reestimate_[:] = 0
        self.new_means_numerator_[:] = 0
        #self.new_means_denominator_[:] = 0
        self.new_variances_obs_T[:] = 0
        self._init_distributions()

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    cdef void _accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight) nogil:
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :]
        observed : DOUBLE_DTYPE_t[:, :]

        """
        cdef:
            Py_ssize_t hidden_state, component, l, m, t
            DOUBLE_DTYPE_t gamma, gamma_denom
        for t in range(observed.shape[0]):
            for hidden_state in range(self._N):
                # Need the normalized density that this observation came from each component
                gamma_denom = 0 # self.density_for(hidden_state, observed[t])
                self.individual_component_density_for(hidden_state, observed[t], self.tmp_n_components)
                for component in range(self._n_components):
                    self.tmp_n_components[component] *= self.weights_[hidden_state, component]
                    gamma_denom += self.tmp_n_components[component]
                for component in range(self._n_components):
                    #  Now we have the relative proportion that the observation came from this component
                    if gamma_denom > 0:
                        gamma = digamma_1[t, hidden_state] * self.tmp_n_components[component] / gamma_denom
                    else:
                        gamma = 0

                    self.weights_reestimate_[hidden_state, component] += weight * gamma
                    #self.new_means_denominator_[hidden_state, component] += gamma
                    for l in range(self._n_variates):
                        self.new_means_numerator_[hidden_state, component, l] += weight * gamma * observed[t, l]

                    for l in range(self._n_variates):
                        for m in range(self._n_variates):
                            self.new_variances_obs_T[hidden_state, component, l, m] += weight * gamma * observed[t, l] * observed[t, m]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    @cython.profile(False)
    cdef void individual_component_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state, DOUBLE_DTYPE_t[:] output) nogil:
        """
        Get the density of the of the observed state given by the hidden_state for a specific component

        Parameters
        ----------
        hidden_state : Py_ssize_t
        component : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            Py_ssize_t component

        for component in range(self._n_components):
            output[component] = multivariate_normal_density(
                observed_state,
                self.means_[hidden_state, component],
                self.precisions_[hidden_state][component],
                self.two_pi_k_det_[hidden_state][component],
                self.tmp_n_variates
            )


    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    @cython.profile(False)
    cdef inline DOUBLE_DTYPE_t individual_component_log_density_for(self, Py_ssize_t hidden_state, Py_ssize_t component, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state for a specific component

        Parameters
        ----------
        hidden_state : Py_ssize_t
        component : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        return log_multivariate_normal_density(
            observed_state,
            self.means_[hidden_state, component],
            self.precisions_[hidden_state, component],
            self.log_two_pi_k_det_[hidden_state, component],
            self.tmp_n_variates
        )

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef DOUBLE_DTYPE_t log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef :
            DOUBLE_DTYPE_t d
            Py_ssize_t component

        for component in range(self._n_components):
            self.tmp_n_components[component] = self.log_weights_[hidden_state, component] + self.individual_component_log_density_for(hidden_state, component, observed_state)
        d = _logsumexp(self.tmp_n_components)
        return d


    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef DOUBLE_DTYPE_t density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef :
            DOUBLE_DTYPE_t d = 0
            Py_ssize_t component
        self.individual_component_density_for(hidden_state, observed_state, self.tmp_n_components)
        for component in range(self._n_components):
            d += self.weights_[hidden_state, component] * self.tmp_n_components[component]
        return d

    cdef list _sample(self, Py_ssize_t hidden_state, random_state):
        """
        Generate an emission for the given latent state

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        random_state :  np.random_state
        """
        cdef:
            Py_ssize_t i, component
            DOUBLE_DTYPE_t rand
            DOUBLE_DTYPE_t[:] cumsum = np.cumsum(self.weights_[hidden_state])
            # first choose a mixture component
        rand = random_state.rand()
        for i in range(self._n_components):
            if cumsum[i] > rand:
                component = i
                break
        # The generate from distribution
        return random_state.multivariate_normal(self.means_[hidden_state][component], self.covariances_[hidden_state][component]).tolist()

    def __str__(self):
        return "Not Implemented"

    cpdef _log_state(self):
        cdef:
            Py_ssize_t i, j

        for i in range(self._N):
            for j in range(self._n_components):
                self._logger.info("emissions[{}, {}] weight={}, mean={} var={}".format(i, j, self.weights_[i, j], np.asarray(self.means_[i, j]), np.asarray(self.covariances_[i, j])))


cdef class TiedGaussianMixtureModel(EmissionsBase):
    """

    """
    #[N_states, n_componets]
    cdef public DOUBLE_DTYPE_t[:, :] weights_
    cdef public DOUBLE_DTYPE_t[:, :] log_weights_

    #[N states, n_componens, N_Variates]
    cdef public DOUBLE_DTYPE_t[:, :, :] means_

    #[N States, n_componens, N_Covariances, N_Variates]
    cdef public DOUBLE_DTYPE_t[:, :, :] covariances_
    cdef public DOUBLE_DTYPE_t[:, :, :] precisions_

    cdef DOUBLE_DTYPE_t[:, :] weights_reestimate_
    cdef DOUBLE_DTYPE_t[:, :, :] new_means_numerator_
    #cdef DOUBLE_DTYPE_t[:, :] new_means_denominator_

    cdef DOUBLE_DTYPE_t[:, :, :] new_variances_obs_T

    cdef DOUBLE_DTYPE_t[:] two_pi_k_det_
    cdef DOUBLE_DTYPE_t[:] log_two_pi_k_det_

    cdef public DOUBLE_DTYPE_t variance_regularization
    cdef public str covariance_type
    cdef Py_ssize_t _n_components
    cdef Py_ssize_t _n_variates

    # Temporary buffers for density estimation and accumulating statistics
    cdef DOUBLE_DTYPE_t[:] tmp_n_variates
    cdef DOUBLE_DTYPE_t[:] tmp_n_components


    def __init__(self, DOUBLE_DTYPE_t[:, :] weights, DOUBLE_DTYPE_t[:, :, :] means, DOUBLE_DTYPE_t[:, :, :] covariances, variance_regularization=1e-6, log_level=logging.ERROR):
        super(TiedGaussianMixtureModel, self).__init__(means.shape[0], log_level)
        self.weights_ = weights
        self.log_weights_ = np.zeros_like(weights)
        self.means_ = means
        self._n_components = means.shape[1]
        self._n_variates = means.shape[2]
        self.covariances_ = covariances
        self.precisions_ = np.zeros_like(covariances)
        self.two_pi_k_det_ = np.zeros((self._N), dtype=DOUBLE_DTYPE)
        self.log_two_pi_k_det_ = np.zeros((self._N), dtype=DOUBLE_DTYPE)
        self.variance_regularization = variance_regularization

        self.weights_reestimate_ = np.zeros((self._N, self._n_components), dtype=DOUBLE_DTYPE)
        self.new_means_numerator_ = np.zeros((self._N, self._n_components, self._n_variates), dtype=DOUBLE_DTYPE)
        #self.new_means_denominator_ = np.zeros((self._N, self._n_components), dtype=DOUBLE_DTYPE)
        self.new_variances_obs_T = np.zeros((self._N, self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)

        self.tmp_n_variates = np.zeros(self._n_variates)
        self.tmp_n_components = np.zeros(self._n_components)
        self._init_distributions()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef _init_distributions(self):
        cdef:
            Py_ssize_t state, component, i, j
            DOUBLE_DTYPE_t[:, :] tmp

        for state in range(self._N):
            for component in range(self._n_components):
                self.log_weights_[state, component] = log(self.weights_[state, component])

            # Variance is across all components
            for i in range(self._n_variates):
                tmp = np.linalg.inv(self.covariances_[state])
                for j in range(self._n_variates):
                    self.precisions_[state, i, j] = tmp[i, j]

            self.two_pi_k_det_[state] = sqrt((2.0 * M_PI) ** self._n_variates * np.linalg.det(self.covariances_[state]))
            self.log_two_pi_k_det_[state] = log(self.two_pi_k_det_[state])
                #self._logger.info(np.asarray(self.covariances_[state, component]))
                #self._logger.info("{} {} {} {}".format(state, component, self.two_pi_k_det_[state,component],np.linalg.det(self.covariances_[state, component])))

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef void _m_step(self):
        cdef:
            Py_ssize_t hidden_state, component, k, l
            DOUBLE_DTYPE_t weights_denominator, covariance_denominator
        self.covariances_[:] = 0
        self.weights_[:] = 0
        # #self.variance_regularization
        for hidden_state in range(self._N):

            # Adjust weights
            weights_denominator = min_density
            for component in range(self._n_components):
                weights_denominator += self.weights_reestimate_[hidden_state, component]

            for component in range(self._n_components):
                self.weights_[hidden_state, component] = self.weights_reestimate_[hidden_state, component] / weights_denominator

            # adjust means
            for component in range(self._n_components):
                for k in range(self._n_variates):
                    self.means_[hidden_state][component][k] = self.new_means_numerator_[hidden_state, component, k] / self.weights_reestimate_[hidden_state, component]

        # Update Covariances
        for hidden_state in range(self._N):
            covariance_denominator = 0
            for k in range(self._n_variates):
                for l in range(self._n_variates):
                    self.covariances_[hidden_state, k, l] = self.new_variances_obs_T[hidden_state, k, l]
            for component in range(self._n_components):
                for k in range(self._n_variates):
                    for l in range(self._n_variates):
                        self.covariances_[hidden_state, k, l] -= (self.new_means_numerator_[hidden_state, component, k] * self.means_[hidden_state, component, l])
                        self.covariances_[hidden_state, k, l] -= (self.new_means_numerator_[hidden_state, component, l] * self.means_[hidden_state, component, k])
                        self.covariances_[hidden_state, k, l] += (self.weights_reestimate_[hidden_state, component]  * self.means_[hidden_state, component, k] * self.means_[hidden_state, component, l])

                covariance_denominator += self.weights_reestimate_[hidden_state, component]

            for k in range(self._n_variates):
                for l in range(self._n_variates):
                    self.covariances_[hidden_state, k, l] /= covariance_denominator
                    # To make invertifble
                    if k == l:
                        self.covariances_[hidden_state, k, l] += self.variance_regularization

        self.weights_reestimate_[:] = 0
        self.new_means_numerator_[:] = 0
        #self.new_means_denominator_[:] = 0
        self.new_variances_obs_T[:] = 0
        self._init_distributions()

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cdef void _accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight) nogil:
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t hidden_state, component, l, m, t
            DOUBLE_DTYPE_t gamma, gamma_denom

        for t in range(observed.shape[0]):
            for hidden_state in range(self._N):
                # Need the normalized density that this observation came from each component
                gamma_denom = 0 # self.density_for(hidden_state, observed[t])
                self.individual_component_density_for(hidden_state, observed[t], self.tmp_n_components)
                for component in range(self._n_components):
                    self.tmp_n_components[component] =  self.weights_[hidden_state, component] * self.tmp_n_components[component]
                    gamma_denom += self.tmp_n_components[component]
                for component in range(self._n_components):
                    #  Now we have the relative proportion that the observation came from this component
                    if gamma_denom > 0:
                        gamma = digamma_1[t, hidden_state] * self.tmp_n_components[component] / gamma_denom
                    else:
                        gamma = 0

                    self.weights_reestimate_[hidden_state, component] += weight * gamma
                    #self.new_means_denominator_[hidden_state, component] += gamma
                    for l in range(self._n_variates):
                        self.new_means_numerator_[hidden_state, component, l] += weight * gamma * observed[t, l]

                    # Ignore component aspect of variance
                    for l in range(self._n_variates):
                        for m in range(self._n_variates):
                            self.new_variances_obs_T[hidden_state, l, m] += weight * gamma * observed[t, l] * observed[t, m]

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cdef inline DOUBLE_DTYPE_t individual_component_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state, DOUBLE_DTYPE_t[:] output) nogil:
        """
        Get the density of the of the observed state given by the hidden_state for a specific component

        Parameters
        ----------
        hidden_state : Py_ssize_t
        component : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            Py_ssize_t component

        for component in range(self._n_components):
            output[component] = multivariate_normal_density(
                observed_state,
                self.means_[hidden_state, component],
                self.precisions_[hidden_state],
                self.two_pi_k_det_[hidden_state],
                self.tmp_n_variates
            )

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cdef inline DOUBLE_DTYPE_t individual_component_log_density_for(self, Py_ssize_t hidden_state, Py_ssize_t component, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state for a specific component

        Parameters
        ----------
        hidden_state : Py_ssize_t
        component : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        return log_multivariate_normal_density(
            observed_state,
            self.means_[hidden_state, component],
            self.precisions_[hidden_state],
            self.log_two_pi_k_det_[hidden_state],
            self.tmp_n_variates
        )

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef DOUBLE_DTYPE_t log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef :
            DOUBLE_DTYPE_t d
            Py_ssize_t component

        for component in range(self._n_components):
            self.tmp_n_components[component] = self.log_weights_[hidden_state, component] + self.individual_component_log_density_for(hidden_state, component, observed_state)
        d = _logsumexp(self.tmp_n_components)
        return d


    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef DOUBLE_DTYPE_t density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef :
            DOUBLE_DTYPE_t d = 0
            Py_ssize_t component

        self.individual_component_density_for(hidden_state, observed_state, self.tmp_n_components)
        for component in range(self._n_components):
            d += self.weights_[hidden_state, component] * self.tmp_n_components[component]
        #print(hidden_state, np.asarray(observed_state), d)
        return d

    cdef list _sample(self, Py_ssize_t hidden_state, random_state):
        """
        Generate an emission for the given latent state

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        random_state :  np.random_state
        """
        cdef:
            Py_ssize_t i, component
            DOUBLE_DTYPE_t rand
            DOUBLE_DTYPE_t[:] cumsum = np.cumsum(self.weights_[hidden_state])
            # first choose a mixture component
        rand = random_state.rand()
        for i in range(self._n_components):
            if cumsum[i] > rand:
                component = i
                break
        # The generate from distribution
        return random_state.multivariate_normal(self.means_[hidden_state][component], self.covariances_[hidden_state]).tolist()

    def __str__(self):
        return "Not Implemented"

    cdef _log_state(self):
        cdef:
            Py_ssize_t i, j
        for i in range(self._N):
            for j in range(self._n_components):
                self._logger.info("emissions[{}, {}] weight={}, mean={} var={}".format(i, j, self.weights_[i, j], np.asarray(self.means_[i, j]), np.asarray(self.covariances_[i])))
