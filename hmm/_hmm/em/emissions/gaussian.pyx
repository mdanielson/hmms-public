# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3

import logging
import warnings
import numpy as np

cimport cython
cimport numpy as np
from libc.math cimport exp, log, sqrt, M_PI
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from .base cimport EmissionsBase

cdef class GaussianEmissions(EmissionsBase):
    """

    """
    cdef DOUBLE_DTYPE_t[:] _means
    cdef DOUBLE_DTYPE_t[:] _variances

    cdef DOUBLE_DTYPE_t[:] _means_weight
    cdef DOUBLE_DTYPE_t[:] _new_means_denominator

    cdef DOUBLE_DTYPE_t[:] _new_variances_obs

    cdef DOUBLE_DTYPE_t[:] _divisor1
    cdef DOUBLE_DTYPE_t[:] _divisor2
    cdef DOUBLE_DTYPE_t[:] _log_divisor2

    cdef DOUBLE_DTYPE_t _variance_regularization

    def __init__(self, DOUBLE_DTYPE_t[:] means, DOUBLE_DTYPE_t[:] variances, variance_regularization=1e-6, log_level=logging.ERROR):
        super(GaussianEmissions, self).__init__(means.shape[0], log_level)
        self._means = means
        self._variances = variances
        self._divisor1 = np.zeros_like(self._means)
        self._divisor2 = np.zeros_like(self._means)
        self._log_divisor2 = np.zeros_like(means)
        self._variance_regularization = variance_regularization

        self._init_distributions()
        self._means_weight = np.zeros_like(self._means)
        self._new_means_denominator = np.zeros_like(self._means)

        self._new_variances_obs = np.zeros_like(self._means)

    @property
    def means(self):
        return self._means

    @property
    def variances(self):
        return self._variances

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _init_distributions(self):
        cdef:
            Py_ssize_t i

        for i in range(self._N):
            self._divisor1[i] = 2 * self._variances[i]
            self._divisor2[i] = 1. / sqrt(M_PI * self._divisor1[i])
            self._log_divisor2[i] = log(1. / sqrt(M_PI * self._divisor1[i]))


    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _m_step(self):
        cdef:
            Py_ssize_t i
        for i in range(self._N):
            if self._new_means_denominator[i] > 0:
                self._means[i] = self._means_weight[i] / self._new_means_denominator[i]
            else:
                warnings.warn("Zero denominator...[{}]".format(i))
                self._means[i] = 0
            self._variances[i] = self._new_variances_obs[i] - \
                    2*self._means[i] * self._means_weight[i] + \
                    (self._means[i]**2) * self._new_means_denominator[i]
            self._variances[i] /= self._new_means_denominator[i]
            if self._variances[i] < self._variance_regularization:
                self._variances[i] = self._variance_regularization

        self._means_weight[:] = 0
        self._new_means_denominator[:] = 0
        self._new_variances_obs[:] = 0
        self._init_distributions()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight) nogil:
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t i, t

            DOUBLE_DTYPE_t tmp

        for t in range(observed.shape[0]):
            for i in range(self._N):
                self._new_means_denominator[i] += weight * digamma_1[t, i]
                tmp = digamma_1[t, i] * observed[t, 0]
                self._means_weight[i] += weight * tmp
                self._new_variances_obs[i] += weight * tmp * observed[t, 0]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            DOUBLE_DTYPE_t diff = (observed_state[0] - self._means[hidden_state])
        #assert observed_state.shape[0] == 1
        return self._divisor2[hidden_state] * exp( -(diff**2)/self._divisor1[hidden_state])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            DOUBLE_DTYPE_t diff = (observed_state[0] - self._means[hidden_state])
        #assert observed_state.shape[0] == 1
        return self._log_divisor2[hidden_state]  - (diff**2) / self._divisor1[hidden_state]

    cdef list _sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        random_state :  np.random_state
        """
        return [random_state.normal(self._means[hidden_state], sqrt(self._variances[hidden_state]))]

    def __str__(self):
        return "Not Implemented"

    cdef _log_state(self):
        cdef:
            Py_ssize_t i

        for i in range(self._N):
            self._logger.info("B[{}] mean={} var={}".format(i, self._means[i], self._variances[i]))
