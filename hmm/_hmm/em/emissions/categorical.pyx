# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import numpy as np

cimport cython
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from libc.math cimport log, INFINITY

from .base cimport EmissionsBase
from hmm.utilities.normalize cimport normalize_2d

cdef class SmoothingBase:

    cdef Py_ssize_t _M
    def __init__(self, Py_ssize_t M):
        self._M = M

    cdef void smooth(self, DOUBLE_DTYPE_t[:] B_counts, DOUBLE_DTYPE_t[:] next_B):
        pass


cdef class NoSmoothing(SmoothingBase):
    cdef void smooth(self, DOUBLE_DTYPE_t[:] B_counts, DOUBLE_DTYPE_t[:] next_B):
        cdef:
            Py_ssize_t i
            DOUBLE_DTYPE_t denominator = 0

        for i in range(self._M):
            denominator += B_counts[i]

        for i in range(self._M):
            next_B[i] = B_counts[i] / denominator


cdef class AddK(SmoothingBase):
    cdef DOUBLE_DTYPE_t _k

    def __init__(self, Py_ssize_t M, DOUBLE_DTYPE_t k):
        super(AddK, self).__init__(M)
        self._k = k

    cdef void smooth(self, DOUBLE_DTYPE_t[:] B_counts, DOUBLE_DTYPE_t[:] next_B):
        cdef:
            Py_ssize_t j
            DOUBLE_DTYPE_t N = 0
            DOUBLE_DTYPE_t V = self._M
            DOUBLE_DTYPE_t denominator

        for j in range(self._M):
            N += B_counts[j]
        if N > 0:
            denominator = N + self._k * V;
            for j in range(self._M):
                next_B[j] = (B_counts[j] + self._k)/denominator
        else:
            for j in range(self._M):
                next_B[j] = 0


cdef class LaplaceSmoothing(AddK):
    def __init__(self, Py_ssize_t M):
        super(LaplaceSmoothing, self).__init__(M, 1.)


cdef class CategoricalEmissions(EmissionsBase):
    """

    """

    @property
    def B(self):
        return np.asarray(self._B)

    cdef Py_ssize_t _M
    cdef DOUBLE_DTYPE_t[:, :] _B
    cdef DOUBLE_DTYPE_t[:, :] _log_B
    cdef DOUBLE_DTYPE_t[:, :] _next_B

    cdef SmoothingBase _smoother

    def __init__(self, DOUBLE_DTYPE_t[:, :] B, SmoothingBase smoother not None, log_level=logging.ERROR):
        cdef:
            Py_ssize_t i, j

        super(CategoricalEmissions, self).__init__(B.shape[0], log_level)

        self._B = B
        normalize_2d(self._B, self._B)
        self._log_B = np.zeros_like(self._B)
        self._next_B = np.zeros_like(self._B)
        self._M = B.shape[1]
        self._smoother = smoother
        for i in range(self._N):
            for j in range(self._M):
                self._log_B[i, j] = log(self._B[i, j])

    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _m_step(self):
        cdef:
            Py_ssize_t i, j
        self._B[:] = 0
        self._log_B[:] = -INFINITY

        for i in range(self._N):
            # Apply Smoothing
            self._smoother.smooth(self._next_B[i], self._B[i])

            for j in range(self._M):
                self._log_B[i, j] = log(self._B[i, j])

        self._next_B[:] = 0

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef void _accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight) nogil:
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t i, j, t

        for i in range(self._N):
            for t in range(observed.shape[0]):
                j = int(observed[t, 0])
                self._next_B[i, j] += weight * digamma_1[t, i]

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            Py_ssize_t observed
        assert observed_state.shape[0] == 1
        observed = <Py_ssize_t> int(observed_state[0])
        return  self._B[hidden_state, observed]

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        """
        Get the density of the of the observed state given by the hidden_state

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            Py_ssize_t observed
        assert observed_state.shape[0] == 1
        observed = <Py_ssize_t> int(observed_state[0])
        return  self._log_B[hidden_state, observed]

    cdef list _sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        """
        cdef:
            DOUBLE_DTYPE_t[:] B_cdf
            DOUBLE_DTYPE_t rand
            Py_ssize_t i
        B_cdf = np.cumsum(self._B[hidden_state, :])
        rand = random_state.rand()
        for i in range(B_cdf.shape[0]):
            if B_cdf[i] > rand:
                return [i]


    def __str__(self):
        return str(np.asarray(self._B).tolist())

    cdef _log_state(self):
        cdef:
            Py_ssize_t j

        for j in range(self._B.shape[0]):
            self._logger.info("B[{}, :]={}".format(j, np.asarray(self._B[j, :]).tolist()))
            self._logger.info("logB[{}, :]={}".format(j, np.asarray(self._log_B[j, :]).tolist()))
