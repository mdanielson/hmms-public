# cython: profile=False
# cython: language_level=3
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t

from .emissions cimport base

cdef class HMMTrainer:
    cdef public DOUBLE_DTYPE_t[:] _mixture_weights
    cdef public DOUBLE_DTYPE_t[:] _next_mixture_weights
    cdef public DOUBLE_DTYPE_t[:] _log_mixture_weights
    # initial state
    cdef public DOUBLE_DTYPE_t[:, :] _pi
    cdef public DOUBLE_DTYPE_t[:, :] _logpi
    cdef public DOUBLE_DTYPE_t[:, :] _next_pi

    # transition probabilities
    cdef public DOUBLE_DTYPE_t[:, :, :] _A
    cdef public DOUBLE_DTYPE_t[:, :, :] _logA
    cdef public DOUBLE_DTYPE_t[:, :, :] _next_A

    cdef public list _emissions
    # For log path
    cdef bint _prefer_scaling
    cdef bint _allowed_to_use_log
    cdef public DOUBLE_DTYPE_t[:] _work_buffer

    # Number of hidden states
    cdef public Py_ssize_t _n_mixture_components
    cdef public Py_ssize_t _n_states

    cdef public object logger
    cdef public object random_state

    cpdef list train(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, Py_ssize_t iterations=?, DOUBLE_DTYPE_t tol=?)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] mixture_loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cdef void _accumulate_statistics(self, const DOUBLE_DTYPE_t[:, :] observation_sequence, DOUBLE_DTYPE_t[:] normalized_mixture_component_probabilities, DOUBLE_DTYPE_t[:, :, :, :] digamma, DOUBLE_DTYPE_t[:, :, :] digamma_1)

    cdef void _m_step(self)
    cpdef log_state(self)

    cdef compute_observation_probabilities(self, Py_ssize_t cluster, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cdef compute_observation_log_probabilities(self, Py_ssize_t cluster, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)

    cdef void _update_log_state(self)
