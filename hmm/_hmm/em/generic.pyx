# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import warnings

cimport cython
cimport libc.math
import sklearn.utils
import numpy as np
cimport numpy as np
from hmm import logs

from libc.math cimport INFINITY, log, isinf, isnan

from hmm.utilities.types import DOUBLE_DTYPE, INT_DTYPE
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from hmm.utilities.log_routines cimport _logsumexp, _logaddexp
from hmm.utilities.normalize cimport normalize_1d, normalize_2d

from .emissions cimport base
from .. cimport forward_backward, viterbi


cdef class HMMTrainer:
    """
    Train a Hidden Markov Model via EM:
        http://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf

    """

    def __init__(self, DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, base.EmissionsBase emissions, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.DEBUG):

        self._logger = logs.create_logger(__name__, log_level)
        # The following normalization checks should have already been done,
        # but it is cheap to do here.

        # normalize so sums to 1
        normalize_1d(pi, pi)
        self._pi = pi
        self._logpi = np.zeros_like(self._pi)
        self._next_pi = np.zeros_like(self._pi)

        # Normalize so each row sums to 1
        self._A = A
        normalize_2d(self._A, self._A)
        self._logA = np.zeros_like(self._A)
        self._next_A = np.zeros_like(A)

        self._emissions = emissions
        self._prefer_scaling = prefer_scaling
        self._allowed_to_use_log = allowed_to_use_log
        self._N = self._pi.shape[0]
        assert self._N > 0
        assert self._A.shape[0] == self._N
        assert self._A.shape[1] == self._N

        self._update_log_vars()
        self._log_state()

    # Read only access to some members
    @property
    def N(self):
        return self._N

    @property
    def pi(self):
        return self._pi

    @property
    def A(self):
        return self._A

    @property
    def emissions(self):
        return self._emissions

    cdef _log_state(self):
        cdef:
            Py_ssize_t i

        self._logger.info("pi={}".format(np.asarray(self._pi).tolist()))
        for i in range(self._A.shape[0]):
            self._logger.info("A[{}, :]={}".format(i, np.asarray(self._A[i, :]).tolist()))
        self._emissions._log_state()

    cpdef list train(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, iterations=100, tol=1e-2):
        """
        Train an HMM for a specified number of iterations or tolerance, whichever is met first.

        Parameters
        ----------

        observed: 3-d memory view of observations
        iterations: int
        tol: DOUBLE_DTYPE_t

        """
        cdef:
            bint alpha_pass_success
            Py_ssize_t iteration, i, o_i, t, seq_start, seq_end, max_length, T
            DOUBLE_DTYPE_t log_prob
            DOUBLE_DTYPE_t[:, :] alpha
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] beta
            DOUBLE_DTYPE_t[:, :, :] digamma
            DOUBLE_DTYPE_t[:, :] digamma_1
            DOUBLE_DTYPE_t old_log, delta
            bint is_debug
            DOUBLE_DTYPE_t[:] loglikelihoods = np.full(lengths.shape[0], -INFINITY)
            DOUBLE_DTYPE_t[:, :] observation_probabilities;
            list log_probs = []
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:] work_buffer_N = np.zeros((self._N), dtype=DOUBLE_DTYPE)

        seq_end = np.sum(lengths)
        assert observed.shape[0] == seq_end, "Lengths and sequences mismatch {} vs {}".format(observed.shape[0], seq_end)
        old_log = 0

        max_length = np.max(lengths)
        observation_probabilities = np.zeros((max_length, self._N))
        alpha = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        beta = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        digamma = np.zeros((max_length, self._N, self._N), dtype=DOUBLE_DTYPE)
        digamma_1 = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        c = np.zeros((max_length, ), dtype=DOUBLE_DTYPE)

        is_debug = self._logger.isEnabledFor(logging.DEBUG)

        for iteration in range(iterations):
            seq_start = 0
            for o_i in range(lengths.shape[0]):
                seq_end = seq_start + lengths[o_i]
                alpha_pass_success = False

                c[:] = INFINITY
                digamma[:] = INFINITY
                digamma_1[:] = INFINITY
                beta[:] = INFINITY
                alpha[:] = INFINITY
                observation_probabilities[:] = INFINITY

                # Storage for probabilities
                sequence = observed[seq_start:seq_end]
                T = sequence.shape[0]

                # Update C / Alpha
                if self._prefer_scaling:
                    self._compute_observation_probabilities(sequence, observation_probabilities[:T])
                    alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities[:T], self._pi, self._A, c, alpha)

                if (not alpha_pass_success or not self._prefer_scaling) and self._allowed_to_use_log:
                    self._compute_observation_log_probabilities(sequence, observation_probabilities[:T])
                    forward_backward.log_alpha_pass(observation_probabilities[:T], self._logpi, self._logA, alpha, work_buffer_N)
                    log_prob = _logsumexp(alpha[T-1, :])
                else:
                    # log_prob is the product of the scaling constants
                    log_prob = 0
                    for t in range(T):
                        log_prob -= log(c[t])
                loglikelihoods[o_i] = log_prob
                if libc.math.isnan(log_prob) or libc.math.isinf(log_prob):
                    self._logger.error("observation: {}".format(o_i))
                    self._logger.error("alpha_pass_success: {}".format(alpha_pass_success))
                    self._logger.error("log_prob")
                    self._logger.error(log_prob)
                    self._logger.error("c")
                    self._logger.error(np.asarray(c[:T]).tolist())
                    self._logger.error("alpha")
                    self._logger.error(np.asarray(alpha[:T]).tolist())
                    self._logger.error("beta")
                    self._logger.error(np.asarray(beta[:T]).tolist())
                    self._log_state()
                    assert False

                if alpha_pass_success:
                    # Update Beta
                    forward_backward.scaling_beta_pass(observation_probabilities[:T], self._A, c, beta)
                    forward_backward.scaling_compute_digammas(self._A, alpha, beta, observation_probabilities[:T], digamma, digamma_1)
                else:
                    forward_backward.log_beta_pass(observation_probabilities[:T], self._logA, beta, work_buffer_N)
                    forward_backward.log_compute_digammas(self._logA, alpha, beta, observation_probabilities[:T], digamma, digamma_1)

                # Update digammas
                self._accumulate_statistics(sequence, digamma, digamma_1)
                seq_start = seq_end
            # Reestimate internal state
            self._m_step()

            # Using numpy log - not super performance sensitive here
            log_prob = np.sum(np.asarray(loglikelihoods))
            log_probs.append(log_prob)

            self._logger.info("log_prob[{}]={}".format(iteration, log_prob))
            if is_debug:
                self._log_state()

            if iteration != 0 and log_prob < (old_log - 1e-6):
                for i in range(len(log_probs)):
                    print(i, log_probs[i])
                print(log_prob < (old_log - 1e-6))
                print(log_prob)
                print(old_log - 1e-6)
                assert False, "Convergence Error"

            delta = log_prob - old_log
            if iteration != 0 and delta < libc.math.fabs(tol):
                break

            old_log = log_prob

        return log_probs

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        """
        The loglikelihood is the logsum of the "c"
        """
        cdef:
            bint alpha_pass_success = False
            Py_ssize_t o_i, t, seq_start, seq_end, T
            DOUBLE_DTYPE_t[:, :] alpha
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:] loglikelihoods = np.zeros(lengths.shape[0])
            DOUBLE_DTYPE_t log_prob
            DOUBLE_DTYPE_t[:] work_buffer_N = np.zeros((self._N), dtype=DOUBLE_DTYPE)

        T = max(lengths)
        alpha = np.zeros((T, self._N), dtype=DOUBLE_DTYPE)
        observation_probabilities = np.zeros((T, self._N))
        c = np.zeros((T,), dtype=DOUBLE_DTYPE)
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            T = sequence.shape[0]
            if self._prefer_scaling:
                self._compute_observation_probabilities(sequence, observation_probabilities)
                alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities[:T], self._pi, self._A, c, alpha)
            if not alpha_pass_success:
                self._compute_observation_log_probabilities(sequence, observation_probabilities)
                forward_backward.log_alpha_pass(observation_probabilities[:T], self._logpi, self._logA, alpha, work_buffer_N)
                loglikelihoods[o_i] = _logsumexp(alpha[T-1, :])
            else:
                log_prob = 0
                for t in range(T):
                    log_prob -= log(c[t])
                loglikelihoods[o_i] = log_prob
            seq_start = seq_end
        return np.asarray(loglikelihoods)

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] observation_probabilities;
            const DOUBLE_DTYPE_t[:, :] sequence
            list paths

        paths = []
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]

            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self._compute_observation_probabilities(sequence, observation_probabilities)
            paths.extend(
                viterbi.viterbi_path(self._pi, self._A, observation_probabilities)
            )
            seq_start = seq_end

        return np.asarray(paths)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef tuple scaling_alpha_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] alpha
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence

        c = np.zeros((observed.shape[0],), dtype=DOUBLE_DTYPE)
        alpha = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self._compute_observation_probabilities(sequence, observation_probabilities)
            forward_backward.scaling_alpha_pass(observation_probabilities, self._pi, self._A, c[seq_start:seq_end], alpha[seq_start:seq_end])
            seq_start = seq_end

        return c, alpha

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] log_alpha_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] alpha
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:] work_buffer_N = np.zeros((self._N), dtype=DOUBLE_DTYPE)

        seq_start = 0
        alpha = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self._compute_observation_log_probabilities(sequence, observation_probabilities)
            forward_backward.log_alpha_pass(observation_probabilities, self._logpi, self._logA, alpha[seq_start:seq_end], work_buffer_N)
            seq_start = seq_end
        return np.asarray(alpha)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] scaling_beta_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, DOUBLE_DTYPE_t[:] c):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] beta
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
        beta = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)

        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self._compute_observation_probabilities(sequence, observation_probabilities)
            forward_backward.scaling_beta_pass(observation_probabilities, self._A, c[seq_start:seq_end], beta[seq_start:seq_end])
            seq_start = seq_end
        return np.asarray(beta)


    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] log_beta_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] beta
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:] work_buffer_N = np.zeros((self._N), dtype=DOUBLE_DTYPE)

        beta = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)

        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self._compute_observation_log_probabilities(sequence, observation_probabilities)
            forward_backward.log_beta_pass(observation_probabilities, self._logA, beta[seq_start:seq_end], work_buffer_N)
            seq_start = seq_end
        return np.asarray(beta)

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef tuple scaling_compute_digammas(self, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            DOUBLE_DTYPE_t[:, :, :] digamma
            DOUBLE_DTYPE_t[:, :] digamma_1
            Py_ssize_t o_i, seq_start, seq_end
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:, :] observation_probabilities;
        digamma = np.zeros((observed.shape[0], self._N, self._N), dtype=DOUBLE_DTYPE)
        digamma_1 = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self._compute_observation_probabilities(sequence, observation_probabilities)
            forward_backward.scaling_compute_digammas(self._A, alpha[seq_start:seq_end], beta[seq_start:seq_end], observation_probabilities, digamma[seq_start:seq_end], digamma_1[seq_start:seq_end])
            seq_start = seq_end
        return digamma, digamma_1

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef tuple log_compute_digammas(self, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            DOUBLE_DTYPE_t[:, :, :] digamma
            DOUBLE_DTYPE_t[:, :] digamma_1
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
            Py_ssize_t o_i, seq_start, seq_end

        digamma = np.zeros((observed.shape[0], self._N, self._N), dtype=DOUBLE_DTYPE)
        digamma_1 = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self._compute_observation_log_probabilities(sequence, observation_probabilities)

            forward_backward.log_compute_digammas(self._logA, alpha[seq_start:seq_end], beta[seq_start:seq_end], observation_probabilities, digamma[seq_start:seq_end], digamma_1[seq_start:seq_end])
            seq_start = seq_end
        return digamma, digamma_1

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _accumulate_statistics(self, const DOUBLE_DTYPE_t[:, :] observation_sequence, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1) nogil:
        cdef:
            Py_ssize_t t, i, j, last_t

        for i in range(self._N):
            self._next_pi[i] += digamma_1[0, i]

        last_t = observation_sequence.shape[0] - 1
        for i in range(self._N):
            for j in range(self._N):
                for t in range(last_t):
                    self._next_A[i, j] += digamma[t, i, j]

        self._emissions._accumulate_statistics(digamma_1, observation_sequence, 1.0)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    @cython.cdivision(True)
    cdef void _m_step(self):
        cdef:
            Py_ssize_t i, j
            DOUBLE_DTYPE_t denom = 0

        for i in range(self._N):
            denom += self._next_pi[i]

        for i in range(self._N):
            self._pi[i] = self._next_pi[i] / denom

        self._next_pi[:] = 0
        for i in range(self._N):
            denom = 0
            for j in range(self._N):
                denom += self._next_A[i, j]
            # Normalize
            if denom > 0:
                for j in range(self._N):
                    self._A[i, j] = self._next_A[i, j]/denom

        self._next_A[:] = 0

        self._emissions._m_step()
        self._update_log_vars()

    cpdef sample(self, n_samples=1, length=10, random_state=None):
        cdef:
            Py_ssize_t j, currstate
            DOUBLE_DTYPE_t rand
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] A_cdf
            np.ndarray[DOUBLE_DTYPE_t, ndim=1] pi_cdf
            list lengths = []
            list hidden_sequences = []
            list observation_sequences = []
            object observed

        assert random_state is not None
        A_cdf = np.cumsum(self.A, axis=1)
        pi_cdf = np.cumsum(self.pi)
        for _ in range(n_samples):
            lengths.append(length)
            for j in range(length):
                rand = random_state.rand()
                if j == 0:
                    currstate = (pi_cdf > rand).argmax()
                else:
                    currstate = (A_cdf[currstate] > rand).argmax()

                hidden_sequences.append(currstate)
                observed = self._emissions._sample(currstate, random_state)
                observation_sequences.append(observed)
        return np.asarray(observation_sequences), np.asarray(hidden_sequences), np.asarray(lengths)


    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef _compute_observation_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, j
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for j in range(self._N):
                observation_probabilities[t, j] = self._emissions.density_for(j, observations[t])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef _compute_observation_log_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, j
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for j in range(self._N):
                observation_probabilities[t, j] = self._emissions.log_density_for(j, observations[t])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef void _update_log_vars(self):

        cdef:
            Py_ssize_t i, j
        for i in range(self._N):
            self._logpi[i] = log(self._pi[i])
            for j in range(self._N):
                self._logA[i, j] = log(self._A[i, j])
