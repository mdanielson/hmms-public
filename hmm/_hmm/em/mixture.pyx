# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging

cimport cython
cimport libc.math
import scipy.special
import sklearn.utils
import numpy as np
cimport numpy as np
from hmm import logs

from libc.math cimport INFINITY, log, isinf, isnan, exp

from hmm.utilities.types import DOUBLE_DTYPE, INT_DTYPE
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from hmm.utilities.log_routines cimport _logsumexp, _logaddexp
from hmm.utilities.normalize cimport normalize_1d, normalize_2d

from .emissions cimport base
from .. cimport forward_backward, viterbi


cdef class HMMTrainer:
    """
    Train a Hidden Markov Model via EM:
        http://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf

    """

    # Read only access
    mixture_weights = property(lambda self: np.asarray(self._mixture_weights))
    A = property(lambda self: np.asarray(self._A))
    pi = property(lambda self: np.asarray(self._pi))
    emissions = property(lambda self: self._emissions)

    def __init__(self, DOUBLE_DTYPE_t[:] mixture_weights, DOUBLE_DTYPE_t[:, :] pi, DOUBLE_DTYPE_t[:, :, :] A, emissions, prefer_scaling=True, allowed_to_use_log=True, random_state=None, log_level=logging.DEBUG):

        self.logger = logs.create_logger(__name__, log_level)
        self.random_state = sklearn.utils.check_random_state(random_state)

        # Will be initialized as needed

        # The following normalization checks should have already been done,
        # but it is cheap to do here.

        self._mixture_weights = np.asarray(mixture_weights)
        self._log_mixture_weights = np.zeros_like(mixture_weights)
        self._next_mixture_weights = np.zeros_like(mixture_weights)
        # normalize so sums to 1
        self._pi = np.asarray(pi)
        self._logpi = np.zeros_like(self._pi)
        self._next_pi = np.zeros_like(self._pi)

        # Normalize so each row sums to 1
        self._A = np.asarray(A)
        self._logA = np.zeros_like(self._A)
        self._next_A = np.zeros_like(A)

        self._emissions = emissions
        self._prefer_scaling = prefer_scaling
        self._allowed_to_use_log = allowed_to_use_log
        self._n_mixture_components = self._pi.shape[0]
        self._n_states = self._pi.shape[1]
        assert self._n_states > 0
        assert self._A.shape[0] == self._n_mixture_components
        assert self._A.shape[1] == self._n_states
        assert self._A.shape[2] == self._n_states

        self._update_log_state()
        self.log_state()

    cpdef log_state(self):
        cdef:
            Py_ssize_t c, i
            base.EmissionsBase emissions

        for c in range(self._n_mixture_components):
            self.logger.info("weight[{}]={}|{}".format(c, self._mixture_weights[c], self._log_mixture_weights[c]))
            self.logger.info("pi[{}]={}".format(c, np.asarray(self._pi[c]).tolist()))
            for i in range(self._n_states):
                self.logger.info("A[{}, {}, :]={}".format(c, i, np.asarray(self._A[c, i, :]).tolist()))
            emissions = self._emissions[c]
            emissions._log_state()

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef list train(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, Py_ssize_t iterations=100, DOUBLE_DTYPE_t tol=1e-2):
        """
        Train an HMM for a specified number of iterations or tolerance, whichever is met first.

        Parameters
        ----------

        observed: 3-d memory view of observations
        iterations: int
        tol: DOUBLE_DTYPE_t

        """
        cdef:
            bint alpha_pass_success
            Py_ssize_t mixture_component, iteration, i, o_i, t, seq_start, seq_end, max_length, T
            DOUBLE_DTYPE_t total_log_prob, component_log_prob, normalizer
            DOUBLE_DTYPE_t[:] normalized_mixture_component_probabilities = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t[:] membership_probabilities = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t[:, :] alpha
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] beta
            DOUBLE_DTYPE_t[:, :, :, :] digamma
            DOUBLE_DTYPE_t[:, :, :] digamma_1
            DOUBLE_DTYPE_t old_log, delta
            bint is_debug
            DOUBLE_DTYPE_t[:, :] observation_probabilities;
            list log_probs = []
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:] work_buffer_n_states = np.zeros((self._n_states), dtype=DOUBLE_DTYPE)
        seq_end = np.sum(lengths)
        assert observed.shape[0] == seq_end, "Lengths and sequences mismatch {} vs {}".format(observed.shape[0], seq_end)
        old_log = 0

        is_debug = self.logger.isEnabledFor(logging.DEBUG)

        max_length = np.max(lengths)
        observation_probabilities = np.zeros((max_length, self._n_states))
        alpha = np.zeros((max_length, self._n_states), dtype=DOUBLE_DTYPE)
        beta = np.zeros((max_length, self._n_states), dtype=DOUBLE_DTYPE)
        digamma = np.zeros((self._n_mixture_components, max_length, self._n_states, self._n_states), dtype=DOUBLE_DTYPE)
        digamma_1 = np.zeros((self._n_mixture_components, max_length, self._n_states), dtype=DOUBLE_DTYPE)
        c = np.zeros((max_length,), dtype=DOUBLE_DTYPE)

        #membership_probabilities = np.zeros((observed.shape[0], self._n_mixture_components), dtype=DOUBLE_DTYPE)
        for iteration in range(iterations):
            total_log_prob = 0
            seq_start = 0
            for o_i in range(lengths.shape[0]):
                seq_end = seq_start + lengths[o_i]

                # Storage for probabilities
                sequence = observed[seq_start:seq_end]
                T = sequence.shape[0]
                membership_probabilities[:] = -INFINITY

                digamma[:] = INFINITY
                digamma_1[:] = INFINITY
                for mixture_component in range(self._n_mixture_components):
                    c[:] = INFINITY
                    beta[:] = INFINITY
                    alpha[:] = INFINITY
                    observation_probabilities[:] = INFINITY
                    if self._mixture_weights[mixture_component] == 0:
                        continue
                    # Update C / Alpha
                    alpha_pass_success = False
                    if self._prefer_scaling:
                        self.compute_observation_probabilities(mixture_component, sequence, observation_probabilities[:T])
                        alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities[:T], self._pi[mixture_component], self._A[mixture_component], c, alpha)
                    if (not alpha_pass_success or not self._prefer_scaling) and self._allowed_to_use_log:
                        self.compute_observation_log_probabilities(mixture_component, sequence, observation_probabilities[:T])
                        forward_backward.log_alpha_pass(observation_probabilities[:T], self._logpi[mixture_component], self._logA[mixture_component], alpha, work_buffer_n_states)
                        component_log_prob = _logsumexp(alpha[T-1, :])
                    else:
                        # log_prob is the product of the scaling constants
                        component_log_prob = 0
                        for t in range(T):
                            component_log_prob -= log(c[t])

                    membership_probabilities[mixture_component] = self._log_mixture_weights[mixture_component] + component_log_prob

                    if libc.math.isnan(component_log_prob) or libc.math.isinf(component_log_prob):
                        self.logger.error("Component: {}".format(mixture_component))
                        self.logger.error("observation: {}".format(o_i))
                        self.logger.error("alpha_pass_success: {}".format(alpha_pass_success))
                        self.logger.error("component_log_prob")
                        self.logger.error(component_log_prob)
                        self.logger.error("c")
                        self.logger.error(np.asarray(c[:T]).tolist())
                        self.logger.error("alpha")
                        self.logger.error(np.asarray(alpha[:T]).tolist())
                        self.logger.error("beta")
                        self.logger.error(np.asarray(beta[:T]).tolist())
                        self.log_state()
                        assert False

                    if alpha_pass_success:
                        # Update Beta
                        forward_backward.scaling_beta_pass(observation_probabilities[:T], self._A[mixture_component], c, beta)
                        forward_backward.scaling_compute_digammas(self._A[mixture_component], alpha, beta, observation_probabilities[:T], digamma[mixture_component], digamma_1[mixture_component])
                    else:
                        forward_backward.log_beta_pass(observation_probabilities[:T], self._logA[mixture_component], beta, work_buffer_n_states)
                        forward_backward.log_compute_digammas(self._logA[mixture_component], alpha, beta, observation_probabilities[:T], digamma[mixture_component], digamma_1[mixture_component])

                # Compute normalized mixture components
                normalizer = _logsumexp(membership_probabilities)
                for mixture_component in range(self._n_mixture_components):
                    normalized_mixture_component_probabilities[mixture_component] = exp(membership_probabilities[mixture_component] - normalizer)
                total_log_prob += normalizer

                # Update digammas using mixture component probabilities
                self._accumulate_statistics(sequence, normalized_mixture_component_probabilities, digamma, digamma_1)
                seq_start = seq_end

            # recompute
            for mixture_component in range(self._n_mixture_components):
                self._mixture_weights[mixture_component] = self._next_mixture_weights[mixture_component] / lengths.shape[0]

            self._m_step()
            self._update_log_state()

            log_probs.append(total_log_prob)

            weights = []
            for i in range(self._n_mixture_components):
                weights.append("{:.4f}".format(self._mixture_weights[i]))
            weights = ",".join(weights)
            self.logger.info("iter={}, weights=[{}], log_prob={:.9f}".format(iteration, weights, total_log_prob))
            if is_debug:
                self.log_state()

            if iteration != 0 and total_log_prob < (old_log - 1e-6):
                print("CONVERGENCE ERROR")
                assert False, "Convergence Error"
            delta = old_log - total_log_prob
            if i != 0 and libc.math.fabs(delta) < libc.math.fabs(tol):
                break

            old_log = total_log_prob

        return log_probs

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] loglik(self, const DOUBLE_DTYPE_t[:, :] observed, Py_ssize_t[:] lengths):
        return scipy.special.logsumexp(self.mixture_loglik(observed, lengths), axis=1)

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] mixture_loglik(self, const DOUBLE_DTYPE_t[:, :] observed, Py_ssize_t[:] lengths):
        """
        The loglikelihood is the logsum of the "c"
        """
        cdef:
            bint alpha_pass_success = False
            Py_ssize_t o_i, t, mixture_component, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] alpha
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:, :] loglikelihoods = np.zeros((lengths.shape[0], self._n_mixture_components), dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t log_prob
            DOUBLE_DTYPE_t[:] work_buffer_n_states = np.zeros((self._n_states), dtype=DOUBLE_DTYPE)

        for mixture_component in range(self._n_mixture_components):
            if self._mixture_weights[mixture_component] == 0:
                loglikelihoods[:, mixture_component] = -INFINITY
                continue
            seq_start = 0
            for o_i in range(lengths.shape[0]):
                seq_end = seq_start + lengths[o_i]
                sequence = observed[seq_start:seq_end]
                observation_probabilities = np.zeros((sequence.shape[0], self._n_states))
                if sequence.shape[0] != c.shape[0]:
                    c = np.zeros((sequence.shape[0], ), dtype=DOUBLE_DTYPE)
                    alpha = np.zeros((sequence.shape[0], self._n_states), dtype=DOUBLE_DTYPE)

                if self._prefer_scaling:
                    self.compute_observation_probabilities(mixture_component, sequence, observation_probabilities)
                    alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities, self._pi[mixture_component], self._A[mixture_component], c, alpha)
                if not alpha_pass_success:
                    self.compute_observation_log_probabilities(mixture_component, sequence, observation_probabilities)
                    forward_backward.log_alpha_pass(observation_probabilities, self._logpi[mixture_component], self._logA[mixture_component], alpha, work_buffer_n_states)
                    loglikelihoods[o_i, mixture_component] = _logsumexp(alpha[-1, :])
                else:
                    log_prob = 0
                    for t in range(c.shape[0]):
                        log_prob -= log(c[t])
                    loglikelihoods[o_i, mixture_component]  = log_prob
                loglikelihoods[o_i, mixture_component] += self._log_mixture_weights[mixture_component]
                seq_start = seq_end
        return np.asarray(loglikelihoods)

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi(self, const DOUBLE_DTYPE_t[:, :] observed, Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, mixture_component, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] observation_probabilities;
            const DOUBLE_DTYPE_t[:, :] sequence
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] component_probabilities  = np.asarray(self.mixture_loglik(observed, lengths))
            np.ndarray[INT_DTYPE_t, ndim=1] component_assignments  = component_probabilities.argmax(axis=1)
            list paths

        paths = []
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            mixture_component = component_assignments[o_i]
            observation_probabilities = np.zeros((sequence.shape[0], self._n_states))
            self.compute_observation_probabilities(mixture_component, sequence, observation_probabilities)
            paths.extend(
                viterbi.viterbi_path(self._pi[mixture_component], self._A[mixture_component], observation_probabilities)
            )
            seq_start = seq_end
        return np.asarray(paths)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef void _accumulate_statistics(self, const DOUBLE_DTYPE_t[:, :] observation_sequence, DOUBLE_DTYPE_t[:] normalized_mixture_component_probabilities, DOUBLE_DTYPE_t[:, :, :, :] digamma, DOUBLE_DTYPE_t[:, :, :] digamma_1):
        cdef:
            Py_ssize_t t, i, j, last_t, mixture_component
            base.EmissionsBase emissions
        for mixture_component in range(self._n_mixture_components):
            self._next_mixture_weights[mixture_component] += normalized_mixture_component_probabilities[mixture_component]
            for i in range(self._n_states):
                self._next_pi[mixture_component, i] += normalized_mixture_component_probabilities[mixture_component] * digamma_1[mixture_component, 0, i]

            last_t = observation_sequence.shape[0] - 1
            for i in range(self._n_states):
                for j in range(self._n_states):
                    for t in range(last_t):
                        self._next_A[mixture_component, i, j] += normalized_mixture_component_probabilities[mixture_component] * digamma[mixture_component, t, i, j]

            emissions = self._emissions[mixture_component]
            emissions._accumulate_statistics(digamma_1[mixture_component], observation_sequence, normalized_mixture_component_probabilities[mixture_component])

    cdef void _m_step(self):
        cdef:
            Py_ssize_t mixture_component, i, j
            base.EmissionsBase emissions
            DOUBLE_DTYPE_t denom
        for mixture_component in range(self._n_mixture_components):
            denom = 0
            for i in range(self._n_states):
                denom += self._next_pi[mixture_component, i]
            if denom > 0:
                for i in range(self._n_states):
                    self._pi[mixture_component, i] = self._next_pi[mixture_component, i] / denom
            else:
                self._pi[mixture_component, i] = 0

            for i in range(self._n_states):
                denom = 0
                for j in range(self._n_states):
                    denom += self._next_A[mixture_component, i, j]
                # Normalize
                if denom > 0:
                    for j in range(self._n_states):
                        self._A[mixture_component, i, j] = self._next_A[mixture_component, i, j]/denom
            emissions = self._emissions[mixture_component]
            emissions._m_step()
        self._next_pi[:] = 0
        self._next_A[:] = 0
        self._next_mixture_weights[:] = 0

    def sample(self, n_samples=1, length=10, random_state=None):
        cdef:
            list mixture_components, hidden_sequences, observation_sequences
            Py_ssize_t j, currstate

        assert random_state is not None
        mixture_components = []
        hidden_sequences = []
        observation_sequences = []
        lengths = []
        mixture_cdf = np.cumsum(self.mixture_weights)
        pi_cdf = np.cumsum(self.A, axis=1)
        A_cdf = np.cumsum(self.A, axis=2)
        for _ in range(n_samples):
            hidden_sequence = []
            observation_sequence = []
            rand = random_state.rand()
            mixture_component =  (mixture_cdf > rand).argmax()

            for j in range(length):
                rand = random_state.rand()
                if j == 0:
                    currstate = (pi_cdf[mixture_component] > rand).argmax()
                else:
                    currstate = (A_cdf[mixture_component][currstate] > rand).argmax()

                hidden_sequence.append(currstate)
                observed = self._emissions[mixture_component]._sample(currstate, random_state)
                observation_sequence.append(observed)
            mixture_components.extend(mixture_component)
            hidden_sequences.extend(hidden_sequence)
            observation_sequences.extend(observation_sequence)
            lengths.append(length)
        return np.asarray(mixture_components), np.asarray(observation_sequences), np.asarray(hidden_sequences), np.asarray(lengths)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef compute_observation_probabilities(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, j
            base.EmissionsBase emissions
        # Compute Probabiltiies
        emissions = self._emissions[mixture_component]
        for t in range(observations.shape[0]):
            for j in range(self._n_states):
                observation_probabilities[t, j] = emissions.density_for(j, observations[t])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef compute_observation_log_probabilities(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, j
            base.EmissionsBase emissions
        # Compute Probabiltiies
        emissions = self._emissions[mixture_component]
        for t in range(observations.shape[0]):
            for j in range(self._n_states):
                observation_probabilities[t, j] = emissions.log_density_for(j, observations[t])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef void _update_log_state(self):
        cdef:
            Py_ssize_t mixture_component, i, j
        for mixture_component in range(self._n_mixture_components):
            self._log_mixture_weights[mixture_component] = log(self._mixture_weights[mixture_component])
            for i in range(self._n_states):
                self._logpi[mixture_component, i] = log(self._pi[mixture_component, i])
                for j in range(self._n_states):
                    self._logA[mixture_component, i, j] = log(self._A[mixture_component, i, j])
