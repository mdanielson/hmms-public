# cython: profile=False
# cython: language_level=3
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t

from .emissions cimport base

cdef class HMMTrainer:
    # initial state
    cdef DOUBLE_DTYPE_t[:] _pi
    cdef DOUBLE_DTYPE_t[:] _logpi
    cdef DOUBLE_DTYPE_t[:] _next_pi

    # transition probabilities
    cdef DOUBLE_DTYPE_t[:, :] _A
    cdef DOUBLE_DTYPE_t[:, :] _logA
    cdef DOUBLE_DTYPE_t[:, :] _next_A

    cdef base.EmissionsBase _emissions
    # For log path
    cdef bint _prefer_scaling
    cdef bint _allowed_to_use_log
    cdef DOUBLE_DTYPE_t[:] _work_buffer

    # Number of hidden states
    cdef Py_ssize_t _N

    cdef object _logger

    cpdef list train(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, iterations=?, tol=?)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cpdef tuple scaling_alpha_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] log_alpha_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] scaling_beta_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, DOUBLE_DTYPE_t[:] c)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] log_beta_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cpdef tuple scaling_compute_digammas(self, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef tuple log_compute_digammas(self, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cdef void _accumulate_statistics(self, const DOUBLE_DTYPE_t[:, :] observation_sequence, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1) nogil

    cdef void _m_step(self)
    cdef void _update_log_vars(self)
    cdef _log_state(self)

    cdef _compute_observation_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cdef _compute_observation_log_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cpdef sample(self, n_samples=?, length=?, random_state=?)
