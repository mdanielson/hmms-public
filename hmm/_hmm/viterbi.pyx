# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
cimport cython
cimport numpy as np
import numpy as np

from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE, INT_DTYPE
from libc.math cimport INFINITY, log

@cython.boundscheck(False)
@cython.initializedcheck(False)
cpdef tuple single_viterbi_probability(DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, const DOUBLE_DTYPE_t[:, :] observation_probabilities):
    """
    Parameters
    ----------
    sequence : array-like (m, )

    Returns
    -------
    tuple, (backpointers : array-like (n, m), probabilities : array-like (n, m))

    """

    cdef:
        Py_ssize_t t, hidden_state, j, best_state
        Py_ssize_t N = pi.shape[0]
        DOUBLE_DTYPE_t[:, :] probabilities
        INT_DTYPE_t[:, :] backpointers
        DOUBLE_DTYPE_t best_probability, temp
        bint found

    probabilities = np.zeros((observation_probabilities.shape[0], N), dtype=DOUBLE_DTYPE)
    # The first column of backpointers doesn't matter
    # At column t, what is stored is the best state at time t-1 that took us to time t
    backpointers = np.zeros((observation_probabilities.shape[0], N), dtype=INT_DTYPE)

    for hidden_state in range(N):
        probabilities[0][hidden_state] = log(pi[hidden_state] * observation_probabilities[0, hidden_state]) #self._emissions.density_for(hidden_state, sequence[0]))

    for t in range(1, observation_probabilities.shape[0]):
        # Next State
        for hidden_state in range(N):
            # Previous State
            found = False
            best_state = 0
            for j in range(0, N):
                temp = probabilities[t-1][j] + log(A[j][hidden_state]) + log(observation_probabilities[t, hidden_state]) #self._emissions.density_for(hidden_state, sequence[t]))
                if not found or temp > best_probability:
                    found = True
                    best_state = j
                    best_probability = temp
            assert found
            backpointers[t][hidden_state] = best_state
            probabilities[t][hidden_state] = best_probability
    return backpointers, probabilities

@cython.boundscheck(False)
@cython.initializedcheck(False)
cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi_path(DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, const DOUBLE_DTYPE_t[:, :] observation_probabilities):
    cdef:
        Py_ssize_t t, hidden_state, best_state
        Py_ssize_t N = pi.shape[0]
        DOUBLE_DTYPE_t[:, :] probabilities
        INT_DTYPE_t[:, :] backpointers
        INT_DTYPE_t[:] optimal_path
        DOUBLE_DTYPE_t best_probability
        bint found

    backpointers, probabilities = single_viterbi_probability(pi, A, observation_probabilities)
    optimal_path = np.zeros(observation_probabilities.shape[0], dtype=INT_DTYPE)
    # Final State
    found = False
    best_state = 0
    for hidden_state in range(0, N):
        if not found or probabilities[-1][hidden_state] > best_probability:
            found = True
            best_state = hidden_state
            best_probability = probabilities[-1][hidden_state]
    assert found
    optimal_path[-1] = best_state
    for t in reversed(range(1, observation_probabilities.shape[0])):
        optimal_path[t-1] = backpointers[t][best_state]
        best_state = optimal_path[t-1]

    return np.asarray(optimal_path)


