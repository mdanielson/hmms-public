# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import sklearn.utils
cimport cython
cimport libc.math
import numpy as np
cimport numpy as np
from libc.math cimport exp


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
@cython.initializedcheck(False)
cdef DOUBLE_DTYPE_t multivariate_normal_density(const DOUBLE_DTYPE_t[:] observed, DOUBLE_DTYPE_t[:] mean, DOUBLE_DTYPE_t[:, :] precision, DOUBLE_DTYPE_t two_pi_k_det, DOUBLE_DTYPE_t[:] tmp) nogil:
    cdef :
        DOUBLE_DTYPE_t d = 0
        Py_ssize_t i, j

    tmp[:] = 0
    # First outer product
    for i in range(observed.shape[0]):
        for j in range(observed.shape[0]):
            tmp[i] += (observed[j] - mean[j]) * precision[j, i]

    # Second outer product
    for i in range(observed.shape[0]):
        d += tmp[i] * (observed[i] - mean[i])

    d = exp( -.5 * d)
    d =  d / two_pi_k_det

    return d

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
@cython.initializedcheck(False)
cdef DOUBLE_DTYPE_t log_multivariate_normal_density(const DOUBLE_DTYPE_t[:] observed, DOUBLE_DTYPE_t[:] mean, DOUBLE_DTYPE_t[:, :] precision, DOUBLE_DTYPE_t log_two_pi_k_det, DOUBLE_DTYPE_t[:] tmp) nogil:
    cdef :
        DOUBLE_DTYPE_t d = 0
        Py_ssize_t i, j

    tmp[:] = 0
    # First outer product
    for i in range(observed.shape[0]):
        for j in range(observed.shape[0]):
            tmp[i] += (observed[j] - mean[j]) * precision[j, i]

    # Second outer product
    for i in range(observed.shape[0]):
        d += tmp[i] * (observed[i] - mean[i])

    d = -.5 *  d
    d =  d - log_two_pi_k_det
    return d
