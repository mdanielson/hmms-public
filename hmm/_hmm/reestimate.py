# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import collections

import numpy as np
import scipy.stats
import sklearn.preprocessing

from .. import util


def get_initial_probabilities(hidden, lengths):
    assert len(hidden) == sum(lengths), "Mismatch with lengths and hidden states: {} vs {}".format(sum(lengths), len(hidden))
    hidden_counts = np.bincount(hidden.ravel().astype(int))
    hidden_symbols = np.arange(hidden_counts.shape[0])
    first_indices = [0] + np.cumsum(lengths[:-1]).tolist()
    firsts = [hidden[f] for f in first_indices]
    firsts_counts = np.bincount(firsts)
    num_hidden = np.unique(hidden_symbols).shape[0]

    initials = np.zeros(num_hidden, dtype=float)
    initials[:firsts_counts.shape[0]] = firsts_counts
    initials /= initials.sum()
    return initials


def get_transition_probabilities(observed, hidden, lengths, num_hidden):
    assert len(hidden) == sum(lengths), "Mismatch with lengths and hidden states: {} vs {}".format(sum(lengths), len(hidden))
    assert len(observed) == sum(lengths), "Mismatch with lengths and observed states: {} vs {}".format(sum(observed), len(hidden))
    samples = collections.defaultdict(list)

    # transitions is the transition probability from t-1 to t
    # emissions is the emission probability at time t
    transitions = np.zeros((num_hidden, num_hidden), dtype=float)
    seq_start = 0
    for (o_i, (this_sequence, this_hidden)) in enumerate(zip(util.iter_sequences(observed, lengths), util.iter_sequences(hidden, lengths))):
        previous_hidden_state = None
        for j, (observation_state, hidden_state) in enumerate(zip(this_sequence, this_hidden)):
            samples[hidden_state].append(observation_state)
            if previous_hidden_state is not None:
                transitions[previous_hidden_state, hidden_state] += 1
            previous_hidden_state = hidden_state

    transitions = sklearn.preprocessing.normalize(transitions, "l1", axis=1)
    return samples, transitions


def reestimate_categorical(observed, hidden, lengths):
    """
    Parameters
    ----------

    observed : np.ndarray
               The observed outputs from a presumed categorical hmm
    hidden : np.ndarray
               The hidden/latent states from a presumed categorical hmm

    """
    # initials is the probability of starting in a hidden state
    initials = get_initial_probabilities(hidden, lengths)
    num_hidden = initials.shape[0]
    observed_symbols = np.unique(observed)
    num_observed = observed_symbols.shape[0]

    # transitions is the transition probability from t-1 to t
    # emissions is the emission probability at time t
    samples, transitions = get_transition_probabilities(observed, hidden, lengths,  num_hidden)
    emissions = np.zeros((num_hidden, num_observed), dtype=float)
    for h_j, observed in samples.items():
        observed = np.asarray(observed)
        for o_j in observed:
            emissions[h_j, o_j] += 1

    emissions = sklearn.preprocessing.normalize(emissions, "l1", axis=1)
    return initials, transitions, emissions


def reestimate_multivariate_gaussian(observed, hidden, lengths, covariance_type="full"):
    """
    Parameters
    ----------

    observed : np.ndarray
               The observed outputs from a presumed gaussian hmm
    hidden : np.ndarray
               The hidden/latent states from a presumed gaussian hmm

    """
    assert observed.ndim == 2
    assert util.valid_covariance(covariance_type)
    n_variates = observed.shape[-1]
    # initials is the probability of starting in a hidden state
    initials = get_initial_probabilities(hidden, lengths)
    num_hidden = initials.shape[0]
    means = np.zeros((num_hidden, n_variates))

    if covariance_type == "full":
        covariances = np.zeros((num_hidden, n_variates, n_variates))
    elif covariance_type == "tied":
        covariances = np.zeros((n_variates, n_variates))
    elif covariance_type == "diagonal":
        covariances = np.zeros((n_variates, n_variates, n_variates))
    elif covariance_type == "spherical":
        covariances = np.zeros((n_variates, n_variates, n_variates))
    else:
        assert False, "todo"

    # transitions is the transition probability from t-1 to t
    # emissions is the emission probability at time t
    samples, transitions = get_transition_probabilities(observed, hidden, lengths, num_hidden)

    for h_j, observed in samples.items():
        observed = np.asarray(observed)
        mean = np.mean(observed, axis=0)
        means[h_j] = mean

    if covariance_type == "full":
        for h_j, observed in samples.items():
            observed = np.asarray(observed)
            cov = np.cov(observed.T)
            covariances[h_j] = cov
    elif covariance_type == "tied":
        for h_j, observed in samples.items():
            observed = np.asarray(observed)
            cov = np.cov(observed.T)
            covariances += cov
        covariances /= len(samples)
    elif covariance_type == "diagonal":
        indices = np.arange(n_variates)
        for h_j, observed in samples.items():
            observed = np.asarray(observed)
            cov = np.cov(observed.T)
            covariances[h_j, indices, indices] = np.diagonal(cov)
    elif covariance_type == "spherical":
        indices = np.arange(n_variates)
        for h_j, observed in samples.items():
            observed = np.asarray(observed)
            cov = np.cov(observed.T)
            covariances[h_j, indices, indices] = np.diagonal(cov).mean()

    else:
        assert False, "TODO"
    return initials, transitions, means, covariances


def reestimate_gaussian(observed, hidden, lengths):
    """
    Parameters
    ----------

    observed : np.ndarray
               The observed outputs from a presumed gaussian hmm
    hidden : np.ndarray
               The hidden/latent states from a presumed gaussian hmm

    """

    assert observed.ndim == 2
    assert hidden.ndim == 1
    # initials is the probability of starting in a hidden state
    initials = get_initial_probabilities(hidden, lengths)
    num_hidden = initials.shape[0]
    means = np.zeros_like(initials)
    variances = np.zeros_like(initials)
    samples = collections.defaultdict(list)

    # transitions is the transition probability from t-1 to t
    # emissions is the emission probability at time t
    transitions = np.zeros((num_hidden, num_hidden), dtype=float)

    samples, transitions = get_transition_probabilities(observed, hidden, lengths, num_hidden)

    for h_j, observed in samples.items():
        mean, var = scipy.stats.norm.fit(observed)
        means[h_j] = mean
        variances[h_j] = var*var

    return initials, transitions, means, variances


def reestimate_poisson(observed, hidden, lengths):
    """
    Parameters
    ----------

    observed : np.ndarray
               The observed outputs from a presumed poisson hmm
    hidden : np.ndarray
               The hidden/latent states from a presumed poisson hmm

    """
    # initials is the probability of starting in a hidden state

    initials = get_initial_probabilities(hidden, lengths)
    num_hidden = initials.shape[0]
    means = np.zeros_like(initials)

    # transitions is the transition probability from t-1 to t
    # emissions is the emission probability at time t
    samples, transitions = get_transition_probabilities(observed, hidden, lengths, num_hidden)

    for h_j, observed in samples.items():
        mean = np.mean(observed)
        means[h_j] = mean

    return initials, transitions, means


def reestimate_gmm(observed, hidden, lengths, n_components, mixture_covariance_type, random_state=None):
    initials = get_initial_probabilities(hidden, lengths)
    num_hidden = initials.shape[0]
    samples = collections.defaultdict(list)

    # transitions is the transition probability from t-1 to t
    # emissions is the emission probability at time t
    samples, transitions = get_transition_probabilities(observed, hidden, lengths, num_hidden)
    mixture_weights = []
    mixture_means = []
    mixture_covariances = []

    for h_j, observed in sorted(samples.items()):
        observed = np.asarray(observed)
        mixture = sklearn.mixture.GaussianMixture(n_components=n_components, covariance_type=mixture_covariance_type, random_state=random_state)
        mixture.fit(observed)
        sorting = np.argsort(mixture.means_, axis=0).ravel()
        mixture_weights.append(mixture.weights_[sorting])
        mixture_means.append(mixture.means_[sorting])
        if mixture_covariance_type != "tied":
            mixture_covariances.append(mixture.covariances_[sorting])
        else:
            mixture_covariances.append(mixture.covariances_)
    return initials, transitions, np.asarray(mixture_weights), np.asarray(mixture_means), np.asarray(mixture_covariances)
