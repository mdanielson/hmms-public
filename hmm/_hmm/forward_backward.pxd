
from hmm.utilities.types cimport DOUBLE_DTYPE_t

cdef bint scaling_alpha_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:] c, DOUBLE_DTYPE_t[:, :] alpha)
cdef void log_alpha_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:] log_pi, DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:] work_buffer)

cdef void scaling_beta_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:] c, DOUBLE_DTYPE_t[:, :] beta)
cdef void log_beta_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] beta, DOUBLE_DTYPE_t[:] work_buffer)

cdef scaling_compute_digammas(DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1)
cdef log_compute_digammas(DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1)


cpdef wrapped_scaling_alpha_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:] c, DOUBLE_DTYPE_t[:, :] alpha)
cpdef wrapped_log_alpha_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:] log_pi, DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] alpha)
cpdef wrapped_scaling_beta_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:] c, DOUBLE_DTYPE_t[:, :] beta)
cpdef wrapped_log_beta_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] beta)
cpdef wrapped_scaling_compute_digammas(DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1)
cpdef wrapped_log_compute_digammas(DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1)
