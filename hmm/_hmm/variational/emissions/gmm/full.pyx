# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import warnings
import numpy as np

cimport cython
cimport numpy as np
import scipy.special
from libc.math cimport isinf, isnan, exp, log, M_PI
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE
from hmm.utilities.log_routines cimport _logsumexp
from hmm.utilities.matrix_vector cimport do_dot, do_outer_product, do_vector_matrix_dot
from hmm._hmm.util cimport multivariate_normal_density, log_multivariate_normal_density
from hmm.utilities.kl_divergences cimport kl_multivariate_normal, kl_wishart, kl_dirichlet_distribution
from ..base cimport EmissionsBase


cdef class FullGaussianEmissions(EmissionsBase):
    """

    """
    cdef public Py_ssize_t n_mixture_components_

    cdef public DOUBLE_DTYPE_t[:, :] mixture_weights_prior_
    cdef public DOUBLE_DTYPE_t[:, :] mixture_weights_posterior_
    cdef public DOUBLE_DTYPE_t[:, :] mixture_weights_normalized_
    cdef public DOUBLE_DTYPE_t[:, :] mixture_weights_lognormalized_

    cdef public DOUBLE_DTYPE_t[:, :, :] mixture_means_prior_
    cdef public DOUBLE_DTYPE_t[:, :, :] mixture_means_posterior_

    cdef public DOUBLE_DTYPE_t[:, :] mixture_beta_prior_
    cdef public DOUBLE_DTYPE_t[:, :] mixture_beta_posterior_

    cdef public DOUBLE_DTYPE_t[:, :] mixture_degrees_of_freedom_prior_
    cdef public DOUBLE_DTYPE_t[:, :] mixture_degrees_of_freedom_posterior_

    # w_0_inv and w_k_inv are "covariance"
    cdef public DOUBLE_DTYPE_t[:, :, :, :] W_0_inv_
    cdef public DOUBLE_DTYPE_t[:, :, :, :] W_k_inv_
    # w_0 and w_k are "precision"
    cdef public DOUBLE_DTYPE_t[:, :, :, :] W_k_
    cdef public DOUBLE_DTYPE_t[:, :, :, :] W_0_
    # Helpful to see these
    cdef public DOUBLE_DTYPE_t[:, :, :, :] mixture_covariances_posterior_
    cdef public DOUBLE_DTYPE_t[:, :, :, :] mixture_precisions_posterior_

    # Sufficient Statistics
    cdef DOUBLE_DTYPE_t[:, :] _q_sum
    cdef DOUBLE_DTYPE_t[:, :, :] _new_means_numerator
    cdef DOUBLE_DTYPE_t[:, :, :, :] S_ks
    # For computing likelihoods
    cdef DOUBLE_DTYPE_t[:, :] two_pi_k_det_

    # For efficient storage of intermediate quantities.
    cdef public DOUBLE_DTYPE_t[:] tmp_vector_1_
    cdef public DOUBLE_DTYPE_t[:] tmp_vector_2_
    cdef public DOUBLE_DTYPE_t[:, :] tmp_storage_1_
    cdef public DOUBLE_DTYPE_t[:, :] tmp_storage_2_


    cdef DOUBLE_DTYPE_t[:, :] _first_expectation

    cdef Py_ssize_t _n_variates
    cdef DOUBLE_DTYPE_t[:] tmp_n_variates
    cdef DOUBLE_DTYPE_t[:] tmp_n_components

    def __init__(self, DOUBLE_DTYPE_t[:, :] mixture_weights_prior, DOUBLE_DTYPE_t[:, :] mixture_weights_posterior, DOUBLE_DTYPE_t[:, :, :] mixture_means_prior, DOUBLE_DTYPE_t[:, :, :] mixture_means_posterior, DOUBLE_DTYPE_t[:, :] mixture_beta_prior, DOUBLE_DTYPE_t[:, :] mixture_beta_posterior, DOUBLE_DTYPE_t[:, :] mixture_degrees_of_freedom_prior, DOUBLE_DTYPE_t[:, :] mixture_degrees_of_freedom_posterior, DOUBLE_DTYPE_t[:, :, :, :] mixture_scale_prior, DOUBLE_DTYPE_t[:, :, :, :] mixture_scale_posterior, log_level=logging.ERROR):
        cdef:
            Py_ssize_t hidden_state, mixture_component, i, j, k
            DOUBLE_DTYPE_t mixture_sum, weight_sum

        super(FullGaussianEmissions, self).__init__(mixture_means_prior.shape[0], log_level)

        self.mixture_weights_prior_ = np.copy(mixture_weights_prior)
        self.mixture_weights_posterior_ = np.copy(mixture_weights_posterior)
        self.mixture_weights_normalized_ = np.zeros_like(mixture_weights_posterior)
        self.mixture_weights_lognormalized_ = np.zeros_like(mixture_weights_posterior)

        self.mixture_means_prior_ = np.copy(mixture_means_prior)
        self.mixture_means_posterior_  = np.copy(mixture_means_posterior)
        self.n_mixture_components_ = self.mixture_means_prior_.shape[1]

        self.mixture_beta_prior_ = np.copy(mixture_beta_prior)
        self.mixture_beta_posterior_ = np.copy(mixture_beta_posterior)

        self._n_variates = mixture_means_prior[0, 0].shape[0]
        self.mixture_degrees_of_freedom_prior_ = np.copy(mixture_degrees_of_freedom_prior)
        self.mixture_degrees_of_freedom_posterior_ = np.copy(mixture_degrees_of_freedom_posterior)

        self.W_k_ = np.zeros_like(mixture_scale_posterior)
        self.W_0_ = np.zeros_like(mixture_scale_prior)
        self.W_0_inv_ = np.copy(mixture_scale_prior)
        self.W_k_inv_ = np.copy(mixture_scale_posterior)
        for hidden_state in range(self._N):
            mixture_sum = np.sum(self.mixture_weights_posterior_[hidden_state])
            for mixture_component in range(self.n_mixture_components_):
                tmp = np.linalg.inv(mixture_scale_posterior[hidden_state, mixture_component])
                for i in range(self._n_variates):
                    for j in range(self._n_variates):
                        self.W_k_[hidden_state, mixture_component, i, j] = tmp[i, j]

                tmp = np.linalg.inv(mixture_scale_prior[hidden_state, mixture_component])
                for i in range(self._n_variates):
                    for j in range(self._n_variates):
                        self.W_0_[hidden_state, mixture_component, i, j] = tmp[i, j]
                self.mixture_weights_normalized_[hidden_state, mixture_component] = self.mixture_weights_posterior_[hidden_state, mixture_component] / mixture_sum
        self.mixture_covariances_posterior_ = np.zeros_like(mixture_scale_posterior)
        self.mixture_precisions_posterior_ = np.zeros_like(mixture_scale_posterior)

        self.two_pi_k_det_ = np.zeros((self._N, self.n_mixture_components_), dtype=DOUBLE_DTYPE)
        self.tmp_vector_1_ = np.zeros((self._n_variates, ), dtype=DOUBLE_DTYPE)
        self.tmp_vector_2_ = np.zeros((self._n_variates, ), dtype=DOUBLE_DTYPE)
        self.tmp_storage_1_ = np.zeros((self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)
        self.tmp_storage_2_ = np.zeros((self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)
        self._first_expectation = np.zeros((self._N, self.n_mixture_components_), dtype=DOUBLE_DTYPE)

        self._q_sum = np.zeros((self._N, self.n_mixture_components_ ), dtype=DOUBLE_DTYPE)
        self._new_means_numerator = np.zeros((self._N, self.n_mixture_components_, self._n_variates), dtype=DOUBLE_DTYPE)
        self.S_ks = np.zeros((self._N, self.n_mixture_components_, self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)
        self.tmp_n_variates = np.zeros(self._n_variates)
        self.tmp_n_components = np.zeros(self.n_mixture_components_)
        for hidden_state in range(self._N):
            for mixture_component in range(self.n_mixture_components_):
                for j in range(self._n_variates):
                    for k in range(self._n_variates):
                        self.mixture_precisions_posterior_[hidden_state, mixture_component, j, k] = self.W_k_[hidden_state, mixture_component, j, k] * self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component]
                        self.mixture_covariances_posterior_[hidden_state, mixture_component, j, k] = self.W_k_inv_[hidden_state,mixture_component, j, k]/self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component]

        self.update_subnormalized()
        self.log_state()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef update_subnormalized(self):
        cdef:
            Py_ssize_t hidden_state, j

        # compute posterior precision and covariance estimates
        for hidden_state in range(self._N):

            self.mixture_weights_lognormalized_ = (
                scipy.special.digamma(np.asarray(self.mixture_weights_posterior_))
                - scipy.special.digamma(np.asarray(self.mixture_weights_posterior_).sum(axis=1)[:, None])
            )

            for mixture_component in range(self.n_mixture_components_):

                # Precompute constants for determining observation probabilities
                self.two_pi_k_det_[hidden_state, mixture_component] = np.sqrt((2.0 * np.pi) ** self._n_variates * np.linalg.det(self.mixture_covariances_posterior_[hidden_state, mixture_component]))

                # in the Gruhl/ Sick paper:
                # .5 (\sum_{d=1}^{D} \digamma(\frac{dof_k + 1 - d}{2}) + D \log(2) + \log \det{W_j})
                self._first_expectation[hidden_state, mixture_component] = 0
                for j in range(1, self._n_variates+1):
                    self._first_expectation[hidden_state, mixture_component] += scipy.special.digamma(.5 * (self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component] + 1 - j))
                self._first_expectation[hidden_state, mixture_component] += self._n_variates * log(2) + log(np.linalg.det(np.asarray(self.W_k_[hidden_state, mixture_component])))
                self._first_expectation[hidden_state, mixture_component] /= 2

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef m_step(self):
        cdef:
            Py_ssize_t hidden_state, j, k

        for hidden_state in range(self._N):
            for mixture_component in range(self.n_mixture_components_):
                self.mixture_weights_posterior_[hidden_state, mixture_component] = self.mixture_weights_prior_[hidden_state, mixture_component] + self._q_sum[hidden_state, mixture_component]

                # \beta_{hidden_state} = beta_{0} + q_sum_{hidden_state}
                self.mixture_beta_posterior_[hidden_state, mixture_component] = self.mixture_beta_prior_[hidden_state, mixture_component] + self._q_sum[hidden_state, mixture_component]

                # Reestimate means as sum of prior + estimates per state
                for j in range(self._n_variates):
                    self.mixture_means_posterior_[hidden_state, mixture_component, j] = self.mixture_beta_prior_[hidden_state, mixture_component] * self.mixture_means_prior_[hidden_state, mixture_component, j]
                    self.mixture_means_posterior_[hidden_state, mixture_component, j] += self._new_means_numerator[hidden_state, mixture_component, j]
                    self.mixture_means_posterior_[hidden_state, mixture_component, j] /= self.mixture_beta_posterior_[hidden_state, mixture_component]

                # Wishart DOF is similar to mixture_beta_posterior_
                # TODO (combine?)
                self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component] = self.mixture_degrees_of_freedom_prior_[hidden_state, mixture_component] + self._q_sum[hidden_state, mixture_component]

                # Compute new W_k_inv
                # This is (means_{hidden_state}- means_0)(means_{hidden_state} - means_0)^{T}
                # expanded out
                do_outer_product(self.mixture_means_prior_[hidden_state, mixture_component], self.mixture_means_prior_[hidden_state, mixture_component], self.tmp_storage_1_)
                do_outer_product(self.mixture_means_posterior_[hidden_state, mixture_component], self.mixture_means_posterior_[hidden_state, mixture_component], self.tmp_storage_2_)

                for j in range(self._n_variates):
                    for k in range(self._n_variates):
                        self.W_k_inv_[hidden_state, mixture_component, j, k] = self.W_0_inv_[hidden_state, mixture_component, j, k] + self.S_ks[hidden_state, mixture_component, j, k] \
                                + self.mixture_beta_prior_[hidden_state, mixture_component] * self.tmp_storage_1_[j, k] \
                                - self.mixture_beta_posterior_[hidden_state, mixture_component] * self.tmp_storage_2_[j, k]
            for mixture_component in range(self.n_mixture_components_):
                weight_sum = np.sum(self.mixture_weights_posterior_[hidden_state])
                self.mixture_weights_normalized_[hidden_state, mixture_component] = self.mixture_weights_posterior_[hidden_state, mixture_component]/weight_sum
                tmp = np.linalg.inv(self.W_k_inv_[hidden_state, mixture_component])
                for j in range(self._n_variates):
                    for k in range(self._n_variates):
                        self.W_k_[hidden_state, mixture_component, j, k] = tmp[j,k]
                        self.mixture_precisions_posterior_[hidden_state, mixture_component, j, k] = self.W_k_[hidden_state, mixture_component, j, k] * self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component]
                        self.mixture_covariances_posterior_[hidden_state, mixture_component, j, k] = self.W_k_inv_[hidden_state,mixture_component, j, k]/self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component]

        # Reset all intermediate bits to 0
        self._q_sum[:] = 0
        self._new_means_numerator[:] = 0
        self.S_ks[:] = 0
        self.tmp_storage_1_[:] = 0
        self.tmp_storage_2_[:] = 0
        #print("MV DONE")

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight):
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t hidden_state, mixture_component, t, l, m
            DOUBLE_DTYPE_t gamma_denom

        for t in range(observed.shape[0]):
            for hidden_state in range(self._N):
                # Normalize component densities
                self.individual_component_subnormalized_log_density_for(hidden_state, observed[t], self.tmp_n_components)
                gamma_denom = _logsumexp(self.tmp_n_components)
                for mixture_component in range(self.n_mixture_components_):
                    self.tmp_n_components[mixture_component] = exp(self.tmp_n_components[mixture_component] - gamma_denom)

                #print(np.asarray(self.tmp_n_components))
                for mixture_component in range(self.n_mixture_components_):
                    #  Now we have the relative proportion that the observation came from this component
                    gamma = digamma_1[t, hidden_state] * self.tmp_n_components[mixture_component]

                    self._q_sum[hidden_state, mixture_component] += weight * gamma
                    for l in range(self._n_variates):
                        self._new_means_numerator[hidden_state, mixture_component, l] += weight * gamma * observed[t, l]

                    for l in range(self._n_variates):
                        for m in range(self._n_variates):
                            self.S_ks[hidden_state, mixture_component, l, m] += weight * gamma * observed[t, l] * observed[t, m]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef void individual_component_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state, DOUBLE_DTYPE_t[:] output) :
        """
        Get the density of the of the observed state given by the hidden_state for a specific component

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            Py_ssize_t mixture_component

        for mixture_component in range(self.n_mixture_components_):
            output[mixture_component] = self.mixture_weights_normalized_[hidden_state, mixture_component] *  multivariate_normal_density(
                observed_state,
                self.mixture_means_posterior_[hidden_state, mixture_component],
                self.mixture_precisions_posterior_[hidden_state, mixture_component],
                self.two_pi_k_det_[hidden_state, mixture_component],
                self.tmp_n_variates

            )

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef void individual_component_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state, DOUBLE_DTYPE_t[:] output) nogil:
        """
        Get the density of the of the observed state given by the hidden_state for a specific component

        Parameters
        ----------
        hidden_state : Py_ssize_t
        observed_state : DOUBLE_DTYPE_t TODO: Fused Type Jazz
        """
        cdef:
            Py_ssize_t mixture_component

        for mixture_component in range(self.n_mixture_components_):
            output[mixture_component] = log_multivariate_normal_density(
                observed_state,
                self.mixture_means_posterior_[hidden_state, mixture_component],
                self.mixture_precisions_posterior_[hidden_state, mixture_component],
                log(self.two_pi_k_det_[hidden_state, mixture_component]),
                self.tmp_n_variates

            )

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            DOUBLE_DTYPE_t answer = 0
            Py_ssize_t mixture_component

        self.individual_component_density_for(hidden_state, observed_state, self.tmp_n_components)
        for mixture_component in range(self.n_mixture_components_):
            ## TODO WEIGHTS
            answer += self.tmp_n_components[mixture_component]
        return answer

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        ans = exp(self.subnormalized_log_density_for(hidden_state, observed_state))
        return ans

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        return log(self.normalized_density_for(hidden_state, observed_state))

    cdef void individual_component_subnormalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state, DOUBLE_DTYPE_t[:] output):
        cdef:
            DOUBLE_DTYPE_t component_ans, tmp4, tmp5, first_expectation, second_expectation, middle_term
            Py_ssize_t mixture_component

        for mixture_component in range(self.n_mixture_components_):
            # Use cached values
            first_expectation = self._first_expectation[hidden_state, mixture_component]

            # This is a constant, so not truely needed...?
            middle_term = 0#self._n_variates *log(2 * M_PI ) / 2

            # Second expectation
            tmp4 = self._n_variates / self.mixture_beta_posterior_[hidden_state, mixture_component]

            # DOF * np.dot((X - mean) W np.dot(X-mean)
            # Compute vector difference
            for i in range(self._n_variates):
                self.tmp_vector_2_[i] = observed_state[i] - self.mixture_means_posterior_[hidden_state, mixture_component, i]

            do_vector_matrix_dot(self.tmp_vector_2_, self.W_k_[hidden_state, mixture_component], self.tmp_vector_1_)
            tmp5 = do_dot(self.tmp_vector_1_, self.tmp_vector_2_)
            tmp5 *= self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component]
            second_expectation = .5 * (tmp4 + tmp5)
            component_ans = first_expectation - middle_term  - second_expectation
            ## TODO: Multiply by component weight!!!
            output[mixture_component] = self.mixture_weights_lognormalized_[hidden_state, mixture_component] + component_ans

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            DOUBLE_DTYPE_t final_ans

        self.individual_component_subnormalized_log_density_for(hidden_state, observed_state, self.tmp_n_components)
        final_ans = _logsumexp(self.tmp_n_components)
        #print("MV", hidden_state, observed_state[0], final_ans, self.normalized_log_density_for(hidden_state, observed_state))
        return final_ans

    cdef sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        random_state :  np.random_state
        """
        cdef:
            Py_ssize_t i, component
            DOUBLE_DTYPE_t rand
            DOUBLE_DTYPE_t[:] cumsum = np.cumsum(self.mixture_weights_normalized_[hidden_state])
            # first choose a mixture component
        rand = random_state.rand()
        for i in range(self.n_mixture_components_):
            if cumsum[i] > rand:
                component = i
                break
        # The generate from distribution
        return random_state.multivariate_normal(self.mixture_means_posterior_[hidden_state][component], self.mixture_covariances_posterior_[hidden_state][component])

    def __str__(self):
        return "Not Implemented"

    cdef log_state(self):
        cdef:
            Py_ssize_t i

        for i in range(self._N):
            self._logger.debug("emissions[{}] weights={}, mean={} bp={} dof={} cov={}, prec={} W_k={} W_k_inv_={}".format(i, np.asarray(self.mixture_weights_posterior_[i]).tolist(), np.asarray(self.mixture_means_posterior_[i]).tolist(), np.asarray(self.mixture_beta_posterior_[i]).tolist(), np.asarray(self.mixture_degrees_of_freedom_posterior_[i]).tolist(), np.asarray(self.mixture_covariances_posterior_[i]).tolist(), np.asarray(self.mixture_precisions_posterior_[i]).tolist(), np.asarray(self.W_k_[i]).tolist(), np.asarray(self.W_k_inv_[i]).tolist()))

    cdef DOUBLE_DTYPE_t free_energy(self):
        cdef:
            Py_ssize_t hidden_state, mixture_component
            DOUBLE_DTYPE_t answer, kln, klw, kld
        answer = 0
        for hidden_state in range(self._N):

            kld = kl_dirichlet_distribution(self.mixture_weights_posterior_[hidden_state], self.mixture_weights_prior_[hidden_state])
            answer -= kld

            ## TODO: KL DIRICHLET
            for mixture_component in range(self.n_mixture_components_):
                # Compute precision for Normal KL Divergence
                term1 = np.linalg.inv(self.mixture_beta_posterior_[hidden_state, mixture_component] * np.asarray(self.mixture_precisions_posterior_[hidden_state, mixture_component]))
                term2 = np.linalg.inv(self.mixture_beta_prior_[hidden_state, mixture_component] * np.asarray(self.mixture_precisions_posterior_[hidden_state, mixture_component]))
                kln = kl_multivariate_normal(self.mixture_means_posterior_[hidden_state, mixture_component], term1, self.mixture_means_prior_[hidden_state, mixture_component], term2)
                # Wishart KL Divergence
                klw = kl_wishart(self.mixture_degrees_of_freedom_posterior_[hidden_state, mixture_component], self.W_k_inv_[hidden_state, mixture_component], self.mixture_degrees_of_freedom_prior_[hidden_state, mixture_component], self.W_0_inv_[hidden_state, mixture_component])
                answer -= kln
                answer -= klw

        return answer
