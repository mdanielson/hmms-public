# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import os.path


def configuration(parent_package="", top_path=None):
    if os.path.exists('MANIFEST'):
        os.remove('MANIFEST')

    from numpy.distutils.misc_util import Configuration
    import numpy

    config = Configuration("gmm", parent_package, top_path)
    libraries = []
    if os.name == 'posix':
        libraries.append('m')
    config.add_extension("full",
                         sources=["full.pyx"],
                         include_dirs=[numpy.get_include()],
                         libraries=libraries,
                         extra_compile_args=["-O3"]
                         )

    config.add_extension("tied",
                         sources=["tied.pyx"],
                         include_dirs=[numpy.get_include()],
                         libraries=libraries,
                         extra_compile_args=["-O3"]
                         )

    return config


if __name__ == "__main__":
    from numpy.distutils.core import setup
    setup(**configuration().todict())
