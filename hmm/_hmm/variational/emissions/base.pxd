
# cython: language_level=3

cimport cython
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from libc.math cimport NAN

cdef class EmissionsBase:
    cdef public Py_ssize_t _N
    cdef object _logger

    cdef initialize_counts(self, const DOUBLE_DTYPE_t[:, :, :] observed)
    cdef update_subnormalized(self)
    cdef m_step(self)
    cdef accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight)
    cpdef DOUBLE_DTYPE_t normalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state)
    cpdef DOUBLE_DTYPE_t subnormalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state)
    cpdef DOUBLE_DTYPE_t normalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state)
    cpdef DOUBLE_DTYPE_t subnormalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state)
    cpdef DOUBLE_DTYPE_t dic_contribution(self, DOUBLE_DTYPE_t[:, :] digamma_1) except NAN
    cdef DOUBLE_DTYPE_t free_energy(self)
    cdef sample(self, Py_ssize_t hidden_state, object random_state)
    cdef log_state(self)
    cdef set_log_level(self, log_level)
