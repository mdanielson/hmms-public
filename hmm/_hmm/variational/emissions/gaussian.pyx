# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import numpy as np
import scipy.special

cimport cython
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE
from libc.math cimport log, exp, sqrt, isnan, NAN, M_PI

from .base cimport EmissionsBase
from hmm.utilities.kl_divergences cimport kl_normal_distribution, kl_gamma_distribution

cdef class GaussianEmissions(EmissionsBase):
    """
    """

    cdef public DOUBLE_DTYPE_t[:, :] _next_B_statistics
    cdef public DOUBLE_DTYPE_t[:] _means_prior
    cdef public DOUBLE_DTYPE_t[:] _means_posterior
    cdef public DOUBLE_DTYPE_t[:] _beta_prior
    cdef public DOUBLE_DTYPE_t[:] _beta_posterior
    cdef public DOUBLE_DTYPE_t[:] _a_prior
    cdef public DOUBLE_DTYPE_t[:] _a_posterior
    cdef public DOUBLE_DTYPE_t[:] _b_prior
    cdef public DOUBLE_DTYPE_t[:] _b_posterior

    cdef public DOUBLE_DTYPE_t[:] _variances_posterior
    cdef public DOUBLE_DTYPE_t[:] _variances_prior

    cdef DOUBLE_DTYPE_t[:] _q_sum
    cdef DOUBLE_DTYPE_t[:] _new_means_numerator
    cdef DOUBLE_DTYPE_t[:] _new_variances_obs_2
    cdef DOUBLE_DTYPE_t[:] _digamma_a


    cdef DOUBLE_DTYPE_t[:] divisor1_
    cdef DOUBLE_DTYPE_t[:] divisor2_
    cdef DOUBLE_DTYPE_t[:] log_divisor2_

    def __init__(self, DOUBLE_DTYPE_t[:] means_prior, DOUBLE_DTYPE_t[:] means_posterior,
                 DOUBLE_DTYPE_t[:] beta_prior, DOUBLE_DTYPE_t[:] beta_posterior,
                 DOUBLE_DTYPE_t[:] a_prior, DOUBLE_DTYPE_t[:] a_posterior,
                 DOUBLE_DTYPE_t[:] b_prior, DOUBLE_DTYPE_t[:] b_posterior,
                 log_level=logging.ERROR):
        cdef:
            Py_ssize_t hidden_state
        super(GaussianEmissions, self).__init__(means_prior.shape[0], log_level)

        self._means_prior = np.copy(means_prior)
        self._means_posterior = np.copy(means_posterior)
        self._beta_prior = np.copy(beta_prior)
        self._beta_posterior = np.copy(beta_posterior)
        self._a_prior = np.copy(a_prior)
        self._a_posterior = np.copy(a_posterior)
        self._b_prior = np.copy(b_prior)
        self._b_posterior = np.copy(b_posterior)

        self._variances_posterior = np.zeros_like(self._means_prior, dtype=DOUBLE_DTYPE)
        self._variances_prior = np.zeros_like(self._means_prior, dtype=DOUBLE_DTYPE)
        for hidden_state in range(self._N):
            self._variances_prior[hidden_state] = (self._b_prior[hidden_state] / self._a_prior[hidden_state])
            self._variances_posterior[hidden_state] = (self._b_posterior[hidden_state] / self._a_posterior[hidden_state])

        self._q_sum = np.zeros_like(self._means_prior, dtype=DOUBLE_DTYPE)
        self._new_means_numerator = np.zeros_like(self._means_prior, dtype=DOUBLE_DTYPE)
        self._new_variances_obs_2 = np.zeros_like(self._means_prior, dtype=DOUBLE_DTYPE)
        self._digamma_a = np.zeros_like(self._means_prior, dtype=DOUBLE_DTYPE)

        self.divisor1_ = np.zeros_like(self._means_prior)
        self.divisor2_ = np.zeros_like(self._means_prior)
        self.log_divisor2_ = np.zeros_like(self._means_prior)

        assert self._N == self._means_prior.shape[0]
        assert self._N == self._means_posterior.shape[0]
        self.log_state()

    cdef update_subnormalized(self):
        cdef:
            Py_ssize_t i
        for i in range(self._N):
            self._digamma_a[i] = .5 * scipy.special.digamma(.5 * self._a_posterior[i])

            #print("vp", self._variances_posterior[i])
        for i in range(self._N):
            self.divisor1_[i] = 2 * self._variances_posterior[i]
            self.divisor2_[i] = 1. / sqrt(M_PI * self.divisor1_[i])
            self.log_divisor2_[i] = log(1. / sqrt(M_PI * self.divisor1_[i]))

        #for i in range(self._N):
        #    print("G", np.asarray(self._means_posterior[i]))
        #    print("G", np.asarray(self._variances_posterior[i]))
    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef m_step(self):
        cdef:
            Py_ssize_t j

        for j in range(self._N):
            #print("means_post q", np.asarray(self._q_sum[j]))
            self._beta_posterior[j] = self._beta_prior[j] + self._q_sum[j]

            self._means_posterior[j] = self._beta_prior[j] * self._means_prior[j]
            self._means_posterior[j] += self._new_means_numerator[j]
            self._means_posterior[j] /= self._beta_posterior[j]
            #print("means_post", self._beta_prior[j], self._means_prior[j], self._new_means_numerator[j], self._beta_posterior[j], self._means_posterior[j])

            self._a_posterior[j] = self._a_prior[j] + self._q_sum[j]
            self._b_posterior[j] = self._b_prior[j]
            self._b_posterior[j] += self._new_variances_obs_2[j]
            self._b_posterior[j] += self._beta_prior[j] * self._means_prior[j] * self._means_prior[j] - self._beta_posterior[j] * self._means_posterior[j] * self._means_posterior[j]

            self._variances_posterior[j] = (self._b_posterior[j] / self._a_posterior[j])
            #print("varsu", self._a_prior[j], self._a_posterior[j])
            #print("varsu", self._b_posterior[j])
            assert self._b_posterior[j] >= 0, (j, self._b_posterior[j])
            assert self._a_posterior[j] >= 0, self._a_posterior[j]
            assert not isnan(self._b_posterior[j]), self._b_posterior[j]
            assert not isnan(self._a_posterior[j]), self._a_posterior[j]

        self._q_sum[:] = 0
        self._new_means_numerator[:] = 0
        self._new_variances_obs_2[:] = 0
        #print("G DONE")

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight):
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t hidden_state, t
        for hidden_state in range(self._N):
            for t in range(observed.shape[0]):
                assert not isnan(digamma_1[t, hidden_state]), (t,hidden_state)
                self._q_sum[hidden_state] += weight * digamma_1[t, hidden_state]
                self._new_means_numerator[hidden_state] += weight * digamma_1[t, hidden_state] * observed[t, 0]
                self._new_variances_obs_2[hidden_state] += weight * digamma_1[t, hidden_state] * observed[t, 0] * observed[t, 0]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            DOUBLE_DTYPE_t diff = (observed_state[0] - self._means_posterior[hidden_state])
        #assert observed_state.shape[0] == 1
        return self.divisor2_[hidden_state] * exp( -(diff**2)/self.divisor1_[hidden_state])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        ans = exp(self.subnormalized_log_density_for(hidden_state, observed_state))
        return ans

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            DOUBLE_DTYPE_t diff = (observed_state[0] - self._means_posterior[hidden_state])
        #assert observed_state.shape[0] == 1
        return self.log_divisor2_[hidden_state]  - (diff**2) / self.divisor1_[hidden_state]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            DOUBLE_DTYPE_t observed = observed_state[0]
            DOUBLE_DTYPE_t ans, tmp1, tmp2, tmp3

        tmp1 = .5 * log(.5 * self._b_posterior[hidden_state])
        tmp2 = .5 * (self._a_posterior[hidden_state]/self._b_posterior[hidden_state]) * (observed - self._means_posterior[hidden_state])**2
        tmp3 = 1 / ( 2 * self._beta_posterior[hidden_state])
        ans = self._digamma_a[hidden_state] - tmp1 - tmp2 - tmp3
        # first_expectation = self._digamma_a[hidden_state] - tmp1
        # second_expectation = -tmp2 - tmp3
        # print("G", hidden_state, observed_state[0], self._digamma_a[hidden_state],  tmp1, first_expectation, second_expectation, ans, self.normalized_log_density_for(hidden_state, observed_state))
        return ans

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        """

        return [random_state.normal(self._means_posterior[hidden_state], sqrt(self._variances_posterior[hidden_state]))]

    cdef DOUBLE_DTYPE_t free_energy(self):
        """
        Negative KL Divergence.
        Note: Our a distribution is parameterized for b/2, a/2, so carry this over
        """
        cdef:
            Py_ssize_t i
            DOUBLE_DTYPE_t tmp, tau
        tmp = 0
        for i in range(self._N):
            tau = (self._a_posterior[i]/self._b_posterior[i])
            tmp -= kl_normal_distribution(self._means_posterior[i], 1/(self._beta_posterior[i] * tau ), self._means_prior[i], 1/(self._beta_prior[i] * tau))
            tmp -= kl_gamma_distribution(self._a_posterior[i]/2, self._b_posterior[i]/2, self._a_prior[i]/2, self._b_prior[i]/2)

        return tmp

    def __str__(self):
        return str(np.asarray(self._B).tolist())

    cdef log_state(self):
        cdef:
            Py_ssize_t j
        for j in range(self._N):
            self._logger.info(
                "emissions[{}] mean={} beta_posterior={} a_={} b={} var={:.9f} dg={:.9f}".format(j, self._means_posterior[j], self._beta_posterior[j], self._a_posterior[j], self._b_posterior[j], self._variances_posterior[j], self._digamma_a[j])
            )

    cpdef DOUBLE_DTYPE_t dic_contribution(self, DOUBLE_DTYPE_t[:, :] digamma_1) except NAN:
        cdef:
            Py_ssize_t hidden_state, t
            DOUBLE_DTYPE_t result = 0
        assert digamma_1.shape[1] == self._N
        for t in range(digamma_1.shape[0]):
            for hidden_state in range(self._N):
                result += digamma_1[t, hidden_state] * (
                    log(self._a_posterior[hidden_state]/self._b_posterior[hidden_state])
                    - scipy.special.digamma(self._a_posterior[hidden_state]/2)
                    + log(self._b_posterior[hidden_state]/2)
                    + 1 / self._beta_posterior[hidden_state]
                )
        return result
