# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import numpy as np
import scipy.special

cimport cython
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from libc.math cimport exp, log, INFINITY, NAN

from .base cimport EmissionsBase
from hmm.utilities.kl_divergences cimport kl_dirichlet_distribution
from hmm.utilities.normalize cimport normalize_2d

cdef class CategoricalEmissions(EmissionsBase):
    """
    """
    @property
    def B_posterior(self):
        return np.asarray(self._B_posterior)

    @property
    def B_normalized(self):
        return np.asarray(self._B_normalized)

    @property
    def B_subnormalized(self):
        return np.asarray(self._B_subnormalized)

    cdef public Py_ssize_t _M
    cdef public DOUBLE_DTYPE_t[:, :] _next_B_statistics
    cdef public DOUBLE_DTYPE_t[:, :] _B_prior
    cdef public DOUBLE_DTYPE_t[:, :] _B_subnormalized
    cdef public DOUBLE_DTYPE_t[:, :] _log_B_subnormalized
    cdef public DOUBLE_DTYPE_t[:, :] _B_normalized
    cdef public DOUBLE_DTYPE_t[:, :] _B_posterior

    def __init__(self, DOUBLE_DTYPE_t[:, :] B_posterior, DOUBLE_DTYPE_t[:, :] B_prior, log_level=logging.ERROR):
        super(CategoricalEmissions, self).__init__(B_prior.shape[0], log_level)

        self._B_prior = np.copy(B_prior)
        self._next_B_statistics = np.zeros_like(self._B_prior)
        self._B_posterior = np.copy(B_posterior)
        self._B_subnormalized = np.zeros_like(self._B_posterior)
        self._log_B_subnormalized = np.zeros_like(self._B_posterior)
        self._B_normalized = np.zeros_like(self._B_posterior)
        self._M = self._B_posterior.shape[1]

    cdef update_subnormalized(self):
        cdef:
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] posterior = np.asarray(self._B_posterior)
            Py_ssize_t i, m

        normalize_2d(self._B_posterior, self._B_normalized)
        self._log_B_subnormalized = (
            scipy.special.digamma(posterior)
                   - scipy.special.digamma(posterior.sum(axis=1)[:, None])
        )

        for i in range(self._N):
            for m in range(self._M):
                self._B_subnormalized[i, m] = exp(self._log_B_subnormalized[i, m])

    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef m_step(self):
        cdef:
            Py_ssize_t i, j

        self._B_posterior[:] = 0
        for i in range(self._N):
            for j in range(self._M):
                self._B_posterior[i, j] = self._B_prior[i, j] + self._next_B_statistics[i, j]
        self._next_B_statistics[:] = 0

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight):
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t i, j, t
        for i in range(self._N):
            for t in range(observed.shape[0]):
                j = int(observed[t, 0])
                self._next_B_statistics[i, j] += weight * digamma_1[t, i]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            Py_ssize_t observed
        assert observed_state.shape[0] == 1
        observed = <Py_ssize_t> int(observed_state[0])
        return  self._B_normalized[hidden_state, observed]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            Py_ssize_t observed
        assert observed_state.shape[0] == 1
        observed = <Py_ssize_t> int(observed_state[0])
        return  self._B_subnormalized[hidden_state, observed]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        assert observed_state.shape[0] == 1
        return log(self.normalized_density_for(hidden_state, observed_state))

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            Py_ssize_t observed
        assert observed_state.shape[0] == 1
        observed = <Py_ssize_t> int(observed_state[0])
        return self._log_B_subnormalized[hidden_state, observed]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        """

        cdef:
            DOUBLE_DTYPE_t[:] B_cdf
            DOUBLE_DTYPE_t rand
            Py_ssize_t i
        B_cdf = np.cumsum(self._B_normalized[hidden_state, :])
        rand = random_state.rand()
        for i in range(B_cdf.shape[0]):
            if B_cdf[i] > rand:
                return [i]

    cdef DOUBLE_DTYPE_t free_energy(self):
        """
        Negative KL divergence
        """
        cdef:
            Py_ssize_t i
            DOUBLE_DTYPE_t tmp
        tmp = 0
        for i in range(self._N):
            tmp -= kl_dirichlet_distribution(np.asarray(self._B_posterior[i]), np.asarray(self._B_prior[i]))
        return tmp

    def __str__(self):
        return str(np.asarray(self._B).tolist())

    cdef log_state(self):
        cdef:
            Py_ssize_t j
        for j in range(self._B_posterior.shape[0]):
            self._logger.info("B_posterior[{}, :]={}".format(j, np.asarray(self._B_posterior[j, :]).tolist()))
        for j in range(self._B_posterior.shape[0]):
            self._logger.info("B_normalized[{}, :]={}".format(j, np.asarray(self._B_normalized[j, :]).tolist()))

    cpdef DOUBLE_DTYPE_t dic_contribution(self, DOUBLE_DTYPE_t[:, :] digamma_1) except NAN:
        return -INFINITY
