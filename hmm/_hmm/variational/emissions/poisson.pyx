# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import numpy as np
import scipy.special

cimport cython
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE
from libc.math cimport log, exp, sqrt, isnan, NAN, M_PI, INFINITY

from .base cimport EmissionsBase
from hmm.utilities.kl_divergences cimport kl_gamma_distribution

cdef class PoissonEmissions(EmissionsBase):
    """
    """

    cdef public DOUBLE_DTYPE_t[:, :] _next_B_statistics
    cdef public DOUBLE_DTYPE_t[:] _rates_prior
    cdef public DOUBLE_DTYPE_t[:] _rates_posterior
    cdef public DOUBLE_DTYPE_t[:] _a_prior
    cdef public DOUBLE_DTYPE_t[:] _a_posterior
    cdef public DOUBLE_DTYPE_t[:] _b_prior
    cdef public DOUBLE_DTYPE_t[:] _b_posterior

    cdef public DOUBLE_DTYPE_t[:] _variances_posterior
    cdef public DOUBLE_DTYPE_t[:] _variances_prior

    cdef DOUBLE_DTYPE_t[:] _q_sum
    cdef DOUBLE_DTYPE_t[:] _new_rates_numerator
    cdef DOUBLE_DTYPE_t[:] _dia_b

    def __init__(self, DOUBLE_DTYPE_t[:] rates_prior, DOUBLE_DTYPE_t[:] rates_posterior,
                 DOUBLE_DTYPE_t[:] a_prior, DOUBLE_DTYPE_t[:] a_posterior,
                 DOUBLE_DTYPE_t[:] b_prior, DOUBLE_DTYPE_t[:] b_posterior,
                 log_level=logging.ERROR):
        super(PoissonEmissions, self).__init__(rates_prior.shape[0], log_level)

        self._rates_prior = np.array(rates_prior)
        self._rates_posterior = np.array(rates_posterior)
        self._b_prior = np.array(b_prior)
        self._b_posterior = np.array(b_posterior)
        self._a_prior = np.array(a_prior)
        self._a_posterior = np.array(a_posterior)

        self._q_sum = np.zeros_like(self._rates_prior, dtype=DOUBLE_DTYPE)
        self._new_rates_numerator = np.zeros_like(self._rates_prior, dtype=DOUBLE_DTYPE)
        self._dia_b = np.zeros_like(self._rates_prior, dtype=DOUBLE_DTYPE)


        assert self._N == self._rates_prior.shape[0]
        assert self._N == self._rates_posterior.shape[0]

    cdef update_subnormalized(self):
        cdef:
            Py_ssize_t i
        for i in range(self._N):
            self._dia_b[i] = scipy.special.digamma(self._a_posterior[i]) - log(self._b_posterior[i])

    @cython.boundscheck(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef m_step(self):
        cdef:
            Py_ssize_t j

        for j in range(self._N):
            self._a_posterior[j] = self._a_prior[j] + self._new_rates_numerator[j]
            self._b_posterior[j] = self._b_prior[j] + self._q_sum[j]
            self._rates_posterior[j] = self._a_posterior[j] / self._b_posterior[j]

        self._q_sum[:] = 0
        self._new_rates_numerator[:] = 0

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight):
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t hidden_state, t
        for hidden_state in range(self._N):
            for t in range(observed.shape[0]):
                self._q_sum[hidden_state] += weight * digamma_1[t, hidden_state]
                self._new_rates_numerator[hidden_state] += weight * digamma_1[t, hidden_state] * observed[t, 0]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            DOUBLE_DTYPE_t d = exp(-self._rates_posterior[hidden_state])
            DOUBLE_DTYPE_t mean = self._rates_posterior[hidden_state]
            Py_ssize_t j, i

        assert observed_state.shape[0] == 1
        j = <Py_ssize_t> observed_state[0] + 1
        for i in range(1, j):
            d *= mean
            d /= i
        return d

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        return log(self.normalized_density_for(hidden_state, observed_state))

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            Py_ssize_t observed = <Py_ssize_t> observed_state[0]
            DOUBLE_DTYPE_t mean1 = exp(self._dia_b[hidden_state])
            DOUBLE_DTYPE_t d = exp(-self._rates_posterior[hidden_state])
            Py_ssize_t i

        for i in range(1, observed+1):
            d *= mean1
            d /= i
        return d

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        return log(self.subnormalized_density_for(hidden_state, observed_state))

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        """

        return [random_state.poisson(self._rates_posterior[hidden_state])]

    cdef DOUBLE_DTYPE_t free_energy(self):
        """
        Negative KL Divergence.
        Note: Our gamma distribution is parameterized for delta/2, gamma/2, so carry this over
        """
        cdef:
            Py_ssize_t i
            DOUBLE_DTYPE_t tmp
        tmp = 0
        for i in range(self._N):
            tmp -= kl_gamma_distribution(self._a_posterior[i], self._b_posterior[i], self._a_prior[i], self._b_prior[i])
        return tmp

    def __str__(self):
        return str(np.asarray(self._B).tolist())

    cdef log_state(self):
        cdef:
            Py_ssize_t j
        for j in range(self._N):
            self._logger.info(
                "B[{}] rate={} a={} b={}".format(j, self._rates_posterior[j], self._a_posterior[j], self._b_posterior[j])
            )

    cpdef DOUBLE_DTYPE_t dic_contribution(self, DOUBLE_DTYPE_t[:, :] digamma_1) except NAN:
        return -INFINITY
