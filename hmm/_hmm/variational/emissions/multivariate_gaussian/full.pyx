#  Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
import warnings
import numpy as np

cimport cython
cimport numpy as np
import scipy.special
from libc.math cimport isinf, isnan, exp, log, M_PI
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE
from hmm.utilities.matrix_vector cimport do_dot, do_outer_product, do_vector_matrix_dot
from hmm._hmm.util cimport multivariate_normal_density, log_multivariate_normal_density
from hmm.utilities.kl_divergences cimport kl_multivariate_normal, kl_wishart
from ..base cimport EmissionsBase


cdef class FullGaussianEmissions(EmissionsBase):
    """

    """
    cdef public DOUBLE_DTYPE_t[:, :] means_prior_
    cdef public DOUBLE_DTYPE_t[:, :] means_posterior_

    cdef public DOUBLE_DTYPE_t[:] beta_prior_
    cdef public DOUBLE_DTYPE_t[:] beta_posterior_

    cdef public DOUBLE_DTYPE_t[:] degrees_of_freedom_prior_
    cdef public DOUBLE_DTYPE_t[:] degrees_of_freedom_posterior_

    # w_0_inv and w_k_inv are "covariance"
    cdef public DOUBLE_DTYPE_t[:, :, :] W_0_inv_
    cdef public DOUBLE_DTYPE_t[:, :, :] W_k_inv_
    # w_0 and w_k are "precision"
    cdef public DOUBLE_DTYPE_t[:, :, :] W_k_
    cdef public DOUBLE_DTYPE_t[:, :, :] W_0_
    # Helpful to see these
    cdef public DOUBLE_DTYPE_t[:, :, :] covariances_posterior_
    cdef public DOUBLE_DTYPE_t[:, :, :] precisions_posterior_

    # Sufficient Statistics
    cdef DOUBLE_DTYPE_t[:] _q_sum
    cdef DOUBLE_DTYPE_t[:, :] _new_means_numerator
    cdef DOUBLE_DTYPE_t[:, :, :] S_ks
    # For computing likelihoods
    cdef DOUBLE_DTYPE_t[:] two_pi_k_det_

    # For efficient storage of intermediate quantities.
    cdef public DOUBLE_DTYPE_t[:] tmp_vector_1_
    cdef public DOUBLE_DTYPE_t[:] tmp_vector_2_
    cdef public DOUBLE_DTYPE_t[:, :] tmp_storage_1_
    cdef public DOUBLE_DTYPE_t[:, :] tmp_storage_2_


    cdef DOUBLE_DTYPE_t[:] _first_expectation

    cdef Py_ssize_t _n_variates
    cdef DOUBLE_DTYPE_t[:] density_tmp

    def __init__(self, DOUBLE_DTYPE_t[:, :] means_prior, DOUBLE_DTYPE_t[:, :] means_posterior, DOUBLE_DTYPE_t[:] beta_prior, DOUBLE_DTYPE_t[:] beta_posterior, DOUBLE_DTYPE_t[:] degrees_of_freedom_prior, DOUBLE_DTYPE_t[:] degrees_of_freedom_posterior, DOUBLE_DTYPE_t[:, :, :] scale_prior, DOUBLE_DTYPE_t[:, :, :] scale_posterior, log_level=logging.ERROR):
        super(FullGaussianEmissions, self).__init__(means_prior.shape[0], log_level)
        self.means_prior_ = np.copy(means_prior)
        self.means_posterior_  = np.copy(means_posterior)

        self.beta_prior_ = np.copy(beta_prior)
        self.beta_posterior_ = np.copy(beta_posterior)

        self._n_variates = means_prior[0].shape[0]
        self.degrees_of_freedom_prior_ = np.copy(degrees_of_freedom_prior)
        self.degrees_of_freedom_posterior_ = np.copy(degrees_of_freedom_posterior)

        self.W_0_inv_ = np.copy(scale_prior)
        self.W_k_inv_ = np.copy(scale_posterior)
        self.W_k_ = np.linalg.inv(scale_posterior)
        self.W_0_ = np.linalg.inv(scale_prior)
        self.covariances_posterior_ = np.zeros_like(scale_posterior)
        self.precisions_posterior_ = np.zeros_like(scale_posterior)

        self.two_pi_k_det_ = np.zeros((self._N, ), dtype=DOUBLE_DTYPE)
        self.tmp_vector_1_ = np.zeros((self._n_variates, ), dtype=DOUBLE_DTYPE)
        self.tmp_vector_2_ = np.zeros((self._n_variates, ), dtype=DOUBLE_DTYPE)
        self.tmp_storage_1_ = np.zeros((self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)
        self.tmp_storage_2_ = np.zeros((self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)
        self._first_expectation = np.zeros((self._N,), dtype=DOUBLE_DTYPE)

        self._q_sum = np.zeros((self._N, ), dtype=DOUBLE_DTYPE)
        self._new_means_numerator = np.zeros((self._N, self._n_variates), dtype=DOUBLE_DTYPE)
        self.S_ks = np.zeros((self._N, self._n_variates, self._n_variates), dtype=DOUBLE_DTYPE)
        self.density_tmp = np.zeros(self._n_variates)

        for hidden_state in range(self._N):
            for j in range(self._n_variates):
                for k in range(self._n_variates):
                    self.precisions_posterior_[hidden_state,  j, k] = self.W_k_[hidden_state,  j, k] * self.degrees_of_freedom_posterior_[hidden_state]
                    self.covariances_posterior_[hidden_state,  j, k] = self.W_k_inv_[hidden_state, j, k]/self.degrees_of_freedom_posterior_[hidden_state]
        self.update_subnormalized()
        self.log_state()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef update_subnormalized(self):
        cdef:
            Py_ssize_t hidden_state, j
        # compute posterior precision and covariance estimates
        for hidden_state in range(self._N):

            # Precompute constants for determining observation probabilities
            self.two_pi_k_det_[hidden_state] = np.sqrt((2.0 * np.pi) ** self._n_variates * np.linalg.det(self.covariances_posterior_[hidden_state]))

            # in the Gruhl/ Sick paper:
            # .5 (\sum_{d=1}^{D} \digamma(\frac{dof_k + 1 - d}{2}) + D \log(2) + \log \det{W_j})
            self._first_expectation[hidden_state] = 0
            for j in range(1, self._n_variates+1):
                self._first_expectation[hidden_state] += scipy.special.digamma(.5 * (self.degrees_of_freedom_posterior_[hidden_state] + 1 - j))
            self._first_expectation[hidden_state] += self._n_variates * log(2) + log(np.linalg.det(np.asarray(self.W_k_[hidden_state])))
            self._first_expectation[hidden_state] /= 2

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef m_step(self):
        cdef:
            Py_ssize_t hidden_state, j, k

        for hidden_state in range(self._N):
            # \beta_{hidden_state} = beta_{0} + q_sum_{hidden_state}
            self.beta_posterior_[hidden_state] = self.beta_prior_[hidden_state] + self._q_sum[hidden_state]

            # Reestimate means as sum of prior + estimates per state
            for j in range(self._n_variates):
                self.means_posterior_[hidden_state, j] = self.beta_prior_[hidden_state] * self.means_prior_[hidden_state,j]
                self.means_posterior_[hidden_state, j] += self._new_means_numerator[hidden_state, j]
                self.means_posterior_[hidden_state, j] /= self.beta_posterior_[hidden_state]

            # Wishart DOF is similar to beta_posterior_
            # TODO (combine?)
            self.degrees_of_freedom_posterior_[hidden_state] = self.degrees_of_freedom_prior_[hidden_state] + self._q_sum[hidden_state]

            # Compute new W_k_inv
            # This is (means_{hidden_state}- means_0)(means_{hidden_state} - means_0)^{T}
            # expanded out
            do_outer_product(self.means_prior_[hidden_state], self.means_prior_[hidden_state], self.tmp_storage_1_)
            do_outer_product(self.means_posterior_[hidden_state], self.means_posterior_[hidden_state], self.tmp_storage_2_)

            for j in range(self._n_variates):
                for k in range(self._n_variates):
                    self.W_k_inv_[hidden_state, j, k] = self.W_0_inv_[hidden_state, j, k] + self.S_ks[hidden_state, j, k] \
                            + self.beta_prior_[hidden_state] * self.tmp_storage_1_[j, k] \
                            - self.beta_posterior_[hidden_state] * self.tmp_storage_2_[j, k]

            tmp = np.linalg.inv(self.W_k_inv_[hidden_state])
            for j in range(self._n_variates):
                for k in range(self._n_variates):
                    self.W_k_[hidden_state, j, k] = tmp[j,k]
                    self.precisions_posterior_[hidden_state, j, k] = self.W_k_[hidden_state,j,k] * self.degrees_of_freedom_posterior_[hidden_state]
                    self.covariances_posterior_[hidden_state, j, k] = self.W_k_inv_[hidden_state,j,k]/self.degrees_of_freedom_posterior_[hidden_state]

        # Reset all intermediate bits to 0
        self._q_sum[:] = 0
        self._new_means_numerator[:] = 0
        self.S_ks[:] = 0
        self.tmp_storage_1_[:] = 0
        self.tmp_storage_2_[:] = 0
        #print("MV DONE")

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cdef accumulate_statistics(self,  DOUBLE_DTYPE_t[:, :] digamma_1,  const DOUBLE_DTYPE_t[:, :] observed, DOUBLE_DTYPE_t weight):
        """
        Recompute emission densities from the associated digammas and observed sequences.

        Parameters
        ----------

        digamma_1 : DOUBLE_DTYPE_t[:, :, :]
        observed : DOUBLE_DTYPE_t[:, :]  TODO: Cython can't do the correct method resolution with a fused type

        """
        cdef:
            Py_ssize_t i, t, l, m

        for t in range(observed.shape[0]):
            for i in range(self._N):
                self._q_sum[i] += weight * digamma_1[t, i]
                for l in range(self._n_variates):
                    self._new_means_numerator[i, l] += weight * digamma_1[t, i] * observed[t, l]
                for l in range(self._n_variates):
                    for m in range(self._n_variates):
                        self.S_ks[i, l, m] += weight * digamma_1[t, i] * observed[t, l] * observed[t, m]

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        return multivariate_normal_density(
                observed_state,
                self.means_posterior_[hidden_state],
                self.precisions_posterior_[hidden_state],
                self.two_pi_k_det_[hidden_state],
                self.density_tmp
                )


    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        ans = exp(self.subnormalized_log_density_for(hidden_state, observed_state))
        return ans

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t normalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        return log(self.normalized_density_for(hidden_state, observed_state))

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef DOUBLE_DTYPE_t subnormalized_log_density_for(self, Py_ssize_t hidden_state, const DOUBLE_DTYPE_t[:] observed_state):
        cdef:
            DOUBLE_DTYPE_t ans, tmp4, tmp5, first_expectation, second_expectation, middle_term

        # Use cached values
        first_expectation = self._first_expectation[hidden_state]

        # This is a constant, so not truely needed...?
        middle_term = 0#self._n_variates *log(2 * M_PI ) / 2

        # Second expectation
        tmp4 = self._n_variates / self.beta_posterior_[hidden_state]

        # DOF * np.dot((X - mean) W np.dot(X-mean)
        # Compute vector difference
        for i in range(self._n_variates):
            self.tmp_vector_2_[i] = observed_state[i] - self.means_posterior_[hidden_state, i]

        do_vector_matrix_dot(self.tmp_vector_2_, self.W_k_[hidden_state], self.tmp_vector_1_)
        tmp5 = do_dot(self.tmp_vector_1_, self.tmp_vector_2_)
        tmp5 *= self.degrees_of_freedom_posterior_[hidden_state]
        second_expectation = .5 * (tmp4 + tmp5)
        ans = first_expectation - middle_term  - second_expectation
        #print("MV", hidden_state, observed_state[0], tmp1, tmp2, first_expectation, second_expectation, ans, self.normalized_log_density_for(hidden_state, observed_state))
        return ans

    cdef sample(self, Py_ssize_t hidden_state, object random_state):
        """
        Generate an emission for the given latent state
        TODO: Incorporate the random_state somehow...

        Parameters
        ----------
        hidden_state :  Py_ssize_t
        random_state :  np.random_state
        """
        return random_state.multivariate_normal(self.means_posterior_[hidden_state], self.covariances_posterior_[hidden_state])

    def __str__(self):
        return "Not Implemented"

    cdef log_state(self):
        cdef:
            Py_ssize_t i

        for i in range(self._N):
            self._logger.debug("emissions[{}] mean={} bp={} dof={} cov={}, prec={} W_k={} W_k_inv_={}".format(i, np.asarray(self.means_posterior_[i]).tolist(), self.beta_posterior_[i], self.degrees_of_freedom_posterior_[i], np.asarray(self.covariances_posterior_[i]).tolist(), np.asarray(self.precisions_posterior_[i]).tolist(), np.asarray(self.W_k_[i]).tolist(), np.asarray(self.W_k_inv_[i]).tolist()))

    cdef DOUBLE_DTYPE_t free_energy(self):
        cdef:
            Py_ssize_t i
            DOUBLE_DTYPE_t tmp, kln, klw
        tmp = 0
        for i in range(self._N):
            # Compute precision for Normal KL Divergence
            term1 = np.linalg.inv(self.beta_posterior_[i] * np.asarray(self.precisions_posterior_[i]))
            term2 = np.linalg.inv(self.beta_prior_[i] * np.asarray(self.precisions_posterior_[i]))
            kln = kl_multivariate_normal(self.means_posterior_[i], term1, self.means_prior_[i], term2)

            # Wishart KL Divergence
            # print("kln", kln)
            klw = kl_wishart(self.degrees_of_freedom_posterior_[i], self.W_k_inv_[i], self.degrees_of_freedom_prior_[i], self.W_0_inv_[i])
            # print("mvklw",klw)
            tmp -= kln
            tmp -= klw

        return tmp
