# cython: profile=False
# cython: language_level=3
cimport numpy as np
from libc.math cimport NAN
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from .emissions cimport base

cdef class HMMTrainer:
    cdef public DOUBLE_DTYPE_t[:] _mixture_weights_prior
    cdef public DOUBLE_DTYPE_t[:] _next_mixture_weights
    cdef public DOUBLE_DTYPE_t[:] _mixture_weights_posterior
    cdef public DOUBLE_DTYPE_t[:] _subnormalized_mixture_weights
    cdef public DOUBLE_DTYPE_t[:] _normalized_mixture_weights
    cdef public DOUBLE_DTYPE_t[:] _log_subnormalized_mixture_weights
    cdef public DOUBLE_DTYPE_t[:] _log_normalized_mixture_weights
    # initial state
    cdef public DOUBLE_DTYPE_t[:, :] _pi_posterior
    cdef public DOUBLE_DTYPE_t[:, :] _pi_subnormalized
    cdef public DOUBLE_DTYPE_t[:, :] _pi_normalized
    cdef public DOUBLE_DTYPE_t[:, :] _pi_prior
    cdef public DOUBLE_DTYPE_t[:, :] _log_subnormalized_pi
    cdef public DOUBLE_DTYPE_t[:, :] _log_normalized_pi
    cdef public DOUBLE_DTYPE_t[:, :] _next_pi

    # transition probabilities
    cdef public DOUBLE_DTYPE_t[:, :, :] _A_posterior
    cdef public DOUBLE_DTYPE_t[:, :, :] _A_subnormalized
    cdef public DOUBLE_DTYPE_t[:, :, :] _A_normalized
    cdef public DOUBLE_DTYPE_t[:, :, :] _A_prior

    cdef public DOUBLE_DTYPE_t[:, :, :] _log_subnormalized_A
    cdef public DOUBLE_DTYPE_t[:, :, :] _log_normalized_A
    cdef public DOUBLE_DTYPE_t[:, :, :] _next_A

    cdef public list _emissions
    # For log path
    cdef bint _prefer_scaling
    cdef bint _allowed_to_use_log
    cdef public DOUBLE_DTYPE_t[:] _work_buffer

    # Number of hidden states
    cdef public Py_ssize_t _n_mixture_components
    cdef public Py_ssize_t _N
    cdef DOUBLE_DTYPE_t _min_a_value

    cdef public object logger

    cpdef list train(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, Py_ssize_t iterations=?, DOUBLE_DTYPE_t tol=?)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] mixture_loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef tuple deviance_information_criterion(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cdef _update_subnormalized(self)

    cdef _accumulate_statistics(self, const DOUBLE_DTYPE_t[:, :] observation_sequence, DOUBLE_DTYPE_t[:] normalized_mixture_component_probabilities, DOUBLE_DTYPE_t[:, :, :, :] digamma, DOUBLE_DTYPE_t[:, :, :] digamma_1)

    cdef _m_step(self)
    cpdef log_state(self)

    cdef compute_normalized_observation_probabilities(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cdef compute_subnormalized_observation_probabilities(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cdef compute_normalized_log_observation_probabilities(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cdef compute_subnormalized_log_observation_probabiltiies(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
