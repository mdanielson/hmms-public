# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging

cimport cython
cimport libc.math
cimport cython.parallel
import scipy.special
import sklearn.utils
import numpy as np
cimport numpy as np
from hmm import logs

from libc.math cimport INFINITY, log, isinf, isnan, NAN, fabs

from hmm.utilities.types import DOUBLE_DTYPE, INT_DTYPE
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from hmm.utilities.log_routines cimport _logsumexp, _logaddexp
from hmm.utilities.normalize cimport normalize_1d, normalize_2d

from hmm.utilities.kl_divergences cimport  kl_dirichlet_distribution
from .emissions cimport base
from .. cimport forward_backward, viterbi


cdef class HMMTrainer:
    """
    Train a Hidden Markov Model via Variational Inference:
    """

    # Read only access
    @property
    def A_posterior(self):
        return np.asarray(self._A_posterior)

    @property
    def A_normalized(self):
        return np.asarray(self._A_normalized)

    @property
    def A_subnormalized(self):
        return np.asarray(self._A_subnormalized)

    @property
    def pi_posterior(self):
        return np.asarray(self._pi_posterior)

    @property
    def pi_normalized(self):
        return np.asarray(self._pi_normalized)

    @property
    def pi_subnormalized(self):
        return np.asarray(self._pi_subnormalized)

    @property
    def emissions(self):
        return self._emissions


    def __init__(self, DOUBLE_DTYPE_t[:] pi_posterior, DOUBLE_DTYPE_t[:] pi_prior, DOUBLE_DTYPE_t[:, :] A_posterior, DOUBLE_DTYPE_t[:, :] A_prior, base.EmissionsBase emissions, prefer_scaling=True, allowed_to_use_log=False, log_level=logging.DEBUG):
        self.logger = logs.create_logger(__name__, log_level)

        # Will be initialized as needed

        # The following normalization checks should have already been done,
        # but it is cheap to do here.

        # normalize so sums to 1
        self._pi_prior = np.copy(pi_prior)
        self._pi_posterior = np.copy(pi_posterior)
        self._pi_subnormalized = np.zeros_like(self._pi_posterior)
        self._pi_normalized = np.zeros_like(self._pi_posterior)

        self._log_subnormalzed_pi = np.zeros_like(self._pi_prior)
        self._log_normalized_pi = np.zeros_like(self._pi_posterior)
        self._pi_statistics = np.zeros_like(self._pi_posterior)

        # Normalize so each row sums to 1
        self._A_prior = np.copy(A_prior)
        self._A_posterior = np.copy(A_posterior)
        self._A_subnormalized = np.zeros_like(self._A_posterior)
        self._A_normalized = np.zeros_like(self._A_posterior)

        self._log_subnormalized_A = np.zeros_like(self._A_posterior)
        self._log_normalized_A = np.zeros_like(self._A_posterior)
        self._A_statistics = np.zeros_like(self._A_posterior)

        self._emissions = emissions

        self._prefer_scaling = prefer_scaling
        self._allowed_to_use_log = allowed_to_use_log
        self._N = self._pi_posterior.shape[0]
        self._work_buffer = np.zeros((self._N), dtype=DOUBLE_DTYPE)

        assert self._N > 0
        assert self._A_posterior.shape[0] == self._N
        assert self._A_posterior.shape[1] == self._N
        assert self._A_prior.shape[0] == self._N
        assert self._A_prior.shape[1] == self._N
        # Update subnormalized probabilities
        self._update_subnormalized()

        self.log_state()

    cpdef log_state(self):
        cdef:
            Py_ssize_t hidden_state
        self.logger.info("pi_posterior={}".format(np.asarray(self._pi_posterior).tolist()))
        for hidden_state in range(self._N):
            self.logger.info("A_posterior[{}, :]={}".format(hidden_state, np.asarray(self._A_posterior[hidden_state, :]).tolist()))
        self._emissions.log_state()

    @cython.initializedcheck(False)
    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef _update_subnormalized(self):
        cdef:
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] posterior = np.asarray(self._A_posterior)
            np.ndarray[DOUBLE_DTYPE_t, ndim=1] pi_posterior = np.asarray(self._pi_posterior)
            Py_ssize_t hidden_state, j
        # Update PI
        self._log_subnormalzed_pi = scipy.special.digamma(pi_posterior) - scipy.special.digamma(pi_posterior.sum())
        self._pi_subnormalized = np.exp(self._log_subnormalzed_pi)
        normalize_1d(self._pi_posterior, self._pi_normalized)
        # Update A
        self._log_subnormalized_A = scipy.special.digamma(posterior) - scipy.special.digamma(posterior.sum(axis=1)[:, None])
        self._A_subnormalized = np.exp(self._log_subnormalized_A)
        normalize_2d(self._A_posterior, self._A_normalized)

        # Update Log-normalized versions
        for hidden_state in range(self._N):
            self._log_normalized_pi[hidden_state] = log(self._pi_normalized[hidden_state])
            for j in range(self._N):
                self._log_normalized_A[hidden_state, j] = log(self._A_normalized[hidden_state, j])
        self._emissions.update_subnormalized()

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef list train(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, Py_ssize_t iterations=100, DOUBLE_DTYPE_t tol=1e-2):
        """
        Train an HMM for a specified number of iterations or tolerance, whichever is met first.

        Parameters
        ----------

        observed: 3-d memory view of observations
        iterations: int
        tol: DOUBLE_DTYPE_t

        """
        cdef:
            bint alpha_pass_success, is_debug
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t delta, free_energy_iter, old_free_energy_iter
            DOUBLE_DTYPE_t log_prob, all_log_prob, tmp_free_A, tmp_free_pi, tmp_free_B
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] alpha, beta, digamma_1, observation_probabilities
            DOUBLE_DTYPE_t[:, :, :] digamma
            Py_ssize_t hidden_state, o_i, t, iteration, seq_start, seq_end, max_length, T
            list free_energy = []
            dict item

        assert observed.shape[0] == np.sum(lengths), "Lengths and sequences mismatch {} vs {}".format(observed.shape[0], np.sum(lengths))
        old_free_energy_iter = 0

        is_debug = self.logger.isEnabledFor(logging.DEBUG)

        # Allocate storage once, for the longest sequence we see
        max_length = np.max(lengths)
        observation_probabilities = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        alpha = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        beta = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        digamma = np.zeros((max_length, self._N, self._N), dtype=DOUBLE_DTYPE)
        digamma_1 = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        c = np.zeros((max_length,), dtype=DOUBLE_DTYPE)

        self.log_state()

        for iteration in range(iterations):

            # Update our knowledge of the distributions based on the posterior
            self._update_subnormalized()
            seq_start = 0
            all_log_prob = 0
            for o_i in range(lengths.shape[0]):
                # Extract out our sequence
                seq_end = seq_start + lengths[o_i]
                sequence = observed[seq_start:seq_end]
                T = sequence.shape[0]
                seq_start = seq_end

                # Now perform training iteration
                alpha_pass_success = False
                # Set all arrays to have a default value of infinity, which w ewill override.
                c[:] = INFINITY
                digamma[:] = INFINITY
                digamma_1[:] = INFINITY
                beta[:] = INFINITY
                alpha[:] = INFINITY
                observation_probabilities[:] = INFINITY

                # Update C / Alpha
                # Try to use scaling at first, and then use logarithms if we fail
                if self._prefer_scaling:
                    self.compute_subnormalized_observation_probabilities(sequence, observation_probabilities)
                    alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities[:T], self._pi_subnormalized, self._A_subnormalized, c, alpha)
                if not alpha_pass_success or not self._prefer_scaling:
                    self.compute_subnormalized_log_observation_probabiltiies(sequence, observation_probabilities)
                    forward_backward.log_alpha_pass(observation_probabilities[:T], self._log_subnormalzed_pi, self._log_subnormalized_A, alpha, self._work_buffer)
                    log_prob = _logsumexp(alpha[T-1, :])
                else:
                    log_prob = 0
                    for t in range(T):
                        log_prob -= log(c[t])

                all_log_prob += log_prob
                if isnan(log_prob) or isinf(log_prob):
                    self.logger.error("iteration:{}".format(iteration))
                    self.logger.error("observation: {}".format(o_i))
                    self.logger.error("observation: {}".format(np.asarray(sequence)))
                    self.logger.error("observation probabilities: {}".format(np.asarray(observation_probabilities)))
                    self.logger.error("alpha_pass_success: {}".format(alpha_pass_success))
                    self.logger.error("log_prob")
                    self.logger.error(log_prob)
                    self.logger.error("c")
                    self.logger.error(np.asarray(c))
                    self.logger.error("alpha")
                    self.logger.error(np.asarray(alpha))
                    self.logger.error("beta")
                    self.logger.error(np.asarray(beta))
                    self.logger.setLevel(logging.DEBUG)
                    self._emissions.set_log_level(logging.DEBUG)
                    self.log_state()
                    assert False
                if alpha_pass_success:
                    # Update Beta
                    forward_backward.scaling_beta_pass(observation_probabilities[:T], self._A_subnormalized, c, beta)
                    forward_backward.scaling_compute_digammas(self._A_subnormalized, alpha, beta, observation_probabilities[:T], digamma, digamma_1)
                else:
                    forward_backward.log_beta_pass(observation_probabilities[:T], self._log_subnormalized_A, beta, self._work_buffer)
                    forward_backward.log_compute_digammas(self._log_subnormalized_A, alpha, beta, observation_probabilities[:T], digamma, digamma_1)
                # Update digammas
                self._accumulate_statistics(sequence, digamma, digamma_1)

            # Compute Lower Bound
            tmp_free_pi = -kl_dirichlet_distribution(self._pi_posterior, self._pi_prior)
            tmp_free_A = 0
            for hidden_state in range(self._N):
                tmp_free_A -= kl_dirichlet_distribution(self._A_posterior[hidden_state], self._A_prior[hidden_state])

            tmp_free_B = self._emissions.free_energy()

            free_energy_iter = tmp_free_pi + tmp_free_A + tmp_free_B + all_log_prob
            free_energy.append(
                {
                    "iteration":iteration,
                    "total": free_energy_iter,
                    "pi": tmp_free_pi,
                    "A": tmp_free_A,
                    "B": tmp_free_B,
                    "subnormed_log_prob": all_log_prob,
                }
            )
            # Update estimates of all parameters
            self._m_step()
            self.logger.info("iter={} total_free={:.9f} pi={:.9f} A={:.9f} B={:.9f}, log_prob={:.9f}".format(iteration, free_energy_iter, tmp_free_pi, tmp_free_A, tmp_free_B, log_prob))

            if is_debug:
                self.log_state()
            # Free energy / elbo should increase at every iteration
            # But sometimes we don't see that, especially with regards to small data sets
            if iteration != 0 and free_energy_iter < (old_free_energy_iter - 1e-6):
                for item in free_energy:
                    self.logger.error("iter={iteration} total_free={total:.9f} pi={pi:.9f} A={A:.9f} B={B:.9f}, log_prob={subnormed_log_prob:.9f}".format(**item))
                assert False, "Convergence Error"
            delta = free_energy_iter - old_free_energy_iter
            if iteration != 0 and fabs(delta) < tol:
                break

            old_free_energy_iter = free_energy_iter
            # Now update posteriors, etc

        return free_energy

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        """
        The logikelihood is the logsum of the "c" in the scaling case
        And the logsum of the final states in the log case

        * Use HMM parameters at their mean
        """
        cdef:
            bint alpha_pass_success = False
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t log_prob
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:] loglikelihoods = np.zeros(lengths.shape[0])
            DOUBLE_DTYPE_t[:, :] alpha, observation_probabilities
            Py_ssize_t o_i, t, seq_start, seq_end, longest, T

        seq_start = 0
        # Allocate storage for the longest seen sequence
        longest = max(lengths)
        observation_probabilities = np.zeros((longest, self._N), dtype=DOUBLE_DTYPE)
        c = np.zeros((longest,), dtype=DOUBLE_DTYPE)
        alpha = np.zeros((longest, self._N), dtype=DOUBLE_DTYPE)

        # For each sequence compute the likelihood
        for o_i in range(lengths.shape[0]):
            c[:] = INFINITY
            alpha[:] = INFINITY
            observation_probabilities[:] = INFINITY
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            T = sequence.shape[0]
            seq_start = seq_end

            if self._prefer_scaling:
                self.compute_normalized_observation_probabilities(sequence, observation_probabilities)
                alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities[:T, :], self._pi_normalized, self._A_normalized, c, alpha)
            if not alpha_pass_success:
                self.compute_normalized_log_observation_probabilities(sequence, observation_probabilities)
                forward_backward.log_alpha_pass(observation_probabilities[:T, :], self._log_normalized_pi, self._log_normalized_A, alpha, self._work_buffer)
                loglikelihoods[o_i] = _logsumexp(alpha[T-1, :])
            else:
                log_prob = 0
                for t in range(T):
                    log_prob -= log(c[t])
                loglikelihoods[o_i] = log_prob
        return np.asarray(loglikelihoods)

    @cython.boundscheck(False)
    @cython.wraparound(True)
    @cython.initializedcheck(False)
    cpdef tuple deviance_information_criterion(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths) :
        cdef:
            Py_ssize_t o_i, t, hidden_state, j, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] alpha, beta
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:] deviances = np.zeros(lengths.shape[0])
            DOUBLE_DTYPE_t[:] pds = np.zeros(lengths.shape[0])
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t log_prob
            DOUBLE_DTYPE_t p_d
            DOUBLE_DTYPE_t[:, :, :] digamma
            DOUBLE_DTYPE_t[:, :] digamma_1
            bint alpha_pass_success

        self._update_subnormalized()
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            seq_start = seq_end
            if o_i == 0 or observation_probabilities.shape[0] != sequence.shape[0]:
                observation_probabilities = np.zeros((sequence.shape[0], self._N))
                alpha = np.zeros((sequence.shape[0], self._N), dtype=DOUBLE_DTYPE)
                beta = np.zeros((sequence.shape[0], self._N), dtype=DOUBLE_DTYPE)
                digamma = np.zeros((sequence.shape[0], self._N, self._N), dtype=DOUBLE_DTYPE)
                digamma_1 = np.zeros((sequence.shape[0], self._N), dtype=DOUBLE_DTYPE)
                c = np.zeros((sequence.shape[0], ), dtype=DOUBLE_DTYPE)

            if self._prefer_scaling:
                self.compute_subnormalized_observation_probabilities(sequence, observation_probabilities)
                alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities, self._pi_subnormalized, self._A_subnormalized, c, alpha)
            if not alpha_pass_success:
                self.compute_subnormalied_log_observation_probabilities(sequence, observation_probabilities)
                forward_backward.log_alpha_pass(observation_probabilities, self._log_subnormalzed_pi, self._log_subnormalized_A, alpha, self._work_buffer)
                log_prob = _logsumexp(alpha[-1, :])
            else:
                log_prob = 0
                for t in range(c.shape[0]):
                    log_prob -= log(c[t])

            if alpha_pass_success:
                # Update Beta
                forward_backward.scaling_beta_pass(observation_probabilities, self._A_subnormalized, c, beta)
                forward_backward.scaling_compute_digammas(self._A_subnormalized, alpha, beta, observation_probabilities, digamma, digamma_1)
            else:
                forward_backward.log_beta_pass(observation_probabilities, self._log_subnormalized_A, beta, self._work_buffer)
                forward_backward.log_compute_digammas(self._log_subnormalized_A, alpha, beta, observation_probabilities, digamma, digamma_1)

            p_d = 0
            for hidden_state in range(self._N):
                p_d += 2 * digamma_1[0, hidden_state] * (self._log_normalized_pi[hidden_state] - self._log_subnormalzed_pi[hidden_state])
            for t in range(1, sequence.shape[0]):
                for hidden_state in range(self._N):
                    for j in range(self._N):
                        p_d += 2 *digamma[t-1, hidden_state, j] * ( self._log_normalized_A[hidden_state, j] - self._log_subnormalized_A[hidden_state, j])
            p_d += self._emissions.dic_contribution(digamma_1)

            # Mcgrory & Titterington claim to use the posterior mean/mode, but in actuality it seems like they use the log_prob
            # computed with the forward/backward pass with params at the subnormalized values
            deviances[o_i] = 2 * p_d - 2 * log_prob
            pds[o_i] = p_d

        return np.asarray(pds), np.asarray(deviances)

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] observation_probabilities;
            const DOUBLE_DTYPE_t[:, :] sequence
            list paths

        self._update_subnormalized()
        paths = []
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            seq_start = seq_end
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self.compute_normalized_observation_probabilities(sequence, observation_probabilities)
            paths.extend(
                viterbi.viterbi_path(self._pi_normalized, self._A_normalized, observation_probabilities)
            )

        return np.asarray(paths)


    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef tuple scaling_alpha_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] alpha, observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence

        c = np.zeros((observed.shape[0], ), dtype=DOUBLE_DTYPE)
        alpha = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((observed[o_i].shape[0], self._N))
            self.compute_subnormalized_observation_probabilities(sequence, observation_probabilities)
            forward_backward.scaling_alpha_pass(observation_probabilities, self._pi_normalized, self._A_normalized, c[seq_start:seq_end], alpha[seq_start:seq_end])
            seq_start = seq_end
        return c, alpha

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=3] log_alpha_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] alpha, observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence

        alpha = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self.compute_subnormalized_log_observation_probabiltiies(sequence, observation_probabilities)
            forward_backward.log_alpha_pass(observation_probabilities, self._log_subnormalzed_pi, self._log_subnormalized_A, alpha[seq_start:seq_end], self._work_buffer)
            seq_start = seq_end
        return np.asarray(alpha)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef scaling_beta_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, DOUBLE_DTYPE_t[:] c):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] beta, observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
        beta = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)

        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((observed[o_i].shape[0], self._N))
            self.compute_subnormalized_observation_probabilities(sequence, observation_probabilities)
            forward_backward.scaling_beta_pass(observation_probabilities, self._A_subnormalized, c[seq_start:seq_end], beta[seq_start:seq_end])
            seq_start = seq_end
        return beta


    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cpdef log_beta_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] beta, observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
        beta = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)

        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self.compute_subnormalized_log_observation_probabiltiies(sequence, observation_probabilities)
            forward_backward.log_beta_pass(observation_probabilities, self._log_subnormalized_A, beta[seq_start:seq_end], self._work_buffer)
            seq_start = seq_end
        return beta

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef scaling_compute_digammas(self, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] digamma_1, observation_probabilities
            DOUBLE_DTYPE_t[:, :, :] digamma
            const DOUBLE_DTYPE_t[:, :] sequence

        digamma = np.zeros((observed.shape[0], self._N, self._N), dtype=DOUBLE_DTYPE)
        digamma_1 = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self.compute_subnormalized_observation_probabilities(sequence, observation_probabilities)
            forward_backward.scaling_compute_digammas(self._A_subnormalized, alpha[seq_start:seq_end], beta[seq_start:seq_end], observation_probabilities, digamma[seq_start:seq_end], digamma_1[seq_start:seq_end])
            seq_start = seq_end
        return digamma, digamma_1

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    @cython.wraparound(False)
    cpdef log_compute_digammas(self, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] digamma_1, observation_probabilities
            DOUBLE_DTYPE_t[:, :, :] digamma
            const DOUBLE_DTYPE_t[:, :] sequence

        digamma = np.zeros((observed.shape[0], self._N, self._N), dtype=DOUBLE_DTYPE)
        digamma_1 = np.zeros((observed.shape[0], self._N), dtype=DOUBLE_DTYPE)
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self.compute_subnormalized_log_observation_probabiltiies(sequence, observation_probabilities)
            forward_backward.log_compute_digammas(self._log_subnormalized_A, alpha[seq_start:seq_end], beta[seq_start:seq_end], observation_probabilities, digamma[seq_start:seq_end], digamma_1[seq_start:seq_end])
            seq_start = seq_end

        return digamma, digamma_1

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef _accumulate_statistics(self, const DOUBLE_DTYPE_t[:, :] observation_sequence, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1):
        """
        Accumulate statistics for a single sequence
        """
        cdef:
            Py_ssize_t t, hidden_state, j, last_t

        for hidden_state in range(self._N):
            self._pi_statistics[hidden_state] += digamma_1[0, hidden_state]

        last_t = observation_sequence.shape[0] - 1
        for hidden_state in range(self._N):
            for j in range(self._N):
                for t in range(last_t):
                    self._A_statistics[hidden_state, j] += digamma[t, hidden_state, j]

        self._emissions.accumulate_statistics(digamma_1, observation_sequence, 1.0)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef _m_step(self):
        """
        Reestimate model parameters
        """
        cdef:
            Py_ssize_t hidden_state, j

        self._pi_posterior[:] = 0
        self._A_posterior[:] = 0

        for hidden_state in range(self._pi_posterior.shape[0]):
            self._pi_posterior[hidden_state] = self._pi_statistics[hidden_state] + self._pi_prior[hidden_state]

        for hidden_state in range(self._N):
            for j in range(self._N):
                self._A_posterior[hidden_state, j] = self._A_statistics[hidden_state, j] + self._A_prior[hidden_state, j]

        self._A_statistics[:] = 0
        self._pi_statistics[:] = 0

        # Now update emissions
        self._emissions.m_step()

    def sample(self, n_samples=1, length=10, random_state=None):
        cdef:
            Py_ssize_t currstate, hidden_state, j
            DOUBLE_DTYPE_t rand
            list hidden_sequences, observation_sequences, lengths
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] A_cdf
            np.ndarray[DOUBLE_DTYPE_t, ndim=1] pi_cdf

        assert random_state is not None
        self._update_subnormalized()
        hidden_sequences = []
        observation_sequences = []
        lengths = []
        A_cdf = np.cumsum(self._A_normalized, axis=1)
        pi_cdf = np.cumsum(self._pi_normalized)
        for hidden_state in range(n_samples):
            hidden_sequence = []
            observation_sequence = []

            for j in range(length):
                rand = random_state.rand()
                if j == 0:
                    currstate = (pi_cdf > rand).argmax()
                else:
                    currstate = (A_cdf[currstate] > rand).argmax()

                hidden_sequence.append(currstate)
                observed = self._emissions.sample(currstate, random_state)
                observation_sequence.append(observed)
            hidden_sequences.extend(hidden_sequence)
            observation_sequences.extend(observation_sequence)
            lengths.append(len(observation_sequence))
        return np.asarray(observation_sequences), np.asarray(hidden_sequences), np.asarray(lengths)

    #
    # Compute probabilities for the entire sequence
    #
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef inline compute_normalized_observation_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, hidden_state
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for hidden_state in range(self._N):
                observation_probabilities[t, hidden_state] = self._emissions.normalized_density_for(hidden_state, observations[t])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef inline compute_subnormalized_observation_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, hidden_state
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for hidden_state in range(self._N):
                observation_probabilities[t, hidden_state] = self._emissions.subnormalized_density_for(hidden_state, observations[t])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef inline compute_normalized_log_observation_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, hidden_state
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for hidden_state in range(self._N):
                observation_probabilities[t, hidden_state] = self._emissions.normalized_log_density_for(hidden_state, observations[t])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    cdef inline compute_subnormalized_log_observation_probabiltiies(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, hidden_state
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for hidden_state in range(self._N):
                observation_probabilities[t, hidden_state] = self._emissions.subnormalized_log_density_for(hidden_state, observations[t])
