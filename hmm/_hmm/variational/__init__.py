# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import numpy as np

from . import generic, mixture
from .emissions import categorical, gaussian, gmm, multivariate_gaussian, poisson


def _get_scaling(prefer_scaling, impl):
    if impl == "scaling":
        return True
    elif impl == "log":
        return False
    return prefer_scaling


def new_variational_categorical(pi, pi_prior,  A, A_prior, B, B_prior, impl=None, prefer_scaling=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    return generic.HMMTrainer(
        pi_posterior=np.asarray(pi, dtype=float),
        pi_prior=np.asarray(pi_prior, dtype=float),
        A_posterior=np.asarray(A, dtype=float),
        A_prior=np.asarray(A_prior, dtype=float),
        emissions=categorical.CategoricalEmissions(
            np.asarray(B, dtype=float),
            np.asarray(B_prior, dtype=float),
            log_level=log_level
        ),
        prefer_scaling=prefer_scaling,
        log_level=log_level
    )


def new_variational_poisson(pi_prior,  pi_posterior, A_prior, A_posterior, rates_prior, rates_posterior, a_prior, a_posterior, b_prior, b_posterior, impl=None, prefer_scaling=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    return generic.HMMTrainer(
        pi_posterior=np.asarray(pi_posterior, dtype=float),
        pi_prior=np.asarray(pi_prior, dtype=float),
        A_posterior=np.asarray(A_posterior, dtype=float),
        A_prior=np.asarray(A_prior, dtype=float),
        emissions=poisson.PoissonEmissions(
            np.asarray(rates_prior, dtype=float),
            np.asarray(rates_posterior, dtype=float),
            np.asarray(a_prior, dtype=float),
            np.asarray(a_posterior, dtype=float),
            np.asarray(b_prior, dtype=float),
            np.asarray(b_posterior, dtype=float),
            log_level=log_level
        ),
        prefer_scaling=prefer_scaling,
        log_level=log_level
    )


def new_variational_gaussian(pi_prior,  pi_posterior,  A_prior, A_posterior, means_prior, means_posterior, beta_prior, beta_posterior, a_prior, a_posterior, b_prior, b_posterior, impl=None, prefer_scaling=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    return generic.HMMTrainer(
        pi_posterior=np.asarray(pi_posterior, dtype=float),
        pi_prior=np.asarray(pi_prior, dtype=float),
        A_posterior=np.asarray(A_posterior, dtype=float),
        A_prior=np.asarray(A_prior, dtype=float),
        emissions=gaussian.GaussianEmissions(
            np.asarray(means_prior, dtype=float),
            np.asarray(means_posterior, dtype=float),
            np.asarray(beta_prior, dtype=float),
            np.asarray(beta_posterior, dtype=float),
            np.asarray(a_prior, dtype=float),
            np.asarray(a_posterior, dtype=float),
            np.asarray(b_prior, dtype=float),
            np.asarray(b_posterior, dtype=float),
            log_level=log_level
        ),
        prefer_scaling=prefer_scaling,
        log_level=log_level
    )


def new_variational_multivariate_gaussian(pi_prior, pi_posterior, A_prior, A_posterior, means_prior, means_posterior, beta_prior, beta_posterior, degrees_of_freedom_prior, degrees_of_freedom_posterior, scale_prior, scale_posterior, covariance_type="full", impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    emissions = None
    if covariance_type == "full":
        emissions = multivariate_gaussian.FullGaussianEmissions(
            means_prior=np.asarray(means_prior, dtype=float),
            means_posterior=np.asarray(means_posterior, dtype=float),
            beta_prior=np.asarray(beta_prior, dtype=float),
            beta_posterior=np.asarray(beta_posterior, dtype=float),
            degrees_of_freedom_prior=np.asarray(degrees_of_freedom_prior, dtype=float),
            degrees_of_freedom_posterior=np.asarray(degrees_of_freedom_posterior, dtype=float),
            scale_prior=np.asarray(scale_prior, dtype=float),
            scale_posterior=np.asarray(scale_posterior, dtype=float),
            log_level=log_level
        )
    elif covariance_type == "tied":
        emissions = multivariate_gaussian.TiedGaussianEmissions(
            means_prior=np.asarray(means_prior, dtype=float),
            means_posterior=np.asarray(means_posterior, dtype=float),
            beta_prior=np.asarray(beta_prior, dtype=float),
            beta_posterior=np.asarray(beta_posterior, dtype=float),
            degrees_of_freedom_prior=degrees_of_freedom_prior,
            degrees_of_freedom_posterior=degrees_of_freedom_posterior,
            scale_prior=np.asarray(scale_prior, dtype=float),
            scale_posterior=np.asarray(scale_posterior, dtype=float),
            log_level=log_level
        )
    elif covariance_type == "diagonal":
        emissions = multivariate_gaussian.DiagonalGaussianEmissions(
            means_prior=np.asarray(means_prior, dtype=float),
            means_posterior=np.asarray(means_posterior, dtype=float),
            beta_prior=np.asarray(beta_prior, dtype=float),
            beta_posterior=np.asarray(beta_posterior, dtype=float),
            degrees_of_freedom_prior=np.asarray(degrees_of_freedom_prior, dtype=float),
            degrees_of_freedom_posterior=np.asarray(degrees_of_freedom_posterior, dtype=float),
            scale_prior=np.asarray(scale_prior, dtype=float),
            scale_posterior=np.asarray(scale_posterior, dtype=float),
            log_level=log_level
        )
    elif covariance_type == "spherical":
        emissions = multivariate_gaussian.SphericalGaussianEmissions(
            means_prior=np.asarray(means_prior, dtype=float),
            means_posterior=np.asarray(means_posterior, dtype=float),
            beta_prior=np.asarray(beta_prior, dtype=float),
            beta_posterior=np.asarray(beta_posterior, dtype=float),
            degrees_of_freedom_prior=np.asarray(degrees_of_freedom_prior, dtype=float),
            degrees_of_freedom_posterior=np.asarray(degrees_of_freedom_posterior, dtype=float),
            scale_prior=np.asarray(scale_prior, dtype=float),
            scale_posterior=np.asarray(scale_posterior, dtype=float),
            log_level=log_level
        )

    else:
        assert False, covariance_type

    return generic.HMMTrainer(
        np.asarray(pi_posterior, dtype=float),
        np.asarray(pi_prior, dtype=float),
        np.asarray(A_posterior, dtype=float),
        np.asarray(A_prior, dtype=float),
        emissions,
        prefer_scaling=prefer_scaling,
        log_level=log_level
    )


def new_variational_gmm(pi_prior, pi_posterior, A_prior, A_posterior, mixture_weights_prior, mixture_weights_posterior, mixture_means_prior, mixture_means_posterior, mixture_beta_prior, mixture_beta_posterior, mixture_degrees_of_freedom_prior, mixture_degrees_of_freedom_posterior, mixture_scale_prior, mixture_scale_posterior, mixture_covariance_type="full", impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    emissions = None
    if mixture_covariance_type == "full":
        emissions = gmm.FullGaussianEmissions(
            mixture_weights_prior,
            mixture_weights_posterior,
            mixture_means_prior,
            mixture_means_posterior,
            mixture_beta_prior,
            mixture_beta_posterior,
            mixture_degrees_of_freedom_prior,
            mixture_degrees_of_freedom_posterior,
            mixture_scale_prior=mixture_scale_prior,
            mixture_scale_posterior=mixture_scale_posterior,
            log_level=log_level
        )
    elif mixture_covariance_type == "tied":
        emissions = gmm.TiedGaussianEmissions(
            mixture_weights_prior,
            mixture_weights_posterior,
            mixture_means_prior,
            mixture_means_posterior,
            mixture_beta_prior,
            mixture_beta_posterior,
            mixture_degrees_of_freedom_prior,
            mixture_degrees_of_freedom_posterior,
            mixture_scale_prior,
            mixture_scale_posterior,
            log_level=log_level
        )

    else:
        assert False, mixture_covariance_type

    return generic.HMMTrainer(
        pi_posterior=np.asarray(pi_posterior, dtype=float),
        pi_prior=np.asarray(pi_prior, dtype=float),
        A_posterior=np.asarray(A_posterior, dtype=float),
        A_prior=np.asarray(A_prior, dtype=float),
        emissions=emissions,
        prefer_scaling=prefer_scaling,
        log_level=log_level
    )


def new_mixture_categorical(mixture_weights_prior, mixture_weights_posterior, pi_prior, pi_posterior, A_prior, A_posterior, B_prior, B_posterior, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    emissions = []
    for i in range(B_prior.shape[0]):
        emissions.append(
            categorical.CategoricalEmissions(
                B_posterior[i].copy(),
                B_prior[i].copy(),
                log_level=log_level
            )
        )
    return mixture.HMMTrainer(
        mixture_weights_prior,
        mixture_weights_posterior,
        pi_prior,
        pi_posterior,
        A_prior,
        A_posterior,
        emissions,
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )


def new_mixture_gaussian(mixture_weights_prior, mixture_weights_posterior, pi_prior, pi_posterior, A_prior, A_posterior, means_prior, means_posterior, beta_prior, beta_posterior, a_prior, a_posterior, b_prior, b_posterior, impl=None, prefer_scaling=True, allowed_to_use_log=True, log_level=logging.INFO):
    prefer_scaling = _get_scaling(prefer_scaling, impl)
    emissions = []
    for mixture_component in range(mixture_weights_posterior.shape[0]):
        emissions.append(
            gaussian.GaussianEmissions(
            np.asarray(means_prior[mixture_component]),
            np.asarray(means_posterior[mixture_component]),
            np.asarray(beta_prior[mixture_component]),
            np.asarray(beta_posterior[mixture_component]),
            np.asarray(a_prior[mixture_component]),
            np.asarray(a_posterior[mixture_component]),
            np.asarray(b_prior[mixture_component]),
            np.asarray(b_posterior[mixture_component]),
            log_level=log_level
        ),

        )
    return mixture.HMMTrainer(
        mixture_weights_prior=mixture_weights_prior,
        mixture_weights_posterior=mixture_weights_posterior,
        pi_prior=pi_prior,
        pi_posterior=pi_posterior,
        A_prior=A_prior,
        A_posterior=A_posterior,
        emissions=emissions,
        prefer_scaling=prefer_scaling,
        allowed_to_use_log=allowed_to_use_log,
        log_level=log_level
    )
