# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
import logging
cimport cython
cimport libc.math
cimport cython.parallel
import scipy.stats
import scipy.special
import sklearn.utils
import numpy as np
cimport numpy as np
from hmm import logs

from libc.math cimport INFINITY, log, isinf, isnan, exp

from hmm.utilities.types import DOUBLE_DTYPE, INT_DTYPE
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from hmm.utilities.log_routines cimport _logsumexp, _logaddexp
from hmm.utilities.normalize cimport normalize_1d, normalize_2d

from hmm.utilities.kl_divergences cimport kl_dirichlet_distribution, kl_categorical_distribution
from .emissions cimport base
from .. cimport forward_backward, viterbi

cdef bint DEBUG = False

cdef class HMMTrainer:
    """
    Train a Hidden Markov Model via Variational:
    """

    # Read only access
    mixture_weights = property(lambda self: self._mixture_weights_posterior)
    A = property(lambda self: np.asarray(self._A_posterior))
    pi = property(lambda self: np.asarray(self._pi_posterior))
    emissions = property(lambda self: self._emissions)


    def __init__(self, DOUBLE_DTYPE_t[:] mixture_weights_prior, DOUBLE_DTYPE_t[:] mixture_weights_posterior,  DOUBLE_DTYPE_t[:, :] pi_prior, DOUBLE_DTYPE_t[:, :] pi_posterior, DOUBLE_DTYPE_t[:, :, :] A_prior, DOUBLE_DTYPE_t[:, :, :] A_posterior, list emissions, bint prefer_scaling=True, allowed_to_use_log=False, log_level=logging.DEBUG):
        self.logger = logs.create_logger(__name__, log_level)

        # Will be initialized as needed
        self._mixture_weights_prior = np.copy(mixture_weights_prior)
        self._next_mixture_weights = np.zeros_like(mixture_weights_prior)
        self._subnormalized_mixture_weights = np.zeros_like(mixture_weights_prior)
        self._normalized_mixture_weights = np.zeros_like(mixture_weights_prior)
        self._log_normalized_mixture_weights = np.zeros_like(mixture_weights_prior)
        self._log_subnormalized_mixture_weights = np.zeros_like(mixture_weights_prior)
        self._mixture_weights_posterior = np.copy(mixture_weights_posterior)

        # normalize so sums to 1
        self._pi_prior = np.copy(pi_prior)
        self._pi_posterior = np.copy(pi_posterior)
        self._pi_normalized = np.zeros_like(self._pi_posterior)
        self._pi_subnormalized = np.zeros_like(self._pi_posterior)
        self._log_subnormalized_pi = np.zeros_like(self._pi_prior)
        self._log_normalized_pi = np.zeros_like(self._pi_posterior)
        self._next_pi = np.zeros_like(self._pi_posterior)

        # Normalize so each row sums to 1
        self._A_prior = np.copy(A_prior)
        self._A_posterior = np.copy(A_posterior)
        self._A_subnormalized = np.zeros_like(self._A_posterior)
        self._A_normalized = np.zeros_like(self._A_posterior)

        self._log_subnormalized_A = np.zeros_like(self._A_posterior)
        self._log_normalized_A = np.zeros_like(self._A_posterior)
        self._next_A = np.zeros_like(self._A_posterior)

        self._emissions = emissions

        self._prefer_scaling = prefer_scaling
        self._allowed_to_use_log = allowed_to_use_log
        self._N = self._pi_posterior.shape[1]
        self._work_buffer = np.zeros((self._N), dtype=DOUBLE_DTYPE)
        self._n_mixture_components = self._mixture_weights_prior.shape[0]

        assert self._N > 0
        assert self._A_posterior.shape[0] == self._n_mixture_components
        assert self._A_posterior.shape[1] == self._N
        assert self._A_posterior.shape[2] == self._N
        assert self._A_prior.shape[0] == self._n_mixture_components
        assert self._A_prior.shape[1] == self._N
        assert self._A_prior.shape[2] == self._N
        self.log_state()

    cpdef log_state(self):
        cdef:
            Py_ssize_t mixture_component, i
            base.EmissionsBase emissions
        #self.logger.info("weights{}|{}".format(np.asarray(self._normalized_mixture_weights), np.asarray(self._subnormalized_mixture_weights)))
        #self.logger.info("logweights{}".format(np.asarray(self._log_subnormalized_mixture_weights).tolist()))
        for mixture_component in range(self._n_mixture_components):
            self.logger.info("weight[{}]={}|{}|{}".format(mixture_component, self._mixture_weights_posterior[mixture_component], self._normalized_mixture_weights[mixture_component], self._subnormalized_mixture_weights[mixture_component]))
            self.logger.info("pi_posterior[{}]={}".format(mixture_component, np.asarray(self._pi_posterior[mixture_component]).tolist()))
            self.logger.info("pi_normalized[{}]={}".format(mixture_component, np.asarray(self._pi_normalized[mixture_component]).tolist()))
            self.logger.info("logpi_subnormalized[{}]={}".format(mixture_component, np.asarray(self._log_subnormalized_pi[mixture_component]).tolist()))

            for i in range(self._N):
                self.logger.info("A_posterior[{}, {}, :]={}".format(mixture_component, i, np.asarray(self._A_posterior[mixture_component, i, :]).tolist()))
            for i in range(self._N):
                self.logger.info("A_normalized[{}, {}, :]={}".format(mixture_component, i, np.asarray(self._A_normalized[mixture_component, i, :]).tolist()))
            emissions = self._emissions[mixture_component]
            emissions.log_state()

    cdef _update_subnormalized(self):
        cdef:
            Py_ssize_t i, j, mixture_component
            np.ndarray[DOUBLE_DTYPE_t, ndim=1] mixture_counts = np.asarray(self._mixture_weights_posterior)
            np.ndarray[DOUBLE_DTYPE_t, ndim=3] counts = np.asarray(self._A_posterior)
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] A_normed
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] pi_posterior = np.asarray(self._pi_posterior)
            np.ndarray[DOUBLE_DTYPE_t, ndim=1] pi_subnormed
            DOUBLE_DTYPE_t tmp_sum
            base.EmissionsBase emissions

        self._log_subnormalized_mixture_weights = scipy.special.digamma(mixture_counts) - scipy.special.digamma(mixture_counts.sum())
        self._subnormalized_mixture_weights = np.exp(self._log_subnormalized_mixture_weights)
        self._normalized_mixture_weights = mixture_counts / mixture_counts.sum()
        self._log_normalized_mixture_weights = np.log(self._normalized_mixture_weights)

        for mixture_component in range(self._n_mixture_components):
            A_normed = scipy.special.digamma(counts[mixture_component]) - scipy.special.digamma(counts[mixture_component].sum(axis=1)[:, None])
            for i in range(self._N):
                for j in range(self._N):
                    self._log_subnormalized_A[mixture_component, i, j] = A_normed[i, j]
                    self._A_subnormalized[mixture_component, i, j] = exp(A_normed[i, j])

            A_normed = counts[mixture_component]/counts[mixture_component].sum(axis=1)[:, None]
            for i in range(self._N):
                for j in range(self._N):
                    self._log_normalized_A[mixture_component, i, j] = log(A_normed[i, j])
                    self._A_normalized[mixture_component, i, j] = A_normed[i, j]

            pi_subnormed = scipy.special.digamma(pi_posterior[mixture_component]) - scipy.special.digamma(pi_posterior[mixture_component].sum())
            for i in range(self._N):
                self._log_subnormalized_pi[mixture_component, i] = pi_subnormed[i]
                self._pi_subnormalized[mixture_component, i] = exp(pi_subnormed[i])

            tmp_sum = pi_posterior[mixture_component].sum()
            for i in range(self._N):
                self._pi_normalized[mixture_component, i] = pi_posterior[mixture_component, i] / tmp_sum
                self._log_normalized_pi[mixture_component, i] = log(self._pi_normalized[mixture_component, i])

            emissions = self._emissions[mixture_component]
            emissions.update_subnormalized()


    @cython.boundscheck(False)
    @cython.wraparound(True)
    @cython.initializedcheck(False)
    cpdef list train(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, Py_ssize_t iterations=100, DOUBLE_DTYPE_t tol=1e-2):
        """
        Train an HMM for a specified number of iterations or tolerance, whichever is met first.

        Parameters
        ----------

        observed: 3-d memory view of observations
        iterations: int
        tol: DOUBLE_DTYPE_t

        """
        cdef:
            bint alpha_pass_success
            Py_ssize_t mixture_component, iteration, o_i, t, state, seq_start, seq_end, T, max_length
            base.EmissionsBase emissions
            DOUBLE_DTYPE_t component_log_prob, all_Zeta_k, normalizer
            DOUBLE_DTYPE_t[:, :] alpha
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] beta
            DOUBLE_DTYPE_t[:, :, :, :] digamma
            DOUBLE_DTYPE_t[:, :, :] digamma_1
            DOUBLE_DTYPE_t[:] free_energy_pi = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t[:] free_energy_A = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t[:] free_energy_B = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t[:] normalized_mixture_component_probabilities = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t[:] membership_probabilities = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)

            DOUBLE_DTYPE_t delta, free_energy_iter, old_free_energy_iter
            DOUBLE_DTYPE_t free_energy_weights, free_energy_pi_sum, free_energy_A_sum, free_energy_B_sum
            bint is_debug
            DOUBLE_DTYPE_t[:, :] observation_probabilities;
            list verbose_free_energy = []
            list weights
            const DOUBLE_DTYPE_t[:, :] sequence

        global DEBUG


        old_free_energy_iter = 0

        is_debug = self.logger.isEnabledFor(logging.DEBUG)
        DEBUG = is_debug
        max_length = np.max(lengths)

        c = np.zeros((max_length,), dtype=DOUBLE_DTYPE)
        observation_probabilities = np.zeros((max_length, self._N))
        alpha = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        beta = np.zeros((max_length, self._N), dtype=DOUBLE_DTYPE)
        digamma = np.zeros((self._n_mixture_components, max_length, self._N, self._N), dtype=DOUBLE_DTYPE)
        digamma_1 = np.zeros((self._n_mixture_components, max_length, self._N), dtype=DOUBLE_DTYPE)

        self._update_subnormalized()
        self.log_state()
        for iteration in range(iterations):

            self._update_subnormalized()
            all_Zeta_k = 0
            seq_start = 0
            for o_i in range(lengths.shape[0]):
                seq_end = seq_start + lengths[o_i]
                sequence = observed[seq_start:seq_end]
                seq_start = seq_end
                T = sequence.shape[0]

                digamma[:] = INFINITY
                digamma_1[:] = INFINITY
                for mixture_component in range(self._n_mixture_components):
                    c[:] = INFINITY
                    beta[:] = INFINITY
                    alpha[:] = INFINITY
                    observation_probabilities[:] = INFINITY

                    c[:] = 0
                    alpha[:] = 0
                    beta[:] = 0
                    alpha_pass_success = False
                    # Update C / Alpha
                    if self._prefer_scaling:
                        self.compute_subnormalized_observation_probabilities(mixture_component, sequence, observation_probabilities[:T])
                        alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities[:T], self._pi_subnormalized[mixture_component], self._A_subnormalized[mixture_component], c, alpha)
                    if not alpha_pass_success or not self._prefer_scaling:
                        self.compute_subnormalized_log_observation_probabiltiies(mixture_component, sequence, observation_probabilities[:T])
                        forward_backward.log_alpha_pass(observation_probabilities[:T], self._log_subnormalized_pi[mixture_component], self._log_subnormalized_A[mixture_component], alpha, self._work_buffer)
                        component_log_prob = _logsumexp(alpha[T-1, :])
                    else:
                        component_log_prob = 0
                        for t in range(T):
                            component_log_prob -= log(c[t])

                    if isnan(component_log_prob) or isinf(component_log_prob):
                        self.logger.error("iteration:{}".format(iteration))
                        self.logger.error("observation: {}".format(o_i))
                        self.logger.error("observation: {}".format(np.asarray(sequence)))
                        self.logger.error("observation probabilities: {}".format(np.asarray(observation_probabilities[:T])))
                        self.logger.error("alpha_pass_success: {}".format(alpha_pass_success))
                        self.logger.error("component_log_prob")
                        self.logger.error(component_log_prob)
                        self.logger.error("c")
                        self.logger.error(np.asarray(c[:T]))
                        self.logger.error("alpha")
                        self.logger.error(np.asarray(alpha))
                        self.logger.error("beta")
                        self.logger.error(np.asarray(beta))
                        self.logger.setLevel(logging.DEBUG)
                        self._emissions[mixture_component].set_log_level(logging.DEBUG)
                        self.log_state()
                        assert False

                    if alpha_pass_success:
                        # Update Beta
                        forward_backward.scaling_beta_pass(observation_probabilities[:T], self._A_subnormalized[mixture_component], c, beta)
                        forward_backward.scaling_compute_digammas(self._A_subnormalized[mixture_component], alpha, beta, observation_probabilities[:T], digamma[mixture_component], digamma_1[mixture_component])
                    else:
                        forward_backward.log_beta_pass(observation_probabilities[:T], self._log_subnormalized_A[mixture_component], beta, self._work_buffer)
                        forward_backward.log_compute_digammas(self._log_subnormalized_A[mixture_component], alpha, beta, observation_probabilities[:T], digamma[mixture_component], digamma_1[mixture_component])

                    membership_probabilities[mixture_component] = component_log_prob + self._log_subnormalized_mixture_weights[mixture_component]
                # To Compute normalized mixture components
                # Lower bound latent state and mixture contribution is equal to the sum of the mixture probabilities
                normalizer = _logsumexp(membership_probabilities)
                all_Zeta_k += normalizer

                # Compute this sequences contribution to the log-likelihood
                for mixture_component in range(self._n_mixture_components):
                    normalized_mixture_component_probabilities[mixture_component] = exp(membership_probabilities[mixture_component] - normalizer)
                # Update statistics
                self._accumulate_statistics(sequence, normalized_mixture_component_probabilities, digamma, digamma_1)

            # KL(q(W)| p(W))
            free_energy_weights = -kl_dirichlet_distribution(self._mixture_weights_posterior, self._mixture_weights_prior)
            free_energy_pi_sum = 0
            free_energy_A_sum = 0
            free_energy_B_sum = 0
            for mixture_component in range(self._n_mixture_components):
                free_energy_pi[mixture_component] = -kl_dirichlet_distribution(self._pi_posterior[mixture_component], self._pi_prior[mixture_component])
                free_energy_pi_sum += free_energy_pi[mixture_component]

                emissions = self._emissions[mixture_component]
                free_energy_B[mixture_component] = emissions.free_energy()
                free_energy_B_sum += free_energy_B[mixture_component]

                free_energy_A[mixture_component] = 0
                for state in range(self._N):
                    free_energy_A[mixture_component] += -kl_dirichlet_distribution(self._A_posterior[mixture_component, state], self._A_prior[mixture_component, state])
                free_energy_A_sum += free_energy_A[mixture_component]

            # allow for minor amounts of rounding error
            assert np.all(np.asarray(free_energy_pi) <= 1E-10), free_energy_pi
            assert np.all(np.asarray(free_energy_A)  <= 1E-10), free_energy_A
            assert np.all(np.asarray(free_energy_B)  <= 1E-10), free_energy_B

            assert free_energy_weights <= 0, free_energy_weights
            assert free_energy_pi_sum <= 0, free_energy_pi_sum
            assert free_energy_A_sum <= 0, free_energy_A_sum
            assert free_energy_B_sum <= 0, free_energy_B_sum
            #assert all_Zeta_k <= 0, all_Zeta_k

            free_energy_iter = free_energy_weights
            free_energy_iter += free_energy_pi_sum + free_energy_A_sum + free_energy_B_sum
            free_energy_iter += all_Zeta_k
            verbose_free_energy.append(
                {
                    "iteration":iteration,
                    "total": free_energy_iter,
                    "component_weights": free_energy_weights,
                    "pi": free_energy_pi_sum,
                    "A": free_energy_A_sum,
                    "B": free_energy_B_sum,
                    "subnormed_log_prob": all_Zeta_k,
                }
            )

            # Reestimate internal state
            weights = []
            for mixture_component in range(self._n_mixture_components):
                weights.append("{:.4f}".format(self._mixture_weights_posterior[mixture_component]))
            self.logger.info("iter={}, weights=[{}], total_free={:.12f} component_weights={:.9f}, pi={:.9f} A={:.9f} B={:.9f}, log_prob={:.9f}".format(
                iteration,
                ",".join(weights),
                free_energy_iter,
                free_energy_weights,
                free_energy_pi_sum,
                free_energy_A_sum,
                free_energy_B_sum,
                all_Zeta_k)
            )

            self._m_step()
            if is_debug:
                self.log_state()
            if iteration != 0 and free_energy_iter < (old_free_energy_iter - 1e-6):
                for item in verbose_free_energy:
                    self.logger.error("iter={iteration} total_free={total:.9f} component_weights={component_weights:.9f} pi={pi:.9f} A={A:.9f} B={B:.9f} log_prob={subnormed_log_prob:.9f}".format(**item))
                assert False, "Convergence Error"
            delta = old_free_energy_iter - free_energy_iter
            if iteration != 0 and libc.math.fabs(delta) < libc.math.fabs(tol):
                break

            old_free_energy_iter = free_energy_iter

        return verbose_free_energy

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] loglik(self, const DOUBLE_DTYPE_t[:, :] observed, Py_ssize_t[:] lengths):
        return scipy.special.logsumexp(self.mixture_loglik(observed, lengths), axis=1)

    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=2] mixture_loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        """
        The logikelihood is the logsum of the "c" in the scaling case
        And the logsum of the final states in the log case
        """
        cdef:
            bint alpha_pass_success = False
            Py_ssize_t mixture_component, o_i, t, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] alpha
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:, :] loglikelihoods = np.zeros((lengths.shape[0], self._n_mixture_components), dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t log_prob

        self._update_subnormalized()
        for mixture_component in range(self._n_mixture_components):
            seq_start = 0
            for o_i in range(lengths.shape[0]):
                seq_end = seq_start + lengths[o_i]
                sequence = observed[seq_start:seq_end]
                seq_start = seq_end
                c = np.zeros((sequence.shape[0],), dtype=DOUBLE_DTYPE)
                alpha = np.zeros((sequence.shape[0], self._N), dtype=DOUBLE_DTYPE)
                observation_probabilities = np.zeros((sequence.shape[0], self._N))
                if self._prefer_scaling:
                    self.compute_normalized_observation_probabilities(mixture_component, sequence, observation_probabilities)
                    alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities, self._pi_normalized[mixture_component], self._A_normalized[mixture_component], c, alpha)
                if not alpha_pass_success:
                    self.compute_normalized_log_observation_probabilities(mixture_component, sequence, observation_probabilities)
                    forward_backward.log_alpha_pass(observation_probabilities, self._log_normalized_pi[mixture_component], self._log_normalized_A[mixture_component], alpha, self._work_buffer)
                    loglikelihoods[o_i, mixture_component] = _logsumexp(alpha[-1, :])
                else:
                    log_prob = 0
                    for t in range(c.shape[0]):
                        log_prob -= log(c[t])
                    loglikelihoods[o_i, mixture_component] = log_prob
                loglikelihoods[o_i, mixture_component] += self._log_normalized_mixture_weights[mixture_component]
        return np.asarray(loglikelihoods)

    @cython.boundscheck(False)
    @cython.wraparound(True)
    @cython.initializedcheck(False)
    cpdef tuple deviance_information_criterion(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            DOUBLE_DTYPE_t total_log_prob
            Py_ssize_t o_i, t, i, j, mixture_component, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] alpha, beta
            DOUBLE_DTYPE_t[:] c
            DOUBLE_DTYPE_t p_d
            DOUBLE_DTYPE_t[:] deviances = np.zeros(lengths.shape[0])
            DOUBLE_DTYPE_t[:,:] pds = np.zeros((lengths.shape[0], self._n_mixture_components))
            DOUBLE_DTYPE_t[:, :] observation_probabilities
            const DOUBLE_DTYPE_t[:, :] sequence
            DOUBLE_DTYPE_t[:] logikelihoods = np.zeros(lengths.shape[0])
            DOUBLE_DTYPE_t log_prob
            DOUBLE_DTYPE_t[:, :, :] digamma
            DOUBLE_DTYPE_t[:, :] digamma_1
            base.EmissionsBase emissions
            DOUBLE_DTYPE_t[:] membership_probabilities = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)
            DOUBLE_DTYPE_t[:] normalized_mixture_component_probabilities = np.zeros(self._n_mixture_components, dtype=DOUBLE_DTYPE)
            bint alpha_pass_success

        self._update_subnormalized()
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            seq_end = seq_start + lengths[o_i]
            sequence = observed[seq_start:seq_end]
            seq_start = seq_end
            if o_i == 0 or observation_probabilities.shape[0] != sequence.shape[0]:
                c = np.zeros((sequence.shape[0],), dtype=DOUBLE_DTYPE)
                observation_probabilities = np.zeros((sequence.shape[0], self._N))
                alpha = np.zeros((sequence.shape[0], self._N), dtype=DOUBLE_DTYPE)
                beta = np.zeros((sequence.shape[0], self._N), dtype=DOUBLE_DTYPE)
                digamma = np.zeros((sequence.shape[0], self._N, self._N), dtype=DOUBLE_DTYPE)
                digamma_1 = np.zeros((sequence.shape[0], self._N), dtype=DOUBLE_DTYPE)

            for mixture_component in range(self._n_mixture_components):
                if self._prefer_scaling:
                    self.compute_subnormalized_observation_probabilities(mixture_component, sequence, observation_probabilities)
                    alpha_pass_success = forward_backward.scaling_alpha_pass(observation_probabilities, self._pi_subnormalized[mixture_component], self._A_subnormalized[mixture_component], c, alpha)
                if not alpha_pass_success:
                    self.compute_subnormalized_log_observation_probabiltiies(mixture_component, sequence, observation_probabilities)
                    forward_backward.log_alpha_pass(observation_probabilities, self._log_subnormalized_pi[mixture_component], self._log_subnormalized_A[mixture_component], alpha, self._work_buffer)
                    logikelihoods[o_i] = _logsumexp(alpha[-1, :])
                else:
                    log_prob = 0
                    for t in range(c.shape[0]):
                        log_prob -= log(c[t])
                    logikelihoods[o_i] = log_prob

                if alpha_pass_success:
                    # Update Beta
                    forward_backward.scaling_beta_pass(observation_probabilities, self._A_subnormalized[mixture_component], c, beta)
                    forward_backward.scaling_compute_digammas(self._A_subnormalized[mixture_component], alpha, beta, observation_probabilities, digamma, digamma_1)
                else:
                    forward_backward.log_beta_pass(observation_probabilities, self._log_subnormalized_A[mixture_component], beta, self._work_buffer)
                    forward_backward.log_compute_digammas(self._log_subnormalized_A[mixture_component], alpha, beta, observation_probabilities, digamma, digamma_1)

                membership_probabilities[mixture_component] = self._log_subnormalized_mixture_weights[mixture_component] + log_prob

            total_log_prob = _logsumexp(membership_probabilities)
            for mixture_component in range(self._n_mixture_components):
                normalized_mixture_component_probabilities[mixture_component] = exp(membership_probabilities[mixture_component] - total_log_prob)

            # Deviance component for the mixture weights
            for mixture_component in range(self._n_mixture_components):
                pds[o_i, mixture_component] += 2 * normalized_mixture_component_probabilities[mixture_component] * (self._log_normalized_mixture_weights[mixture_component] - self._log_subnormalized_mixture_weights[mixture_component])
            # Deviance component for component hmm parameter
            for mixture_component in range(self._n_mixture_components):
                for i in range(self._N):
                    pds[o_i, mixture_component] += 2 * normalized_mixture_component_probabilities[mixture_component] * digamma_1[0, i] * (self._log_normalized_pi[mixture_component, i] - self._log_subnormalized_pi[mixture_component, i])

                for t in range(0, sequence.shape[0]-1):
                    for i in range(self._N):
                        for j in range(self._N):
                            pds[o_i, mixture_component] += 2 * normalized_mixture_component_probabilities[mixture_component] * digamma[t, i, j] * (self._log_normalized_A[mixture_component, i, j] - self._log_subnormalized_A[mixture_component, i, j])

            for mixture_component in range(self._n_mixture_components):
                emissions = self._emissions[mixture_component]
                pds[o_i, mixture_component] +=  normalized_mixture_component_probabilities[mixture_component] * emissions.dic_contribution(digamma_1)

            p_d = np.asarray(pds[o_i]).sum()

            deviances[o_i] = 2 * p_d - 2 * total_log_prob
        return np.asarray(pds), np.asarray(deviances)


    @cython.boundscheck(False)
    @cython.initializedcheck(False)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths):
        cdef:
            Py_ssize_t o_i, mixture_component, seq_start, seq_end
            DOUBLE_DTYPE_t[:, :] observation_probabilities;
            const DOUBLE_DTYPE_t[:, :] sequence
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] component_probabilities  = np.asarray(self.mixture_loglik(observed, lengths))
            np.ndarray[INT_DTYPE_t, ndim=1] component_assignments  = component_probabilities.argmax(axis=1)
            list paths

        paths = []
        seq_start = 0
        for o_i in range(lengths.shape[0]):
            mixture_component = component_assignments[o_i]
            seq_end = lengths[o_i]
            sequence = observed[seq_start:seq_end]
            seq_start = seq_end
            observation_probabilities = np.zeros((sequence.shape[0], self._N))
            self.compute_normalized_observation_probabilities(mixture_component, sequence, observation_probabilities)
            paths.extend(
                viterbi.viterbi_path(self._pi_normalized[mixture_component], self._A_normalized[mixture_component], observation_probabilities)
            )

        return np.asarray(paths)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    @cython.initializedcheck(False)
    cdef _accumulate_statistics(self, const DOUBLE_DTYPE_t[:, :] observation_sequence, DOUBLE_DTYPE_t[:] normalized_mixture_component_probabilities, DOUBLE_DTYPE_t[:, :, :, :] digamma, DOUBLE_DTYPE_t[:, :, :] digamma_1):
        cdef:
            Py_ssize_t mixture_component, t, i, j, last_t
            base.EmissionsBase emissions

        for mixture_component in range(self._n_mixture_components):
            self._next_mixture_weights[mixture_component] += normalized_mixture_component_probabilities[mixture_component]

            for i in range(self._N):
                self._next_pi[mixture_component, i] += normalized_mixture_component_probabilities[mixture_component] * digamma_1[mixture_component, 0, i]

            last_t = observation_sequence.shape[0] - 1
            for i in range(self._N):
                for j in range(self._N):
                    for t in range(last_t):
                        self._next_A[mixture_component, i, j] += normalized_mixture_component_probabilities[mixture_component] * digamma[mixture_component, t, i, j]
            emissions = self._emissions[mixture_component]
            emissions.accumulate_statistics(digamma_1[mixture_component], observation_sequence, normalized_mixture_component_probabilities[mixture_component])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.initializedcheck(False)
    @cython.cdivision(True)
    cdef _m_step(self):
        cdef:
            Py_ssize_t mixture_component, i, j
            base.EmissionsBase emissions

        self._pi_posterior[:] = 0

        self._A_posterior[:] = 0
        for mixture_component in range(self._n_mixture_components):
            self._mixture_weights_posterior[mixture_component] = self._mixture_weights_prior[mixture_component] + self._next_mixture_weights[mixture_component]

            for i in range(self._N):
                self._pi_posterior[mixture_component, i] = self._next_pi[mixture_component, i] + self._pi_prior[mixture_component, i]

            for i in range(self._N):
                for j in range(self._N):
                    self._A_posterior[mixture_component, i, j] = self._next_A[mixture_component, i, j] + self._A_prior[mixture_component, i, j]

            emissions = self._emissions[mixture_component]
            emissions.m_step()

        self._next_mixture_weights[:] = 0
        self._next_A[:] = 0
        self._next_pi[:] = 0

    def sample(self, n_samples=1, length=10, random_state=None):
        cdef:
            Py_ssize_t currstate, hidden_state, j
            list hidden_sequences, observation_sequences, lengths
            np.ndarray[DOUBLE_DTYPE_t, ndim=2] A_cdf
            np.ndarray[DOUBLE_DTYPE_t, ndim=1] pi_cdf
        assert random_state is not None
        self._update_subnormalized()
        mixture_components = []
        hidden_sequences = []
        observation_sequences = []
        lengths = []
        mixture_cdf = np.cumsum(self.mixture_weights)
        pi_cdf = np.cumsum(self.A, axis=1)
        A_cdf = np.cumsum(self.A, axis=2)
        for _ in range(n_samples):
            hidden_sequence = []
            observation_sequence = []
            rand = random_state.rand()
            mixture_component =  (mixture_cdf > rand).argmax()

            for j in range(length):
                rand = random_state.rand()
                if j == 0:
                    currstate = (pi_cdf[mixture_component] > rand).argmax()
                else:
                    currstate = (A_cdf[mixture_component][currstate] > rand).argmax()

                hidden_sequence.append(currstate)
                observed = self._emissions[mixture_component].sample(currstate, random_state)
                observation_sequence.append(observed)

            mixture_components.extend(mixture_component)
            hidden_sequences.extend(hidden_sequence)
            observation_sequences.extend(observation_sequence)
            lengths.append(length)
        return np.asarray(mixture_components), np.asarray(observation_sequences), np.asarray(hidden_sequences), np.asarray(lengths)

    #
    # Compute probabilities for the entire sequence
    #
    cdef compute_normalized_observation_probabilities(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, j
            base.EmissionsBase emissions
        emissions = self._emissions[mixture_component]
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for j in range(self._N):
                observation_probabilities[t, j] = emissions.normalized_density_for(j, observations[t])

    cdef compute_subnormalized_observation_probabilities(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, j
            base.EmissionsBase emissions
        emissions = self._emissions[mixture_component]
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for j in range(self._N):
                observation_probabilities[t, j] = emissions.subnormalized_density_for(j, observations[t])

    cdef compute_normalized_log_observation_probabilities(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, j
            base.EmissionsBase emissions
        emissions = self._emissions[mixture_component]
        # Compute Probabiltiies
        for t in range(observations.shape[0]):
            for j in range(self._N):
                observation_probabilities[t, j] = emissions.normalized_log_density_for(j, observations[t])

    cdef compute_subnormalized_log_observation_probabiltiies(self, Py_ssize_t mixture_component, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities):
        cdef:
            Py_ssize_t t, j
            base.EmissionsBase emissions
        # Compute Probabiltiies
        emissions = self._emissions[mixture_component]
        for t in range(observations.shape[0]):
            for j in range(self._N):
                observation_probabilities[t, j] = emissions.subnormalized_log_density_for(j, observations[t])
