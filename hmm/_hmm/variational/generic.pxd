# cython: profile=False
# cython: language_level=3
cimport numpy as np
from libc.math cimport NAN
from hmm.utilities.types cimport DOUBLE_DTYPE_t, INT_DTYPE_t
from .emissions cimport base

cdef class HMMTrainer:
    # initial state
    cdef public DOUBLE_DTYPE_t[:] _pi_posterior
    cdef public DOUBLE_DTYPE_t[:] _pi_subnormalized
    cdef public DOUBLE_DTYPE_t[:] _pi_normalized
    cdef public DOUBLE_DTYPE_t[:] _pi_prior
    cdef public DOUBLE_DTYPE_t[:] _log_subnormalzed_pi
    cdef public DOUBLE_DTYPE_t[:] _log_normalized_pi
    cdef public DOUBLE_DTYPE_t[:] _pi_statistics

    # transition probabilities
    cdef public DOUBLE_DTYPE_t[:, :] _A_posterior
    cdef public DOUBLE_DTYPE_t[:, :] _A_subnormalized
    cdef public DOUBLE_DTYPE_t[:, :] _A_normalized
    cdef public DOUBLE_DTYPE_t[:, :] _A_prior

    cdef public DOUBLE_DTYPE_t[:, :] _log_subnormalized_A
    cdef public DOUBLE_DTYPE_t[:, :] _log_normalized_A
    cdef public DOUBLE_DTYPE_t[:, :] _A_statistics

    cdef public base.EmissionsBase _emissions
    # For log path
    cdef bint _prefer_scaling
    cdef bint _allowed_to_use_log
    cdef public DOUBLE_DTYPE_t[:] _work_buffer

    # Number of hidden states
    cdef public Py_ssize_t _N

    cdef public object logger

    cpdef list train(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, Py_ssize_t iterations=?, DOUBLE_DTYPE_t tol=?)
    cpdef tuple deviance_information_criterion(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] loglik(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cdef _update_subnormalized(self)

    cpdef tuple scaling_alpha_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=3] log_alpha_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cpdef scaling_beta_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths, DOUBLE_DTYPE_t[:] c)
    cpdef log_beta_pass(self, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cpdef scaling_compute_digammas(self, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)
    cpdef log_compute_digammas(self, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observed, const Py_ssize_t[:] lengths)

    cdef _accumulate_statistics(self, const DOUBLE_DTYPE_t[:, :] observation_sequence, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1)

    cdef _m_step(self)
    cpdef log_state(self)

    cdef compute_normalized_observation_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cdef compute_subnormalized_observation_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cdef compute_normalized_log_observation_probabilities(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
    cdef compute_subnormalized_log_observation_probabiltiies(self, const DOUBLE_DTYPE_t[:, :] observations, DOUBLE_DTYPE_t[:, :] observation_probabilities)
