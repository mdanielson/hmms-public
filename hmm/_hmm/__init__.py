# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

from .em import (new_categorical, new_gaussian, new_gaussian_mixture_model,
                 new_multivariate_gaussian, new_poisson)
from .variational import (new_variational_categorical,
                          new_variational_gaussian,
                          new_variational_multivariate_gaussian,
                          new_variational_gmm,
                          new_variational_poisson)
