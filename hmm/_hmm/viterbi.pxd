
cimport numpy as np
from hmm.utilities.types cimport DOUBLE_DTYPE_t

cpdef tuple single_viterbi_probability(DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, const DOUBLE_DTYPE_t[:, :] observation_probabilities)
cpdef np.ndarray[DOUBLE_DTYPE_t, ndim=1] viterbi_path(DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, const DOUBLE_DTYPE_t[:, :] observation_probabilities)
