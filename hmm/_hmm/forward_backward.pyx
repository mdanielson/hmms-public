# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3
"""
Cython implementations of the forward backwards routinges
"""
cimport cython
cimport numpy as np
import numpy as np


from libc.math cimport log, exp, isnan, isinf
import datetime
from hmm.utilities.types cimport DOUBLE_DTYPE_t
from hmm.utilities.types import DOUBLE_DTYPE, INT_DTYPE
from hmm.utilities.log_routines cimport _logsumexp

cdef DOUBLE_DTYPE_t EPSILON =  1e-10

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
@cython.initializedcheck(False)
cdef bint scaling_alpha_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:] c, DOUBLE_DTYPE_t[:, :] alpha):
    """
    Compute the alpha probabilities using scaling:
        P(O_1, O_2, ..., O_t, q_t=S_i | model)

    Let N be the number of hidden states, Let T be the number of observations in the sequence.
    For a reference, see https://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf.

    :param observation_probabilities:  An array of shape (T, N) for a single sequence, containing the probability of
                                       observation being generated in each state.
    :param pi: An array of shape (N,), containing the probability of the sequence starting in each state
    :param A: An array of shape (N, N), containing the probability of transitioning from one state to another
    :param c: An array of shape (T,), will be used to store the scaling constants at each time t.
    :param alpha: An array of shape (T, N), will be used to compute the alpha probabilities

    :return: A boolean flag if we experienced overflow/underflow, or if we succeeded.
    :rtype bint:
    """
    cdef:
        Py_ssize_t t, i, j, N, T
        DOUBLE_DTYPE_t alpha_t
        DOUBLE_DTYPE_t one = 1.0

    # start = datetime.datetime.now()
    N = alpha.shape[1]
    T = observation_probabilities.shape[0]
    c[:] = 0
    alpha[:] = 0

    # Compute the likelihood of each observation given state i
    for i in range(N):
        alpha[0, i] = pi[i] * observation_probabilities[0, i]

    # Compute the scaling constants
    for i in range(N):
        c[0] += alpha[0, i]

    # Scale A_0_i
    if c[0] < EPSILON:
        return False
    else:
        c[0] = one/c[0]

    for i in range(N):
        alpha[0, i] = c[0] * alpha[0, i]

    # Compute Rest of Alpha
    for t in range(1, T):
        c[t] = 0
        for i in range(N):
            alpha[t, i] = 0
            for j in range(N):
                alpha[t, i] = alpha[t, i] + alpha[t-1 , j] * A[j, i]

            alpha[t, i] = alpha[t, i] * observation_probabilities[t, i]

        # c[t] = c[t] + alpha[:, t].sum()
        alpha_t = 0
        for i in range(N):
            alpha_t += alpha[t, i]

        c[t] += alpha_t
        # C smaller thant C
        if c[t] > EPSILON:
            c[t] = one/c[t]
        else:
            return False

        for i in range(N):
            alpha[t, i] = c[t] * alpha[t, i]

    #finish = datetime.datetime.now()
    #print(N,(finish - start) /datetime.timedelta(microseconds=1))
    return True

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cdef void log_alpha_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:] log_pi, DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:] work_buffer):
    """
    Compute the alpha probabilities using logarithms
        P(O_1, O_2, ..., O_t, q_t=S_i | model)

    Let N be the number of hidden states, Let T be the number of observations in the sequence.
    For a reference, see https://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf.

    :param observation_probabilities:  An array of shape (T, N) for a single sequence, containing the log probability of
                                       observation being generated in each state.
    :param log_pi: An array of shape (N,), containing the log probability of the sequence starting in each state
    :param log_A: An array of shape (N, N), containing the log probability of transitioning from one state to another
    :param alpha: An array of shape (T, N), will be used to compute the alpha probabilities
    :param work_buffer: An array of shape (T, ).  Will be used for temporary storage internally.

    :return: A boolean flag if we experienced overflow/underflow, or if we succeeded.
    :rtype bint:
    """
    cdef:
        Py_ssize_t t, i, j, T, N
    alpha[:] = 0
    N = alpha.shape[1]
    T = observation_probabilities.shape[0]

    # ALPHA PASS
    # Compute A_0_i
    for i in range(N):
        alpha[0, i] = log_pi[i] + observation_probabilities[0, i]
    # Compute Rest of Alpha
    for t in range(1, T):
        for i in range(N):
            for j in range(N):
                work_buffer[j] = alpha[t-1 , j] + log_A[j, i]
            alpha[t, i] = _logsumexp(work_buffer) + observation_probabilities[t, i]

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cdef void scaling_beta_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:] c, DOUBLE_DTYPE_t[:, :] beta):
    """
    Compute the backwards probabilities using scaling.

    Let N be the number of hidden states, Let T be the number of observations in the sequence.
    For a reference, see https://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf.

    :param observation_probabilities:  An array of shape (T, N) for a single sequence, containing the probability of
                                       observation being generated in each state.
    :param A: An array of shape (N, N), containing the probability of transitioning from one state to another
    :param c: An array of shape (T,), containing the scaling factors from the forward pass.
    :param beta: An array of shape (T, N), will be used to compute the beta probabilities

    """
    cdef:
        Py_ssize_t t, i, j, sequence_length
        Py_ssize_t N = A.shape[0]

    beta[:] = 0
    sequence_length = observation_probabilities.shape[0] - 1
    beta[sequence_length, :] = c[sequence_length]
    # Pretty neat the cython doe the right thing with reversed(range(sequence_length))
    for t in reversed(range(sequence_length)):
        for i in range(N):
            for j in range(N):
                beta[t, i] = beta[t, i] + A[i, j] * observation_probabilities[t+1, j] * beta[t + 1, j]
            beta[t, i] = c[t] * beta[t, i]

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cdef void log_beta_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] beta, DOUBLE_DTYPE_t[:] work_buffer):
    """
    Compute the backwards probabilities using logarithms.

    Let N be the number of hidden states, Let T be the number of observations in the sequence.
    For a reference, see https://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf.

    :param observation_probabilities:  An array of shape (T, N) for a single sequence, containing the log probability of
                                       observation being generated in each state.
    :param log_A: An array of shape (N, N), containing the log probability of transitioning from one state to another
    :param beta: An array of shape (T, N), will be used to compute the beta probabilities
    :param work_buffer: An array of shape (T, ).  Will be used for temporary storage internally.
    """
    cdef:
        Py_ssize_t t, i, j, sequence_length
        Py_ssize_t N = log_A.shape[0]

    beta[:] = 0
    sequence_length = observation_probabilities.shape[0] - 1
    beta[sequence_length, :] = log(1)
    for t in reversed(range(sequence_length)):
        for i in range(N):
            for j in range(N):
                work_buffer[j] = log_A[i, j] + observation_probabilities[t+1, j] + beta[t + 1, j]
            beta[t, i] = _logsumexp(work_buffer)


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cdef scaling_compute_digammas(DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1):
    """
    Compute the *digammas* from the alpha and beta passes using scaling

    Let N be the number of hidden states, Let T be the number of observations in the sequence.
    For a reference, see https://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf.

    :param A: An array of shape (N, N), containing the log probability of transitioning from one state to another
    :param alpha: An array of shape (T, N) containing the results of calling scaling_alpha_pass.
    :param beta: An array of shape (T, N) containing th results of calling scaling_beta_pass.
    :param observation_probabilities:  An array of shape (T, N) for a single sequence, containing the log probability of
                                       observation being generated in each state.
    :param digamma: An array of shape (T, N, N).
    :param digamma_1: An array of shape (T, N ).
    """

    cdef:
        Py_ssize_t t, i, j, sequence_length
        Py_ssize_t N = A.shape[0]

    digamma[:] = 0
    digamma_1[:] = 0
    sequence_length = observation_probabilities.shape[0]-1
    for t in range(sequence_length):
        for i in range(N):
            for j in range(N):
                digamma[t, i, j] = alpha[t, i] * A[i, j] * observation_probabilities[t+1, j] * beta[t+1, j]
            for j in range(N):
                digamma_1[t, i] += digamma[t, i, j]
    digamma_1[sequence_length, :] = alpha[sequence_length, :]

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cdef log_compute_digammas(DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1):
    """
    Compute the *digammas* from the alpha and beta passes using logarithms

    Let N be the number of hidden states, Let T be the number of observations in the sequence.
    For a reference, see https://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf.

    :param log_A: An array of shape (N, N), containing the log probability of transitioning from one state to another
    :param alpha: An array of shape (T, N) containing the results of calling scaling_alpha_pass.
    :param beta: An array of shape (T, N) containing th results of calling scaling_beta_pass.
    :param observation_probabilities:  An array of shape (T, N) for a single sequence, containing the log probability of
                                       observation being generated in each state.
    :param digamma: An array of shape (T, N, N).
    :param digamma_1: An array of shape (T, N ).
    """
    cdef:
        Py_ssize_t t, i, j, sequence_length
        DOUBLE_DTYPE_t denom
        Py_ssize_t N = log_A.shape[0]

    digamma[:] = 0
    digamma_1[:] = 0
    sequence_length = observation_probabilities.shape[0]-1
    denom = _logsumexp(alpha[sequence_length, :])
    for t in range(sequence_length):
        for i in range(N):
            for j in range(N):
                digamma[t, i, j] = exp(alpha[t, i] + log_A[i, j] + observation_probabilities[t+1, j] +  beta[t+1, j] - denom)
            for j in range(N):
                digamma_1[t, i] += digamma[t, i, j]

    for i in range(N):
        digamma_1[sequence_length, i] = exp(alpha[sequence_length, i] - denom)


cpdef wrapped_scaling_alpha_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:] pi, DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:] c, DOUBLE_DTYPE_t[:, :] alpha):
    return scaling_alpha_pass(observation_probabilities, pi, A, c, alpha)

cpdef wrapped_log_alpha_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:] log_pi, DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] alpha):
    work_buffer = np.zeros((log_pi.shape[0]), dtype=DOUBLE_DTYPE)
    return log_alpha_pass(observation_probabilities, log_pi, log_A, alpha, work_buffer)

cpdef wrapped_scaling_beta_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:] c, DOUBLE_DTYPE_t[:, :] beta):
    return scaling_beta_pass(observation_probabilities, A, c, beta)

cpdef wrapped_log_beta_pass(const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] beta):
    work_buffer = np.zeros((log_A.shape[0]), dtype=DOUBLE_DTYPE)
    log_beta_pass(observation_probabilities, log_A, beta, work_buffer)

cpdef wrapped_scaling_compute_digammas(DOUBLE_DTYPE_t[:, :] A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1):
    scaling_compute_digammas(A, alpha, beta, observation_probabilities, digamma, digamma_1)

cpdef wrapped_log_compute_digammas(DOUBLE_DTYPE_t[:, :] log_A, DOUBLE_DTYPE_t[:, :] alpha, DOUBLE_DTYPE_t[:, :] beta, const DOUBLE_DTYPE_t[:, :] observation_probabilities, DOUBLE_DTYPE_t[:, :, :] digamma, DOUBLE_DTYPE_t[:, :] digamma_1):
    log_compute_digammas(log_A, alpha, beta, observation_probabilities, digamma, digamma_1)
