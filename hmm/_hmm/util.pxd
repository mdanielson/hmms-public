# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# cython: profile=False
# cython: language_level=3

from hmm.utilities.types cimport DOUBLE_DTYPE_t

cdef DOUBLE_DTYPE_t multivariate_normal_density(const DOUBLE_DTYPE_t[:] observed, DOUBLE_DTYPE_t[:] mean, DOUBLE_DTYPE_t[:, :] precision, DOUBLE_DTYPE_t two_pi_k_det, DOUBLE_DTYPE_t[:] tmp) nogil
cdef DOUBLE_DTYPE_t log_multivariate_normal_density(const DOUBLE_DTYPE_t[:] observed, DOUBLE_DTYPE_t[:] mean, DOUBLE_DTYPE_t[:, :] precision, DOUBLE_DTYPE_t log_two_pi_k_det, DOUBLE_DTYPE_t[:] tmp) nogil
