# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

import abc
import base64
import numbers
import pickle
from abc import abstractmethod

import numpy as np


import sklearn.base
import sklearn.utils
from sklearn.utils.validation import check_is_fitted

from . import base
from .distributions import dirichlet


class VariationalMHMMBase(base.VariationalBase, base.MHMMMixin):

    def __init__(self, n_mixture_components, n_components, n_iterations, n_inits, n_jobs, tol, mixture_weights_prior, mixture_weights_posterior,
                 pi_prior, pi_posterior, A_prior, A_posterior, implementation,
                 prior_threshold_for_num_free_params=1e-2, allowed_to_use_log=True, random_state=None, verbose=0):
        """

        Parameters
        ----------
        n_components : int, defaults to 1
            The number of hidden states for the HMM

        n_iterations : int, defaults to 100
            The number of iterations to run for EM

        n_inits : int, defaults to 1
            The number of random initializations to run. The best results are kept

        """
        self.n_mixture_components = n_mixture_components
        self.n_components = n_components
        self.n_iterations = n_iterations
        self.n_inits = n_inits
        self.n_jobs = n_jobs
        self.tol = tol
        self.mixture_weights_prior = mixture_weights_prior
        self.mixture_weights_posterior = mixture_weights_posterior
        self.pi_prior = pi_prior
        self.pi_posterior = pi_posterior
        self.A_prior = A_prior
        self.A_posterior = A_posterior
        self.implementation = implementation
        self.prior_threshold_for_num_free_params = prior_threshold_for_num_free_params
        self.allowed_to_use_log = allowed_to_use_log
        self.random_state = random_state
        self.verbose = verbose

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
         * Ensure all matrices are properly normalized
         * Ensure all matrices have the correct shape
        """
        assert self.pi_posterior_.shape[1] == self.A_posterior_.shape[1], self.pi_posterior_
        assert self.A_posterior_.shape[1] == self.A_posterior_.shape[2], self.A_posterior_

    def _assert_is_fitted(self):
        super()._prepare_for_fit()
        check_is_fitted(self, ["mixture_weights_posterior_", "pi_posterior_", "A_posterior_"])

    def n_parameters(self):
        """
        Matrix rows must sum to 1, so final value is dependenct on other values
        """
        valid_mixtures = (self.mixture_weights_posterior_ - self.mixture_weights_prior_) > self.prior_threshold_for_num_free_params
        mixture_params = max(0, valid_mixtures.sum()-1)
        pi_params = 0
        A_params = 0
        B_params = 0

        for mixture_component, valid in enumerate(valid_mixtures):
            if not valid:
                continue
            pi_posterior = self.pi_posterior_[mixture_component]
            pi_prior = self.pi_prior_[mixture_component]
            A_posterior = self.A_posterior_[mixture_component]
            A_prior = self.A_prior_[mixture_component]
            pi_posterior = pi_posterior[(pi_posterior - pi_prior) > self.prior_threshold_for_num_free_params]
            pi_params += pi_posterior.shape[0] - 1
            for hidden_state in range(A_posterior.shape[0]):
                A_posterior_row = A_posterior[hidden_state]
                A_prior_row = A_prior[hidden_state]
                vals = A_posterior_row[(A_posterior_row - A_prior_row) > self.prior_threshold_for_num_free_params]
                num_params = vals.shape[0]
                if num_params > 0:
                    A_params += max(0, num_params-1)
                    B_params += self._emissions_params()

        return mixture_params + pi_params + A_params + B_params

    @abc.abstractmethod
    def _emissions_params(self):
        """
        Return the number of free parameters in this HMM.
        """
        raise NotImplementedError()

    def _init_for_fit(self, X, lengths):
        """
        Parameters
        ----------
        X : np.ndarray

        Initialize pi, A, and B.

        Christopher Bishop, Pattern Recognition and Machine Learning
            Page 617 - pi/A select random values subject to normalization and non-negativity

        Rabiner paper:
            Initialize pi/A to uniform
        """
        self._init_mixture_weights(X, lengths)
        self._init_pi(X, lengths)
        self._init_A(X, lengths)
        self._init_emissions(X, lengths)
        self._prepare_for_fit()

    def _init_mixture_weights(self, X, lengths):
        """
        Mixture weights initialized to uniform
        """
        if self.mixture_weights_prior is None:
            self.mixture_weights_prior_ = np.zeros(self.n_mixture_components) + 1/self.n_mixture_components
        else:
            self.mixture_weights_prior_ = np.zeros(self.n_mixture_components) + self.mixture_weights_prior

        self.mixture_weights_posterior_ = np.full((self.n_mixture_components,), 1/self.n_mixture_components) * lengths.shape[0]
        self.mixture_weights_normalized_ = self.mixture_weights_posterior_ / self.mixture_weights_posterior_.sum()

    def _init_pi(self, X, lengths):
        n_samples = lengths.shape[0]
        random_state = sklearn.utils.check_random_state(self.random_state)
        # Get default value for pi
        pi_prior = self.pi_prior
        if pi_prior is None:
            pi_prior = 1/self.n_components

        # Initialize Prior
        self.pi_prior_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
        if isinstance(pi_prior, numbers.Number):
            self.pi_prior_[:] = pi_prior
        elif isinstance(pi_prior, np.ndarray):
            if pi_prior.ndim == 1:
                for i in range(self.n_mixture_components):
                    self.pi_prior_[i] = pi_prior
            elif pi_prior.ndim == 2:
                self.pi_prior_ += pi_prior
            else:
                raise ValueError("Unknown value for pi_prior:{}".format(pi_prior))
        else:
            raise ValueError("Unknown value for pi_prior:{}".format(pi_prior))

        # Initialize Posterior
        if self.pi_posterior == "sample":
            self.pi_posterior_ = np.zeros_like(self.pi_prior_)
            for mixture_component in range(self.n_mixture_components):
                pi_samples = max(1, int(self.mixture_weights_normalized_[mixture_component] * n_samples))
                self.pi_posterior_[mixture_component] = dirichlet.setup_dirichlet_posterior(self.pi_prior_[mixture_component].reshape(1, -1), pi_samples, random_state).ravel()
        elif self.pi_posterior == "uniform":
            self.pi_posterior_ = np.copy(self.pi_prior_) * lengths.shape[0]
            for mixture_component in range(self.n_mixture_components):
                pi_samples = max(1, int(self.mixture_weights_normalized_[mixture_component] * n_samples))
                self.pi_posterior_[mixture_component] *= pi_samples
        else:
            raise ValueError("Unknown value for pi_posterior:{}".format(self.pi_posterior))

    def _init_A(self, X, lengths):
        random_state = sklearn.utils.check_random_state(self.random_state)
        n_samples = lengths.sum()

        A_prior = self.A_prior
        if A_prior is None:
            A_prior = 1 / self.n_components

        self.A_prior_ = np.zeros((self.n_mixture_components, self.n_components, self.n_components), dtype=float) + A_prior
        if isinstance(A_prior, numbers.Number):
            self.A_prior_[:] = A_prior
        elif isinstance(A_prior, np.ndarray):
            if A_prior.ndim == 2:
                for i in range(self.n_mixture_components):
                    self.A_prior_[i] = A_prior
            elif A_prior.ndim == 3:
                self.A_prior_ += A_prior
            else:
                raise ValueError("Unknown value for A_prior:{}".format(A_prior))
        else:
            raise ValueError("Unknown value for A_prior:{}".format(A_prior))

        if self.A_posterior == "sample":
            self.A_posterior_ = np.zeros_like(self.A_prior_)
            for mixture_component in range(self.n_mixture_components):
                A_samples = max(1, int(self.mixture_weights_normalized_[mixture_component] * n_samples))
                self.A_posterior_[mixture_component] = dirichlet.setup_dirichlet_posterior(self.A_prior_[mixture_component], A_samples, random_state)
        elif self.A_posterior == "uniform":
            self.A_posterior_ = np.copy(self.A_prior_) * lengths.shape[0]
            for mixture_component in range(self.n_mixture_components):
                A_samples = max(1, int(self.mixture_weights_normalized_[mixture_component] * n_samples))
                self.A_posterior_[mixture_component] *= A_samples

    @abstractmethod
    def _update_after_fit(self, trainer):
        """
        Called during fit().  Updates internal state with new learned parameters.
        """
        self.mixture_weights_posterior_ = np.asarray(trainer._mixture_weights_posterior)
        self.mixture_weights_normalized_ = np.asarray(trainer._normalized_mixture_weights)
        self.mixture_weights_log_normalized_ = np.log(self.mixture_weights_normalized_)
        self.mixture_weights_subnormalized_ = np.asarray(trainer._subnormalized_mixture_weights)

        self.pi_posterior_ = np.asarray(trainer._pi_posterior)
        self.pi_normalized_ = np.asarray(trainer._pi_normalized)
        self.pi_subnormalized_ = np.asarray(trainer._pi_subnormalized)

        self.A_posterior_ = np.asarray(trainer._A_posterior)
        self.A_normalized_ = np.asarray(trainer._A_normalized)
        self.A_subnormalized_ = np.asarray(trainer._A_subnormalized)

    def to_dict(self):
        """
        Serialize models... ignores random state
        """
        mixture_weights = None
        pi = None
        A = None
        if hasattr(self, "mixture_weights_"):
            mixture_weights = np.asarray(self.mixture_weights_).tolist()
        if hasattr(self, "pi_"):
            pi = np.asarray(self.pi_).tolist()

        if hasattr(self, "A_"):
            A = np.asarray(self.A_).tolist()

        params = self.get_params()
        params["random_state"] = base64.b64encode(pickle.dumps(params["random_state"])).decode("ascii")
        return {
            "params": params,
            "mixture_weights": mixture_weights,
            "pi": pi,
            "A": A,
            "emissions": self._emissions_to_dict(),
        }

    @classmethod
    def from_dict(cls, blob):
        instance = cls()
        instance.set_params(**blob["params"])
        instance.random_state = pickle.loads(base64.b64decode(instance.random_state))
        if blob["mixture_weights"] is not None:
            instance.mixture_weights_ = np.asarray(blob["mixture_weights"])
        if blob["pi"] is not None:
            instance.pi_ = np.asarray(blob["pi"])
        if blob["A"] is not None:
            instance.A_ = np.asarray(blob["A"])
        instance._emissions_from_dict(blob["emissions"])
        return instance
