# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import abc
import base64
import numbers
import pickle

import numpy as np

import sklearn.base
import sklearn.utils
from sklearn.utils.validation import check_is_fitted

from . import base
from .distributions import dirichlet


class VariationalHMMBase(base.VariationalBase, base.HMMMixin):

    def __init__(self, n_components, n_iterations, n_inits, n_jobs, tol,
                 pi_prior, pi_posterior, A_prior, A_posterior,
                 prior_threshold_for_num_free_params=1e-2, implementation="scaling",
                 allowed_to_use_log=True, random_state=None, verbose=0):
        """

        Parameters
        ----------
        n_components : int, defaults to 1
            The number of hidden states for the HMM

        n_iterations : int, defaults to 100
            The number of iterations to run for EM

        n_inits : int, defaults to 1
            The number of random initializations to run. The best results are kept

        """
        self.n_components = n_components
        self.n_iterations = n_iterations
        self.n_inits = n_inits
        self.n_jobs = n_jobs
        self.tol = tol
        self.pi_prior = pi_prior
        self.pi_posterior = pi_posterior
        self.A_prior = A_prior
        self.A_posterior = A_posterior
        self.implementation = implementation
        self.prior_threshold_for_num_free_params = prior_threshold_for_num_free_params
        self.allowed_to_use_log = allowed_to_use_log
        self.random_state = random_state
        self.verbose = verbose

    def _reestimate_with_prior(self, pi, A, state_counts, lengths):

        self.pi_prior_ = pi
        self.pi_posterior_ = pi * lengths.shape[0]
        self.pi_normalized_ = pi
        self.pi_subnormalized_ = pi

        self.A_posterior_ = np.zeros_like(A)
        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                self.A_posterior_[i, j] = A[i, j] * state_counts[i]

        self.A_prior_ = A
        self.A_normalized_ = A
        self.A_subnormalized_ = A

    def _assert_is_fitted(self):
        check_is_fitted(self, ["pi_posterior_", "A_posterior_", ])

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
         * Ensure all matrices are properly normalized
         * Ensure all matrices have the correct shape
        """
        super(VariationalHMMBase, self)._prepare_for_fit()
        self.A_posterior_ = np.asarray(self.A_posterior_, dtype=float)
        self.pi_posterior_ = np.asarray(self.pi_posterior_, dtype=float)

    def n_parameters(self):
        """
        Matrix rows must sum to 1, so final value is dependenct on other values
        """

        pi = self.pi_posterior_[(self.pi_posterior_ - self.pi_prior_) > self.prior_threshold_for_num_free_params]
        pi_params = pi.shape[0] - 1
        A_params = B_params = 0
        for hidden_state in range(self.A_posterior_.shape[0]):
            counts = self.A_posterior_[hidden_state]
            prior = self.A_prior_[hidden_state]
            vals = counts[(counts - prior) > self.prior_threshold_for_num_free_params]
            A_params += max(0, vals.shape[0] - 1)
            B_params += self._emissions_params()

        return pi_params + A_params + B_params

    @abc.abstractmethod
    def _emissions_params(self):
        """
        Return the number of free parameters in this HMM.
        """
        raise NotImplementedError()

    def _init_for_fit(self, X, lengths):
        """
        Parameters
        ----------
        X : np.ndarray

        Initialize pi, A, and B.

        Christopher Bishop, Pattern Recognition and Machine Learning
            Page 617 - pi/A select random values subject to normalization and non-negativity

        Rabiner paper:
            Initialize pi/A to uniform
        """
        self._init_pi(X, lengths)
        self._init_A(X, lengths)
        self._init_emissions(X, lengths)
        self._prepare_for_fit()

    def _init_pi(self, X, lengths):
        pi_prior = self.pi_prior
        random_state = sklearn.utils.check_random_state(self.random_state)
        if pi_prior is None or isinstance(pi_prior, np.ndarray) \
           or isinstance(pi_prior, numbers.Number):
            # Default initialization
            if pi_prior is None:
                pi_prior = 1/self.n_components

            self.pi_prior_ = np.zeros((self.n_components,), dtype=float) + pi_prior
        else:
            raise ValueError("Unknown value for pi_prior:{}".format(self.pi_prior))
        if self.pi_posterior == "sample":
            self.pi_posterior_ = dirichlet.setup_dirichlet_posterior(self.pi_prior_.reshape(1, -1), lengths.shape[0], random_state).ravel()
        elif self.pi_posterior == "uniform":
            self.pi_posterior_ = np.full((self.n_components,), 1/self.n_components) * lengths.shape[0]
        else:
            raise ValueError("Unknown value for pi_posterior:{}".format(self.pi_prior))

    def _init_A(self, X, lengths):
        random_state = sklearn.utils.check_random_state(self.random_state)
        A_prior = self.A_prior
        if A_prior is None or isinstance(A_prior, np.ndarray) \
           or isinstance(self.A_prior, numbers.Number):
            # Default initialization
            if A_prior is None:
                A_prior = 1/self.n_components

            self.A_prior_ = np.zeros((self.n_components, self.n_components), dtype=float) + A_prior
        else:
            raise ValueError("Unknown value for A_prior:{}".format(self.A_prior))

        if self.A_posterior == "sample":
            self.A_posterior_ = dirichlet.setup_dirichlet_posterior(self.A_prior_, lengths.sum(), random_state)
        elif self.A_posterior == "uniform":
            self.A_posterior_ = np.full((self.n_components, self.n_components), 1/self.n_components) * lengths.sum()
        else:
            raise ValueError("Unknown value for A_posterior:{}".format(self.pi_prior))

    def _update_after_fit(self, trainer):
        """
        Called during fit().  Updates internal state with new learned parameters.
        """
        self.pi_posterior_ = trainer.pi_posterior
        self.pi_normalized_ = trainer.pi_normalized
        self.pi_subnormalized_ = trainer.pi_subnormalized

        self.A_posterior_ = trainer.A_posterior
        self.A_normalized_ = trainer.A_normalized
        self.A_subnormalized_ = trainer.A_subnormalized

    def to_dict(self):
        """
        Serialize models... ignores random state
        """
        pi_prior_ = pi_posterior_ = None
        A_prior_ = A_posterior_ = None
        pi_normalized_ = pi_subnormalized_ = None
        A_normalized_ = A_subnormalized_ = None
        lower_bound = None
        explored_lower_bounds_ = None
        if hasattr(self, "pi_posterior_"):
            pi_prior_ = np.asarray(self.pi_prior_).tolist()
            pi_posterior_ = np.asarray(self.pi_posterior_).tolist()
            pi_normalized_ = np.asarray(self.pi_normalized_).tolist()
            pi_subnormalized_ = np.asarray(self.pi_subnormalized_).tolist()

            A_prior_ = np.asarray(self.A_prior_).tolist()
            A_posterior_ = np.asarray(self.A_posterior_).tolist()
            A_normalized_ = np.asarray(self.A_normalized_).tolist()
            A_subnormalized_ = np.asarray(self.A_subnormalized_).tolist()
            lower_bound = self.lower_bound_ if hasattr(self, "lower_bound_") else None
            explored_lower_bounds_ = self.explored_lower_bounds_ if hasattr(self, "explored_lower_bounds_") else None
        params = self.get_params()
        params["random_state"] = base64.b64encode(pickle.dumps(params["random_state"])).decode("ascii")
        return {
            "params": params,
            "pi_prior_": pi_prior_,
            "pi_posterior_": pi_posterior_,
            "pi_normalized_": pi_normalized_,
            "pi_subnormalized_": pi_subnormalized_,

            "A_prior_": A_prior_,
            "A_posterior_": A_posterior_,
            "A_normalized_": A_normalized_,
            "A_subnormalized_": A_subnormalized_,
            "emissions": self._emissions_to_dict(),
            "lower_bound_": lower_bound,
            "explored_lower_bounds_": explored_lower_bounds_,
        }

    @classmethod
    def from_dict(cls, blob):
        instance = cls()
        instance.set_params(**blob["params"])
        instance.random_state = pickle.loads(base64.b64decode(instance.random_state))
        if blob["pi_posterior_"] is not None:
            instance.pi_posterior_ = np.asarray(blob["pi_posterior_"])
            instance.pi_prior_ = np.asarray(blob["pi_prior_"])
            instance.pi_normalized_ = np.asarray(blob["pi_normalized_"])
            instance.pi_subnormalized_ = np.asarray(blob["pi_subnormalized_"])
            instance.A_posterior_ = np.asarray(blob["A_posterior_"])
            instance.A_prior_ = np.asarray(blob["A_prior_"])
            instance.A_normalized_ = np.asarray(blob["A_normalized_"])
            instance.A_subnormalized_ = np.asarray(blob["A_subnormalized_"])
            if blob["lower_bound_"] and blob["lower_bound_"] is not None:
                instance.lower_bound_ = blob["lower_bound_"]
            if blob["explored_lower_bounds_"] and blob["explored_lower_bounds_"] is not None:
                instance.explored_lower_bounds_ = blob["explored_lower_bounds_"]
        instance._emissions_from_dict(blob["emissions"])
        return instance
