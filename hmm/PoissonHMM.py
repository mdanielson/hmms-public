# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

import numpy as np
import sklearn.cluster
from sklearn.utils import check_array, check_random_state
from sklearn.utils.validation import check_is_fitted

from . import EMHMMBase, confidence_interval, util
from ._hmm import new_poisson, reestimate
from .mixtures import PoissonMixtureModel


class PoissonHMM(EMHMMBase.EMHMMBase):
    """
    A Hidden Markov Model with Poisson Emissions

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    init_pi : bool
    init_A : bool
    init_emissions : bool / string
      One of "mixture", "random"
    """

    def __init__(self, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, init_pi="random", init_A="random", init_emissions="kmeans",
                 implementation="scaling", allowed_to_use_log=True, random_state=None, verbose=0):
        super(PoissonHMM, self).__init__(n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                         n_jobs=n_jobs, tol=tol, init_pi=init_pi, init_A=init_A,
                                         implementation=implementation, allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.init_emissions = init_emissions

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, lengths, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        PoissonHMM
        """
        observed_states = check_array(observed_states)
        hidden_states = np.asarray(hidden_states)
        pi, A, rates = reestimate.reestimate_poisson(observed_states, hidden_states, lengths)
        instance = cls(
            n_iterations=0,
            init_pi=False,
            init_A=False,
            init_emissions=False,
            random_state=random_state,
        )
        instance.pi_ = pi
        instance.A_ = A
        instance.rates_ = rates
        return instance

    def _init_emissions(self, X, lengths):
        """
        Parameters
        ----------
        X : np.ndarray

        Initialize pi, A, and B.

        Christopher Bishop, Pattern Recognition and Machine Learning
            Page 617 - pi/A select random values subject to normalization and non-negativity
        """
        rs = check_random_state(self.random_state)
        if self.init_emissions is None:
            pass
        elif isinstance(self.init_emissions, str):
            if self.init_emissions == "kmeans":
                kmeans = sklearn.cluster.KMeans(n_clusters=self.n_components, random_state=rs)
                data = np.asarray(X).ravel()[:, None]
                kmeans.fit(data)
                self.rates_ = kmeans.cluster_centers_.ravel()
            elif self.init_emissions == "random":
                self.rates_ = rs.randint(X.min(), X.max()+1, size=(self.n_components))
            else:
                raise ValueError("Unknown strategy: {}".format(self.init_emissions))
        else:
            if not isinstance(self.init_emissions, np.ndarray) and not isinstance(self.init_emissions, list):
                raise ValueError("init_emissions parameter should be an array or a list, got {}".format(type(self.init_emissions)))
            self.rates_ = np.asarray(self.init_emissions, dtype=float)
            self.n_components = self.rates_.shape[0]
    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(PoissonHMM, self)._prepare_for_fit()
        self.rates_ = np.asarray(self.rates_, dtype=float)
        assert self.A_.shape[0] == self.rates_.shape[0]

    def _assert_is_fitted(self):
        super(PoissonHMM, self)._assert_is_fitted()
        check_is_fitted(self, ["rates_"])

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_poisson(self.pi_, self.A_, self.rates_, impl=self.implementation, allowed_to_use_log=self.allowed_to_use_log, log_level=self.verbose)

    def _update_after_fit(self, trainer):
        super()._update_after_fit(trainer)
        self.rates_ = np.asarray(trainer.emissions.rates_)

    def _emissions_params(self):
        """
        Matrix rows must sum to 1, so final value is dependenct on other values
        """
        return self.n_components

    def _compute_confidence_intervals(self, n_samples, length, n_iterations, ci):
        """
        Call implementation specific confidence interval code
        """
        return confidence_interval.compute_common_with_rates(self, n_samples=n_samples, length=length, n_iterations=n_iterations, ci=ci)

    def _emissions_to_dict(self):
        rates = None
        if hasattr(self, "rates_"):
            rates = np.asarray(self.rates_).tolist()
        return rates

    def _emissions_from_dict(self, rates):
        if rates is not None:
            self.rates_ = np.asarray(rates)
