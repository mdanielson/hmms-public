# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import datetime
import numbers
import warnings

import joblib

import numpy as np

import sklearn.base
import sklearn.model_selection

from .. import util


def fit_and_score_models_cv(model, params, X, lengths, groups, cv, scorers):
    """
    Fit the model to the data in X
    Call scorers with test_X
    """
    # Generate splits
    # train model on each split,
    # gather scores on test data
    cv_results = []
    for iteration, (train_indices, test_indices) in enumerate(cv.split(np.arange(lengths.shape[0]), groups=groups)):
        train_X, train_lengths, test_X, test_lengths = util.partition_train_test(X, lengths, train_indices, test_indices)
        out = train_and_score_one(iteration, model, params, train_X, train_lengths, test_X, test_lengths, scorers)
        cv_results.append(out)

    return score_cv_results(params, cv_results, scorers)


def train_and_score_one(iteration, model, params, train_X, train_lengths, test_X, test_lengths, scorers):
    new_model = util.sklearn.base.clone(model)
    new_model.set_params(**params)
    start = datetime.datetime.now()
    new_model = util.train_one(new_model, train_X, train_lengths)
    elapsed = (datetime.datetime.now() - start).total_seconds()

    out = {}
    out["train_time"] = elapsed
    out["train_n_sequences"] = train_lengths.shape[0]
    out["test_n_sequences"] = test_lengths.shape[0]
    out["iteration"] = iteration
    n_iterations_elapsed = len(new_model.loglikelihoods_) if hasattr(new_model, "loglikelihoods_") else len(new_model.lower_bound_)
    out["n_iterations_elapsed"] = n_iterations_elapsed
    for name, scorer in scorers.items():
        out["train_{}".format(name)] = scorer(new_model, train_X, train_lengths)
        out["test_{}".format(name)] = scorer(new_model, test_X, test_lengths)

    for k, v in params.items():
        out["params_{}".format(k)] = v
    return out, new_model


def score_cv_results(params, cv_results_and_models, scorers):
    cv_results = [r[0] for r in cv_results_and_models]
    models = [r[1] for r in cv_results_and_models]
    overall_scores = {}
    for k, v in params.items():
        overall_scores["params_{}".format(k)] = v
    overall_scores["mean_train_time"] = np.mean([item["train_time"] for item in cv_results])
    overall_scores["std_train_time"] = np.std([item["train_time"] for item in cv_results])
    overall_scores["min_train_time"] = np.min([item["train_time"] for item in cv_results])
    overall_scores["max_train_time"] = np.max([item["train_time"] for item in cv_results])

    overall_scores["min_n_iterations_elapsed"] = np.min([item["n_iterations_elapsed"] for item in cv_results])
    overall_scores["max_n_iterations_elapsed"] = np.max([item["n_iterations_elapsed"] for item in cv_results])
    overall_scores["mean_n_iterations_elapsed"] = np.mean([item["n_iterations_elapsed"] for item in cv_results])
    overall_scores["std_n_iterations_elapsed"] = np.std([item["n_iterations_elapsed"] for item in cv_results])

    for k, v in scorers.items():
        overall_scores["mean_test_{}".format(k)] = np.mean([item["test_{}".format(k)] for item in cv_results])
        overall_scores["std_test_{}".format(k)] = np.std([item["test_{}".format(k)] for item in cv_results])
        overall_scores["min_test_{}".format(k)] = np.min([item["test_{}".format(k)] for item in cv_results])
        overall_scores["max_test_{}".format(k)] = np.max([item["test_{}".format(k)] for item in cv_results])

    for k, v in scorers.items():
        overall_scores["mean_train_{}".format(k)] = np.mean([item["train_{}".format(k)] for item in cv_results])
        overall_scores["std_train_{}".format(k)] = np.std([item["train_{}".format(k)] for item in cv_results])
        overall_scores["min_train_{}".format(k)] = np.min([item["train_{}".format(k)] for item in cv_results])
        overall_scores["max_train_{}".format(k)] = np.max([item["train_{}".format(k)] for item in cv_results])
    return cv_results, overall_scores, models


def fit_and_score_models_cv_parallel(model, params, X, lengths, groups, cv, scorers, n_jobs=-1, verbose=False):
    """
    Fit the model to the data in X
    Call scorers with test_X
    """
    # Generate splits
    # train model on each split,
    # gather scores on test data
    work = []
    for iteration, (train_indices, test_indices) in enumerate(cv.split(np.arange(lengths.shape[0]), groups=groups)):
        train_X, train_lengths, test_X, test_lengths = util.partition_train_test(X, lengths, train_indices, test_indices)
        work.append(
            (
                iteration,
                model,
                params,
                train_X,
                train_lengths,
                test_X,
                test_lengths,
                scorers,
            )
        )
    with joblib.Parallel(n_jobs=n_jobs, verbose=verbose) as parallel:
        cv_results = parallel(joblib.delayed(train_and_score_one)(*args) for args in work)

    return score_cv_results(params, cv_results, scorers)


class HMMSearchCV(sklearn.base.BaseEstimator):
    """
    Find an optimal number of hidden states using an HMM

    We are always multi-score

    Parameters
    ----------

    :param estimator : an HMM class
    :param param_grid : a dict to pass to sklearn.model_selection.ParameterGrid()
    :param scoring : a dict of scoring names to callables
    :param refit: A string corresponding to a scorer

    # TODO #
    * accept a cross-validation strategy
    * call refit
    * mean vs min
    """

    def __init__(self, estimator, param_grid, cv=5, scoring=None, refit=None, n_jobs=-1, parallel_level="hmm", verbose=False):
        self.estimator = estimator
        self.param_grid = param_grid
        self.cv = cv
        self.scoring = scoring
        self.refit = refit
        self.n_jobs = n_jobs
        self.parallel_level = parallel_level
        self.verbose = verbose

    def fit(self, X, lengths, groups=None):
        """
        Find the best model for the prented data

        Parameters
        ----------
        X : np.ndarray
        """

        X, lengths = util.check_arguments(X, lengths)
        self.best_model_ = None
        self.best_params_ = None
        self.results_ = None
        assert self.estimator is not None, "Please set estimator"
        if isinstance(self.estimator.random_state, numbers.Number):
            warnings.warn("Warning - the estimator random state is fixed. CV searching may not be as intersting")
        assert self.param_grid is not None, "Please set param_grid"
        assert self.scoring is not None, "Please set scoring"
        assert self.refit is None or self.refit in self.scoring, "Refit parameter should be either None or the name of a valid scoring method"

        cross_val = sklearn.model_selection.check_cv(self.cv)
        if self.parallel_level == "hmm":
            params_to_train = []
            for params in sklearn.model_selection.ParameterGrid(self.param_grid):
                if self.n_jobs != 1:
                    params["n_jobs"] = self.n_jobs

                params_to_train.append(params)
            trained_results = []
            for params in params_to_train:
                if self.verbose:
                    util.msg(str(params))
                trained_results.append(
                    fit_and_score_models_cv(
                        self.estimator,
                        params,
                        X,
                        lengths,
                        groups,
                        cv=cross_val,
                        scorers=self.scoring
                    )
                )
        elif self.parallel_level == "cv":
            # Don't want parallelism during fitting for the estimator
            if self.n_jobs != 1 and self.estimator.n_jobs > 1:
                warnings.warn("Setting estimator.n_jobs = 1, parallelism may happen only in the HMMSearch instance")

            params_to_train = []
            for params in sklearn.model_selection.ParameterGrid(self.param_grid):
                params["n_jobs"] = 1
                params_to_train.append(params)
            trained_results = []
            for params in params_to_train:
                if self.verbose:
                    util.msg(str(params))
                trained_results.append(
                    fit_and_score_models_cv_parallel(
                        self.estimator,
                        params,
                        X,
                        lengths,
                        groups,
                        cv=cross_val,
                        scorers=self.scoring,
                        n_jobs=self.n_jobs,
                        verbose=self.verbose
                    )
                )
        else:
            raise NotImplemented("Parallel_level={}".format(self.parallel_level))

        self.cv_scores_, scores, self.models_ = self.collect_model_results(trained_results)
        if self.refit is not None:
            self.best_params_ = self._select_best(scores, params_to_train)
            self.best_model_ = self.do_refit(self.best_params_, X, lengths)
        else:
            self.best_params_ = None
            self.best_model_ = None
        params_as_strings = (str(p) for p in params_to_train)
        self.scores_ = dict(zip(params_as_strings, scores))

        return self

    def do_refit(self, params, X, lengths):

        estimator = sklearn.base.clone(self.estimator)
        estimator.set_params(**params)
        estimator.set_params(n_jobs=self.n_jobs)
        return estimator.fit(X, lengths)

    def _select_best(self, scores, params_to_train):
        """
        Select the best model according to the criterion choice
        """
        these_scores = [val["mean_test_{}".format(self.refit)] for val in scores]
        # Ignore models that return "inf" on the test data
        # They haven't modelled it well
        scores_index = np.where(np.logical_not(np.isinf(these_scores)))[0]
        good_params = [params_to_train[i] for i in scores_index]
        best_idx = np.argmin([these_scores[i] for i in scores_index])
        return good_params[best_idx]

    def collect_model_results(self, trained_results):
        cv_scores = []
        aggregated_scores = []
        models = []

        for m in trained_results:
            cv_scores.extend(m[0])
            aggregated_scores.append(m[1])
            models.append(m[2])
        return cv_scores, aggregated_scores, models

    def transform(self, X, lengths):
        return self.best_model_.transform(X, lengths)

    def score(self, X, lengths):
        return self.best_model_.score(X, lengths)

    def score_samples(self, X, lengths):
        return self.best_model_.score_samples(X, lengths)

    def sample(self, n_samples=1, length=10, random_state=None):
        return self.best_model_.sample(n_samples=n_samples, length=length, random_state=random_state)
