# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import datetime
import numbers
import warnings

import numpy as np

import sklearn.base
import sklearn.model_selection

from .. import util


def fit_and_score_models(model, params, X, lengths, scorers):
    """
    Fit the model to the data in X
    Call scorers with test_X
    """
    model = sklearn.base.clone(model)
    model.set_params(**params)
    start = datetime.datetime.now()
    model = util.train_one(model, X, lengths)
    elapsed = (datetime.datetime.now() - start).total_seconds()
    out = {}
    out["train_time"] = elapsed
    out["n_sequences"] = lengths.shape[0]

    n_iterations_elapsed = len(model.loglikelihoods_) if hasattr(model, "loglikelihoods_") else len(model.lower_bound_)
    out["n_iterations_elapsed"] = n_iterations_elapsed
    init_scores = []
    if model.n_inits > 1:
        init_scores = model.explored_loglikelihoods_ if hasattr(model, "explored_loglikelihoods_") else model.explored_lower_bounds_
        all_n_iterations = np.asarray([len(scores) for scores in init_scores])
        out["n_iterations_min"] = all_n_iterations.min()
        out["n_iterations_max"] = all_n_iterations.max()
        out["n_iterations_mean"] = all_n_iterations.mean()
        out["n_iterations_std"] = all_n_iterations.std()
    else:
        out["n_iterations_min"] = n_iterations_elapsed
        out["n_iterations_max"] = n_iterations_elapsed
        out["n_iterations_mean"] = n_iterations_elapsed
        out["n_iterations_std"] = n_iterations_elapsed

    for k, v in params.items():
        out["params_{}".format(k)] = v
    out["params"] = str(params)

    for name, scorer in scorers.items():
        out[name] = scorer(model, X, lengths)

    return params, out, model


class HMMSearch(sklearn.base.BaseEstimator):
    """
    Find an optimal number of hidden states using an HMM

    We are always multi-score

    Parameters
    ----------

    :param estimator : an HMM class
    :param param_grid : a dict to pass to sklearn.model_selection.ParameterGrid()
    :param scoring : a dict of scoring names to callables
    :param refit: A string corresponding to a scorer

    # TODO #
    * accept a cross-validation strategy
    * call refit
    * mean vs min
    """

    def __init__(self, estimator, param_grid, scoring=None, refit=None, n_jobs=-1, verbose=False):
        self.estimator = estimator
        self.param_grid = param_grid
        self.scoring = scoring
        self.refit = refit
        self.n_jobs = n_jobs
        self.verbose = verbose

    def fit(self, X, lengths):
        """
        Find the best model for the prented data

        Parameters
        ----------
        X : np.ndarray
        """

        X, lengths = util.check_arguments(X, lengths)
        assert self.estimator is not None, "Please set estimator"
        if isinstance(self.estimator.random_state, numbers.Number):
            assert self.estimator.n_inits > 1, "The estimator's random state is an integer, and yet n_inits > 1 is requested."
        assert self.param_grid is not None, "Please set param_grid"
        assert self.scoring is not None, "Please set scoring"
        assert self.refit is None or self.refit in self.scoring, "Refit parameter should be either None or the name of a valid scoring method"

        params_to_train = []
        for params in sklearn.model_selection.ParameterGrid(self.param_grid):
            if self.n_jobs != 1:
                params["n_jobs"] = self.n_jobs

            params_to_train.append(params)
        trained_results = []
        for params in params_to_train:
            if self.verbose:
                util.msg(str(params))
            trained_results.append(
                fit_and_score_models(
                    self.estimator,
                    params,
                    X,
                    lengths,
                    self.scoring
                )
            )

        scores, self.models_ = self.collect_model_results(trained_results)
        if self.refit is not None:
            self.best_params_, self.best_model_ = self._select_best(scores, params_to_train)
        else:
            self.best_params_ = None
            self.best_model_ = None
        params_as_strings = (str(p) for p in params_to_train)
        self.scores_ = dict(zip(params_as_strings, scores))

        return self

    def collect_model_results(self, trained_results):
        model_results = [m[1] for m in trained_results]
        models = [m[2] for m in trained_results]
        return model_results, models

    def _select_best(self, scores, params):
        """
        Select the best model according to the criterion choice
        """
        these_scores = [score_values[self.refit] for score_values in scores]
        # Ignore models that return "inf" on the test data
        # They haven't modelled it well
        scores_index = np.where(np.logical_not(np.isinf(these_scores)))[0]
        good_models = [self.models_[i] for i in scores_index]
        good_params = [params[i] for i in scores_index]
        best_idx = np.argmin([these_scores[i] for i in scores_index])
        return good_params[best_idx], good_models[best_idx]

    def transform(self, X, lengths):
        return self.best_model_.transform(X, lengths)

    def score(self, X, lengths):
        return self.best_model_.score(X, lengths)

    def score_samples(self, X, lengths):
        return self.best_model_.score_samples(X, lengths)

    def sample(self, n_samples=1, length=10, random_state=None):
        return self.best_model_.sample(n_samples=n_samples, length=length, random_state=random_state)
