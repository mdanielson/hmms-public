# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

import numpy as np


def perplexity(estimator, X, lengths):
    """
    Compute the perplexity of the data for the estimator

    Perplexity is defined as the nth root of 1/P(data)

    See https://web.stanford.edu/~jurafsky/slp3/3.pdf
    """
    probs = np.exp(estimator.score_samples(X, lengths))
    for i, p in enumerate(probs):
        probs[i] = p ** (-1/lengths[i])
    return probs


def consistent_akaike_information_criterion(loglik, num_data, num_free_params):
    """
    CAIC = -2*logLike + num_free_params * (log(num_of_data) + 1)
    """
    return -2 * loglik + num_free_params * (np.log(num_data) + 1)


def bayesian_information_criterion(loglik, num_data, num_free_params):
    """
    BIC = -2*logLike + num_free_params * log(num_of_data)
    """
    return -2 * loglik + num_free_params * np.log(num_data)


def akaike_information_criterion(loglik, num_data, num_free_params):
    """
    AIC = -2*logLike + 2 * num_free_params
    """
    return -2 * loglik + 2 * num_free_params


def tll_scoring(m, x, l):
    return -1 * m.score(x, l)


def aic_scoring(m, x, l):
    return m.aic(x, l)


def bic_scoring(m, x, l):
    return m.bic(x, l)


def caic_scoring(m, x, l):
    return m.caic(x, l)


def lb_scoring(m, x, l):
    return -1 * m.lower_bound_[-1]


def dic_scoring(m, x, l):
    return m.dic(x, l)


# Score Setups
# "SMALLER SCORE is better"
EM_SCORERS = {
    "tll": tll_scoring,
    "aic": aic_scoring,
    "bic": bic_scoring,
    "caic": caic_scoring,
    #"perplexity": lambda m, x, l: np.mean(perplexity(m, x, l))
}
VARIATIONAL_SCORERS = {
    "lb": lb_scoring,
    "tll": tll_scoring,
    #"perplexity": lambda m, x, l: np.mean(perplexity(m, x, l))
}

GAUSSIAN_VARIATIONAL_SCORERS = {
    "DIC": dic_scoring
}

for k, v in VARIATIONAL_SCORERS.items():
    GAUSSIAN_VARIATIONAL_SCORERS[k] = v
