import pandas as pd


from .scoring import perplexity, consistent_akaike_information_criterion, akaike_information_criterion, bayesian_information_criterion
from .scoring import EM_SCORERS, VARIATIONAL_SCORERS, GAUSSIAN_VARIATIONAL_SCORERS

from .search import HMMSearch
from .search_cv import HMMSearchCV


def make_model_summary(models, X, lengths):
    """
    Parameters
    ----------
    models : dict
    """

    summary = []
    for n_components, model in models.items():
        summary.append(
            {
                "n_components": n_components,
                "num_free_params": model.n_parameters(),
                "ll": model.score_samples(X, lengths).sum(),
                "aic": model.aic(X, lengths),
                "bic": model.bic(X, lengths),
                "caic": model.caic(X, lengths),
            }
        )
    return pd.DataFrame(summary).set_index("n_components")[["num_free_params", "aic", "bic", "caic"]]


