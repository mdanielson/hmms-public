# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numbers

import numpy as np
import sklearn.utils
from sklearn.utils import check_array
from sklearn.utils.validation import check_is_fitted

from hmm.distributions import dirichlet
from hmm._hmm.variational import new_mixture_categorical
from hmm._hmm import reestimate

from . import VariationalMHMMBase


class CategoricalVariationalMHMM(VariationalMHMMBase.VariationalMHMMBase):
    def __init__(self, n_mixture_components=1, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, mixture_weights_prior=None, mixture_weights_posterior="uniform",
                 pi_prior=None, pi_posterior="sample", A_prior=None, A_posterior="sample", B_prior=None,  B_posterior="sample", n_features=None, implementation="scaling",
                 allowed_to_use_log=True, random_state=None, verbose=0):
        super(CategoricalVariationalMHMM, self).__init__(n_mixture_components=n_mixture_components, n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                                         n_jobs=n_jobs, tol=tol, mixture_weights_prior=mixture_weights_prior, mixture_weights_posterior=mixture_weights_posterior,
                                                         pi_prior=pi_prior, pi_posterior=pi_posterior, A_prior=A_prior, A_posterior=A_posterior, implementation=implementation,
                                                         allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.B_prior = B_prior
        self.B_posterior = B_posterior
        self.n_features = n_features

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        CategoricalVariationalMHMM
        """
        raise NotImplementedError("TODO")
        observed_states = check_array(observed_states)
        hidden_states = check_array(hidden_states)
        pi, A, B = reestimate.reestimate_categorical(observed_states, hidden_states)
        instance = cls(
            n_iterations=0,
            init_pi=None,
            A_strategy=None,
            init_emissions=None,
            random_state=random_state,
        )
        instance.pi_ = pi
        instance.A_ = A
        instance.B_ = B
        return instance

    def _init_emissions(self, X, lengths):
        random_state = sklearn.utils.check_random_state(self.random_state)
        # This guess can go bad
        if self.n_features is None:
            symbols = np.unique(X)
            self.n_features_ = int(max(symbols.max()+1, symbols.shape[0]))
        else:
            self.n_features_ = self.n_features

        B_prior = self.B_prior
        n_samples = lengths.sum()
        # Setup prior estimate..
        if B_prior is None:
            B_prior = 1 / self.n_features_

        self.B_prior_ = np.zeros((self.n_mixture_components, self.n_components, self.n_features_), dtype=float)
        if isinstance(B_prior, numbers.Number):
            self.B_prior_[:] += B_prior
        elif isinstance(B_prior, np.ndarray):
            if B_prior.ndim == 2:
                for i in range(self.n_mixture_components):
                    self.B_prior_[i] = B_prior
            elif B_prior.ndim == 3:
                self.B_prior_ += B_prior
            else:
                raise ValueError("Unknown value for B_prior:{}".format(B_prior))
        else:
            raise ValueError("Unknown value for B_prior:{}".format(B_prior))

        if self.B_posterior == "sample":
            self.B_posterior_ = np.zeros_like(self.B_prior_)
            for mixture_component in range(self.n_mixture_components):
                B_samples = max(1, int(self.mixture_weights_normalized_[mixture_component] * n_samples))
                self.B_posterior_[mixture_component] = dirichlet.setup_dirichlet_posterior(self.B_prior_[mixture_component], B_samples, random_state)
        elif self.B_posterior == "uniform":
            self.B_posterior_ = np.copy(self.B_prior_) * lengths.shape[0]
            for mixture_component in range(self.n_mixture_components):
                B_samples = max(1, int(self.mixture_weights_normalized_[mixture_component] * n_samples))
                self.B_posterior_[mixture_component] *= B_samples

    def _update_after_fit(self, trainer):
        super(CategoricalVariationalMHMM, self)._update_after_fit(trainer)
        self.B_posterior_ = np.zeros_like(self.B_posterior_)
        self.B_normalized_ = np.zeros_like(self.B_posterior_)
        self.B_subnormalized_ = np.zeros_like(self.B_posterior_)
        for mixture_component in range(self.n_mixture_components):
            self.B_posterior_[mixture_component] = trainer.emissions[mixture_component].B_posterior
            self.B_normalized_[mixture_component] = trainer.emissions[mixture_component].B_normalized
            self.B_subnormalized_[mixture_component] = trainer.emissions[mixture_component].B_subnormalized

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(CategoricalVariationalMHMM, self)._prepare_for_fit()
        assert self.A_posterior_.shape[1] == self.B_posterior_.shape[1]

    def _assert_is_fitted(self):
        super(CategoricalVariationalMHMM, self)._assert_is_fitted()
        check_is_fitted(self, ["B_posterior_"])

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_mixture_categorical(
            self.mixture_weights_prior_,
            self.mixture_weights_posterior_,
            self.pi_prior_,
            self.pi_posterior_,
            self.A_prior_,
            self.A_posterior_,
            self.B_prior_,
            self.B_posterior_,
            impl="scaling",
            log_level=self.verbose,
        )

    def _emissions_params(self):
        posterior = self.B_posterior_[mixture_component, hidden_state]
        prior = self.B_prior_[mixture_component, hidden_state]
        vals = posterior[(posterior - prior) > self.prior_threshold_for_num_free_params]
        return max(0, vals.shape[0] - 1)

    def _emissions_to_dict(self):
        if hasattr(self, "B_"):
            return np.asarray(self.B_).tolist()
        return None

    def _emissions_from_dict(self, B):
        if B is not None:
            self.B_ = np.asarray(B)
