# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

import numpy as np
from sklearn.preprocessing import normalize
from sklearn.utils import check_random_state
from sklearn.utils.validation import check_is_fitted

from hmm._hmm.em import new_mixture_categorical

from . import EMMHMMBase


class CategoricalMHMM(EMMHMMBase.EMMHMMBase):
    def __init__(self, n_mixture_components=1, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6,
                 init_mixture_weights="uniform", init_pi="random", init_A="random", init_emissions="random",
                 n_features=None, smoothing=None, smoothing_params=None, implementation="scaling", allowed_to_use_log=True, random_state=None, verbose=0):
        super(CategoricalMHMM, self).__init__(n_mixture_components=n_mixture_components, n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                              n_jobs=n_jobs, tol=tol, init_mixture_weights=init_mixture_weights, init_pi=init_pi, init_A=init_A,
                                              implementation=implementation, allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.init_emissions = init_emissions
        self.n_features = n_features
        self.smoothing = smoothing
        self.smoothing_params = smoothing_params

    @classmethod
    def reestimate_from_sequences(cls, mixture_components, observed_states, hidden_states, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        CategoricalMHMM
        """
        raise NotImplementedError("TODO")

    def _init_emissions(self, X, lengths):
        if self.n_features is None:
            symbols = np.unique(X)
            self.n_features_ = int(max(symbols.max()+1, symbols.shape[0]))
        else:
            self.n_features_ = self.n_features

        rs = check_random_state(self.random_state)
        if self.init_emissions is None:
            pass
        elif self.init_emissions == "random":
            self.B_ = rs.rand(self.n_mixture_components, self.n_components, self.n_features_)
            for mixture_component in range(self.n_mixture_components):
                self.B_[mixture_component] = normalize(
                    self.B_[mixture_component],
                    norm="l1",
                    axis=1
                )
        elif self.init_emissions == "uniform":
            self.B_ = np.full((self.n_mixture_components, self.n_components, self.n_features_), 1. / self.n_features_)
        else:
            if not isinstance(self.init_emissions, np.ndarray) and not isinstance(self.init_emissions, list):
                raise ValueError("init_emissions parameter should be an array or a list, got {}".format(type(self.init_emissions)))
            self.B_ = np.asarray(self.init_emissions)

    def _update_after_fit(self, trainer):
        super()._update_after_fit(trainer)
        self.B_ = np.zeros((self.n_mixture_components, self.n_components, self.n_features_))
        for i in range(self.n_mixture_components):
            self.B_[i] = trainer.emissions[i].B

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super(CategoricalMHMM, self)._prepare_for_fit()
        self.B_ = np.asarray(self.B_)
        for mixture_component in range(self.n_mixture_components):
            self.B_[mixture_component] = normalize(
                self.B_[mixture_component],
                norm="l1",
                axis=1
            )

        assert self.A_.shape[0] == self.B_.shape[0]

    def _assert_is_fitted(self):
        super(CategoricalMHMM, self)._assert_is_fitted()
        check_is_fitted(self, ["B_"])

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_mixture_categorical(
            self.mixture_weights_,
            self.pi_,
            self.A_,
            self.B_,
            smoothing=self.smoothing,
            smoothing_params=self.smoothing_params,
            impl=self.implementation,
            allowed_to_use_log=self.allowed_to_use_log,
            log_level=self.verbose
        )

    def _emissions_params(self):
        B = self.B_[mixture_component, hidden_state]
        return B.shape[0]

    def _emissions_to_dict(self):
        if hasattr(self, "B_"):
            return np.asarray(self.B_).tolist()
        return None

    def _emissions_from_dict(self, B):
        if B is not None:
            self.B_ = np.asarray(B)
