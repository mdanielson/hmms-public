# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import sklearn.base
import sklearn.cluster
from sklearn.utils import check_array, check_random_state
from sklearn.utils.validation import check_is_fitted

from . import VariationalMHMMBase, confidence_interval, base
from ._hmm.variational import new_mixture_gaussian


class GaussianVariationalMHMM(VariationalMHMMBase.VariationalMHMMBase, base.VariationalGaussianMixin):
    """
    A Hidden Markov Model with Univariate Gaussian Emissions.

    Parameters
    ----------
    n_components : int
        Number of Hidden States

    """

    def __init__(self, n_mixture_components=1, n_components=1, n_iterations=100, n_inits=1, n_jobs=-1, tol=1e-6, mixture_weights_prior=None, mixture_weights_posterior="sample",
                 pi_prior=None, pi_posterior="sample", A_prior=None, A_posterior="sample", means_prior=None, means_posterior="kmeans", variances_prior=None, variances_posterior="kmeans",
                 implementation="scaling", allowed_to_use_log=True, random_state=None, verbose=0):
        super().__init__(n_mixture_components=n_mixture_components, n_components=n_components, n_iterations=n_iterations, n_inits=n_inits,
                                                      n_jobs=n_jobs, tol=tol, mixture_weights_prior=mixture_weights_prior, mixture_weights_posterior=mixture_weights_posterior,
                                                      pi_prior=pi_prior, pi_posterior=pi_posterior, A_prior=A_prior, A_posterior=A_posterior,
                                                      implementation=implementation, allowed_to_use_log=allowed_to_use_log, random_state=random_state, verbose=verbose)
        self.means_prior = means_prior
        self.means_posterior = means_posterior
        self.variances_prior = variances_prior
        self.variances_posterior = variances_posterior

    @classmethod
    def reestimate_from_sequences(cls, observed_states, hidden_states, random_state=None):
        """
        Initialize a new HMM based on the provided sequences of observed and hidden states.

        Parameters
        ----------
        observed_state : array-like, shape (n_samples, length)
        hidden_states : array-like, shape (n_samples, length)
        random_state : None or np.random_state instance

        Returns
        -------
        GaussianVariationalMHMM
        """
        raise NotImplementedError("TODO")

    def _init_emissions(self, X, lengths):
        """
        https://www.fil.ion.ucl.ac.uk/~wpenny/publications/

        # TODO: support better initialization

        Right now "data" initializes based on global data mean/variance, and "uninformed"
        initializes based upon a mean centered at 0 and a flat variance (a=b=1e-3)

        """
        random_state = check_random_state(self.random_state)
        self.variances_prior_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
        self.means_posterior_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
        self.beta_posterior_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
        self.a_prior_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
        self.a_posterior_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
        self.b_prior_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)
        self.b_posterior_ = np.zeros((self.n_mixture_components, self.n_components), dtype=float)

        data = np.asarray(X).ravel()[:, None]

        # first setup priors
        if self.means_prior is None or self.means_prior == "uninformed":
            self.means_prior_ = np.full((self.n_mixture_components, self.n_components, ), data.mean(), dtype=float)
            # similar to sklearn
            self.beta_prior_ = np.full((self.n_mixture_components, self.n_components, ), 1., dtype=float)
        elif self.means_prior == "data":
            self.means_prior_ = np.full((self.n_mixture_components, self.n_components, ), data.mean(), dtype=float)
            # similar to sklearn
            self.beta_prior_ = np.full((self.n_mixture_components, self.n_components, ), data.shape[0], dtype=float)
        else:
            assert False, "Not an option: {}".format(self.means_prior)

        if isinstance(self.variances_prior, tuple):
            # Format is (a, b)
            self.a_prior_ = np.zeros_like(self.means_prior_, dtype=float) + self.variances_prior[0]
            self.b_prior_ = np.zeros_like(self.means_prior_, dtype=float) + self.variances_prior[1]
        elif self.variances_prior is None or self.variances_prior == "uninformed":
            # From ~wpenny publication
            self.a_prior_ = np.zeros_like(self.means_prior_, dtype=float) + 1
            self.b_prior_ = np.zeros_like(self.means_prior_, dtype=float) + 1e-3
        elif self.variances_prior == "data":
            self.a_prior_ = np.zeros_like(self.means_prior_, dtype=float) + data.shape[0]/2
            self.b_prior_ = np.zeros_like(self.means_prior_, dtype=float) + data.shape[0] / (2/data.var())
        else:
            raise ValueError("Invalid option for variances_prior:{}".format(self.variances_prior))


        for mixture_component in range(self.n_mixture_components):
            kmeans = sklearn.cluster.KMeans(n_clusters=self.n_components, random_state=random_state)
            labels = kmeans.fit(data).labels_
            variances = np.zeros(kmeans.n_clusters)
            counts = np.zeros(kmeans.n_clusters)
            for i in sorted(set(labels)):
                these = labels == i
                variances[i] = np.var(data[these])
                counts[i] = these.sum()
            counts *= self.mixture_weights_normalized_[mixture_component]

            if self.variances_posterior == "kmeans":
                self.a_posterior_[mixture_component] = counts
                self.b_posterior_[mixture_component]  = np.full((variances.shape[0],), variances) * counts
            else:
                raise ValueError("Invalid option for variances_posterior:{}".format(self.variances_posterior))
            if self.means_posterior == "kmeans":
                self.means_posterior_[mixture_component] = kmeans.cluster_centers_.ravel()
                self.beta_posterior_[mixture_component] = 1
                self.beta_posterior_[mixture_component] = counts
            else:
                raise ValueError("Invalid option for means_posterior:{}".format(self.means_posterior))

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
        """
        super()._prepare_for_fit()
        #self.means_ = np.asarray(self.means_, dtype=float)
        #self.variances_ = np.asarray(self.variances_, dtype=float)
        #assert self.A_.shape[1] == self.means_.shape[1]
        #assert self.A_.shape[1] == self.variances_.shape[1]

    def _assert_is_fitted(self):
        super()._assert_is_fitted()
        #check_is_fitted(self, ["means_", "variances_"])

    def _new_trainer(self):
        """
        Retrieve an instance of the underlying performance HMM code
        """
        return new_mixture_gaussian(
            mixture_weights_prior=self.mixture_weights_prior_,
            mixture_weights_posterior=self.mixture_weights_posterior_,
            pi_prior=self.pi_prior_,
            pi_posterior=self.pi_posterior_,
            A_prior=self.A_prior_,
            A_posterior=self.A_posterior_,
            means_prior=self.means_prior_,
            means_posterior=self.means_posterior_,
            beta_prior=self.beta_prior_,
            beta_posterior=self.beta_posterior_,
            a_prior=self.a_prior_,
            a_posterior=self.a_posterior_,
            b_prior=self.b_prior_,
            b_posterior=self.b_posterior_,
            impl=self.implementation,
            allowed_to_use_log=self.allowed_to_use_log,
            log_level=self.verbose)

    def _update_after_fit(self, trainer):
        super()._update_after_fit(trainer)
        self.variances_posterior_ = np.zeros((self.n_mixture_components, self.n_components))
        for mixture_component in range(self.n_mixture_components):
            self.means_posterior_[mixture_component] = np.asarray(trainer.emissions[mixture_component]._means_posterior)
            self.beta_posterior_[mixture_component] = np.asarray(trainer.emissions[mixture_component]._beta_posterior)
            self.a_posterior_[mixture_component] = np.asarray(trainer.emissions[mixture_component]._a_posterior)
            self.b_posterior_[mixture_component] = np.asarray(trainer.emissions[mixture_component]._b_posterior)
            self.variances_posterior_[mixture_component] = np.asarray(trainer.emissions[mixture_component]._variances_posterior)

    def _emissions_to_dict(self):
        means = None
        variances = None
        if hasattr(self, "means_"):
            means = np.asarray(self.means_).tolist()
        if hasattr(self, "variances_"):
            variances = np.asarray(self.variances_).tolist()
        return means, variances

    def _emissions_from_dict(self, emissions):
        means, variances = emissions
        if means is not None:
            self.means_ = np.asarray(means)
        if variances is not None:
            self.variances_ = np.asarray(variances)

    def _emissions_params(self):
        return 4
