# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import matplotlib.gridspec as gs
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import seaborn as sns

sns.set()


def plot_hmm_states(observed_sequence, state_sequence, figsize=(15, 5), states_lines=True, ylabel_size=14, xlabel_size=14, title_size=16):
    """
    A timeseries plot  to show the provided observation sequence, as well as the corresponding hidden state_sequence
    """
    f, ax1 = plt.subplots(figsize=figsize)
    line1 = ax1.plot(np.arange(observed_sequence.shape[0]), observed_sequence,  linestyle="-", label="Observed")
    ax2 = ax1.twinx()
    ax2.grid(False)
    if states_lines:
        line2 = ax2.plot(np.arange(state_sequence.shape[0]), state_sequence, color="red", linestyle="-.", marker="o", label="HMM", alpha=.5, )
    else:
        line2 = ax2.plot(np.arange(state_sequence.shape[0]), state_sequence, color="red", linestyle="", marker="o", label="HMM", alpha=.5, )
    num_states = max(state_sequence) + 1
    ax2.set_yticks(np.arange(num_states))
    ax2.set_yticklabels(["State-{}".format(i) for i in range(num_states)])

    lines = line1 + line2
    labels = [l.get_label() for l in lines]
    ax2.legend(lines, labels, loc="best")
    ax1.set_ylabel("Observations", size=ylabel_size)
    ax2.set_ylabel("Hidden States", size=ylabel_size)

    ax1.set_title("Observations and Hidden States", size=title_size)
    return f


def categorical_hinton_diagram(pi, A, B, vmin=0, vmax=1, b_vals=None, fontsize=14):
    return hinton_diagram(pi, A, B, vmin=vmin, vmax=vmax, b_vals=b_vals, fontsize=fontsize)


def hinton_diagram(pi, A, B, vmin=0, vmax=1, b_vals=None, fontsize=14):
    """
    Show the initial state probabilities, the transition probabilities, and the emission probabilities as a heatmap
    """
    num_states = A.shape[0]

    f, grid = generic_hinton_diagram(pi, A, vmin, vmax, fontsize)
    ax = f.add_subplot(grid[2:num_states+2, num_states:])
    if b_vals is None:
        b_vals = np.arange(B.shape[1])
    sns.heatmap(B, ax=ax, vmin=vmin, vmax=vmax, xticklabels=b_vals)
    ax.set_title("Emissions Probabilities", size=fontsize)

    return f


def categorical_mixture_hinton_diagram(mixture_weights, pi, A, B, vmin=0, vmax=1, b_vals=None, fontsize=14):

    f, grid = generic_mixture_hinton_diagram(mixture_weights, pi, A, vmin, vmax, fontsize)

    num_components = A.shape[0]
    num_states = A.shape[1]
    start = 0
    if b_vals is None:
        b_vals = np.arange(B.shape[-1])
    for component in range(num_components):
        start += 2
        ax = f.add_subplot(grid[start:start + num_states, num_states:])
        sns.heatmap(B[component], ax=ax, vmin=vmin, vmax=vmax, xticklabels=b_vals)
        ax.set_title("Emissions Probabilities", size=fontsize)
        start += num_states

    return f


def gaussian_mixture_hinton_diagram(mixture_weights, pi, A, means, variances, vmin=0, vmax=1, infer_hidden=True, fontsize=14):

    f, grid = generic_mixture_hinton_diagram(mixture_weights, pi, A, vmin, vmax, fontsize)

    num_components = A.shape[0]
    num_states = A.shape[1]
    start = 0
    xxmin = None
    xxmax = None
    for component in range(num_components):
        start += 2
        ax = f.add_subplot(grid[start:start + num_states, num_states:])
        start += num_states
        for i in range(num_states):
            keep = True
            if infer_hidden:
                if np.all(np.abs(A[component, i] - A[component, i, 0]) < 1e-2):
                    keep = False
            if keep:
                s_min = means[component, i] - 10 * variances[component, i]
                s_max = means[component, i] + 10 * variances[component, i]
                xx = np.arange(s_min, s_max, (s_max - s_min) / 1000)
                norm = scipy.stats.norm(means[component, i], np.sqrt(variances[component, i]))
                yy = norm.pdf(xx)
                keep = yy > .01
                ax.plot(xx[keep], yy[keep], label="State: {}".format(i))
                xmin, xmax = ax.get_xlim()

        ax.set_title("Emissions Probabilities", size=fontsize+2)
        ax.legend(loc="best")
    f.tight_layout()
    return f


def generic_mixture_hinton_diagram(mixture_weights, pi, A, vmin, vmax, fontsize):
    num_components = A.shape[0]
    num_states = A.shape[1]

    f = plt.figure(figsize=(4*(num_states), 2*num_states * num_components))
    grid = gs.GridSpec(num_components * (2 + num_states), num_states*4)

    start = 0
    for component in range(num_components):
        ax = f.add_subplot(grid[start+1:start+2, num_states:])
        ax.set_title("Component: {}, size: {:.3f}".format(component, mixture_weights[component]), size=fontsize + 2)
        ax.grid(False)
        # Hide axes ticks
        ax.set_xticks([])
        ax.set_yticks([])
        ax.axis("off")
        ax = f.add_subplot(grid[start:start+2, :num_states])
        sns.heatmap(pi[component, None, :], ax=ax, vmin=vmin, vmax=vmax)
        ax.set_title("Initial Probabilities", size=fontsize)
        start += 2
        ax = f.add_subplot(grid[start:start+num_states, :num_states])
        sns.heatmap(A[component], ax=ax, vmin=vmin, vmax=vmax)
        ax.set_title("Transition Probabilities", size=fontsize)
        start += num_states
    return f, grid


def generic_hinton_diagram(pi, A, vmin, vmax, fontsize):
    num_states = A.shape[0]

    f = plt.figure(figsize=(4*(num_states), 2*num_states))
    grid = gs.GridSpec(2 + num_states, num_states*4)

    ax = f.add_subplot(grid[:1, :num_states])
    sns.heatmap(pi[None, :], ax=ax, vmin=vmin, vmax=vmax)
    ax.set_title("Initial Probabilities", size=fontsize)
    ax = f.add_subplot(grid[2:num_states+2, :num_states])
    sns.heatmap(A, ax=ax, vmin=vmin, vmax=vmax)
    ax.set_title("Transition Probabilities", size=fontsize)
    return f, grid


def gaussian_hinton_diagram(pi, A, means, variances, vmin=0, vmax=1, infer_hidden=True, fontsize=14):
    """
    Show the initial state probabilities, the transition probabilities as heatmaps, and draw the emission distributions.
    """
    num_states = A.shape[0]
    f, grid = generic_hinton_diagram(pi, A, vmin, vmax, fontsize)

    ax = f.add_subplot(grid[2:num_states+2, num_states:])
    for i in range(num_states):
        keep = True
        if infer_hidden:
            if np.all(np.abs(A[i] - A[i][0]) < 1e-4):
                keep = False
        if keep:
            s_min = means[i] - 10 * variances[i]
            s_max = means[i] + 10 * variances[i]
            xx = np.arange(s_min, s_max, (s_max - s_min) / 1000)
            norm = scipy.stats.norm(means[i], np.sqrt(variances[i]))
            yy = norm.pdf(xx)
            keep = yy > .01
            ax.plot(xx[keep], yy[keep], label="State: {}".format(i))
    ax.set_title("Emissions Probabilities")
    ax.legend(loc="best")
    f.tight_layout()
    return f


def poisson_hinton_diagram(pi, A, rates, vmin=0, vmax=1, infer_hidden=True, fontsize=14):
    """
    Show the initial state probabilities, the transition probabilities as heatmaps, and draw the emission distributions for a poisson
    """
    num_states = A.shape[0]
    fontsize = 14
    f, grid = generic_hinton_diagram(pi, A, vmin, vmax, fontsize)
    ax = f.add_subplot(grid[2:num_states+2, num_states:])

    for i in range(num_states):
        keep = True
        if infer_hidden:
            if np.all(np.abs(A[i] - A[i][0]) < 1e-2):
                keep = False
        if keep:
            s_min = rates[i] - 10 * rates[i]
            s_max = rates[i] + 10 * rates[i]
            xx = np.arange(max(0, int(s_min)), int(s_max))
            norm = scipy.stats.poisson(rates[i])
            yy = norm.pmf(xx)
            keep = yy > .01
            ax.bar(xx[keep], yy[keep], label="State: {}".format(i), alpha=.5)
    ax.set_title("Emissions Probabilities: Poisson", size=fontsize)
    ax.legend(loc="best")
    f.tight_layout()
    return f


def gmm_hinton_diagram(pi, A, weights, means, variances, vmin=0, vmax=1, infer_hidden=True, fontsize=14):
    """
    Show the initial state probabilities, the transition probabilities as heatmaps, and draw the emission distributions.
    """
    num_states = A.shape[0]
    f, grid = generic_hinton_diagram(pi, A, vmin, vmax, fontsize)

    ax = f.add_subplot(grid[2:num_states+2, num_states:])
    for i in range(num_states):

        keep = True
        if infer_hidden:
            if np.all(np.abs(A[i] - A[i][0]) < 1e-4):
                keep = False
        if keep:
            color = None
            for j in range(means[i].shape[0]):
                if variances[i, j].shape[0] > 1:
                    raise NotImplementedError("covariance of more than two dimensions is not supported")
                s_min = means[i, j] - 10 * variances[i, j].ravel()
                s_max = means[i, j] + 10 * variances[i, j].ravel()
                xx = np.arange(s_min, s_max, (s_max - s_min) / 1000)
                norm = scipy.stats.norm(means[i, j], np.sqrt(variances[i, j].ravel()))
                yy = norm.pdf(xx)
                keep = yy > .01
                if color:
                    ax.plot(xx[keep], yy[keep],  color=color)
                else:
                    line = ax.plot(xx[keep], yy[keep], label="State {}".format(i, j), color=color)[0]
                    color = line.get_color()
                ax.fill_between(xx[keep], yy[keep], color=color, alpha=weights[i, j])
    ax.set_title("Emissions Probabilities")
    ax.legend(loc="best")
    f.tight_layout()
    return f


def learning_curve(scores, title1="Scores During Training", title2="Delta Score"):
    """
    Create a pair of plots showing the convergence of a model scores in one plot,
    and the relative change at each iteration in a separate plot.
    """
    scores = np.asarray(scores)
    f = plt.figure(figsize=(16, 8))
    grid = gs.GridSpec(1, 2)

    ax = f.add_subplot(grid[0, 0])
    ax.plot(np.arange(scores.shape[0]), scores)
    ax.set_title(title1, fontsize=16)
    ax = f.add_subplot(grid[0, 1])
    delta = scores[1:] - scores[:-1]
    ax.semilogy(np.arange(delta.shape[0]), delta)
    ax.set_title(title2, fontsize=16)
    return f


def multiple_learning_curve(explored_scores, title1="Scores During Training", title2="Delta Scores", legend=True, keep_top=20):
    """
    Similar to learning_curve, but for multiple sequences of scores.  The best sequence of scores is plotted
    in a bold line, while the rest are plotted using dashes.
    """

    f, axs = plt.subplots(ncols=2, nrows=1, figsize=(12, 6), sharex=True)

    ax1 = axs[0]
    ax2 = axs[1]

    best_model_id = None
    lasts = []
    for model_id, scores in enumerate(explored_scores):
        lasts.append(scores[-1])

    idx = np.argsort(lasts)[::-1]
    if keep_top:
        idx = idx[:keep_top]
    best_model_id = idx[0]

    # Present in order of oiginal model-id
    for model_id in sorted(idx):
        scores = explored_scores[model_id]
        scores = np.asarray(scores)
        linestyle = ":"
        annotation = ""
        if model_id == best_model_id:
            linestyle = "-"
            annotation = " (best)" if model_id == best_model_id else ""
        ax1.plot(np.arange(scores.shape[0]), scores, label="Model: {}{}".format(model_id, annotation), linestyle=linestyle)
        delta = scores[1:] - scores[:-1]
        ax2.semilogy(np.arange(delta.shape[0]), delta, label="Model: {}{}".format(model_id, annotation), linestyle=linestyle)

    ax1.set_title(title1, fontsize=16)
    ax2.set_title(title2, fontsize=16)

    ax1.set_xlabel("Training Iteration", size=14)
    ax1.set_xlabel("Training Iteration", size=14)

    if legend:
        ax1.legend(loc="best", fontsize=12)
        ax2.legend(loc="best", fontsize=12)
    return f


def plot_bic_aic(scores, bic="mean_test_bic", aic="mean_test_aic", caic="mean_test_caic", xticklabels=None, figsize=(8, 8)):
    """
    Plot the resulting scores from an HMMSearchCV object.

    Parameters
    ==========
    scores : dict
        A nested dictionary.  The keys are the parameters.  The values of dictionaries of score names to values.
    xticklabels: list
        Optional set of xticklabels
    figsize : tuple
    """
    index = np.asarray(list(scores.keys()))
    aic = np.asarray([scores[i][aic] for i in index])
    bic = np.asarray([scores[i][bic] for i in index])
    caic = np.asarray([scores[i][caic] for i in index])
    f, ax = plt.subplots(figsize=figsize)
    ax.plot(index, aic,  alpha=.5, linestyle="--", label="AIC: {}".format(index[aic.argmin()]))
    ax.plot(index, bic,  alpha=.5, linestyle="--", label="BIC: {}".format(index[bic.argmin()]))
    ax.plot(index, caic, alpha=.5, linestyle=":", label="CAIC: {}".format(index[caic.argmin()]))
    if xticklabels is None:
        xticklabels = index
    else:
        ax.set_xticklabels(xticklabels, rotation="vertical")
    ax.set_title("Model Evaluation: AIC/BIC/BIC", size=18)
    ax.set_xlabel("Number of Components", size=16)
    ax.set_ylabel("Score", size=16)
    ax.legend(loc="best")
    return f


def plot_mixture_energies(verbose_energies):
    f = plt.figure(figsize=(7, 20))
    grid = gs.GridSpec(7, 1)
    keys = ["total", "subnormed_log_prob", "component_weights", "pi", "A", "B"]
    for i, key in enumerate(keys):
        ax = f.add_subplot(grid[i, 0])
        data = [v[key] for v in verbose_energies]
        idx = np.arange(len(data))
        ax.plot(idx, data)
        ax.set_title(key)
    f.tight_layout()
    return f


def plot_verbose_energies(verbose_energies):
    f = plt.figure(figsize=(7, 20))
    grid = gs.GridSpec(7, 1)
    keys = ["total", "subnormed_log_prob", "pi", "A", "B"]
    for i, key in enumerate(keys):
        ax = f.add_subplot(grid[i, 0])
        data = [v[key] for v in verbose_energies]
        idx = np.arange(len(data))
        ax.plot(idx, data)
        ax.set_title(key)
    f.tight_layout()
    return f
