# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
from abc import abstractmethod
import base64
import pickle

import numpy as np

import sklearn.utils
from sklearn.preprocessing import normalize
from sklearn.utils.validation import check_is_fitted

from . import base

from .types import Optional, Any


class EMHMMBase(base.ConfidenceIntervalMixin, base.EMBase, base.HMMMixin):

    def __init__(self, n_components: int, n_iterations: int, n_inits: int, n_jobs: int, tol: float,
                 init_pi: Optional[str], init_A: Optional[str], implementation: str,
                 dirichlet_random_seed: int = 1000, allowed_to_use_log: bool = True,
                 random_state: Optional[Any] = None, verbose: int = 0):
        """

        Parameters
        ----------
        n_components : int, defaults to 1
            The number of hidden states for the HMM

        n_iterations : int, defaults to 100
            The number of iterations to run for EM

        n_inits : int, defaults to 1
            The number of random initializations to run. The best results are kept

        """
        self.n_components = n_components
        self.n_iterations = n_iterations
        self.n_inits = n_inits
        self.n_jobs = n_jobs
        self.tol = tol
        self.init_pi = init_pi
        self.init_A = init_A
        self.implementation = implementation
        self.allowed_to_use_log = allowed_to_use_log
        self.random_state = random_state
        self.verbose = verbose
        self.dirichlet_random_seed = dirichlet_random_seed

    def _prepare_for_fit(self):
        """
        Ensure everything is in the correct format.
         * Ensure all matrices are properly normalized
         * Ensure all matrices have the correct shape
        """
        super(EMHMMBase, self)._prepare_for_fit()
        self.A_ = np.asarray(self.A_, dtype=float)
        self.pi_ = np.asarray(self.pi_, dtype=float)
        self.A_ = normalize(self.A_, norm="l1", axis=1)
        self.pi_ /= self.pi_.sum()
        self.n_components = self.pi_.shape[0]
        assert self.pi_.shape[0] == self.A_.shape[0], (self.pi_.shape, self.A_.shape)
        assert self.A_.shape[0] == self.A_.shape[1]

    def _assert_is_fitted(self):
        check_is_fitted(self, ["pi_", "A_", ])

    @abstractmethod
    def _emissions_params(self):
        """
        Return the number of free parameters in this HMM.
        """
        raise NotImplementedError()

    def n_parameters(self):
        """
        Return the number of free parameters in this HMM.
        """
        pi = self.pi_
        A = self.A_
        pi_params = max(0, pi.shape[0]-1)
        A_params = max(0, A.shape[0] * (A.shape[1]-1))

        return pi_params + A_params + self._emissions_params()

    def _init_for_fit(self, X, lengths):
        """
        Parameters
        ----------
        X : np.ndarray

        Initialize pi, A, and B.

        Christopher Bishop, Pattern Recognition and Machine Learning
            Page 617 - pi/A select random values subject to normalization and non-negativity

        Rabiner paper:
            Initialize pi/A to uniform
        """
        self._init_pi(X, lengths)
        self._init_A(X, lengths)
        self._init_emissions(X, lengths)
        self._prepare_for_fit()

    def _init_pi(self, X, lengths):
        rs = sklearn.utils.check_random_state(self.random_state)
        if self.init_pi is None:
            pass
        elif isinstance(self.init_pi, str):
            if self.init_pi == "random":
                self.pi_ = rs.dirichlet([self.dirichlet_random_seed]*self.n_components)
            elif self.init_pi == "uniform":
                self.pi_ = np.full(self.n_components, 1. / self.n_components)
            else:
                raise ValueError("Unknown strategy: {}".format(self.init_pi))
        else:
            if not isinstance(self.init_pi, np.ndarray) and not isinstance(self.init_pi, list):
                raise ValueError("init_pi parameter should be an array or a list, got {}".format(type(self.init_pi)))
            self.pi_ = np.asarray(self.init_pi)

    def _init_A(self, X, lengths):
        rs = sklearn.utils.check_random_state(self.random_state)
        if self.init_A is None:
            pass
        elif isinstance(self.init_A, str):
            if self.init_A == "random":
                A = np.zeros((self.n_components, self.n_components))
                seed = np.array([self.dirichlet_random_seed] * self.n_components)
                rs = sklearn.utils.check_random_state(self.random_state)
                for i in range(self.n_components):
                    A[i] = rs.dirichlet(seed)
                self.A_ = A
            elif self.init_A == "uniform":
                self.A_ = np.full((self.n_components, self.n_components), 1. / self.n_components)
            elif self.init_A == "diagonal":
                self.A_ = np.full((self.n_components, self.n_components), (.5 / (self.n_components - 1)))
                indices = np.arange(self.n_components)
                self.A_[indices, indices] = .5
            elif self.init_A == "left-to-right":
                assert False, "Not implemented"
            else:
                raise ValueError("Unknown strategy: {}".format(self.init_A))
        else:
            if not isinstance(self.init_A, np.ndarray) and not isinstance(self.init_A, list):
                raise ValueError("init_A parameter should be an array or a list, got {}".format(type(self.init_A)))
            self.A_ = np.asarray(self.init_A)
    def _update_after_fit(self, trainer):
        self.pi_ = np.asarray(trainer.pi)
        self.A_ = np.asarray(trainer.A)

    def to_dict(self):
        """
        Serialize models... ignores random state
        """
        pi = None
        A = None
        if hasattr(self, "pi_"):
            pi = np.asarray(self.pi_).tolist()
        if hasattr(self, "A_"):
            A = np.asarray(self.A_).tolist()

        params = self.get_params()
        params["random_state"] = base64.b64encode(pickle.dumps(params["random_state"])).decode("ascii")
        return {
            "params": params,
            "pi": pi,
            "A": A,
            "emissions": self._emissions_to_dict(),
            "loglikelihoods_": self.loglikelihoods_ if hasattr(self, "loglikelihoods_") else None,
            "explored_loglikelihoods_": self.explored_loglikelihoods_ if hasattr(self, "explored_loglikelihoods_") else None
        }

    @classmethod
    def from_dict(cls, model_dict):
        instance = cls()
        instance.set_params(**model_dict["params"])
        instance.random_state = pickle.loads(base64.b64decode(instance.random_state))
        if model_dict["pi"] is not None:
            instance.pi_ = np.asarray(model_dict["pi"])
        if model_dict["A"] is not None:
            instance.A_ = np.asarray(model_dict["A"])
        instance._emissions_from_dict(model_dict["emissions"])
        if "loglikelihoods_" in model_dict and model_dict["loglikelihoods_"] is not None:
            instance.loglikelihoods_ = model_dict["loglikelihoods_"]
        if "explored_loglikelihoods_" in model_dict and model_dict["explored_loglikelihoods_"] is not None:
            instance.explored_loglikelihoods_ = model_dict["explored_loglikelihoods_"]
        return instance
