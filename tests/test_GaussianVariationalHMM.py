# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import json
import logging

import numpy as np
import pandas as pd
import pytest
import sklearn.cluster
import sklearn.utils

from hmm import GaussianHMM, GaussianVariationalHMM
from hmm.datasets import for_tests

pd.options.display.float_format = "{:.4f}".format


def test_normals_simple():
    hidden_states = np.asarray([0]*100 + [1] * 100)
    rs = sklearn.utils.check_random_state(1)
    observed = np.concatenate([rs.normal(5, np.sqrt(5), size=100), rs.normal(10, 1, size=100)]).ravel()[:, None]
    lengths = np.asarray([200])
    trainer = GaussianVariationalHMM.GaussianVariationalHMM(n_components=2, pi_posterior="uniform", random_state=rs, verbose=logging.DEBUG, implementation="scaling", tol=1e-6)
    trainer.fit(observed, lengths)
    print(trainer.transform(observed, lengths))
    print("after fit")
    print(trainer.means_posterior_)
    print(trainer.beta_posterior_)
    print(trainer.beta_prior_)
    print("ASDF")
    print(trainer.variances_posterior_)
    print("delta", trainer.b_posterior_)
    print("gamma", trainer.a_posterior_)
    print("delta", trainer.b_prior_)
    print("gamma", trainer.a_prior_)
    print()
    new = GaussianVariationalHMM.GaussianVariationalHMM.reestimate_from_sequences(observed, hidden_states, lengths)
    print(new.pi_posterior_)
    print(new.A_posterior_)
    print(new.means_posterior_)
    print(new.variances_posterior_)
    assert new.pi_posterior_[0] == 1
    assert new.pi_normalized_[0] == 1
    assert new.pi_normalized_[1] == 0
    assert new.A_normalized_[0, 0] == pytest.approx(0.99, 1e-6)
    assert new.A_normalized_[1, 1] == pytest.approx(1, 1e-6)
    assert new.A_posterior_[0, 0] == pytest.approx(99., 1e-6)
    assert new.A_posterior_[1, 1] == pytest.approx(100, 1e-6)
    assert new.means_posterior_[0] == pytest.approx(5.13546738, 1e-6)
    assert new.means_posterior_[1] == pytest.approx(10.15279478, 1e-6)
    assert new.variances_posterior_[0] == pytest.approx(3.91750761, 1e-6)
    assert new.variances_posterior_[1] == pytest.approx(0.86861434, 1e-6)

    print(new.transform(observed, lengths))
    new = GaussianVariationalHMM.GaussianVariationalHMM(n_components=3, verbose=logging.DEBUG)
    new.fit(observed, lengths)
    print(new.pi_normalized_)
    print(new.A_normalized_)
    print(new.means_posterior_)
    print(new.variances_posterior_)


def get_data():
    random_state = sklearn.utils.check_random_state(2)
    pi = [.5, .5]
    A = [
        [.3, .7],
        [.7, .3],
    ]
    means = [-10, 5]
    variances = [5, 3]

    model = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None)
    model.pi_ = pi
    model.A_ = A
    model.means_ = means
    model.variances_ = variances
    observed_sequences, hidden_sequences, lengths = model.sample(10, 500, random_state=random_state)

    return observed_sequences, hidden_sequences, lengths


def test_matt2():
    for impl in ["log", ]:#"scaling"]:
        random_state = sklearn.utils.check_random_state(2)
        observed_sequences, hidden_sequences, lengths = get_data()
        observed_sequences = np.asarray(observed_sequences)
        new = GaussianVariationalHMM.GaussianVariationalHMM.reestimate_from_sequences(observed_sequences, hidden_sequences, lengths)
        print(new.pi_posterior_)
        print(new.A_posterior_)
        print(new.means_posterior_)
        print(new.variances_posterior_)


        trainer = GaussianVariationalHMM.GaussianVariationalHMM(n_components=3,  n_iterations=100, n_inits=1, verbose=logging.DEBUG, random_state=random_state, implementation=impl)
        trainer.fit(observed_sequences, lengths)


        print(trainer.pi_normalized_)
        print(trainer.A_normalized_)
        print(trainer.means_posterior_)
        print(trainer.variances_posterior_)


def test_normals_two_state_variational_for_real():

    observed_sequences, hidden_sequences, lengths = get_data()
    observed_sequences = np.asarray(observed_sequences)
    for impl in ["scaling", "log"]:
        estimator = GaussianVariationalHMM.GaussianVariationalHMM(n_components=3, random_state=sklearn.utils.check_random_state(49), implementation=impl, n_iterations=1000)  # , means_prior="data", varianaces_prior="data")

        estimator.fit(observed_sequences, lengths)
        print(estimator.pi_posterior_.tolist())
        print(estimator.A_posterior_.tolist())
        print(estimator.A_normalized_.tolist())
        print("means")
        print(estimator.means_posterior_.tolist())
        print("betas")
        print(estimator.beta_posterior_.tolist())
        print("variances")
        print(estimator.variances_posterior_.tolist())
        print("gamma")
        print(pd.Series(estimator.a_posterior_))
        print("delta")
        print(pd.Series(estimator.b_posterior_))
        check_pi = np.asarray([6.333333332034687, 4.333333334631999, 0.3333333333333333])
        check_means = np.asarray([4.99946968220844, -10.03153769608108, -2.483216635338578])
        check_variances = np.asarray([2.936641234677004, 4.879112509306887, 0.001013285683421748])

        check_sort = np.argsort(check_pi)
        pi_sort = np.argsort(estimator.pi_posterior_)
        print(estimator.means_posterior_[pi_sort])
        print(check_means[check_sort])
        print(estimator.pi_posterior_[pi_sort] - check_pi[check_sort])
        assert estimator.pi_posterior_[pi_sort] == pytest.approx(check_pi[check_sort], abs=1e-4), impl
        assert estimator.means_posterior_[pi_sort] == pytest.approx(check_means[check_sort], abs=1e-3), impl
        assert estimator.variances_posterior_[pi_sort] == pytest.approx(check_variances[check_sort], abs=1e-4), impl

        #check_A = [[748.2460093181613, 1760.341602928382, 0.3333333333333333], [1757.3387492879112, 725.40126451783, 0.33618697385680957], [0.3361869738567795, 0.3333333333333634, 0.3333333333333333]]

        #for i in range(3):
        #    assert estimator.A_posterior_[i] == pytest.approx(check_A[i]), i
        translated = estimator.transform(observed_sequences, lengths)
        mutual = sklearn.metrics.mutual_info_score(hidden_sequences, translated)
        assert mutual == pytest.approx(0.6931375005287114, 1e-6), mutual

        new_estimator = GaussianVariationalHMM.GaussianVariationalHMM.from_dict(json.loads(json.dumps(estimator.to_dict())))
        translated = new_estimator.transform(observed_sequences, lengths)
        mutual = sklearn.metrics.mutual_info_score(hidden_sequences, translated)
        assert mutual == pytest.approx(0.6931375005287114, 1e-6), mutual

        new_observations, new_hidden, new_lengths = estimator.sample(10, 500)
        new_estimator = GaussianVariationalHMM.GaussianVariationalHMM(n_components=3, pi_posterior="uniform", random_state=sklearn.utils.check_random_state(42), implementation=impl)
        new_estimator.fit(new_observations, new_lengths)
        print(new_estimator.means_posterior_)
        print(new_estimator.variances_posterior_)

        learned = GaussianVariationalHMM.GaussianVariationalHMM.reestimate_from_sequences(observed_sequences, hidden_sequences, lengths)
        print(learned.means_posterior_)
        print(learned.variances_posterior_)


def check(pi, check_pi, A, check_A, means, check_means, variances, check_variances, abs_tol=1e-5, sort_means=False):
    pi_sort = np.argsort(pi)
    check_sort = np.argsort(check_pi)

    pi = np.asarray(pi)[pi_sort]
    check_pi = np.asarray(check_pi)[check_sort]
    assert pi == pytest.approx(check_pi, abs=abs_tol)
    means = np.asarray(means)[pi_sort]
    check_means = np.asarray(check_means)[check_sort]
    assert check_means == pytest.approx(means, abs=abs_tol)
    variances = np.asarray(variances)[pi_sort]
    check_variances = np.asarray(check_variances)[check_sort]
    assert check_variances == pytest.approx(variances, abs=abs_tol)
    for i,j  in zip(pi_sort, check_sort):
        a_i = np.asarray(A)[i]
        a_j = np.asarray(check_A)[j]
        assert a_i[pi_sort] == pytest.approx(a_j[check_sort], abs=abs_tol)


def get_data4(n_samples=10, sample_size=50, random_state=2):
    random_state = sklearn.utils.check_random_state(random_state)
    return for_tests.get_gaussian_mcgrory_and_titterington_sequences(n_samples, sample_size, random_state)


def test_normals_4_state():
    observed_sequences, hidden_sequences, lengths = get_data4(1, 500)
    observed_sequences = np.asarray(observed_sequences)
    for impl in ["scaling", "log"]:
        estimator = GaussianVariationalHMM.GaussianVariationalHMM(n_components=6, n_inits=10, n_jobs=1, pi_posterior="uniform", random_state=sklearn.utils.check_random_state(54), verbose=logging.DEBUG, implementation=impl, n_iterations=1000)

        estimator.fit(observed_sequences, lengths)
        print(pd.Series(estimator.pi_posterior_))
        print(pd.DataFrame(estimator.A_posterior_))
        print(pd.DataFrame(estimator.A_normalized_))
        print("means")
        print(estimator.means_posterior_.tolist())
        print("betas")
        print(estimator.beta_posterior_.tolist())
        print("variances")
        print(pd.Series(estimator.variances_posterior_))
        print("gamma")
        print(pd.Series(estimator.a_posterior_))
        print("delta")
        print(pd.Series(estimator.b_posterior_))
        means = np.asarray([3.001609816331035, -0.0026712447398772325, 0.5833425480358588, 1.4425579563822202, -1.471920823803236, 0.5833425480358588])
        variances = np.asarray([0.13160778447534582, 0.05990311738473421, 0.000999995124396639, 0.07496766220054674, 0.11025154460443634, 0.0009999951243966471])
        print(estimator.means_posterior_.tolist())
        print(estimator.variances_posterior_.tolist())
        print(estimator.means_prior_.tolist())
        assert sorted(estimator.means_posterior_) == pytest.approx(sorted(means), abs=1e-4)
        assert sorted(estimator.variances_posterior_) == pytest.approx(sorted(variances), abs=1e-4)


def test_going_to_zero():
    for impl in ["log", "scaling"]:
        random_state = sklearn.utils.check_random_state(12)
        data1 = [int(i) for i in random_state.normal(10, 2, 1000)]
        data2 = [5] * 100 + [6]*10 + [4]*10
        data3 = [0] * 200
        data = np.asarray(data1 + data2 + data3)
        data2d = data[:, None]
        lengths = np.asarray([1000, 120, 200])
        model = GaussianVariationalHMM.GaussianVariationalHMM(n_components=4, pi_posterior="uniform", random_state=random_state, implementation=impl, n_iterations=600, verbose=logging.INFO)
        model.fit(data2d, lengths)
        print(pd.DataFrame(model.A_posterior_))
        print(pd.DataFrame(model.means_posterior_))
        print(pd.DataFrame(model.variances_posterior_))
        print(pd.DataFrame(model.b_posterior_))
        print(pd.DataFrame(model.a_posterior_))
        print(model.score(data2d, lengths))
        assert model.score(data2d, lengths) == pytest.approx(-762.4986824136532, 1e-6)

        #assert model.means_[0] == pytest.approx(0, 1e-6), impl
        #assert model.means_[1] == pytest.approx(5, 1e-6), impl
        #assert model.means_[2] == pytest.approx(8.61064741, 1e-6), impl
        #assert model.variances_[0] == 1e-6, impl
        #assert model.variances_[1] == 1e-6, impl
        #assert model.variances_[2] == pytest.approx(2.36056431e+01), impl
#
        #assert model.score(data2d) == pytest.approx(1426.1535387398171, 1e-6), impl
        #assert model.aic(data2d) == pytest.approx(-2824.3070774796342, 1e-6), impl
        #assert model.bic(data2d) == pytest.approx(-2767.7435115217504, 1e-6), impl


def test_dic():
    observed_sequences, hidden_sequences, lengths = get_data4(1, 500, 2323)
    observed_sequences = np.asarray(observed_sequences)
    print(lengths)
    trainers = {}
    dics = {}
    pds = {}
    dic_values = []
    for i in range(1, 8):
        trainer = GaussianVariationalHMM.GaussianVariationalHMM(n_components=i, n_iterations=1000, pi_posterior="uniform", n_inits=10)  # , verbose=logging.DEBUG)
        trainer.fit(observed_sequences, lengths)
        trainers[i] = trainer
        pds[i], dics[i] = trainer.verbose_dic(observed_sequences, lengths)
        dic_values.append(trainer.dic(observed_sequences, lengths))

    for i, m in trainers.items():
        print(i, pds[i].mean(),  dics[i].mean(), m.score(observed_sequences, lengths))

    assert np.argmin(dic_values) == 3, dic_values


if __name__ == "__main__":
    #test_matt2()
    #test_dic()
    #test_normals_simple()
    test_normals_two_state_variational_for_real()
    # test_normals_two_state_variational_for_real()
    #test_normals_4_state()
    #test_going_to_zero()
    #test_dic()
