import numpy as np

import pytest

from hmm import wrapped_hmm


def test_wikipedia():
    """
    https://en.wikipedia.org/wiki/Viterbi_algorithm#Example
    """

    print("Test Viterbi")
    start_p = {'Healthy': 0.6, 'Fever': 0.4}
    trans_p = {
        'Healthy': {'Healthy': 0.7, 'Fever': 0.3},
        'Fever': {'Healthy': 0.4, 'Fever': 0.6}
    }
    emit_p = {
        'Healthy': {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
        'Fever': {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6}
    }

    for impl in ["log", "scaling"]:
        wrapped = wrapped_hmm.WrappedHmm(
            start_p,
            trans_p,
            emit_p,
            implementation=impl
        )
        for hidden_state, probability in start_p.items():
            assert wrapped.pi[hidden_state] == pytest.approx(probability, 1e-5), impl

        for hidden_state, emissions in emit_p.items():
            for observed, emit in emissions.items():
                assert wrapped.B[hidden_state][observed] == pytest.approx(emit, 1e-5), impl

        for hidden_state, transition_probs in trans_p.items():
            for next_hidden_state, prob in transition_probs.items():
                assert wrapped.A[hidden_state][next_hidden_state] == pytest.approx(prob, 1e-5), impl

        tests = [
            ['normal', 'cold', 'dizzy']
        ]
        hidden_path = wrapped.viterbi(tests)
        assert hidden_path == ["Healthy", "Healthy", "Fever"], impl
        print(hidden_path)


def test_revealing():

    print("Test Revealing")

    start_p = {"Hot": 0.6, "Cold": 0.4}
    trans_p = {
        "Hot": {
            "Hot": 0.7,
            "Cold": 0.3,
        },
        "Cold": {
            "Hot": 0.4,
            "Cold": 0.6
        }
    }
    emit_p = {
        "Hot": {
            "Small": 0.1,
            "Medium": 0.4,
            "Large": 0.5,
        },
        "Cold": {
            "Small": 0.7,
            "Medium": 0.2,
            "Large": 0.1,
        }
    }

    wrapped = wrapped_hmm.WrappedHmm(
        start_p,
        trans_p,
        emit_p,
    )

    ans = wrapped.viterbi(
        [
            [
                "Small",
                "Medium",
                "Small",
                "Large",
            ]
        ]
    )
    assert ans == ["Cold", "Cold", "Cold", "Hot"], ans


def test_speech_and_language_processing():
    print("Test Speech and Language Processing")

    start_probability = {"Hot": 0.8, "Cold": 0.2}
    transition_probability = {
        "Hot": {
            "Hot": 0.6,
            "Cold": 0.4,
        },
        "Cold": {
            "Hot": 0.5,
            "Cold": 0.5
        }
    }

    emission_probability = {
        "Hot": {
            "one": 0.2,
            "two": 0.4,
            "three": 0.4,
        },
        "Cold": {
            "one": 0.5,
            "two": 0.4,
            "three": 0.1,
        }
    }

    for impl in ["log", "scaling"]:
        wrapped = wrapped_hmm.WrappedHmm(
            start_probability,
            transition_probability,
            emission_probability,
            implementation=impl
        )
        for hidden_state, probability in start_probability.items():
            assert wrapped.pi[hidden_state] == pytest.approx(probability, 1e-5), impl

        for hidden_state, emissions in emission_probability.items():
            for observed, emit in emissions.items():
                assert wrapped.B[hidden_state][observed] == pytest.approx(emit, 1e-5), impl

        for hidden_state, transition_probs in transition_probability.items():
            for next_hidden_state, prob in transition_probs.items():
                assert wrapped.A[hidden_state][next_hidden_state] == pytest.approx(prob, 1e-5), impl
        tests = [
            ["three", "one", "three"],
            #                ["three", "two", "three"],
        ]
        likelihood = wrapped.loglik(
            tests
        )
        print(wrapped.viterbi(tests))
        print(likelihood)
        assert np.exp(likelihood[0]) == pytest.approx(0.028562, 1e-5), impl
        #assert likelihood[1] == pytest.approx(0.036424, 1e-5)


def test_longer():
    """
    http://cecas.clemson.edu/~ahoover/ece854/refs/Gonze-ViterbiAlgorithm.pdf
    """
    print("Test Longer")
    start_probability = {"H": 0.5, "L": 0.5}
    transition_probability = {
        "H": {
            "H": 0.5,
            "L": 0.5,
        },
        "L": {
            "H": 0.4,
            "L": 0.6
        }
    }

    emission_probability = {
        "H": {
            "A": 0.2,
            "C": 0.3,
            "G": 0.3,
            "T": 0.2,
        },
        "L": {
            "A": 0.3,
            "C": 0.2,
            "G": 0.2,
            "T": 0.3,
        }
    }
    for impl in ["log", "scaling"]:
        wrapped = wrapped_hmm.WrappedHmm(
            start_probability,
            transition_probability,
            emission_probability,
            implementation=impl,
        )
        for hidden_state, probability in start_probability.items():
            assert wrapped.pi[hidden_state] == pytest.approx(probability, 1e-5), impl

        for hidden_state, emissions in emission_probability.items():
            for observed, emit in emissions.items():
                assert wrapped.B[hidden_state][observed] == pytest.approx(emit, 1e-5), impl

        for hidden_state, transition_probs in transition_probability.items():
            for next_hidden_state, prob in transition_probs.items():
                assert wrapped.A[hidden_state][next_hidden_state] == pytest.approx(prob, 1e-5), impl
        tests = [
            ["G", "G", "C", "A", "C", "T", "G", "A", "A"]
        ]
        likelihood = wrapped.likelihood(
            tests
        )
        print(likelihood)
        #assert likelihood[0] == pytest.approx(0.028562, 1e-5)
        #assert likelihood[1] == pytest.approx(0.036424, 1e-5)

        hidden_path = wrapped.viterbi(tests)
        print(hidden_path)
        assert hidden_path == ['H', 'H', 'H', 'L', 'L', 'L', 'L', 'L', 'L'], impl



