# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import base64
import contextlib
import json
import logging
import os.path
import shutil
import tempfile

import numpy as np
import pandas as pd
import pytest
import sklearn.metrics
import sklearn.utils

from hmm import CategoricalHMM, CategoricalVariationalHMM, distance, model_selection, util
from hmm.datasets.for_tests import get_categorical_beal
from hmm.plots import hinton_diagram

pd.options.display.float_format = "{:.3f}".format
pd.options.display.max_rows = 20
pd.options.display.max_columns = 20
pd.set_option('max_colwidth', 90)


@contextlib.contextmanager
def delete_temp(suffix=""):
    """
    Efficient use of Temp dirs during tests
    """
    temp_dir = tempfile.mkdtemp(suffix)
    shutil.rmtree(temp_dir)
    os.makedirs(temp_dir)
    print("Created", temp_dir)
    yield temp_dir
    print("deleting", temp_dir)
    shutil.rmtree(temp_dir)


def test_reestimate():
    rs = sklearn.utils.check_random_state(1)
    observed, lengths, labels, hidden_states = get_categorical_beal(rs, unique_hidden_states=True, max_length=39)
    new = CategoricalVariationalHMM.CategoricalVariationalHMM.reestimate_from_sequences(observed, hidden_states, lengths)
    print(new.pi_posterior_)
    print(new.A_normalized_)
    print(new.A_subnormalized_)
    print(new.A_posterior_)
    print(new.B_posterior_)
    assert new.pi_posterior_[0] == 2
    assert new.A_posterior_[0, 1] == 66
    assert new.B_posterior_[0, 0] == 66
    assert int(new.B_posterior_[6, 0]) == 99, new.B_posterior_[6,0]

    transformed = new.transform(observed, lengths)
    score = sklearn.metrics.v_measure_score(hidden_states, transformed)
    assert score == pytest.approx(1), score

    print(new.score(observed, lengths))
    print(new.score_samples(observed, lengths))
    new_observations, new_hidden, new_lengths = new.sample(100, 18, random_state=43)
    print(new_observations)
    print(new_hidden)
    trainer = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=7, n_iterations=500, n_inits=4, n_jobs=1, random_state=42, verbose=logging.INFO)
    trainer.fit(new_observations, new_lengths)
    print(trainer.pi_prior_)
    print(trainer.pi_posterior_)
    print(trainer.A_posterior_)
    print(trainer.B_posterior_)
    predicted = trainer.transform(new_observations, new_lengths)
    score = sklearn.metrics.adjusted_rand_score(new_hidden, predicted)
    print(score)
    print(pd.crosstab(new_hidden, predicted))
    assert score >= .89


def test_save_load():
    rs = sklearn.utils.check_random_state(422)
    data, lengths, _, _ = get_categorical_beal(rs)
    trainer = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=4, n_iterations=1000, random_state=rs, verbose=logging.DEBUG, n_inits=1, n_jobs=1)
    trainer.fit(data, lengths)
    print(trainer.lower_bound_)

    new = CategoricalVariationalHMM.CategoricalVariationalHMM.from_dict(trainer.to_dict())
    print(new.lower_bound_)
    print(trainer.score_samples(data, lengths))
    print(new.score_samples(data, lengths))
    assert new.score(data, lengths) == trainer.score(data, lengths)

    model = CategoricalVariationalHMM.CategoricalVariationalHMM()
    new_model = CategoricalVariationalHMM.CategoricalVariationalHMM.from_dict(model.to_dict())


def test_simple_bad():
    training_data = [
        [0, 0, ] * 20 + [1] * 10 + [0]*20 + [5] * 10 + [0] * 20
    ]

    training_data = np.asarray(training_data).ravel()[:, None]
    lengths = np.asarray([100])
    tol = 1e-6
    model = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=10, n_iterations=40,  n_features=8, random_state=32, verbose=logging.ERROR, tol=tol)

    model.fit(training_data, lengths)
    print(training_data)
    viterbid = model.transform(training_data, lengths)
    for row in viterbid:
        print(row)
    print("pi")
    print(pd.DataFrame(model.pi_posterior_))
    print(pd.DataFrame(model.pi_subnormalized_))
    print(pd.DataFrame(model.pi_normalized_))
    print("A")
    print(pd.DataFrame(model.A_posterior_))
    print(pd.DataFrame(model.A_subnormalized_))
    print(pd.DataFrame(model.A_normalized_))
    print("B")
    print(pd.DataFrame(model.B_posterior_))
    print(pd.DataFrame(model.B_subnormalized_))
    print(pd.DataFrame(model.B_normalized_))

    perplexity = model_selection.perplexity(model, training_data, lengths)
    assert perplexity[0] == pytest.approx(1.2975411846842695), perplexity[0]
    # $should have expected tolerance
    delta = model.lower_bound_[-1] - model.lower_bound_[-2]
    assert delta < tol


def test_simple2():
    data = [
        [0, 1, 2] * 20,
        [1, 2, 0] * 20,
        [2, 0, 1] * 20,
    ]

    data = np.asarray(data).ravel()[:, None]
    lengths = np.asarray([60, 60, 60])
    models = {}
    for implementation in ["log", "scaling"]:
        with delete_temp(implementation) as tmp:
            models[implementation] = model = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=4, n_iterations=40,  random_state=sklearn.utils.check_random_state(32), verbose=logging.INFO, implementation=implementation)

            print(data, lengths)
            model.fit(data, lengths)
            viterbid = model.transform(data, lengths)
            seq_start = 0
            for l in lengths:
                seq_end = seq_start + l
                row = viterbid[seq_start:seq_end]
                assert len(set(row)) == 3
                seq_start = seq_end
            print("pi")
            print(pd.DataFrame(model.pi_posterior_))
            print(pd.DataFrame(model.pi_subnormalized_))
            print(pd.DataFrame(model.pi_normalized_))
            print("A")
            print(pd.DataFrame(model.A_posterior_))
            print(pd.DataFrame(model.A_subnormalized_))
            print(pd.DataFrame(model.A_normalized_))
            print("B")
            print(pd.DataFrame(model.B_posterior_))
            print(pd.DataFrame(model.B_subnormalized_))
            print(pd.DataFrame(model.B_normalized_))

            fig = hinton_diagram(model.pi_posterior_, model.A_posterior_, model.B_posterior_, None, None)
            name = os.path.join(tmp, "test_simple2_counts.png")
            fig.savefig(name)
            assert os.path.exists(name)

            fig = hinton_diagram(model.pi_normalized_, model.A_normalized_, model.B_normalized_)
            name = os.path.join(tmp, "test_simple2_normed.png")
            fig.savefig(name)
            assert os.path.exists(name)
            print(model.score_samples(data, lengths).tolist())
            blob = json.dumps(model.to_dict())
            model2 = model.from_dict(json.loads(blob))
            print("perplexity")
            assert np.all(model_selection.perplexity(model, data, lengths) == model_selection.perplexity(model2, data, lengths))

    assert models["log"].score_samples(data, lengths).tolist() == pytest.approx(models["scaling"].score_samples(data, lengths).tolist(), 1e-9)
    assert models["log"].score(data, lengths) == pytest.approx(models["scaling"].score(data, lengths), 1e-9)


def test_simple4():
    random_state = sklearn.utils.check_random_state(43)
    data, lengths, labels, _ = get_categorical_beal(random_state, num_each=7, max_length=39)
    models = {}
    for implementation in ["log", "scaling"]:
        with delete_temp(implementation) as tmp:
            models[implementation] = model = CategoricalHMM.CategoricalHMM(
                n_components=12,
                n_iterations=200,
                random_state=sklearn.utils.check_random_state(1983),
                verbose=logging.ERROR,
                tol=1e-6,
                implementation=implementation)
            model.fit(data, lengths)
            print("pi")
            print(pd.DataFrame(model.pi_))
            print("A")
            print(pd.DataFrame(model.A_))
            print("B")
            print(pd.DataFrame(model.B_))
            viterbid = model.transform(data, lengths)
            seq_start = 0
            for i, l in enumerate(lengths):
                seq_end = seq_start + l
                row = viterbid[seq_start:seq_end]
                print(row)
                seq_start = seq_end
                if 0 <= i < 6:
                    assert len(set(row)) in (3, 4), i
                elif 6 <= i < 12:
                    assert len(set(row)) in (2, 3, 4), i
                else:
                    assert len(set(row)) in (3, 4, 5), i

            fig = hinton_diagram(model.pi_, model.A_, model.B_)
            name = os.path.join(tmp, "test_simple4_em_normed.png")
            fig.savefig(name)
            assert os.path.exists(name)
            print("Variational")

            model = CategoricalVariationalHMM.CategoricalVariationalHMM(
                n_components=12,
                n_iterations=400,
                n_inits=1,
                pi_prior=1,
                pi_posterior="uniform",
                n_jobs=1,
                random_state=sklearn.utils.check_random_state(1986),
                verbose=logging.ERROR,
                tol=1e-6,
                implementation=implementation)
            model.fit(data, lengths)
            print("pi")
            print(pd.DataFrame(model.pi_posterior_))
            print(pd.DataFrame(model.pi_subnormalized_))
            print(pd.DataFrame(model.pi_normalized_))
            print("A")
            print(pd.DataFrame(model.A_posterior_))
            print(pd.DataFrame(model.A_subnormalized_))
            print(pd.DataFrame(model.A_normalized_))
            print("B")
            print(pd.DataFrame(model.B_posterior_))
            print(pd.DataFrame(model.B_subnormalized_))
            print(pd.DataFrame(model.B_normalized_))
            viterbid = model.transform(data, lengths)
            seq_start = 0
            for i, l in enumerate(lengths):
                seq_end = seq_start + l
                row = viterbid[seq_start:seq_end]
                print(row)
                seq_start = seq_end

                if labels[i] == 0 or labels[i] == 1:
                    assert len(set(row)) == 3, set(row)
                else:
                    assert len(set(row)) == 1, set(row)

            fig = hinton_diagram(model.pi_posterior_, model.A_posterior_, model.B_posterior_, None, None)
            name = os.path.join(tmp, "test_simple4_variational_counts.png")
            fig.savefig(name)
            fig = hinton_diagram(model.pi_normalized_, model.A_normalized_, model.B_normalized_)
            name = os.path.join(tmp, "test_simple4_variational_normed.png")
            fig.savefig(name)
            print(model.lower_bound_)
            print(model.score_samples(data, lengths).tolist())
            print("perplexity")
            print(model_selection.perplexity(model, data, lengths))
            blob = json.dumps(model.to_dict())
            model2 = model.from_dict(json.loads(blob))
            assert np.all(model_selection.perplexity(model, data, lengths) == model_selection.perplexity(model2, data, lengths))

    assert models["log"].score_samples(data, lengths).tolist() == pytest.approx(models["scaling"].score_samples(data, lengths).tolist(), 1e-9)
    assert models["log"].score(data, lengths) == pytest.approx(models["scaling"].score(data, lengths), 1e-9)


def test_model_distance():
    random_state = sklearn.utils.check_random_state(43)
    pi = [.5, .5]
    A = [[.7, .3], [.3, .7]]
    B = [[.5, .25, .25, 0], [.0, .5, .0, .5]]

    m1 = CategoricalHMM.CategoricalHMM(n_components=2, init_pi=None, init_A=None, init_emissions=None)
    m1.pi_ = pi
    m1.A_ = A
    m1.B_ = B

    observed, hidden, lengths = m1.sample(10, 100, random_state=random_state)
    m2 = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=4, n_iterations=1000, random_state=random_state)
    m2.fit(observed, lengths)
    m3 = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=4, n_iterations=1000, random_state=random_state)
    m3.fit(observed, lengths)
    print(pd.DataFrame(m2.pi_posterior_))
    print(pd.DataFrame(m3.pi_posterior_))
    print(pd.DataFrame(m2.A_posterior_))
    print(pd.DataFrame(m3.A_posterior_))
    print(pd.DataFrame(m2.B_posterior_))
    print(pd.DataFrame(m3.B_posterior_))

    distances, symmetric_distances, model_0_logliks, model_1_logliks = distance.explore_model_distances(m2, m3)
    assert np.asarray(symmetric_distances).sum() > 0


def test_convergence():

    for impl in ["scaling", "log"]:
        for n_components in range(2, 10):
            try:
                rs1 = sklearn.utils.check_random_state(None)
                rs2 = rs1.get_state()
                observations, lengths, labels, _ = get_categorical_beal(rs1, max_length=39)
                mixture = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=n_components, n_iterations=500, n_inits=100, n_jobs=-1, random_state=rs1)
                mixture.fit(observations, lengths)
            except AssertionError as err:
                print(err)
                print("n_components={}".format(n_components))
                print("Random state was")
                print(base64.b64encode(rs2))
                raise err
            print("pi")
            print(pd.DataFrame(mixture.pi_normalized_))
            print("A")
            print(pd.DataFrame(mixture.A_normalized_))
            print("B")
            print(pd.DataFrame(mixture.B_normalized_))
            print(mixture.lower_bound_[-10:])


def test_matlab():

    data = [
        '012012012012012012012012012012012012012012012012012012012012',
        '120120120120120120120120120120120120120120120120120120120120',
        '201201201201201201201201201201201201201201201201201201201201',
        '021021021021021021021021021021021021021021021021021021021021',
        '210210210210210210210210210210210210210210210210210210210210',
        '102102102102102102102102102102102102102102102102102102102102',
        '010001110101001000011101010100010111110010111010011010101111',
        '001010000010111011101110000101111100100100001111011111010100',
        '101000110001101101110000001000101001110010010010110011000111',
    ]

    def clean_data(data):
        new = []
        for row in data:
            new.append([float(i) for i in row])
        print(new)
        return new

    observations = np.asarray(clean_data(data))
    lengths = np.asarray([observations.shape[1]] * observations.shape[0])
    observations = observations.ravel()[:, None]
    print(observations)
    print(lengths)

    pi_prior = np.full((9,), 1/9)
    pi_posterior = np.asarray([0.0196828933, 0.0210407171, 6.0071713732, 0.0042801612, 0.0002860520, 0.0000012833, 0.0559010359, 2.8902008421, 0.0014356418]) + pi_prior
    print(pi_prior)
    A_prior = np.full((9, 9), 1/9)
    A_posterior = np.asarray([
   [8.9611e+00,  6.7189e-06,  1.5610e+00,  5.5008e-02,  5.1069e+00,  4.3938e+00, 3.2657e+01,  4.1734e-03,  4.8726e+02],
   [1.4665e-01,  4.5585e-05,  4.7601e+02,  6.3515e-01,  1.2218e+01,  8.8587e-03, 6.1410e-02,  1.4783e-02,  5.0906e+01],
   [1.1576e-02,  1.1846e+02,  4.5542e+01,  5.9541e-12,  1.0973e-04,  2.2056e-09, 9.5606e-02,  7.4146e+01,  3.0175e+02],
   [1.3188e-04,  1.0455e-04,  3.4427e-01,  2.3810e+01,  2.1069e-01,  2.7604e-08, 5.1534e+02,  4.0612e-13,  2.8978e-01],
   [2.2743e+02,  1.4500e+02,  3.7442e-01,  2.4395e+00,  1.1185e-17,  4.3252e+00, 1.6002e+02,  6.7181e-02,  3.4574e-01],
   [2.7710e-05,  8.3917e+00,  3.5396e+02,  1.6110e-01,  1.4266e+02,  3.4813e+01, 4.3367e-05,  1.3292e-03,  1.0885e-02],
   [2.9620e+01,  3.1945e-06,  1.5300e-01,  8.1257e-01,  2.6675e+02,  4.2533e-01, 6.3487e+00,  1.0615e+01,  2.2527e+02],
   [1.3239e+00,  2.9374e-09,  9.9327e-04,  1.5855e+01,  5.2194e+02,  5.6585e-02, 8.0129e-01,  2.2702e-02,  5.2822e-04],
   [2.3257e+00,  4.5673e+02,  7.9853e+01,  1.0417e+00,  1.1622e-30,  6.6328e-06, 2.7026e-02,  3.0533e-04,  2.4321e-02],
    ]) + A_prior

    B_prior = np.full((9, 3), 1/3)
    B_posterior = np.asarray([
        [15.22758,  502.26116,   22.51126],
        [9.66384,  18.57201, 511.76416],
        [0.40908, 144.01186, 395.57906],
        [495.32710,  14.24058,  30.43232],
        [372.16599, 167.00096,   0.83306],
        [90.18273, 281.28194, 168.53533],
        [53.62883, 284.22805, 202.14312],
        [159.78561, 149.47575, 230.73864],
        [452.63750,  28.89306,  58.46944],
    ]) + B_prior

    trainer = CategoricalVariationalHMM.new_variational_categorical(
        pi_posterior,
        pi_prior,
        A_posterior,
        A_prior,
        B_posterior,
        B_prior,
        impl="scaling",
        log_level=logging.INFO,
    )
    trainer.train(observations, lengths, 1000, 1e-10)
    print(np.asarray(trainer._A_posterior) > 1)
    print(np.asarray(trainer._A_posterior).tolist())
    print(np.asarray(trainer.viterbi(observations, lengths)))


if __name__ == "__main__":
    #test_matlab()
    #test_simple4()
    # test_convergence()
    #test_bic()
    #test_reestimate()
    test_save_load()
    test_simple_bad()
    # test_simple_bad()
    #test_simple2()
    #test_model_distance()
    #test_reestimate()
