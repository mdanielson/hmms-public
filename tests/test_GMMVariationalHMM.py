# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import json
import logging

import numpy as np
import pytest
import sklearn.metrics
import sklearn.mixture
import sklearn.utils
from hmm.datasets import for_tests
from hmm import GaussianHMM
from hmm import MultivariateGaussianVariationalHMM, GMMVariationalHMM, GMMHMM


def test_normals_simple2():

    hidden_states = np.asarray([0]*100 + [1] * 100)
    random_state = sklearn.utils.check_random_state(1)
    observed = np.concatenate([random_state.normal(5, np.sqrt(5), size=100), random_state.normal(10, 1, size=100)]).ravel()[:, None]
    new = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM.reestimate_from_sequences(observed, hidden_states, [observed.shape[0]])
    new.verbose = logging.INFO
    print(new.pi_normalized_)
    print(new.A_normalized_)
    print(new.A_posterior_)
    print(new.means_posterior_)
    print(new.covariances_posterior_)
    assert new.pi_normalized_[0] == 1
    assert new.pi_normalized_[1] == 0
    assert new.A_normalized_[0][0] == pytest.approx(0.99, 1e-6)
    assert new.A_normalized_[1][1] == pytest.approx(1, 1e-6)
    assert new.means_posterior_[0] == pytest.approx(5.13546738, 1e-6)
    assert new.means_posterior_[1] == pytest.approx(10.15279478, 1e-6)
    assert new.covariances_posterior_[0][0] == pytest.approx(3.9570784, 1e-6)
    assert new.covariances_posterior_[1][0] == pytest.approx(0.87738822, 1e-6)

    trainer = new._new_trainer()
    assert trainer.emissions.normalized_density_for(0, np.asarray([5.])) == pytest.approx(0.20008553505858093, 1e-6)
    assert trainer.emissions.normalized_log_density_for(0, np.asarray([5.])) == pytest.approx(np.log(0.20008553505858093), 1e-6)

    sampled_observed, sampled_hidden, lengths = new.sample(1, 100, random_state=random_state)
    new = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM.reestimate_from_sequences(sampled_observed, sampled_hidden, lengths)
    print(new.means_posterior_)
    assert new.means_posterior_[0] == pytest.approx(5.28219431, 1e-2)
    assert new.means_posterior_[1] == pytest.approx(10.09414668, 1e-4)
    print(new.covariances_posterior_)
    fitting = GMMVariationalHMM.GMMVariationalHMM(n_components=4, mixture_n_components=1, n_inits=4, n_jobs=1, n_iterations=400, random_state=random_state)
    fitting.fit(sampled_observed, lengths)
    print(fitting.mixture_means_posterior_.tolist())
    print(fitting.mixture_scale_posterior_.tolist())
    print(fitting.mixture_degrees_of_freedom_posterior_.tolist())
    print(fitting.mixture_covariances_posterior_.tolist())
    our_pi_sort = np.argsort(fitting.pi_posterior_)
    their_pi_sort = [3, 1, 2, 0]
    means = np.asarray([[[7.718304301744651]], [[10.056628115458626]], [[5.296147437096329]], [[7.976887633673675]]])
    print(fitting.mixture_means_posterior_[our_pi_sort])
    print(means[their_pi_sort])
    np.testing.assert_array_almost_equal(fitting.mixture_means_posterior_[our_pi_sort], means[their_pi_sort])
    cov = np.asarray([[[[0.0676826630123675]]], [[[0.9119002137888055]]], [[[4.146622808551813]]], [[[0.0010000000000046619]]]])
    np.testing.assert_array_almost_equal( fitting.mixture_covariances_posterior_[our_pi_sort], cov[their_pi_sort])

    # Test that we can serialize and use a model
    new_model = GMMVariationalHMM.GMMVariationalHMM.from_dict(fitting.to_dict())
    print(new_model.transform(sampled_observed, lengths))
    print(new_model.score_samples(sampled_observed, lengths))


def test_normals_simple():
    hidden_states = np.asarray([0]*200 + [1] * 200)
    random_state = sklearn.utils.check_random_state(1)
    observed_1 = np.concatenate([random_state.normal(5, np.sqrt(4), size=150), random_state.normal(10, 1, size=50)])
    random_state.shuffle(observed_1)
    observed_2 = np.concatenate([random_state.normal(0, np.sqrt(4), size=125), random_state.normal(-5, 1, size=75)])
    random_state.shuffle(observed_2)
    observed = np.concatenate([observed_1, observed_2])
    observed_2d = observed[:, None]
    lengths = np.asarray([400])

    new = GMMVariationalHMM.GMMVariationalHMM.reestimate_from_sequences(observed_2d, hidden_states, lengths, 2, "full", random_state=random_state)
    print("pi")
    print(new.pi_normalized_)
    print("A")
    print(new.A_normalized_)
    print("weights")
    print(new.mixture_weights_posterior_)
    print("means")
    print(new.mixture_means_posterior_)
    print("covariances")
    print(new.mixture_covariances_posterior_)
    assert new.pi_normalized_[0] == 1
    assert new.pi_normalized_[1] == 0
    assert new.A_normalized_[0, 0] == pytest.approx(0.995, 1e-6)
    assert new.A_normalized_[1, 1] == pytest.approx(1, 1e-6)
    assert new.mixture_weights_posterior_[0][0] == pytest.approx(0.74437001, 1e-6)
    assert new.mixture_weights_posterior_[0][1] == pytest.approx(0.25562999, 1e-6)
    assert new.mixture_weights_posterior_[1][0] == pytest.approx(0.45829608, 1e-6)
    assert new.mixture_weights_posterior_[1][1] == pytest.approx(0.54170392, 1e-6)
    assert new.mixture_means_posterior_[0][0] == pytest.approx(5.11029445, 1e-5)
    assert new.mixture_means_posterior_[0][1] == pytest.approx(10.1873612, 1e-5)
    assert new.mixture_means_posterior_[1][0] == pytest.approx(-4.6524787, 1e-5)
    assert new.mixture_means_posterior_[1][1] == pytest.approx(0.48687233, 1e-5)
    assert new.mixture_covariances_posterior_[0][0][0] == pytest.approx(3.04074412, 1e-6)
    assert new.mixture_covariances_posterior_[0][1][0] == pytest.approx(0.96470431, 1e-6)
    assert new.mixture_covariances_posterior_[1][0][0] == pytest.approx(1.69378313, 1e-6)
    assert new.mixture_covariances_posterior_[1][1][0] == pytest.approx(2.77452446, 1e-6)

    new_observations, new_hidden_states, new_lengths = new.sample(10, 250)
    vi_model = GMMVariationalHMM.GMMVariationalHMM(n_components=2, mixture_n_components=2, verbose=logging.INFO, n_iterations=500, random_state=2342341)
    vi_model.fit(new_observations, new_lengths)
    print("pi")
    print(vi_model.pi_posterior_.tolist())
    print("A")
    print(vi_model.A_normalized_.tolist())
    print("weights")
    print(vi_model.mixture_weights_normalized_.tolist())
    print("means")
    print(vi_model.mixture_means_posterior_.tolist())
    print("covariances")
    print(vi_model.mixture_covariances_posterior_.tolist())
    print(vi_model.transform(new_observations, new_lengths))
    A_normalized = [[0.9942233866311517, 0.00577661336884822], [0.00042122817243910374, 0.9995787718275609]]
    pi_posterior = [10.499971010838838, 0.5000289891611652]
    assert vi_model.pi_posterior_[0] == pytest.approx(pi_posterior[0], 1e-3)
    assert vi_model.pi_posterior_[1] == pytest.approx(pi_posterior[1], 1e-3)
    assert vi_model.A_normalized_[0, 0] == pytest.approx(A_normalized[0][0], 1e-2)
    assert vi_model.A_normalized_[1, 1] == pytest.approx(A_normalized[1][1], 1e-2)
    mixture_weights_normalized = [[0.7137429921141265, 0.28625700788587355], [0.4786856557597593, 0.5213143442402406]]
    np.testing.assert_almost_equal(sorted(vi_model.mixture_weights_normalized_[0]), sorted(mixture_weights_normalized[0]), decimal=3)
    np.testing.assert_almost_equal(sorted(vi_model.mixture_weights_normalized_[1]), sorted(mixture_weights_normalized[1]), decimal=3)
    means_posterior = [[[9.912917283842146], [4.9692891696516455]], [[-4.473339684145923], [0.6187549583486088]]]
    np.testing.assert_almost_equal(sorted(vi_model.mixture_means_posterior_[0]), sorted(means_posterior[0]), decimal=2)
    np.testing.assert_almost_equal(sorted(vi_model.mixture_means_posterior_[1]), sorted(means_posterior[1]), decimal=2)
    covariances_posterior = [[[[2.7306226684971384]], [[1.5204404140224743]]], [[[1.8967672587304722]], [[2.6923386379397645]]]]
    print(vi_model.mixture_covariances_posterior_)
    print(covariances_posterior)
    np.testing.assert_almost_equal(sorted(vi_model.mixture_covariances_posterior_.ravel()), sorted(np.asarray(covariances_posterior).ravel()), decimal=2)
    #assert vi_model.mixture_covariances_posterior_[0][1] pytest.approx(covariances_posterior[0][1])
    #assert vi_model.mixture_covariances_posterior_[1][0] pytest.approx(covariances_posterior[1][0])
    #assert vi_model.mixture_covariances_posterior_[1][1] pytest.approx(covariances_posterior[1][1])

    print(sklearn.metrics.classification_report(vi_model.transform(new_observations, new_lengths).ravel(), new_hidden_states.ravel()))
    score = sklearn.metrics.v_measure_score(vi_model.transform(new_observations, new_lengths).ravel(), new_hidden_states.ravel())
    print(score)
    assert score == pytest.approx(.99, abs=1e-2)


@pytest.mark.skip(reason="no way of currently testing this")
def test_normals_three_state_three_components_full_simple():
    for impl in ["scaling", "log"]:
        hidden_states = []
        observations = []
        state = 0
        random_state = sklearn.utils.check_random_state(1)
        for i in range(5000):

            if i > 0 and i % 5 == 0:
                state += 1
                state = state % 3
            hidden_states.append(state)
            rand = random_state.rand()
            if state == 0:
                if rand < .4:
                    observations.append(
                        random_state.multivariate_normal([1, 2], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([1, 5], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([1, 8], [[2, 1], [1, 2]])
                    )
            elif state == 1:
                if rand < .4:
                    observations.append(
                        random_state.multivariate_normal([2, 1], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([5, 1], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([8, 1], [[2, 1], [1, 2]])
                    )

            elif state == 2:
                if rand < .2:
                    observations.append(
                        random_state.multivariate_normal([-5, 5], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([-5, 2], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([-5, -5], [[2, 1], [1, 2]])
                    )
        observations = np.asarray(observations)
        hidden_states = np.asarray(hidden_states)
        lengths = [len(hidden_states)]

        em_gmm = GMMVariationalHMM.GMMVariationalHMM(n_components=3, mixture_n_components=3, verbose=logging.DEBUG, n_iterations=100, random_state=random_state)
        em_gmm.fit(observations, lengths)
        translated_em_gmm = GMMVariationalHMM.GMMVariationalHMM.from_dict(json.loads(json.dumps(em_gmm.to_dict())))
        print("pi")
        print(translated_em_gmm.pi_posterior_)
        print("A")
        print(translated_em_gmm.A_posterior_)
        print("weights")
        print(translated_em_gmm.mixture_weights_posterior_)
        print("means")
        print(translated_em_gmm.mixture_means_posterior_)
        print("covariances")
        print(translated_em_gmm.mixture_covariances_posterior_)
        transformed = translated_em_gmm.transform(observations, lengths)
        print(hidden_states)
        print(transformed)
        print(sklearn.metrics.classification_report(hidden_states, np.asarray(transformed)))

        score = sklearn.metrics.v_measure_score(translated_em_gmm.transform(observations, lengths).ravel(), hidden_states.ravel())
        print(score)
        assert score == pytest.approx(0.91185905080492)


@pytest.mark.skip(reason="no way of currently testing this")
def test_normals_three_state_three_components_tied():
    for impl in ["scaling", "log"]:
        hidden_states = []
        observations = []
        state = 0
        random_state = sklearn.utils.check_random_state(1)
        for i in range(5000):

            if i > 0 and i % 5 == 0:
                state += 1
                state = state % 3
            hidden_states.append(state)
            rand = random_state.rand()
            if state == 0:
                if rand < .4:
                    observations.append(
                        random_state.multivariate_normal([1, 2], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([1, 5], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([1, 8], [[2, 1], [1, 2]])
                    )
            elif state == 1:
                if rand < .4:
                    observations.append(
                        random_state.multivariate_normal([2, 1], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([5, 1], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([8, 1], [[2, 1], [1, 2]])
                    )

            elif state == 2:
                if rand < .2:
                    observations.append(
                        random_state.multivariate_normal([-5, 5], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([-5, 2], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([-5, -5], [[2, 1], [1, 2]])
                    )
        observations = np.asarray(observations)
        hidden_states = np.asarray(hidden_states)
        lengths = [len(hidden_states)]

        em_gmm = GMMVariationalHMM.GMMVariationalHMM(n_components=3, mixture_n_components=3, mixture_covariance_type="tied", verbose=logging.DEBUG, n_iterations=100, random_state=random_state)
        em_gmm.fit(observations, lengths)
        translated_em_gmm = GMMVariationalHMM.GMMVariationalHMM.from_dict(json.loads(json.dumps(em_gmm.to_dict())))
        print("pi")
        print(translated_em_gmm.pi_posterior_)
        print("A")
        print(translated_em_gmm.A_posterior_)
        print("weights")
        print(translated_em_gmm.mixture_weights_posterior_)
        print("means")
        print(translated_em_gmm.mixture_means_posterior_)
        print("covariances")
        print(translated_em_gmm.mixture_covariances_posterior_)
        transformed = translated_em_gmm.transform(observations, lengths)
        print(hidden_states)
        print(transformed)
        print(sklearn.metrics.classification_report(hidden_states, np.asarray(transformed)))

        score = sklearn.metrics.v_measure_score(translated_em_gmm.transform(observations, lengths).ravel(), hidden_states.ravel())
        print(score)
        assert score == pytest.approx(0.9144643706193004)


@pytest.mark.skip(reason="no way of currently testing this")
def test_normals_three_state_three_components_tied_hard():
    random_state = sklearn.utils.check_random_state(43)
    pi = np.asarray([.25] * 4)

    A = np.asarray([
        [.2, .2, .3, .3],
        [.3, .2, .2, .3],
        [.2, .3, .3, .2],
        [.3, .3, .2, .2]
    ])
    model = GMMHMM.GMMHMM(mixture_n_components=3, init_pi=None, init_A=None, init_emissions=None, mixture_covariance_type="tied")
    model.pi_ = pi
    model.A_ = A
    model.mixture_weights_ = np.asarray(
        [
            [.5, .25, .5],
            [.25, .5, .25],
            [.25, .25, .5],
            [.25, .5, .5],
        ]
    )
    model.mixture_means_ = np.asarray(
        [
            # State 1
            [
                # Component 1
                [0., -1.5],
                # Component 2
                [0., -1.5],
                # Component 3
                [0., -1.5],
            ],
            # State 2
            [
                # Component 1
                [0., 0],
                # Component 2
                [0., 0],
                # Component 3
                [0., 0],
            ],
            # State 3
            [
                # Component 1
                [0., 1.5],
                # Component 2
                [0., 1.5],
                # Component 3
                [0., 1.5],
            ],
            # State 4
            [
                # Component 1
                [0., 3.0],
                # Component 2
                [0., 3.0],
                # Component 3
                [0., 3.0],
            ]
        ])

    model.mixture_covariances_ = np.asarray(
        [
            [
                # State 1
                [.25**2, .125**2],
                [.125**2, .25**2],
            ],
            [
                # State 2
                [.25**2, .125**2],
                [.125**2, .25**2],
            ],
            [
                # State 3
                [.25**2, .125**2],
                [.125**2, .25**2],
            ],
            [
                # State 3
                [.25**2, .125**2],
                [.125**2, .25**2],
            ]
        ]
    )

    #model = for_tests.get_gaussian_mcgrory_and_titterington_model(random_state)
    observations, hidden_states, lengths = model.sample(20, 1000, random_state=random_state)
    scores = {
        "tied": 0.9924363124328849,
        "full": 0.9916161944870437,
    }
    for impl in ["scaling", "log"]:
        for covariance_type in ["tied", "full"]:
            print(impl, covariance_type)
            em_gmm = GMMVariationalHMM.GMMVariationalHMM(
                A_posterior="uniform",
                n_components=4,
                mixture_n_components=3,
                mixture_covariance_type=covariance_type,
                verbose=logging.INFO,
                n_inits=1,
                n_iterations=1000,
                random_state=43
            )
            em_gmm.fit(observations, lengths)
            translated_em_gmm = GMMVariationalHMM.GMMVariationalHMM.from_dict(json.loads(json.dumps(em_gmm.to_dict())))
            print("pi")
            print(translated_em_gmm.pi_posterior_)
            print("A")
            print(translated_em_gmm.A_posterior_)
            print(translated_em_gmm.A_normalized_)
            print("weights")
            print(translated_em_gmm.mixture_weights_posterior_)
            print("means")
            print(translated_em_gmm.mixture_means_posterior_)
            print("covariances")
            print(translated_em_gmm.mixture_covariances_posterior_)
            transformed = translated_em_gmm.transform(observations, lengths)
            print(hidden_states)
            print(transformed)
            print(sklearn.metrics.classification_report(hidden_states, np.asarray(transformed)))

            score = sklearn.metrics.v_measure_score(translated_em_gmm.transform(observations, lengths).ravel(), hidden_states.ravel())
            print(score)
            assert score == pytest.approx(scores[covariance_type]), covariance_type


@pytest.mark.skip(reason="no way of currently testing this")
def test_mcgrory():
    random_state = sklearn.utils.check_random_state(43)

    model = for_tests.get_gaussian_mcgrory_and_titterington_model(random_state)
    observations, hidden_states, lengths = model.sample(20, 1000, random_state=random_state)
    scores = {
        "tied": 0.9885992137447068,
        "full": 0.9890475842989009,
    }
    for impl in ["scaling", "log"]:
        for covariance_type in ["tied", "full"]:
            print(impl, covariance_type)
            em_gmm = GMMVariationalHMM.GMMVariationalHMM(
                A_posterior="uniform",
                n_components=4,
                mixture_n_components=3,
                mixture_covariance_type=covariance_type,
                verbose=logging.INFO,
                n_inits=1,
                n_iterations=100,
                random_state=43
            )
            em_gmm.fit(observations, lengths)
            translated_em_gmm = GMMVariationalHMM.GMMVariationalHMM.from_dict(json.loads(json.dumps(em_gmm.to_dict())))
            print("pi")
            print(translated_em_gmm.pi_posterior_)
            print("A")
            print(translated_em_gmm.A_posterior_)
            print(translated_em_gmm.A_normalized_)
            print("weights")
            print(translated_em_gmm.mixture_weights_posterior_)
            print("means")
            print(translated_em_gmm.mixture_means_posterior_)
            print("covariances")
            print(translated_em_gmm.mixture_covariances_posterior_)
            print(translated_em_gmm)
            transformed = translated_em_gmm.transform(observations, lengths)
            print(hidden_states)
            print(transformed)
            print(transformed.sum())
            score = sklearn.metrics.v_measure_score(translated_em_gmm.transform(observations, lengths).ravel(), hidden_states.ravel())
            print(score)
            assert score == pytest.approx(scores[covariance_type]), (covariance_type, score)


#def test_normals_three_state_three_components_full_hard():
#    for impl in ["log", "scaling"]:
#        hidden_states = []
#        observations = []
#        state = 0
#        random_state = sklearn.utils.check_random_state(1)
#        for i in range(500):
#
#            if i > 0 and i % 5 == 0:
#                state += 1
#                state = state % 3
#            hidden_states.append(state)
#            rand = random_state.rand()
#            if state == 0:
#                if rand < .4:
#                    observations.append(
#                        random_state.multivariate_normal([0, 4], [[2, 1], [1, 2]])
#                    )
#                elif rand < .8:
#                    observations.append(
#                        random_state.multivariate_normal([2, 2], [[2, 1], [1, 2]])
#                    )
#                else:
#                    observations.append(
#                        random_state.multivariate_normal([4, 0], [[2, 1], [1, 2]])
#                    )
#            elif state == 1:
#                if rand < .4:
#                    observations.append(
#                        random_state.multivariate_normal([0, -5], [[2, 1], [1, 2]])
#                    )
#                elif rand < .8:
#                    observations.append(
#                        random_state.multivariate_normal([4, 4], [[2, 1], [1, 2]])
#                    )
#                else:
#                    observations.append(
#                        random_state.multivariate_normal([-5, 0], [[2, 1], [1, 2]])
#                    )
#
#            elif state == 2:
#                if rand < .2:
#                    observations.append(
#                        random_state.multivariate_normal([5, 5], [[2, 1], [1, 2]])
#                    )
#                elif rand < .8:
#                    observations.append(
#                        random_state.multivariate_normal([2, -2], [[2, 1], [1, 2]])
#                    )
#                else:
#                    observations.append(
#                        random_state.multivariate_normal([-5, 5], [[2, 1], [1, 2]])
#                    )
#        observations = np.vstack(observations)
#        hidden_states = np.asarray(hidden_states)
#        lengths = np.asarray([len(hidden_states)])
#        new = GMMHMM.GMMHMM.reestimate_from_sequences(observations, hidden_states, lengths, 3, "full", random_state=random_state)
#        print(new.mixture_weights_)
#        print(new.mixture_means_)
#        print(new.mixture_covariances_)
#
#        em_gmm = GMMHMM.GMMHMM(n_components=3, mixture_n_components=3, random_state=random_state, n_iterations=5000, n_inits=1, n_jobs=1, verbose=logging.INFO, implementation=impl)
#        em_gmm.fit(observations, lengths)
#
#        translated_em_gmm = GMMHMM.GMMHMM.from_dict(json.loads(json.dumps(em_gmm.to_dict())))
#        print(translated_em_gmm.pi_.tolist())
#        print(translated_em_gmm.A_.tolist())
#        print(translated_em_gmm.mixture_weights_.tolist())
#        print(translated_em_gmm.mixture_means_.tolist())
#        print(translated_em_gmm.mixture_covariances_.tolist())
#        print(translated_em_gmm.transform(observations, lengths))
#        assert translated_em_gmm.pi_[2] == 1
#
#        A = [[0.19588416651103038, 0.34408962650341507, 0.4600262069855546], [0.25822260105878536, 0.377336594298997, 0.36444080464221773], [0.31258527184985074, 0.31184857935580645, 0.3755661487943428]]
#        for i, row in enumerate(A):
#            for j, col in enumerate(row):
#                assert translated_em_gmm.A_[i, j] == pytest.approx(col, 1e-6), (impl, i, j)
#        weights = [[0.5605180322969062, 0.2327546292000307, 0.20672733850306305], [0.06721182586487201, 0.6371583408132819, 0.2956298333218461], [0.21190745852624063, 0.5328294455116015, 0.2552630959621579]]
#        for i, row in enumerate(weights):
#            for j, col in enumerate(row):
#                assert translated_em_gmm.mixture_weights_[i, j] == pytest.approx(col, 1e-6), (impl, i, j)
#
#

@pytest.mark.skip(reason="no way of currently testing this")
def get_data():
    random_state = sklearn.utils.check_random_state(2)
    pi = [.5, .5]
    A = [
        [.3, .7],
        [.7, .3],
    ]
    means = [-10, 5]
    variances = [1, 1]

    model = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None)
    model.pi_ = pi
    model.A_ = A
    model.means_ = means
    model.variances_ = variances
    observed_sequences, hidden_sequences, lengths = model.sample(10, 500, random_state=random_state)

    return observed_sequences, hidden_sequences, lengths


@pytest.mark.skip(reason="no way of currently testing this")
def test_matt2():
    for impl in ["log", ]:#"scaling"]:
        random_state = sklearn.utils.check_random_state(4)
        observed_sequences, hidden_sequences, lengths = get_data()
        observed_sequences = np.asarray(observed_sequences)
        new = GMMVariationalHMM.GMMVariationalHMM.reestimate_from_sequences(observed_sequences, hidden_sequences, lengths, 1, "full")
        print(new.pi_posterior_)
        print(new.A_posterior_)
        print(new.mixture_weights_posterior_)
        print(new.mixture_means_posterior_)
        print(new.mixture_covariances_posterior_)

        trainer = GMMVariationalHMM.GMMVariationalHMM(n_components=3, mixture_n_components=2, n_iterations=200, n_inits=1, verbose=logging.DEBUG, random_state=23423423, implementation=impl)
        trainer.fit(observed_sequences, lengths)
        print(trainer.pi_normalized_)
        print(trainer.A_normalized_)
        print(new.mixture_weights_posterior_)
        print(new.mixture_means_posterior_)
        print(new.mixture_covariances_posterior_)

if __name__ == "__main__":
    test_normals_simple2()
    test_normals_simple()
    #test_matt2()
    #test_normals_three_state_three_components_full_simple()
    #test_normals_three_state_three_components_tied_hard()
    #test_mcgrory()
    #test_normals_three_state_three_components_full_hard()
