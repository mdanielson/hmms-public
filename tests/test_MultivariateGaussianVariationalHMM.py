# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import numpy as np
import pytest
import sklearn.utils

from hmm import MultivariateGaussianHMM, MultivariateGaussianVariationalHMM, GaussianHMM

def test_normals_simple():

    hidden_states = np.asarray([0]*100 + [1] * 100)
    random_state = sklearn.utils.check_random_state(1)
    observed = np.concatenate([random_state.normal(5, np.sqrt(5), size=100), random_state.normal(10, 1, size=100)]).ravel()[:, None]
    new = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM.reestimate_from_sequences(observed, hidden_states, [observed.shape[0]])
    new.verbose = logging.DEBUG
    print(new.pi_normalized_)
    print(new.A_normalized_)
    print(new.A_posterior_)
    print(new.means_posterior_)
    print(new.covariances_posterior_)
    assert new.pi_normalized_[0] == 1
    assert new.pi_normalized_[1] == 0
    assert new.A_normalized_[0, 0] == pytest.approx(0.99, 1e-6)
    assert new.A_normalized_[1, 1] == pytest.approx(1, 1e-6)
    assert new.means_posterior_[0] == pytest.approx(5.13546738, 1e-6)
    assert new.means_posterior_[1] == pytest.approx(10.15279478, 1e-6)
    assert new.covariances_posterior_[0][0] == pytest.approx(3.9570784, 1e-6)
    assert new.covariances_posterior_[1][0] == pytest.approx(0.87738822, 1e-6)

    trainer = new._new_trainer()
    assert trainer.emissions.normalized_density_for(0, np.asarray([5.])) == pytest.approx(0.20008553505858093, 1e-6)
    assert trainer.emissions.normalized_log_density_for(0, np.asarray([5.])) == pytest.approx(np.log(0.20008553505858093), 1e-6)

    sampled_observed, sampled_hidden, lengths = new.sample(1, 100, random_state=random_state)
    new = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM.reestimate_from_sequences(sampled_observed, sampled_hidden, lengths)
    print(new.means_posterior_)
    assert new.means_posterior_[0] == pytest.approx(5.28219431, 1e-2)
    assert new.means_posterior_[1] == pytest.approx(10.09414668, 1e-4)
    print(new.covariances_posterior_)
    fitting = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=4, verbose=True, n_iterations=400, random_state=random_state)
    fitting.fit(sampled_observed, lengths)
    print(fitting.means_posterior_)
    print(fitting.scale_posterior_)
    print(fitting.degrees_of_freedom_posterior_)
    print(fitting.covariances_posterior_)
    means = [[10.05696477],
             [ 6.4539219 ],
             [ 3.87210507],
             [ 6.86090226]]
    np.testing.assert_array_almost_equal(fitting.means_posterior_, means, decimal=3)
    cov = [[[0.91220414]],
            [[0.72964617]],
            [[3.36260594]],
            [[2.23814309]]]
    np.testing.assert_array_almost_equal( fitting.covariances_posterior_, cov, decimal=3)

    # test that we can serialize and use a model
    new_model = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM.from_dict(fitting.to_dict())
    print(new_model.transform(sampled_observed, lengths))

def get_data():
    random_state = sklearn.utils.check_random_state(2)
    pi = [.5, .5]
    A = [
        [.3, .7],
        [.7, .3],
    ]
    means = [-10, 5]
    variances = [1, 1]

    model = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None)
    model.pi_ = pi
    model.A_ = A
    model.means_ = means
    model.variances_ = variances
    observed_sequences, hidden_sequences, lengths = model.sample(10, 500, random_state=random_state)

    return observed_sequences, hidden_sequences, lengths


def test_matt2():
    for impl in ["log", ]:#"scaling"]:
        random_state = sklearn.utils.check_random_state(4)
        observed_sequences, hidden_sequences, lengths = get_data()
        observed_sequences = np.asarray(observed_sequences)
        new = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM.reestimate_from_sequences(observed_sequences, hidden_sequences, lengths)
        print(new.pi_posterior_)
        print(new.A_posterior_)
        print(new.means_posterior_)
        print(new.covariances_posterior_)


        trainer = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=3,  n_iterations=60, n_inits=1, verbose=logging.DEBUG, random_state=23423423, implementation=impl)
        trainer.fit(observed_sequences, lengths)
        print(trainer.pi_normalized_)
        print(trainer.A_normalized_)
        print(trainer.means_posterior_)
        print(trainer.covariances_posterior_)


def test_normals_not_simple():
    hidden_states = np.asarray(
        [0]*100 + [1] * 100 + [1]*100 + [0] * 100,
    )

    random_state = sklearn.utils.check_random_state(1)
    observed = np.concatenate([
        np.concatenate([
            random_state.multivariate_normal([0, 4], [[2, 1], [1, 2]], size=100),
            random_state.multivariate_normal([2, 2], [[2, 2], [2, 4]], size=100),
        ]
        ),
        np.concatenate([
            random_state.multivariate_normal([2, 2], [[3, 1], [1, 3]], size=100),
            random_state.multivariate_normal([0, 4], [[2, 1], [1, 2]], size=100),
        ]
        )
    ]
    )
    lengths = np.asarray([200, 200])
    trainer = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=3,  n_iterations=400, n_inits=1, random_state=random_state)
    trainer.fit(observed, lengths)
    print(trainer.pi_normalized_.tolist())
    print(trainer.A_normalized_.tolist())
    means_posterior = np.asarray([[-0.603391013529806, 3.4364713167096697], [1.9476920770463129, 2.148788957619758], [-0.037245037955796326, 3.845419340491883]])
    covariances_posterior = np.asarray([[[0.9629532663341994, -0.2657739467394473], [-0.2657739467394473, 0.07363239813328168]], [[2.4530125013671427, 1.4690159651830306], [1.4690159651830295, 3.9598998355509982]], [[1.5821454371984804, 0.9145539288497198], [0.9145539288497198, 2.044941650133445]]])

    means_sort = np.argsort(means_posterior, axis=0)[:, 0]
    new_means_sort = np.argsort(trainer.means_posterior_, axis=0)[:, 0]

    for i, j in zip(new_means_sort, means_sort):
        np.testing.assert_almost_equal(trainer.means_posterior_[j], means_posterior[i], decimal=4)
        np.testing.assert_almost_equal(trainer.covariances_posterior_[j], covariances_posterior[i], decimal=4)


def test_normals_two_state_full_covariance():
    hidden_states = np.asarray([0]*100 + [1] * 100 +  [1]*100 + [0] * 100)

    for impl in ["scaling", "log"]:
        print(impl)
        random_state = sklearn.utils.check_random_state(1)
        observed = np.concatenate(
            [
                np.concatenate([
                    random_state.multivariate_normal([0, 4], [[1, 2], [2, 1]], size=100),
                    random_state.multivariate_normal([2, 2], [[2, 1], [1, 2]], size=100),
                ]),
                np.concatenate([
                    random_state.multivariate_normal([2, 2], [[2, 2], [2, 2]], size=100),
                    random_state.multivariate_normal([0, 4], [[1, 2], [2, 1]], size=100),
                ])
            ]
        )
        print(observed.shape)
        lengths = np.asarray([200, 200])
        print("Fitting")
        trainer = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=2,  n_iterations=50, covariance_type="full", implementation=impl,  random_state=random_state, tol=1e-6, verbose=logging.INFO)
        trainer.fit(observed, lengths)

        print("pi")
        print(trainer.pi_posterior_.tolist())
        print("A")
        print(trainer.A_posterior_.tolist())
        print("means")
        print(trainer.means_posterior_.tolist())
        print("covars")
        print(trainer.covariances_posterior_.tolist())
        print("dof")
        print(trainer.degrees_of_freedom_posterior_.tolist())

        check_pi = np.asarray([1.5000606152168512, 1.499939384783147])
        check_A = np.asarray([[200.05520814149295, 1.511939255624278], [1.809984134174958, 196.62286846870765]])
        check_means = np.asarray([[-0.1178985101012355, 3.9290139659535437], [2.0102873278766475, 2.0477463190613774]])
        check_covariances = np.asarray([[[2.0562642730812777, 0.9101303908474708], [0.9101303908474708, 1.5923676991909976]], [[1.9481482608995289, 1.5372789408666632], [1.5372789408666632, 2.1179559703805007]]])
        check(
            trainer.pi_posterior_, check_pi,
            trainer.A_posterior_, check_A,
            trainer.means_posterior_, check_means,
            trainer.covariances_posterior_, check_covariances
        )


def test_normals_two_state_diagonal_coverage():
    hidden_states = np.asarray([0]*500 + [1] * 500 +  [1]*500 + [0] * 500)

    for impl in ["scaling", "log"]:
        print(impl)
        random_state = sklearn.utils.check_random_state(1)
        observed = np.concatenate(
            [
                np.concatenate([
                    random_state.multivariate_normal([0, 4], [[2, 0], [0, 2]], size=500),
                    random_state.multivariate_normal([2, 2], [[1, 0], [0, 3]], size=500),
                ]),
                np.concatenate([
                    random_state.multivariate_normal([2, 2], [[1, 0], [0, 3]], size=500),
                    random_state.multivariate_normal([0, 4], [[2, 0], [0, 2]], size=500),
                ])
            ]
        )
        print(observed.shape)
        lengths = np.asarray([1000, 1000])

        print("Fitting")
        trainer = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=2,  n_iterations=50, covariance_type="diagonal", implementation=impl,  random_state=random_state, tol=1e-6, verbose=logging.INFO)
        trainer.fit(observed, lengths)

        print("pi")
        print(trainer.pi_posterior_.tolist())
        print("A")
        print(trainer.A_posterior_.tolist())
        print("means")
        print(trainer.means_posterior_.tolist())
        print("covars")
        print(trainer.covariances_posterior_.tolist())
        print("dof")
        print(trainer.degrees_of_freedom_posterior_.tolist())

        check_pi = np.asarray([1.343367686923464, 1.6566323130765301])
        check_A = np.asarray([[998.5553908914384, 1.5054018583916609], [1.66209153858546, 998.2771157115845]])
        check_means = np.asarray([[0.038182392174655536, 4.019695818133926], [2.003141601888896, 2.0057959877758]])
        check_covariances = np.asarray([[[1.9529758028792488, 0.0], [0.0, 2.0018901886752274]], [[0.966430797180533, 0.0], [0.0, 3.1045549333116904]]])

        check(
            trainer.pi_posterior_, check_pi,
            trainer.A_posterior_, check_A,
            trainer.means_posterior_, check_means,
            trainer.covariances_posterior_, check_covariances
        )


def test_normals_two_state_spherical_covariance():
    hidden_states = np.asarray([0]*500 + [1] * 500 +  [1]*500 + [0] * 500)

    for impl in ["scaling", "log"]:
        print(impl)
        random_state = sklearn.utils.check_random_state(1)
        observed = np.concatenate(
            [
                np.concatenate([
                    random_state.multivariate_normal([0, 4], [[2, 0], [0, 2]], size=500),
                    random_state.multivariate_normal([2, 2], [[1, 0], [0, 1]], size=500),
                ]),
                np.concatenate([
                    random_state.multivariate_normal([2, 2], [[1, 0], [0, 1]], size=500),
                    random_state.multivariate_normal([0, 4], [[2, 0], [0, 2]], size=500),
                ])
            ]
        )
        print(observed.shape)
        lengths = np.asarray([1000, 1000])
        print("Fitting")
        trainer = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=2,  n_iterations=50, covariance_type="spherical", implementation=impl,  random_state=random_state, tol=1e-6, verbose=logging.DEBUG)
        trainer.fit(observed, lengths)

        print("pi")
        print(trainer.pi_posterior_.tolist())
        print("A")
        print(trainer.A_posterior_.tolist())
        print("means")
        print(trainer.means_posterior_.tolist())
        print("covars")
        print(trainer.covariances_posterior_.tolist())
        print("dof")
        print(trainer.degrees_of_freedom_posterior_.tolist())

        check_pi = np.asarray([2.297781561595846, 0.7022184384041501])
        check_A = np.asarray([[1002.4320615054908, 2.303984931875537], [1.505757057041804, 993.7581965055915]])
        check_means = np.asarray([[1.9976941215711541, 2.006220647048804], [0.033661144846802424, 4.027831841369705]])
        check_covariances = np.asarray([[[1.0049882468525975, 0.0], [0.0, 1.0049882468525975]], [[1.9723642903927492, 0.0], [0.0, 1.9723642903927492]]])
        check(
            trainer.pi_posterior_, check_pi,
            trainer.A_posterior_, check_A,
            trainer.means_posterior_, check_means,
            trainer.covariances_posterior_, check_covariances
        )

def test_normals_two_state_tied_covariance():
    hidden_states = np.asarray([0]*100 + [1] * 100 +  [1]*100 + [0] * 100)

    for impl in ["scaling", "log"]:
        print(impl)
        random_state = sklearn.utils.check_random_state(1)
        observed = np.concatenate(
            [
                np.concatenate([
                    random_state.multivariate_normal([0, 4], [[2, 1], [1, 2]], size=100),
                    random_state.multivariate_normal([2, 2], [[2, 1], [1, 2]], size=100),
                ]),
                np.concatenate([
                    random_state.multivariate_normal([2, 2], [[2, 1], [1, 2]], size=100),
                    random_state.multivariate_normal([0, 4], [[2, 1], [1, 2]], size=100),
                ])
            ]
        )
        print(observed.shape)
        lengths = np.asarray([200, 200])

        print("Fitting")
        trainer = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=2,  n_iterations=50, covariance_type="tied", implementation=impl,  random_state=random_state, tol=1e-6, verbose=logging.INFO)
        trainer.fit(observed, lengths)

        print("pi")
        print(trainer.pi_posterior_.tolist())
        print("A")
        print(trainer.A_posterior_.tolist())
        print("means")
        print(trainer.means_posterior_.tolist())
        print("covars")
        print(trainer.covariances_posterior_.tolist())
        print("dof")
        print(trainer.degrees_of_freedom_posterior_.tolist())

        check_pi = np.asarray([1.4978261704209976, 1.5021738295789993])
        check_A = np.asarray([[200.93170445505393, 1.525540039832322], [1.5643256843179811, 195.9784298207954]])
        check_means = np.asarray([[-0.04330253797551568, 3.8476189256348974], [1.9565721783706687, 2.112506962416094]])
        check_covariances = np.asarray([[1.786621362773499, 0.952961195578158], [0.952961195578158, 2.1331082421759264]])

        check(
            trainer.pi_posterior_, check_pi,
            trainer.A_posterior_, check_A,
            trainer.means_posterior_, check_means,
            trainer.covariances_posterior_, check_covariances,
        )


def check(pi, check_pi, A, check_A, means, check_means, covariances, check_covariances, abs_tol=1e-3):
    sort = np.argsort(pi)
    check_sort = np.argsort(check_pi)

    print(check_covariances)
    print(covariances)
    for i,j in zip(check_sort, sort):
        assert check_pi[i] == pytest.approx(pi[j], abs=abs_tol)
        assert check_A[i][check_sort] == pytest.approx(A[j][sort], abs=abs_tol)
        assert check_means[i] == pytest.approx(means[j], abs=abs_tol)
        if check_covariances.ndim == 3:
            assert check_covariances[i].ravel() == pytest.approx(covariances[j].ravel(), abs=abs_tol)
        elif check_covariances.ndim == 2:
            assert check_covariances.ravel() == pytest.approx(covariances.ravel(), abs=abs_tol)
        else:
            assert False




if __name__ == "__main__":
    #test_matt2()
    #test_normals_simple()
    #test_normals_two_state_full_covariance()
    #test_normals_two_state_tied_covariance()
    #test_normals_three_state()
    test_normals_not_simple()
    #test_normals_two_state_diagonal_coverage()
    #test_normals_two_state_spherical_covariance()
