# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import pytest

from hmm.utilities.kl_divergences import (kl_dirichlet_distribution,
                                          kl_gamma_distribution,
                                          kl_normal_distribution,
                                          kl_multivariate_normal,
                                          kl_wishart)


def test_dirichlet():
    d1 = np.zeros(10, dtype=float) + 1./10
    d2 = np.zeros(10, dtype=float) + 1./10
    d1[0] = 10
    d1[4] = 10

    assert kl_dirichlet_distribution(d1, d1) == pytest.approx(0)
    assert kl_dirichlet_distribution(d2, d2) == pytest.approx(0)
    assert kl_dirichlet_distribution(d2, d1) == pytest.approx(174.32721704664056)
    assert kl_dirichlet_distribution(d1, d2) == pytest.approx(5.606667187456793)

    a = np.asarray([0.1000000000167262, 0.5000000000157052])
    b = np.asarray([0.5, 0.5])
    print(kl_dirichlet_distribution(a, b))


def test_gaussian():
    assert kl_normal_distribution(0, 1, 0, 1) == pytest.approx(0)
    assert kl_normal_distribution(0, 1, 1, 1) == pytest.approx(.5)
    assert kl_normal_distribution(0, 1, 0, 2) == pytest.approx(0.0965735902799727)
    assert kl_normal_distribution(0, 2, 0, 1) == pytest.approx(0.1534264097200273)
    assert kl_normal_distribution(0, 2, -10, 1) == pytest.approx(50.153426409720026)


def test_gamma():
    assert kl_gamma_distribution(1, 1, 1, 1) == pytest.approx(0)
    #assert kl_gamma_distribution(100, 189, 1, .001) == pytest.approx(0)

    group1 = [
        {"var": 0.1651810919049714,  "mean": 2.987649774668494, "beta": 113.53741234773265, "gamma": 113.53741234773265, "delta": 18.754233743663463},
        {"var": 0.053952313199543696, "mean": -0.006674714329332304, "beta": 152.1378725813001, "gamma": 152.1378725813001, "delta": 8.208190151018574},
        {"var": 0.09843208595012645, "mean": -1.5078428724582493, "beta": 76.06959669069971, "gamma": 76.06959669069971, "delta": 7.4876890796504085},
        {"var": 0.09173238038619655, "mean": 1.421062541012451, "beta": 101.88805283096379, "gamma": 101.88805283096379, "delta": 9.34643361909886},
        {"var": 2.1102125971309467,  "mean": 1.4653704014882378, "beta": 3.708326992155165, "gamma": 3.708326992155165, "delta": 7.825358333126543},
        {"var": 0.12865209842642464, "mean": -1.3986984885274536, "beta": 58.65873855714868, "gamma": 58.65873855714868, "delta": 7.546569806424202},
    ]
    group2 = [

        {"var": 0.1649073689430538,  "mean": 2.98795621839822, "beta": 113.73201780111309, "gamma": 113.73201780111309, "delta": 18.75524782016612},
        {"var": 0.054012311388198335, "mean": -0.006642347147958222, "beta": 152.21360220575096, "gamma": 152.21360220575096, "delta": 8.221408479856374},
        {"var": 0.09813766405381023, "mean": -1.5065587732906587, "beta": 76.8593159059747, "gamma": 76.8593159059747, "delta": 7.542793723786218},
        {"var": 0.09211997054253072, "mean": 1.420889586186913, "beta": 102.28333700798, "gamma": 102.28333700798, "delta": 9.42233799216686},
        {"var": 2.264275211749401,   "mean": 1.4341689181855326, "beta": 3.001075679460843, "gamma": 3.001075679460843, "delta": 6.795261269587178},
        {"var": 0.12993785106901526, "mean": -1.3985079797371298, "beta": 57.9106513997203, "gamma": 57.9106513997203, "delta": 7.524785596886517},
    ]
    for item1, item2 in zip(group1, group2):
        print(kl_normal_distribution(item1["mean"], item1["var"], 0, 1e3), kl_normal_distribution(item2["mean"], item2["var"], 1, 1e3))
    print("")
    for item1, item2 in zip(group1, group2):
        print(kl_gamma_distribution(item1["gamma"], item1["delta"], item1["gamma"], item1["delta"]), kl_gamma_distribution(item2["gamma"], item2["delta"], 1, 1e-3))

    print(kl_gamma_distribution(255.0, 0.6285134915970896, 0.001, 1.0))
    print(kl_gamma_distribution(113.96557270544477, 20.19138505309502, 1, 0.0010))
    print(kl_gamma_distribution(0.5, 0.0005, 0.5, 0.0005))
    print(kl_gamma_distribution(0.5000884152167334, 0.000500034222297098, 0.5, 0.0005))

    print(kl_gamma_distribution(10220.382480106093, 1022.038708817321, 10220.0, 1022.038708817321))
    print(kl_gamma_distribution(1022.038708817321, 10220.382480106093,  1022.038708817321, 10220.0,))
    print(kl_gamma_distribution(202, 114186.3, 195, 119237.3))
    assert kl_gamma_distribution(202, 114186.3, 195, 119237.3) == pytest.approx(0.6179974526982122, 1e-6)


def test_multivariate_normal():
    means_1 = np.asarray([0., 0])
    vars_1 = np.asarray([[1., 0], [0, 1]])
    means_2 = np.asarray([1., 1])
    vars_2 = np.asarray([[2., 0], [0, 2]])
    assert kl_multivariate_normal(means_1, vars_1, means_1, vars_1) == 0
    assert kl_multivariate_normal(means_1, vars_1, means_2, vars_1) == 1
    assert kl_multivariate_normal(means_1, vars_1, means_2, vars_2) == pytest.approx(0.6931471805599454)
    means_1 = np.asarray([0.])
    vars_1 = np.asarray([[1.]])
    means_2 = np.asarray([1.])
    vars_2 = np.asarray([[2.]])
    assert kl_multivariate_normal(means_1, vars_1, means_1, vars_1) == 0
    print(kl_multivariate_normal(means_1, vars_1, means_2, vars_1))
    print(kl_normal_distribution(means_1[0], vars_1[0][0], means_2[0], vars_1[0][0]))
    assert kl_multivariate_normal(means_1, vars_1, means_2, vars_1) == kl_normal_distribution(means_1[0], vars_1[0][0], means_2[0], vars_1[0][0])
    assert kl_multivariate_normal(means_1, vars_1, means_2, vars_2) == kl_normal_distribution(means_1[0], vars_1[0][0], means_2[0], vars_2[0][0])

def test_wishart():
    """
    Wishart is related to gamma:
        gamma(lambda|a, b) == Wishart(lambda, 2*a, (1/2)*b)
    """
    a1 = 9.0
    dof1 = a1*2
    a2 = 2.0
    dof2 = a2*2
    b1 = 2.0
    scale1 = np.asarray([[(2*b1)]])
    b2 = 2.0
    scale2 = np.asarray([[(2*b2)]])
    print(kl_gamma_distribution(a1, b1, a2, b2))
    print(kl_wishart(dof1, scale1, dof2, scale2))
    assert kl_wishart(dof1, scale1, dof2, scale2) == pytest.approx(kl_gamma_distribution(a1, b1, a2, b2), 1e-5)

    a1 = 2
    dof1 = a1*2
    a2 = 20
    dof2 = a2*2
    b1 = 100
    scale1 = np.asarray([[b1 * 2.]])
    b2 = 4
    scale2 = np.asarray([[b2 * 2.]])
    print(kl_wishart(dof1, scale1, dof2, scale2))
    print(kl_gamma_distribution(a1, b1, a2, b2), 1e-5)
    assert kl_wishart(dof1, scale1, dof2, scale2) == pytest.approx(kl_gamma_distribution(a1, b1, a2, b2), 1e-5)

    dof2 = 3
    dof1 = 3
    scale1 = np.asarray([[1, 0], [0, 1]], dtype=float)
    scale2 = np.asarray([[4, 1], [1, 2]], dtype=float)
    print(kl_wishart(dof1, scale1, dof2, scale2))
    assert kl_wishart(dof1, scale1, dof1, scale1) == 0
    assert kl_wishart(dof1, scale1, dof2, scale2) == pytest.approx(3.0811347764170303, 1e-5)

    dof1 = 952
    scale1 = np.asarray([[339.8474024737109]])
    dof2 = 1.0
    scale2 = np.asarray([[0.001]])
    print(kl_wishart(dof1, scale1, dof2, scale2))
    print(kl_gamma_distribution(dof1/2, scale1[0,0]/2, dof2/2, scale2[0,0]/2))

if __name__ == "__main__":
    #test_multivariate_normal()
    test_wishart()
