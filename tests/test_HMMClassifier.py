# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import copy
import datetime
import json

import numpy as np
import pytest
import sklearn.metrics
import sklearn.model_selection
import sklearn.utils

from hmm import CategoricalHMM, HMMClassifier, PoissonHMM, util

from hmm.utilities import json_encoder


def test_categorical_classification():
    pi_1 = [.7, .3]
    A_1 = [[0.7, 0.3], [0.4, 0.6]]
    B_1 = [
        [0.5, 0.4, 0.1],
        [0.1, 0.3, 0.6]
    ]
    pi_2 = [.4, .6]
    A_2 = [[0.5, 0.5], [0.6, 0.4]]
    B_2 = [
        [0.5, 0.4, 0.1],
        [0.1, 0.3, 0.6]
    ]

    for impl in ["scaling", "log"]:
        print(datetime.datetime.now(), impl)
        random_state = sklearn.utils.check_random_state(1)
        hmm1 = CategoricalHMM.CategoricalHMM(
            init_pi=None,
            init_emissions=None,
            init_A=None,
            random_state=random_state,
            implementation=impl
        )
        hmm1.pi_ = pi_1
        hmm1.A_ = A_1
        hmm1.B_ = B_1

        hmm2 = CategoricalHMM.CategoricalHMM(
            init_pi=None,
            init_emissions=None,
            init_A=None,
            random_state=random_state,
            implementation=impl
        )
        hmm2.pi_ = pi_2
        hmm2.A_ = A_2
        hmm2.B_ = B_2

        X_train, X_test, y_train, y_test, lengths_train, lengths_test = util.sample_and_split([hmm1, hmm2], [2000, 1000], sample_lengths=100, test_size=.25, random_state=random_state)
        y_train = np.asarray([1 if y == 0 else 2 for y in y_train])
        y_test = np.asarray([1 if y == 0 else 2 for y in y_test])
        # MLE Predictions
        a_new = CategoricalHMM.CategoricalHMM(
            n_components=2,
            n_iterations=500,
            n_inits=2,
            random_state=copy.copy(random_state),
            implementation=impl
        )

        b_new = CategoricalHMM.CategoricalHMM(
            n_components=2,
            n_iterations=500,
            n_inits=2,
            random_state=copy.copy(random_state),
            implementation=impl
        )

        classifier = HMMClassifier.HMMClassifier(
            {
                1: a_new,
                2: b_new
            },
            random_state=1,
            predict_method="mlc"
        )
        blob = json.dumps(classifier.to_dict())
        copy_classifier = classifier.from_dict(json.loads(blob))
        # The random states should all be identical
        randints = set()
        randints.add(classifier.estimators[1].random_state.randint(1, 10000))
        randints.add(copy_classifier.estimators[1].random_state.randint(1, 10000))
        randints.add(classifier.estimators[2].random_state.randint(1, 10000))
        randints.add(copy_classifier.estimators[2].random_state.randint(1, 10000))
        assert len(randints) == 1, randints

        classifier.fit(X_train, lengths_train, y_train)
        copy_classifier.fit(X_train, lengths_train, y_train)
        copy_trained_classifier = classifier.from_dict(json.loads(json.dumps(copy_classifier.to_dict(), cls=json_encoder.NumpyEncoder)))
        print(copy_classifier.trained_estimators_[1].A_)
        print(copy_classifier.trained_estimators_[1].B_)
        print(copy_classifier.trained_estimators_[2].A_)
        print(copy_classifier.trained_estimators_[2].B_)
        print(copy_trained_classifier.trained_estimators_[1].A_)
        print(copy_trained_classifier.trained_estimators_[1].B_)
        print(copy_trained_classifier.trained_estimators_[2].A_)
        print(copy_trained_classifier.trained_estimators_[2].B_)
        probs = classifier.predict_log_proba(X_test, lengths_test)
        probs_copy = copy_classifier.predict_log_proba(X_test, lengths_test)
        probs_copy_copy = copy_trained_classifier.predict_log_proba(X_test, lengths_test)
        print(probs_copy)
        print(probs_copy_copy)

        predicted = classifier.predict(X_test, lengths_test)
        predicted_copy = copy_classifier.predict(X_test, lengths_test)
        predicted_copy_copy = copy_trained_classifier.predict(X_test, lengths_test)
        print(predicted_copy)
        print(predicted_copy_copy)
        print(sklearn.metrics.classification_report(y_test, predicted))
        print(sklearn.metrics.classification_report(y_test, predicted_copy))
        print(sklearn.metrics.classification_report(y_test, predicted_copy_copy))
        print(sklearn.metrics.precision_score(y_test == 1, predicted == 1))
        print(sklearn.metrics.precision_score(y_test == 1, predicted_copy == 1))
        print(sklearn.metrics.precision_score(y_test == 1, predicted_copy_copy == 1))
        print(sklearn.metrics.roc_auc_score(y_test, probs[:, 1]))
        print(sklearn.metrics.roc_auc_score(y_test, probs_copy[:, 1]))
        print(sklearn.metrics.roc_auc_score(y_test, probs_copy_copy[:, 1]))

        assert sklearn.metrics.precision_score(y_test == 1, predicted == 1) == pytest.approx(0.8704156479217604, 1e-6), impl
        assert sklearn.metrics.precision_score(y_test == 1, predicted_copy == 1) == pytest.approx(0.8704156479217604, 1e-6), impl
        assert sklearn.metrics.precision_score(y_test == 1, predicted_copy_copy == 1) == pytest.approx(0.8704156479217604, 1e-6), impl
        assert sklearn.metrics.roc_auc_score(y_test, probs[:, 1]) == pytest.approx(0.8219360000000001, 1e-7), impl
        assert sklearn.metrics.roc_auc_score(y_test, probs_copy[:, 1]) == pytest.approx(0.8219360000000001, 1e-7), impl
        assert sklearn.metrics.roc_auc_score(y_test, probs_copy_copy[:, 1]) == pytest.approx(0.8219360000000001, 1e-7), impl

        # MAP Predictions
        a_new = CategoricalHMM.CategoricalHMM(
            n_components=2,
            n_iterations=50,
            random_state=random_state,
            n_inits=1,
            implementation=impl
        )

        b_new = CategoricalHMM.CategoricalHMM(
            n_components=2,
            n_iterations=50,
            random_state=copy.deepcopy(random_state),
            n_inits=1,
            implementation=impl
        )

        classifier = HMMClassifier.HMMClassifier(
            {
                1: a_new,
                2: b_new
            },
            predict_method="map",
            random_state=23,
        )
        copy_classifier = HMMClassifier.HMMClassifier.from_dict(json.loads(json.dumps(classifier.to_dict())))

        classifier.fit(X_train, lengths_train, y_train)
        copy_classifier.fit(X_train, lengths_train, y_train)
        print(classifier.trained_estimators_[1].A_)
        print(copy_classifier.trained_estimators_[1].A_)
        print(classifier.trained_estimators_[2].A_)
        print(copy_classifier.trained_estimators_[2].A_)
        print(classifier.trained_estimators_[1].B_)
        print(copy_classifier.trained_estimators_[1].B_)
        print(classifier.trained_estimators_[2].B_)
        print(copy_classifier.trained_estimators_[2].B_)
        probs = classifier.predict_log_proba(X_train, lengths_train)
        probs = classifier.predict_log_proba(X_test, lengths_test)
        probs_copy = copy_classifier.predict_log_proba(X_test, lengths_test)

        predicted = classifier.predict(X_test, lengths_test)
        predicted_copy = copy_classifier.predict(X_test, lengths_test)
        print(np.all(predicted == predicted_copy))
        probs = classifier.predict_log_proba(X_test, lengths_test)
        probs_copy = copy_classifier.predict_log_proba(X_test, lengths_test)
        print(probs)
        print(probs_copy)
        probs = classifier.predict_proba(X_test, lengths_test)
        probs_copy = copy_classifier.predict_proba(X_test, lengths_test)
        print(probs)
        print(probs_copy)

        print(sklearn.metrics.classification_report(y_test, predicted))
        print(sklearn.metrics.classification_report(y_test, predicted_copy))
        print(sklearn.metrics.precision_score(y_test == 1, predicted == 1))
        print(sklearn.metrics.roc_auc_score(y_test, probs[:, 1]))
        assert sklearn.metrics.precision_score(y_test == 1, predicted == 1) == pytest.approx(0.8097014925373134, 1e-6), impl
        assert sklearn.metrics.precision_score(y_test == 1, predicted_copy == 1) == pytest.approx(0.8097014925373134, 1e-6), impl
        assert sklearn.metrics.roc_auc_score(y_test, probs[:, 1]) == pytest.approx(0.8184159999999999, 1e-7), impl
        assert sklearn.metrics.roc_auc_score(y_test, probs_copy[:, 1]) == pytest.approx(0.8184159999999999, 1e-7), impl



def test_three_classes():

    pi_1 = [.7, .3]
    A_1 = [[0.7, 0.3], [0.4, 0.6]]
    rates_1 = [5, 10]

    hmm1 = PoissonHMM.PoissonHMM(
        init_pi=None,
        init_A=None,
        init_emissions=None,
        random_state=1
    )
    hmm1.pi_ = pi_1
    hmm1.A_ = A_1
    hmm1.rates_ = rates_1

    pi_2 = [.4, .6]
    A_2 = [[0.5, 0.5], [0.6, 0.4]]
    rates_2 = [5, 10]
    hmm2 = PoissonHMM.PoissonHMM(
        init_pi=None,
        init_A=None,
        init_emissions=None,
        random_state=1
    )
    hmm2.pi_ = pi_2
    hmm2.A_ = A_2
    hmm2.rates_ = rates_2

    rates_3 = [7, 15]
    hmm3 = PoissonHMM.PoissonHMM(
        init_pi=None,
        init_A=None,
        init_emissions=None,
        random_state=1
    )
    hmm3.pi_ = pi_2
    hmm3.A_ = A_2
    hmm3.rates_ = rates_3

    X_train, X_test, y_train, y_test, lengths_train, lengths_test = util.sample_and_split([hmm1, hmm2, hmm3], [200, 100, 100], sample_lengths=100, test_size=.25, random_state=1)
    m = {
        0: "class-a",
        1: "class-b",
        2: "class-c",
    }
    y_train = np.asarray([m[y] for y in y_train])
    y_test = np.asarray([m[y] for y in y_test])
    # MLE Predictions
    a_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=500,
        random_state=11
    )

    b_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=500,
        random_state=14
    )
    c_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=500,
        random_state=34
    )
    classifier = HMMClassifier.HMMClassifier(
        {
            "class-a": a_new,
            "class-b": b_new,
            "class-c": c_new,
        },
        predict_method="mlc"
    )

    classifier.fit(X_train, lengths_train, y_train)

    probs = classifier.predict_log_proba(X_train, lengths_train)
    probs = classifier.predict_log_proba(X_test, lengths_test)
    print(probs)
    print(len(classifier.trained_estimators_["class-a"].loglikelihoods_))
    print(len(classifier.trained_estimators_["class-b"].loglikelihoods_))
    print(len(classifier.trained_estimators_["class-c"].loglikelihoods_))

    predicted = classifier.predict(X_test, lengths_test)
    probs = classifier.predict_proba(X_test, lengths_test)
    print(sklearn.metrics.classification_report(y_test, predicted))
    for c in ["class-a", "class-b", "class-c"]:
        print("precision", c, sklearn.metrics.precision_score(y_test == c, predicted == c))

    for c in ["class-a", "class-b", "class-c"]:
        print(c)
        print(classifier.trained_estimators_[c].pi_)
        print(classifier.trained_estimators_[c].A_)
        print(classifier.trained_estimators_[c].rates_)
    for c in ["class-a", "class-b", "class-c"]:
        print("precision", c, sklearn.metrics.precision_score(y_test == c, predicted == c))
    assert sklearn.metrics.precision_score(y_test == "class-a", predicted == "class-a") == pytest.approx(0.9574468085106383, 1e-6)
    assert sklearn.metrics.precision_score(y_test == "class-b", predicted == "class-b") == pytest.approx(0.8214285714285714, 1e-6)
    assert sklearn.metrics.precision_score(y_test == "class-c", predicted == "class-c") == pytest.approx(1, 1e-6)

    # MAP Predictions
    a_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=500,
        random_state=1
    )

    b_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=500,
        random_state=1
    )

    c_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=500,
        random_state=1
    )

    class_log_prior = {}
    for item in set(y_train):
        class_log_prior[item] = np.log(np.sum(item == y_train)) - np.log(y_train.shape[0]) + 12
    classifier = HMMClassifier.HMMClassifier(
        {
            "class-a": a_new,
            "class-b": b_new,
            "class-c": c_new,
        },
        predict_method="map",
        class_log_prior=class_log_prior
    )

    classifier.fit(X_train, lengths_train, y_train)
    for c in ["class-a", "class-b", "class-c"]:
        print(c)
        print(classifier.trained_estimators_[c].pi_)
        print(classifier.trained_estimators_[c].A_)
        print(classifier.trained_estimators_[c].rates_)

    probs = classifier.predict_log_proba(X_test, lengths_test)

    predicted = classifier.predict(X_test, lengths_test)
    probs = classifier.predict_proba(X_test, lengths_test)
    print(sklearn.metrics.classification_report(y_test, predicted))
    for c in ["class-a", "class-b", "class-c"]:
        print("precision", c, sklearn.metrics.precision_score(y_test == c, predicted == c))
    assert sklearn.metrics.precision_score(y_test == "class-a", predicted == "class-a") == pytest.approx(0.9019607843137255, 1e-6)
    assert sklearn.metrics.precision_score(y_test == "class-b", predicted == "class-b") == pytest.approx(0.8333333333333334, 1e-6)
    assert sklearn.metrics.precision_score(y_test == "class-c", predicted == "class-c") == pytest.approx(1.0, 1e-6)


def test_poisson_classification_partial():

    pi_1 = [.7, .3]
    A_1 = [[0.7, 0.3], [0.4, 0.6]]
    rates_1 = [5, 10]

    hmm1 = PoissonHMM.PoissonHMM(
        init_pi=None,
        init_A=None,
        init_emissions=None,
        random_state=1
    )
    hmm1.pi_ = pi_1
    hmm1.A_ = A_1
    hmm1.rates_ = rates_1

    pi_2 = [.4, .6]
    A_2 = [[0.5, 0.5], [0.6, 0.4]]
    rates_2 = [5, 10]
    hmm2 = PoissonHMM.PoissonHMM(
        init_pi=None,
        init_A=None,
        init_emissions=None,
        random_state=1
    )
    hmm2.pi_ = pi_2
    hmm2.A_ = A_2
    hmm2.rates_ = rates_2

    X_train, X_test, y_train, y_test, lengths_train, lengths_test = util.sample_and_split([hmm1, hmm2], [200, 100], sample_lengths=100, test_size=.25, random_state=1)
    X_train = X_train.astype(float)
    X_test = X_test.astype(float)

    y_train = y_train.astype(float)
    y_test = y_test.astype(float)

    y_train = np.asarray(["class-a" if y == 0 else "class-b" for y in y_train])
    y_test = np.asarray(["class-a" if y == 0 else "class-b" for y in y_test])
    # MLE Predictions
    a_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=1,
        random_state=11
    )

    b_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=1,
        random_state=14
    )

    classifier_partial = HMMClassifier.HMMClassifier(
        {
            "class-a": a_new,
            "class-b": b_new
        },
        predict_method="mlc",
        random_state=43,
    )
    a_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=500,
        random_state=11
    )

    b_new = PoissonHMM.PoissonHMM(
        n_components=2,
        n_iterations=500,
        random_state=14
    )

    classifier = HMMClassifier.HMMClassifier(
        {
            "class-a": a_new,
            "class-b": b_new
        },
        predict_method="mlc",
        random_state=43,
    )
    classifier.fit(X_train, lengths_train, y_train)
    classifier_partial.fit(X_train, lengths_train, y_train)
    for i in range(499):
        classifier_partial.partial_fit(X_train, lengths_train, y_train, n_iterations=1)

    for k, val in classifier.trained_estimators_.items():
        print(k, val.loglikelihoods_[:50])
        print(k, classifier_partial.trained_estimators_[k].loglikelihoods_[:50])
        print()

    probs = classifier.predict_log_proba(X_train, lengths_train)
    probs = classifier_partial.predict_log_proba(X_test, lengths_test)

    predicted = classifier.predict(X_test, lengths_test)
    probs = classifier.predict_proba(X_test, lengths_test)
    predicted_copy = classifier_partial.predict(X_test, lengths_test)
    probs_copy = classifier_partial.predict_proba(X_test, lengths_test)
    print(sklearn.metrics.classification_report(y_test, predicted))

    print(sklearn.metrics.precision_score(y_test == "class-a", predicted == "class-a"))
    print(sklearn.metrics.roc_auc_score(y_test, probs[:, 1]))
    assert sklearn.metrics.precision_score(y_test == "class-a", predicted == "class-a") == pytest.approx(0.9375, 1e-6)
    assert sklearn.metrics.precision_score(y_test == "class-a", predicted_copy == "class-a") == pytest.approx(0.9375, 1e-6)
    assert sklearn.metrics.roc_auc_score(y_test, probs[:, 1]) == pytest.approx(0.9536, 1e-6)
    assert sklearn.metrics.roc_auc_score(y_test, probs_copy[:, 1]) == pytest.approx(0.9536, 1e-6)

def test_categorical_partial_fit():

    pi_1 = np.ones(3)/3
    A_1 = [
        [0.4, 0.2, 0.4],
        [0.4, 0.4, 0.2],
        [0.2, 0.4, 0.4]
    ]
    B_1 = [
        [0.85, 0.1, 0.05],
        [0.1, 0.85, 0.05],
        [0.1, 0.05, 0.85]
    ]
    pi_2 = np.ones(3)/3
    A_2 = [
        [0.2, 0.4, 0.4],
        [0.4, 0.2, 0.4],
        [0.4, 0.4, 0.2],
    ]
    B_2 = B_1

    hmm1 = CategoricalHMM.CategoricalHMM(
        init_pi=None,
        init_A=None,
        init_emissions=None,
        random_state=1
    )
    hmm1.pi_ = pi_1
    hmm1.A_ = A_1
    hmm1.B_ = B_1

    hmm2 = CategoricalHMM.CategoricalHMM(
        init_pi=None,
        init_A=None,
        init_emissions=None,
        random_state=1
    )
    hmm2.pi_ = pi_2
    hmm2.A_ = A_2
    hmm2.B_ = B_2

    X_train, X_test, y_train, y_test, lengths_train, lengths_test = util.sample_and_split([hmm1, hmm2], [200, 100], sample_lengths=100, test_size=.25, random_state=1)
    X_train = X_train.astype(float)
    X_test = X_test.astype(float)

    y_train = y_train.astype(float)
    y_test = y_test.astype(float)

    y_train = np.asarray(["class-a" if y == 0 else "class-b" for y in y_train])
    y_test = np.asarray(["class-a" if y == 0 else "class-b" for y in y_test])
    # MLE Predictions
    a_new = CategoricalHMM.CategoricalHMM(
        n_components=3,
        n_iterations=1,
        random_state=11
    )

    b_new = CategoricalHMM.CategoricalHMM(
        n_components=3,
        n_iterations=1,
        random_state=14
    )

    classifier_partial = HMMClassifier.HMMClassifier(
        {
            "class-a": a_new,
            "class-b": b_new
        },
        predict_method="mlc",
        random_state=43,
    )
    a_new = CategoricalHMM.CategoricalHMM(
        n_components=3,
        n_iterations=500,
        random_state=11
    )

    b_new = CategoricalHMM.CategoricalHMM(
        n_components=3,
        n_iterations=500,
        random_state=14
    )

    classifier = HMMClassifier.HMMClassifier(
        {
            "class-a": a_new,
            "class-b": b_new
        },
        predict_method="mlc",
        random_state=43,
    )
    classifier.fit(X_train, lengths_train, y_train)
    classifier_partial.fit(X_train, lengths_train, y_train)
    for i in range(499):
        classifier_partial.partial_fit(X_train, lengths_train, y_train, n_iterations=1)

    for k, val in classifier.trained_estimators_.items():
        print(k, val.loglikelihoods_[:50])
        print(k, classifier_partial.trained_estimators_[k].loglikelihoods_[:50])
        print()

    probs = classifier.predict_log_proba(X_train, lengths_train)
    probs = classifier_partial.predict_log_proba(X_test, lengths_test)

    predicted = classifier.predict(X_test, lengths_test)
    probs = classifier.predict_proba(X_test, lengths_test)
    predicted_copy = classifier_partial.predict(X_test, lengths_test)
    probs_copy = classifier_partial.predict_proba(X_test, lengths_test)
    print(sklearn.metrics.classification_report(y_test, predicted))
    print(sklearn.metrics.precision_score(y_test == "class-a", predicted == "class-a"))
    print(sklearn.metrics.precision_score(y_test == "class-a", predicted_copy == "class-a"))

    assert sklearn.metrics.precision_score(y_test == "class-a", predicted == "class-a") == sklearn.metrics.precision_score(y_test == "class-a", predicted_copy == "class-a")
    assert sklearn.metrics.roc_auc_score(y_test == "class-a", probs[:, 1]) == sklearn.metrics.roc_auc_score(y_test == "class-a", probs_copy[:, 1])


if __name__ == "__main__":
    test_categorical_classification()
    #test_poisson_classification_partial()
    #test_categorical_partial_fit()
    #test_three_classes()
