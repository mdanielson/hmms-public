# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import datetime
import logging

import pandas as pd
import pytest
import sklearn.metrics
import sklearn.model_selection
import sklearn.utils

from hmm import (CategoricalHMM, CategoricalVariationalHMM, GaussianHMM,
                 HMMCluster, PoissonHMM, util)
from hmm.datasets import for_tests
from hmm.scoring import cluster_report


def test_cluster_gaussian():

    for impl in ["log", "scaling"]:
        print(datetime.datetime.now(), impl)
        random_state = sklearn.utils.check_random_state(1983)
        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = for_tests.three_cluster_gaussian(random_state)
        print(train_lengths)
        print(train_observations)
        clusterer = HMMCluster.GaussianHMMDistanceCluster(
            n_clusters=3,
            hmm_impl=GaussianHMM.GaussianHMM(
                n_components=2,
                verbose=logging.WARNING,
                n_iterations=500,
                random_state=random_state,
                implementation=impl
            )
        )
        print(train_labels)
        training_predictions = clusterer.fit_predict(train_observations, train_lengths)

        print(clusterer.composite_model_.pi_)
        print(clusterer.composite_model_.A_)
        print(clusterer.composite_model_.means_)
        print(clusterer.composite_model_.variances_)

        testing_predictions = clusterer.predict(test_observations, test_lengths)
        print(training_predictions)
        print(train_labels)
        print(testing_predictions)
        print(test_labels)

        cluster_report(train_labels, training_predictions)
        cluster_report(test_labels, testing_predictions)

        assert sklearn.metrics.normalized_mutual_info_score(test_labels, testing_predictions) == pytest.approx(0.977, 1e-3), impl


def test_cluster_poisson():

    random_state = sklearn.utils.check_random_state(121232)

    pi_1 = [.5, .5]
    A_1 = [[.6, .4],
           [.4, .6]]
    rates_1 = [4, 8]

    pi_2 = pi_1
    A_2 = [[.4, .6],
           [.6, .4]]
    rates_2 = rates_1

    model_1 = PoissonHMM.PoissonHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_1.pi_ = pi_1
    model_1.A_ = A_1
    model_1.rates_ = rates_1

    model_2 = PoissonHMM.PoissonHMM(init_pi=None, init_A=None, init_emissions=None, n_iterations=0, random_state=random_state)
    model_2.pi_ = pi_2
    model_2.A_ = A_2
    model_2.rates_ = rates_2

    train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = util.sample_and_split([model_1, model_2], random_state=random_state, sample_lengths=400)
    clusterer = HMMCluster.HMMDistanceCluster(n_clusters=2, hmm_impl=PoissonHMM.PoissonHMM(n_components=2, random_state=random_state))

    training_predictions = clusterer.fit_predict(train_observations, train_lengths)
    print(training_predictions)
    print(train_labels)
    testing_predictions = clusterer.predict(test_observations, test_lengths)
    print(testing_predictions)
    print(test_labels)

    cluster_report(train_labels, training_predictions)
    cluster_report(test_labels, testing_predictions)
    print(pd.crosstab(test_labels, testing_predictions))
    assert sklearn.metrics.normalized_mutual_info_score(test_labels, testing_predictions) == pytest.approx(0.748, 1e-3)

    clusterer = HMMCluster.PoissonHMMDistanceCluster(n_clusters=2, hmm_impl=PoissonHMM.PoissonHMM(n_components=2, random_state=random_state))

    clusterer.fit(train_observations, train_lengths)

    print(clusterer.composite_model_.pi_)
    print(clusterer.composite_model_.A_)
    print(clusterer.composite_model_.rates_)

    training_predictions = clusterer.predict(train_observations, train_lengths)
    print(training_predictions)
    print(train_labels)
    testing_predictions = clusterer.predict(test_observations, test_lengths)
    print(testing_predictions)
    print(test_labels)

    cluster_report(train_labels, training_predictions)
    cluster_report(test_labels, testing_predictions)
    print(pd.crosstab(test_labels, testing_predictions))
    assert sklearn.metrics.normalized_mutual_info_score(test_labels, testing_predictions) == pytest.approx(.636, 1e-3)


def test_cluster_categorical1():

    for impl in ["log", "scaling"]:
        print(datetime.datetime.now(), impl)
        random_state = sklearn.utils.check_random_state(1983)
        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = for_tests.two_cluster_categorical_clustering(random_state)
        clusterer = HMMCluster.CategoricalHMMDistanceCluster(
            n_clusters=2,
            # n_iterations_1=100,
            # n_iterations_2=500,
            hmm_impl=CategoricalHMM.CategoricalHMM(
                n_components=2,
                n_iterations=1000,
                tol=1e-6,
                verbose=logging.WARN,
                random_state=random_state,
                implementation=impl,
            )
        )
        print(train_labels)
        clusterer.fit(train_observations, train_lengths)

        print(clusterer.composite_model_.pi_)
        print(clusterer.composite_model_.A_)
        print(clusterer.composite_model_.B_)

        training_predictions = clusterer.predict(train_observations, train_lengths)
        testing_predictions = clusterer.predict(test_observations, test_lengths)
        print(training_predictions)
        print(train_labels)
        print(testing_predictions)
        print(test_labels)
        print(pd.crosstab(test_labels, testing_predictions))
        print("Training data")
        cluster_report(train_labels, training_predictions)
        print("Testing data")
        cluster_report(test_labels, testing_predictions)
        assert sklearn.metrics.normalized_mutual_info_score(test_labels, testing_predictions) == pytest.approx(0.812, 1e-3)


def test_cluster_categorical2():

    for impl in ["scaling", "log"]:
        print(datetime.datetime.now(), impl)
        random_state = sklearn.utils.check_random_state(1983)
        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = for_tests.three_cluster_categorical_clustering(random_state)

        clusterer = HMMCluster.HMMDistanceCluster(
            n_clusters=3,
            n_iterations_1=500,
            n_iterations_2=500,
            hmm_impl=CategoricalVariationalHMM.CategoricalVariationalHMM(
                n_components=3,
                n_features=3,
                tol=1e-4,
                verbose=logging.WARN,
                random_state=random_state,
                implementation=impl
            )
        )
        clusterer.fit(train_observations, train_lengths)
        print(pd.DataFrame(clusterer.grouped_models_[0].A_posterior_))
        print(pd.DataFrame(clusterer.grouped_models_[1].A_posterior_))
        print(pd.DataFrame(clusterer.grouped_models_[2].A_posterior_))

        training_predictions = clusterer.predict(train_observations, train_lengths)
        testing_predictions = clusterer.predict(test_observations, test_lengths)
        print(pd.crosstab(train_labels, training_predictions))
        print(training_predictions)
        print(train_labels)
        print(testing_predictions)
        print(test_labels)
        print("Training data")
        cluster_report(train_labels, training_predictions)
        print("Testing data")
        cluster_report(test_labels, testing_predictions)
        assert sklearn.metrics.normalized_mutual_info_score(test_labels, testing_predictions) == pytest.approx(0.7256787228559137, 1e-3)


if __name__ == "__main__":
    # test_cluster_gaussian()
    # test_cluster_poisson()
    test_cluster_categorical2()
