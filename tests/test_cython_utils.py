# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import pytest
import scipy.stats

from hmm.utilities import normal, poisson


def test_normal():
    n = normal.Normal(0, 1, random_state=32)
    data = []
    for i in range(1000):
        data.append(n.rvs())

    print(np.mean(data))
    print(np.var(data))

    assert np.mean(data) == pytest.approx(0.03285467547381292, 1e-6)
    assert np.var(data) == pytest.approx(0.963887670947021, 1e-6)

    for mean in [-5, 0, 5]:
        for var in [.01, .1, 1, 5, 10]:

            sp_normal = scipy.stats.norm(
                mean,
                np.sqrt(var)
            )

            ours = normal.Normal(mean, var, random_state=32)
            print(mean, var)
            for i in range(-10, 10):
                assert ours.pdf(i) == pytest.approx(sp_normal.pdf(i), 1e-6)
                assert ours.logpdf(i) == pytest.approx(sp_normal.logpdf(i), 1e-6), (mean, var, i)
    mean = -0.12386
    var = 0.019
    ours = normal.Normal(mean, var)
    theirs = scipy.stats.norm(mean, np.sqrt(var))
    print(ours.pdf(0))
    print(theirs.pdf(0))


def test_poisson():
    n = poisson.Poisson(10, random_state=32)
    data = []
    for i in range(1000):
        data.append(n.rvs())

    assert np.mean(data) == pytest.approx(10.004, 1e-2)
    assert np.var(data) == pytest.approx(10.773984, 1e-6)

    for m in range(100):
        ours = poisson.Poisson(m, random_state=32)
        sp_poisson = scipy.stats.poisson(m)
        for i in range(100):
            assert ours.pmf(i) == pytest.approx(sp_poisson.pmf(i), 1e-6), i


def test_multivariate_normal():
    means = np.asarray([0.])
    covariances = np.asarray([[1.]])
    n = normal.MultivariateNormal(means, covariances, random_state=32)
    sp = scipy.stats.multivariate_normal(means, covariances)
    data = []
    for i in range(1000):
        data.append(n.rvs())

    print(np.mean(data))
    print(np.var(data))

    assert np.mean(data) == pytest.approx(0.03285467547381292, 1e-6)
    assert np.var(data) == pytest.approx(0.963887670947021, 1e-6)

    assert n.pdf(np.asarray([-1.])) == pytest.approx(sp.pdf([-1]), 1e-6)
    assert n.pdf(np.asarray([0.])) == pytest.approx(sp.pdf(0), 1e-6)
    assert n.pdf(np.asarray([1.])) == pytest.approx(sp.pdf(1), 1e-6)

    means = np.asarray([0.])
    covariances = np.asarray([[2.]])
    n = normal.MultivariateNormal(means, covariances, random_state=32)
    sp = scipy.stats.multivariate_normal(means, covariances)
    data = []
    for i in range(1000):
        data.append(n.rvs())

    print(np.mean(data))
    print(np.var(data))

    assert np.mean(data) == pytest.approx(0.04646352764243294, 1e-6)
    assert np.var(data) == pytest.approx(1.9277753418940422, 1e-6)
    assert n.pdf(np.asarray([-1.])) == pytest.approx(sp.pdf([-1]), 1e-6)
    assert n.pdf(np.asarray([0.])) == pytest.approx(sp.pdf(0), 1e-6)
    assert n.pdf(np.asarray([1.])) == pytest.approx(sp.pdf(1), 1e-6)
    means = np.asarray([0., 5.])
    covariances = np.asarray(
        [
            [2., 1],
            [1, 1]
        ]
    )
    n = normal.MultivariateNormal(means, covariances, random_state=32)
    sp = scipy.stats.multivariate_normal(means, covariances)

    assert n.pdf(np.asarray([-1., -1])) == pytest.approx(sp.pdf([-1, -1]), 1e-6)
    assert n.pdf(np.asarray([0., 5.])) == pytest.approx(sp.pdf([0, 5]), 1e-6)
    assert n.pdf(np.asarray([1., 3])) == pytest.approx(sp.pdf([1, 3]), 1e-6)


#
#    for mean in [-5, 0, 5]:
#        for var in [.01, .1, 1, 5, 10]:
#
#            sp_normal = scipy.stats.norm(
#                mean,
#                np.sqrt(var)
#            )
#
#            ours = normal.Normal(mean, var, random_state=32)
#            print(mean, var)
#            for i in range(-10, 10):
#                assert ours.pdf(i) == pytest.approx(sp_normal.pdf(i), 1e-6)
#    mean = -0.12386
#    var = 0.019
#    ours = normal.Normal(mean, var)
#    theirs = scipy.stats.norm(mean, np.sqrt(var))
#    print(ours.pdf(0))
#    print(theirs.pdf(0))
#
#
