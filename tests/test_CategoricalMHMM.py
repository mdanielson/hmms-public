# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import numpy as np
import pandas as pd
import pytest
import sklearn.metrics
import sklearn.utils

from hmm import CategoricalHMM, CategoricalMHMM
from hmm.datasets import for_tests, models
from hmm.util import sample_and_split
from hmm.scoring import cluster_report


# def test_categorical_bic():
#
#     random_state = sklearn.utils.check_random_state(1143)
#     observations, lengths, labels, _ = for_tests.get_categorical_beal(random_state)
#     bics = {}
#     scores = {}
#     params = {}
#     for n_mixture_components in range(1, 5):
#         for n_components in range(1,  5):
#             mixture = CategoricalMHMM.CategoricalMHMM(
#                 n_mixture_components=n_mixture_components,
#                 n_components=n_components,
#                 n_features=3,
#                 n_iterations=1000,
#                 n_inits=5,
#                 n_jobs=1,
#                 random_state=random_state,
#                 verbose=logging.ERROR,
#                 smoothing="laplace",
#             )
#             mixture.fit(observations, lengths)
#             bics[n_mixture_components, n_components] = mixture.bic(observations, lengths)
#             scores[n_mixture_components, n_components] = mixture.score(observations, lengths)
#             params[n_mixture_components, n_components] = mixture.n_parameters()
#
#     for (n_mixture_components, n_components), bic in sorted(bics.items(), key=lambda x: x[1]):
#         print("[{},{}] {} {:4f} {:4f}".format(n_mixture_components, n_components, params[n_mixture_components, n_components], bic, scores[n_mixture_components, n_components]))


def test_categorical_two_state():
    for impl in ["log", "scaling"]:
        random_state = sklearn.utils.check_random_state(43343)

        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = for_tests.two_cluster_categorical_clustering(random_state)

        mixture = CategoricalMHMM.CategoricalMHMM(n_mixture_components=2, n_components=2, n_features=3, n_iterations=1000, implementation=impl, random_state=random_state, verbose=logging.INFO)
        mixture.fit(train_observations, train_lengths)
        for i in range(mixture.n_mixture_components):
            print("Component {}: {}".format(i, mixture.mixture_weights_[i]))
            print(mixture.pi_[i])
            print(mixture.A_[i])
            print(mixture.B_[i])

        predicted = mixture.predict(train_observations, train_lengths)
        print(pd.crosstab(train_labels, predicted))
        predicted = mixture.predict(test_observations, test_lengths)
        print(pd.crosstab(test_labels, predicted))
        print(sklearn.metrics.normalized_mutual_info_score(test_labels, predicted))
        cluster_report(test_labels, predicted)
        assert sklearn.metrics.normalized_mutual_info_score(test_labels, predicted) == pytest.approx(0.7787009179931589, 1e-3), impl
        print(mixture.transform(train_observations, train_lengths))


def test_categorical_three_state():
    for impl in ["log", "scaling"]:
        random_state = sklearn.utils.check_random_state(1983)

        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths= for_tests.three_cluster_categorical_clustering(random_state)

        mixture = CategoricalMHMM.CategoricalMHMM(n_mixture_components=3, n_components=2, n_features=3, n_iterations=1000, n_inits=4, implementation=impl, random_state=random_state)
        mixture.fit(train_observations, train_lengths)
        for i in range(mixture.n_mixture_components):
            print("Component {}: {}".format(i, mixture.mixture_weights_[i]))
            print(mixture.pi_[i])
            print(mixture.A_[i])
            print(mixture.B_[i])

        predicted = mixture.predict(train_observations, train_lengths)
        print(pd.crosstab(train_labels, predicted))
        predicted = mixture.predict(test_observations, test_lengths)
        print(pd.crosstab(test_labels, predicted))
        print(sklearn.metrics.normalized_mutual_info_score(test_labels, predicted))
        cluster_report(test_labels, predicted)
        assert sklearn.metrics.normalized_mutual_info_score(test_labels, predicted) == pytest.approx(0.7336804366512105, 1e-3), impl


def test_music_analysis():
    rs = sklearn.utils.check_random_state(2132)
    models = for_tests.get_music_analysis(rs)
    observations = []
    lengths = []
    labels = []
    for m in models:
        (obs, sta, lens) = m.sample(50, 30, random_state=rs)
        observations.extend(obs)
        lengths.extend(lens)

    mixture = CategoricalMHMM.CategoricalMHMM(3, 3, n_iterations=500, n_inits=1, n_jobs=1, random_state=rs, verbose=logging.INFO)
    mixture.fit(observations, lengths)
    for i in range(mixture.n_mixture_components):
        print("pi")
        print(pd.DataFrame(mixture.pi_[i]))
        print("A")
        print(pd.DataFrame(mixture.A_[i]))
        print("B")
        print(pd.DataFrame(mixture.B_[i]))
    print("ll", mixture.n_mixture_components, mixture.n_components, mixture.loglikelihoods_[-10:])


if __name__ == "__main__":
    test_music_analysis()
