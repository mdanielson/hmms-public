# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import pandas as pd
import pytest
import sklearn.utils

from hmm.mixtures import VariationalPoissonMixtureModel, PoissonMixtureModel


def test_variational():
    rs = sklearn.utils.check_random_state(4)
    source = PoissonMixtureModel.PoissonMixtureModel(2, init_emissions=False, init_weights=False, random_state=rs)
    source.rates_ = np.asarray([5, 5, 15])
    source.weights_ = np.ones(3)/3

    data = source.sample(2000)[:, None]
    print(pd.Series(data.ravel()).value_counts().sort_index())

    mixture = VariationalPoissonMixtureModel.VariationalPoissonMixtureModel(n_components=4, n_iterations=1000, random_state=rs, rates_prior="uninformed")
    mixture.fit(data)

    print(mixture.rates_posterior_)
    print(mixture.weights_posterior_)


if __name__ == "__main__":
    test_variational()
