# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import numpy as np
import pytest
import sklearn.utils

from hmm import GaussianHMM


def test_normals_simple():
    hidden_states = np.asarray([0]*100 + [1] * 100)
    hidden_states = hidden_states
    rs = sklearn.utils.check_random_state(1)
    observed = np.concatenate([rs.normal(5, np.sqrt(5), size=100), rs.normal(10, 1, size=100)])
    observed = observed[:, None]
    lengths = [len(hidden_states)]
    print(observed)
    new = GaussianHMM.GaussianHMM.reestimate_from_sequences(observed, hidden_states, lengths)
    print(new.pi_)
    print(new.A_)
    print(new.means_)
    print(new.variances_)
    assert new.pi_[0] == 1
    assert new.pi_[1] == 0
    assert new.A_[0, 0] == pytest.approx(0.99, 1e-6)
    assert new.A_[1, 1] == pytest.approx(1, 1e-6)
    assert new.means_[0] == pytest.approx(5.13546738, 1e-6)
    assert new.means_[1] == pytest.approx(10.15279478, 1e-6)
    assert new.variances_[0] == pytest.approx(3.91750761, 1e-6)
    assert new.variances_[1] == pytest.approx(0.86861434, 1e-6)


def test_normals_two_state_constant_init():
    for impl in ["log", "scaling"]:
        print(impl)
        random_state = sklearn.utils.check_random_state(2)
        pi = [.5, .5]
        A = [
            [.1, .9],
            [.5, .5],
        ]
        means = [-1, 1]
        variances = [.25, .5]

        gaus = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, random_state=random_state, implementation=impl)
        gaus.pi_ = pi
        gaus.A_ = A
        gaus.means_ = means
        gaus.variances_ = variances

        observed_sequences, hidden_sequences, lengths = gaus.sample(100, 100, random_state=random_state)

        new = GaussianHMM.GaussianHMM.reestimate_from_sequences(observed_sequences, hidden_sequences, lengths)
        print(new.pi_)
        print(new.A_)
        print(new.means_)
        print(new.variances_)

        check_pi = [0.46, 0.54]
        check_A = [[0.08726753, 0.91273247],
                    [0.49679938, 0.50320062]]
        check_means = [-0.99043921, 0.99206195]
        check_variances = [0.24998909, 0.48418888]
        check(
            new.pi_, check_pi,
            new.A_, check_A,
            new.means_, check_means,
            new.variances_, check_variances)

        trainer = GaussianHMM.GaussianHMM(n_components=2,  n_iterations=5000, verbose=logging.DEBUG, random_state=random_state, implementation=impl)
        trainer.fit(observed_sequences, lengths)

        check_pi = [0.5491407880855412, 0.45085921191445866]
        check_A = [[0.5063636828028483, 0.4936363171971517],
                   [0.9200140713105204, 0.07998592868947971]]

        check_means = [0.986592756930916, -0.998697658411687]
        check_variances = [0.48822164163958887, 0.2439620110284014]
        check(
            trainer.pi_, check_pi,
            trainer.A_, check_A,
            trainer.means_, check_means,
            trainer.variances_, check_variances
        )


def check(pi, check_pi, A, check_A, means, check_means, variances, check_variances, abs_tol=1e-5, sort_means=False):
    pi_sort = np.argsort(pi)
    check_sort = np.argsort(check_pi)

    pi = np.asarray(pi)[pi_sort]
    check_pi = np.asarray(check_pi)[check_sort]
    assert pi == pytest.approx(check_pi, abs=abs_tol)
    means = np.asarray(means)[pi_sort]
    check_means = np.asarray(check_means)[check_sort]
    assert check_means == pytest.approx(means, abs=abs_tol)
    variances = np.asarray(variances)[pi_sort]
    check_variances = np.asarray(check_variances)[check_sort]
    assert check_variances == pytest.approx(variances, abs=abs_tol)
    for i,j  in zip(pi_sort, check_sort):
        a_i = np.asarray(A)[i]
        a_j = np.asarray(check_A)[j]
        assert a_i[pi_sort] == pytest.approx(a_j[check_sort], abs=abs_tol)


def test_normals_three_state():
    for impl in ["scaling", "log"]:
        print(impl)
        random_state = sklearn.utils.check_random_state(2)
        pi = [.5, .1, .4]
        A = [
            [.4, .2, .4],
            [.2, .3, .5],
            [.1, .1, .8],
        ]
        means = [-1, 1., 4, ]
        variances = [1., 1, 5]

        print(impl)
        gaus = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, random_state=random_state, implementation=impl)
        gaus.pi_ = pi
        gaus.A_ = A
        gaus.means_ = means
        gaus.variances_ = variances

        observed_sequences, hidden_sequences, lengths = gaus.sample(1000, 50, random_state=random_state)

        new = GaussianHMM.GaussianHMM.reestimate_from_sequences(observed_sequences, hidden_sequences, lengths)
        print(new.pi_)
        print(new.A_)
        print(new.means_)
        print(new.variances_)
        check_pi = [0.485, 0.109, 0.406]
        check_A = [[0.40582775, 0.19680414, 0.39736811],
                    [0.19724314, 0.28648572, 0.51627114],
                    [0.10044242, 0.1010403, 0.79851728]]

        check_means = [-1.00828069,  0.99627508,  3.97768968]
        check_variances = [1.01051462, 0.99107139, 4.99162366]
        check(
            new.pi_, check_pi,
            new.A_, check_A,
            new.means_, check_means,
            new.variances_, check_variances)
        print(np.ravel(observed_sequences).min())
        print(np.ravel(observed_sequences).max())
        print(np.ravel(observed_sequences).max())
        #observed_sequences = observed_sequences[:, :, None]
        trainer = GaussianHMM.GaussianHMM(n_components=3, n_iterations=1000, verbose=logging.DEBUG, random_state=random_state, implementation=impl)
        trainer.fit(observed_sequences, lengths)
        print(trainer.pi_.tolist())
        print(trainer.A_.tolist())
        print(trainer.means_.tolist())
        print(trainer.variances_.tolist())
        check_pi = [0.4919724299771605, 0.4018020378876576, 0.1062255321351818]
        check_A = [[0.40692755764203153, 0.3901017877132273, 0.20297065464474118], [0.0997545778382192, 0.8088902028039183, 0.0913552193578626], [0.21219979134513, 0.5106291548973091, 0.27717105375756096]]
        check_means = [-1.0025846606141087, 3.9486674637141905, 1.0138180841157087]
        check_variances = [0.9926458223962221, 5.061769131005801, 0.8902423339839822]

        check(
            trainer.pi_, check_pi,
            trainer.A_, check_A,
            trainer.means_, check_means,
            trainer.variances_, check_variances,
            abs_tol=1e-3
        )


def test_going_to_zero():
    for impl in ["log", "scaling"]:
        random_state = sklearn.utils.check_random_state(12)
        data1 = [int(i) for i in random_state.normal(10, 5, 100)] + [5] * 100 + [6]*10 + [7]*10 + [0] * 200
        data2 = [int(i) for i in random_state.normal(10, 5, 100)]
        data3 = [5] * 100
        data = np.concatenate([data1, data2, data3])
        data2d = data[:, None]
        lengths = [len(data1), len(data2), len(data3)]
        model = GaussianHMM.GaussianHMM(n_components=3, random_state=random_state, implementation=impl)
        model.fit(data2d, lengths)
        print(model.pi_.tolist())
        print(model.A_.tolist())
        print(model.means_.tolist())
        print(model.variances_.tolist())
        pi = [0.3333284970388795, 0.6666715029611204, 0.0]
        A = [[0.9544143317551592, 0.04091307172141161, 0.004672596523429221], [0.04571752931104995, 0.9363201862328512, 0.017962284456098893], [0.0, 0.018180981658467505, 0.9818190183415325]]
        means = [4.999999999999965, 8.78827463970797, 0.0]
        variances = [1e-06, 23.95810591382365, 1e-06]
        check(pi, model.pi_, A, model.A_, means, model.means_, variances, model.variances_)
        print(model.score(data2d, lengths))
        print(model.aic(data2d, lengths))
        print(model.bic(data2d, lengths))
        assert model.score(data2d, lengths) == pytest.approx(575.8403620343387, 1e-6), impl
        assert model.aic(data2d, lengths) == pytest.approx(-3427.042172206032, 1e-6), model.aic(data2d,lengths)
        assert model.bic(data2d, lengths) == pytest.approx(-3365.026099513484, 1e-6), model.bic(data2d, lengths)


def test_scaling_to_log():
    random_state = sklearn.utils.check_random_state(12)
    data1 = random_state.normal(0, 1, 100).tolist()
    data2 = random_state.normal(5, 1, 100).tolist()
    data3 = random_state.normal(0, 1, 100).tolist()
    data4 = random_state.normal(5, 1, 100).tolist()
    data = np.concatenate([data1, data2, data3, data4])
    data[150] = 100
    data[153] = 1000
    data2d = data[:, None]
    lengths = [len(data2d)]

    model = GaussianHMM.GaussianHMM(n_components=2, random_state=random_state, implementation="scaling", allowed_to_use_log=False)
    with pytest.raises(AssertionError):
        model.fit(data2d, lengths)

    for impl in ["scaling", "log"]:
        model = GaussianHMM.GaussianHMM(n_components=2, random_state=random_state, implementation="scaling", allowed_to_use_log=True)
        model.fit(data2d, lengths)
        check_pi = [1, 0]
        check_A = [[0.99748744, 0.00251256],
                   [1.,         0.]]
        means = [2.64528295, 1000.]
        variance = [3.12931978e+01, 1.00000000e-06]
        check(
            check_pi, model.pi_,
            check_A, model.A_,
            means, model.means_,
            variance, model.variances_
        )


if __name__ == "__main__":
    test_going_to_zero()
    #test_scaling_to_log()
    #test_normals_simple()
    #test_normals_two_state_constant_init()
    #test_normals_three_state()
