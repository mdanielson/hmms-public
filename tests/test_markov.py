# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import pytest

from hmm import MarkovModel, util
from hmm.datasets.real import load_old_faithful_long_short


def test_chapter_1_sequences():
    """
    From Hidden Markov Models for TimeSeries: Chapter 1

    """
    example = list("""2332111112 3132332122 3232332222 3132332212 3232132232 3132332223 3232331232 3232331222 3232132123 3132332121""".strip(" "))
    example = np.asarray([int(i) for i in example if i != " "])
    model = MarkovModel.MarkovModel(order=1)
    model.fit(example, [len(example)],)
    print(model.transition_probabilities_)
    assert model.transition_probabilities_[(1,)][1] == pytest.approx(4/17, 1e-5)
    assert model.transition_probabilities_[(1,)][2] == pytest.approx(7/17, 1e-5)
    assert model.transition_probabilities_[(1,)][3] == pytest.approx(6/17, 1e-5)
    assert model.transition_probabilities_[(2,)][1] == pytest.approx(8/42, 1e-5)
    assert model.transition_probabilities_[(2,)][2] == pytest.approx(10/42, 1e-5)
    assert model.transition_probabilities_[(2,)][3] == pytest.approx(24/42, 1e-5)
    assert model.transition_probabilities_[(3,)][1] == pytest.approx(6/40, 1e-5)
    assert model.transition_probabilities_[(3,)][2] == pytest.approx(24/40, 1e-5)
    assert model.transition_probabilities_[(3,)][3] == pytest.approx(10/40, 1e-5)
    assert model.initial_probabilities_[1] == 0
    assert model.initial_probabilities_[2] == 1

    sample = model.sample(random_state=3)

    assert np.all(sample == [[2, 1, 2, 3, 1, 3, 3, 3, 3, 2]])
    probs = np.exp(model.score_samples(
        [
            2,
            2, 3, 3
        ],
        [1, 3]
    ))
    assert probs[0] == 1.
    assert probs[1] == pytest.approx(0.14285714285714285, 1e-5)
    log_probs = model.score_samples(
        [
            2,
            2, 3, 2
        ],
        [1, 3]
    )
    print(log_probs)
    assert log_probs[0] == 0.
    assert log_probs[1] == pytest.approx(-1.0704414117014136, 1e-5)


def test_chapter_10_eruptions_markov():
    eruptions = load_old_faithful_long_short().data.ravel()
    ours = list("10111011010101101011010101011111010101010101010101010101111101010101101011101111101110101010101010101010101010110101010101110111111101111101111111010101010101111110101010111010101101011110101010111010101101101110101010110111111101010111101101110110101110101111101110101011010111111110101010101010110")
    ours = np.asarray([int(i) for i in ours])
    print(len(eruptions))
    lengths = [len(eruptions)]
    model = MarkovModel.MarkovModel(order=2)
    model.fit(eruptions, lengths)
    print(model.transition_probabilities_)
    print(model.initial_probabilities_)
    print(model.initial_probabilities_frame())
    print(model.transition_probabilities_frame().index)
    print(model.transition_probabilities_frame().columns)
    print(model.transition_probabilities_frame())
    assert model.n_parameters() == 4
    print(model.aic(eruptions, lengths))
    assert model.loglikelihoods_[-1] == pytest.approx(-126.99717257219544, 1e-5)
    assert model.aic(eruptions, lengths) == pytest.approx(261.9943451443909, 1e-5)
    assert model.bic(eruptions, lengths) == pytest.approx(276.8094750430157, 1e-5)


def test_chapter_1_sunny():
    model = MarkovModel.MarkovModel()
    model.initial_probabilities_ = np.asarray([0, 1.])
    model.transition_probabilities_ = np.asarray(
        [
            [0.9, 0.1],
            [0.6, 0.4],
        ]
    )
    zero = model.unconditional_state_probabilities(0)
    one = model.unconditional_state_probabilities(1)
    two = model.unconditional_state_probabilities(2)
    assert zero[0] == pytest.approx(0, 1e-4)
    assert zero[1] == pytest.approx(1, 1e-4)
    assert one[0] == pytest.approx(.6, 1e-4)
    assert one[1] == pytest.approx(.4, 1e-4)
    assert two[0] == pytest.approx(.78, 1e-4)
    assert two[1] == pytest.approx(.22, 1e-4)

    next_states = model.predict([1, 1, 1], [1, 1, 1], random_state=23)
    print(next_states)
    assert next_states[0] == 0
    assert next_states[1] == 1
    assert next_states[2] == 1


def test_ngrams():
    sequence = [0, 1, 2, 3]

    assert list(util.ngrams(sequence, pad=True)) == [(0,), (1,), (2,), (3,)]
    assert list(util.ngrams(sequence, n=2, pad=True)) == [(None, 0), (0, 1), (1, 2), (2, 3)]
    assert list(util.ngrams(sequence, pad=False)) == [(0,), (1,), (2,), (3,)]
    assert list(util.ngrams(sequence, n=2, pad=False)) == [(0, 1), (1, 2), (2, 3)]

if __name__ == "__main__":
    test_chapter_1_sequences()
    test_chapter_1_sunny()
    test_chapter_10_eruptions_markov()
