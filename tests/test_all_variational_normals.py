# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
=============================
Demonstration of Gaussian HMM
=============================
"""
import numpy as np
import numpy.testing

import pandas as pd
pd.options.display.float_format = "{:.7f}".format
from hmm import GaussianVariationalHMM, MultivariateGaussianVariationalHMM, GMMVariationalHMM
from hmm.datasets.real import load_old_faithful_waiting, load_old_faithful_duration

from sklearn.mixture import BayesianGaussianMixture
def test_same_answers():
    waiting_times = load_old_faithful_waiting()

    models = {}
    data_2d = waiting_times.data

    model = GaussianVariationalHMM.GaussianVariationalHMM(n_components=3, n_iterations=30, verbose=False, n_inits=1, random_state=32)
    model.fit(waiting_times.data, waiting_times.lengths)

    print(pd.Series(model.pi_posterior_))
    print(pd.DataFrame(model.A_posterior_))
    print(pd.DataFrame(model.A_normalized_))
    print("means")
    print(pd.Series(model.means_posterior_))
    print("variances")
    print(pd.Series(model.variances_posterior_))
    print("stddev")
    print(pd.Series(np.sqrt(model.variances_posterior_)))
    print("a")
    print(pd.Series(model.a_posterior_))
    print("b")
    print(pd.Series(model.b_posterior_))
    print(data_2d.shape)

    mmodel = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=3, n_iterations=30, verbose=False, n_inits=1, random_state=32)
    mmodel.fit(waiting_times.data, waiting_times.lengths)
    print(pd.Series(mmodel.pi_posterior_))
    print(pd.DataFrame(mmodel.A_posterior_))
    print(pd.DataFrame(mmodel.A_normalized_))
    print("means")
    print(pd.DataFrame(mmodel.means_posterior_))
    print("variances")
    print(mmodel.covariances_posterior_)

    gmm = GMMVariationalHMM.GMMVariationalHMM(n_components=3, mixture_n_components=1, n_iterations=30, verbose=False, n_inits=1, random_state=32)
    gmm.fit(waiting_times.data, waiting_times.lengths)
    print(pd.Series(gmm.pi_posterior_))
    print(pd.DataFrame(gmm.A_posterior_))
    print(pd.DataFrame(gmm.A_normalized_))
    print("means")
    print(pd.Series(gmm.mixture_means_posterior_.ravel()))
    print("variances")
    print(gmm.mixture_covariances_posterior_)

    numpy.testing.assert_array_almost_equal(model.pi_posterior_.ravel(),        mmodel.pi_posterior_.ravel())
    numpy.testing.assert_array_almost_equal(model.A_posterior_.ravel(),         mmodel.A_posterior_.ravel())
    numpy.testing.assert_array_almost_equal(model.means_posterior_.ravel(),     mmodel.means_posterior_.ravel())
    numpy.testing.assert_array_almost_equal(model.variances_posterior_.ravel(), mmodel.covariances_posterior_.ravel())

    numpy.testing.assert_array_almost_equal(model.pi_posterior_.ravel(),        gmm.pi_posterior_.ravel())
    numpy.testing.assert_array_almost_equal(model.A_posterior_.ravel(),         gmm.A_posterior_.ravel())
    numpy.testing.assert_array_almost_equal(model.means_posterior_.ravel(),     gmm.mixture_means_posterior_.ravel())
    numpy.testing.assert_array_almost_equal(model.variances_posterior_.ravel(), gmm.mixture_covariances_posterior_.ravel())
if __name__ == "__main__":
    test_same_answers()
