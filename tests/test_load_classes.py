
from hmm import class_for_name
from hmm import CategoricalVariationalHMM
from hmm import CategoricalHMM
def test_load():

    to_load = [
        CategoricalVariationalHMM.CategoricalVariationalHMM,
        CategoricalHMM.CategoricalHMM
    ]

    for item in to_load:
        print(class_for_name.load(item.__name__))


    pass


if __name__ == "__main__":
    test_load()
