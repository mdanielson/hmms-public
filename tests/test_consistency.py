import logging
import pytest

from hmm import MultivariateGaussianVariationalHMM, GaussianVariationalMHMM, util, model_selection, GaussianVariationalHMM, GMMVariationalHMM
from hmm.datasets import models


def test_gaussian():
    random_state = 23
    model1, model2 = models.get_smyth_clustering_models(random_state=random_state)
    train_size = .5
    train_number = 20
    n_samples = int(train_number / train_size)

    train_data_smyth, test_data_smyth, \
            train_labels_smyth, test_labels_smyth, \
            train_lengths_smyth, test_lengths_smyth = util.sample_and_split(
                [model1, model2],
                n_samples=n_samples, sample_lengths=100, test_size=.5, random_state=random_state)
    model1 = GaussianVariationalHMM.GaussianVariationalHMM(
        n_components=4,
        n_inits=1,
        n_iterations=10,
        random_state=4,
    verbose=logging.INFO)
    model2 = GaussianVariationalMHMM.GaussianVariationalMHMM(
        n_mixture_components=1,
        n_components=4,
        n_iterations=10,
        random_state=4,
        verbose=logging.INFO)

    model3 = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(
        n_components=4,
        n_iterations=10,
        random_state=4,
        verbose=logging.INFO
    )
    model4 = GMMVariationalHMM.GMMVariationalHMM(
        n_components=4,
        mixture_n_components=1,
        n_iterations=10,
        random_state=4,
        verbose=logging.INFO
    )

    print("Variational HMM")
    model1.fit(train_data_smyth, train_lengths_smyth)
    print("Variational MHMM")
    model2.fit(train_data_smyth, train_lengths_smyth)
    print("Multivariate Gaussian MHMM")
    model3.fit(train_data_smyth, train_lengths_smyth)
    print("GMM")
    model4.fit(train_data_smyth, train_lengths_smyth)
    print(model1.score(train_data_smyth, train_lengths_smyth), model1.lower_bound_[-1])
    print(model2.score(train_data_smyth, train_lengths_smyth), model2.lower_bound_[-1])
    print(model3.score(train_data_smyth, train_lengths_smyth), model3.lower_bound_[-1])
    print(model4.score(train_data_smyth, train_lengths_smyth), model4.lower_bound_[-1])
    assert model1.lower_bound_[-1] == pytest.approx(model2.lower_bound_[-1], 1e-9)
    assert model1.lower_bound_[-1] == pytest.approx(model3.lower_bound_[-1], 1e-9)
    assert model1.lower_bound_[-1] == pytest.approx(model4.lower_bound_[-1], 1e-9)

    dic1 = model1.dic(train_data_smyth, train_lengths_smyth)
    dic2 = model2.dic(train_data_smyth, train_lengths_smyth)
    print(dic1)
    assert dic1 == pytest.approx(dic2, abs=1e-6)



if __name__ == "__main__":
    test_gaussian()
