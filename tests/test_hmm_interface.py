import logging
import numpy as np

import pandas as pd

import pytest

import sklearn.base
import sklearn.metrics

from hmm import distance

import all_hmms


def test_interface():
    for model_type, sample_model in all_hmms.SAMPLE_MODELS.items():
        print("Interface test: {}".format(model_type))

        (observations, hidden_states, lengths) = sample_model.sample(n_samples=10, length=1000, random_state=2018)

        kwargs_for_init = {}
        if hasattr(sample_model, "covariance_type"):
            reestimated = sample_model.__class__.reestimate_from_sequences(observations, hidden_states, lengths, covariance_type=sample_model.covariance_type)
            kwargs_for_init["covariance_type"] = sample_model.covariance_type
        else:
            reestimated = sample_model.__class__.reestimate_from_sequences(observations, hidden_states, lengths)
        # Fit Serial
        learned = sample_model.__class__(n_iterations=1000, n_components=len(set(hidden_states)), n_inits=1, random_state=123, **kwargs_for_init)
        learned.fit(observations, lengths)
        symmetric_distance_with_reestimated = distance.symmetric_model_distance(sample_model, learned, 500, random_state=2020)
        # Fit Parallel
        learned = sample_model.__class__(n_iterations=1000, n_components=len(set(hidden_states)), n_inits=2, random_state=123, **kwargs_for_init)
        learned.fit(observations, lengths)
        symmetric_distance_with_reestimated = distance.symmetric_model_distance(sample_model, reestimated, 500, random_state=2020)
        symmetric_distance_with_learned = distance.symmetric_model_distance(sample_model, learned, 500, random_state=2020)

        # Partial Fit / fit should behave similarly
        fit = sample_model.__class__(n_iterations=100, n_components=len(set(hidden_states)), n_inits=1, random_state=123, **kwargs_for_init)
        partial_fit = sample_model.__class__(n_iterations=1, n_components=len(set(hidden_states)), n_inits=1, random_state=123, **kwargs_for_init)

        fit.fit(observations, lengths)
        partial_fit.fit(observations, lengths)
        for i in range(99):
            partial_fit.partial_fit(observations, lengths)

        score_of_all_fit = fit.score(observations, lengths)
        score_of_all_partial_fit = partial_fit.score(observations, lengths)
        assert score_of_all_fit == pytest.approx(score_of_all_partial_fit, 1e-5)
        # Serialize models
        blob_learned = learned.to_dict()
        learned_copy = sample_model.from_dict(blob_learned)
        blob_reestimated = reestimated.to_dict()
        reestimated = sample_model.from_dict(blob_reestimated)

        # Models can call transform, and the results should be similar
        reestimated_transformed = reestimated.transform(observations, lengths)
        learned_transformed = learned.transform(observations, lengths)
        learned_copy_transformed = learned_copy.transform(observations, lengths)
        new_orig = sample_model.transform(observations, lengths)
        state_similarity = sklearn.metrics.adjusted_rand_score(reestimated_transformed, learned_transformed)
        assert state_similarity > 0.9, state_similarity
        state_similarity = sklearn.metrics.adjusted_rand_score(reestimated_transformed, learned_copy_transformed)
        assert state_similarity > 0.9, state_similarity

        # call score
        score_of_all = reestimated.score(observations, lengths)
        assert not np.isnan(score_of_all)
        assert not np.isinf(score_of_all)
        score_of_all = learned.score(observations, lengths)
        assert not np.isnan(score_of_all)
        assert not np.isinf(score_of_all)
        score_of_all = learned_copy.score(observations, lengths)
        assert not np.isnan(score_of_all)
        assert not np.isinf(score_of_all)


        # call score
        scored = reestimated.score_samples(observations, lengths)
        assert scored.shape == lengths.shape
        assert not np.all(np.isnan(scored))
        assert not np.all(np.isinf(scored))
        scored = learned.score_samples(observations, lengths)
        assert scored.shape == lengths.shape
        assert not np.all(np.isnan(scored))
        assert not np.all(np.isinf(scored))
        scored = learned_copy.score_samples(observations, lengths)
        assert scored.shape == lengths.shape
        assert not np.all(np.isnan(scored))
        assert not np.all(np.isinf(scored))

        if hasattr(learned_copy, "aic"):
            for model in [learned_copy, reestimated]:
                aic = learned_copy.aic(observations, lengths)
                assert not np.isnan(aic), aic
                assert not np.isinf(aic), aic

                bic = model.bic(observations, lengths)
                assert not np.isnan(bic), bic
                assert not np.isinf(bic), bic

                caic = model.caic(observations, lengths)
                assert not np.isnan(caic), caic
                assert not np.isinf(caic), caic
        if hasattr(learned_copy, "dic"):
            for model in [learned_copy, reestimated]:
                dic = learned_copy.dic(observations, lengths)
                assert not np.isnan(dic), dic
                assert not np.isinf(dic), dic
                dic, pd = learned_copy.verbose_dic(observations, lengths)
                assert not np.any(np.isnan(dic)), dic
                assert not np.any(np.isinf(dic)), dic
                assert not np.any(np.isnan(pd)), pd
                assert not np.any(np.isinf(pd)), pd

if __name__ == "__main__":
    test_interface()

