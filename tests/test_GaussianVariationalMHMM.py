# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import base64
import logging
import pickle

import numpy as np
import pandas as pd
import pytest
import sklearn.metrics
import sklearn.utils

from hmm import (GaussianHMM,
                 GaussianVariationalHMM,
                 GaussianVariationalMHMM)
from hmm.datasets import for_tests
from hmm.util import sample_and_split
from hmm.scoring import cluster_report


N_INITS=10

def test_normals_simple():
    random_state = sklearn.utils.check_random_state(1)
    observed_sequences, hidden_sequences, lengths = for_tests.get_gaussian_mcgrory_and_titterington_sequences(1, 500, random_state)
    trainer = GaussianVariationalMHMM.GaussianVariationalMHMM(n_mixture_components=1, n_components=4, random_state=random_state, implementation="scaling", tol=1e-6)
    trainer.fit(observed_sequences, lengths)
    print(trainer.transform(observed_sequences, lengths))
    print("after fit")
    print(trainer.means_posterior_)
    print(trainer.beta_posterior_)
    print(trainer.beta_prior_)
    print("ASDF")
    print(trainer.variances_posterior_)
    print("a", trainer.a_posterior_)
    print("b", trainer.b_posterior_)
    print()


def test_dic():

    random_state = sklearn.utils.check_random_state(21)
    observed_sequences, hidden_sequences, lengths = for_tests.get_gaussian_mcgrory_and_titterington_sequences(1, 500, random_state)
    observed_sequences = np.asarray(observed_sequences)
    print(lengths)
    trainers_mix = {}
    dics_mix = {}
    pds_mix = {}
    dic_values_mix = []

    trainers = {}
    dics = {}
    pds = {}
    dic_values = []
    for i in range(1, 8):
        trainer = GaussianVariationalMHMM.GaussianVariationalMHMM(n_mixture_components=1, n_components=i, n_iterations=1000, n_inits=1, random_state=12)  # , verbose=logging.DEBUG)
        trainer.fit(observed_sequences, lengths)
        trainers_mix[i] = trainer
        pds_mix[i], dics_mix[i] = trainer.verbose_dic(observed_sequences, lengths)
        dic_values_mix.append(trainer.dic(observed_sequences, lengths))
        trainer2 = GaussianVariationalHMM.GaussianVariationalHMM(n_components=i, n_iterations=1000, n_inits=1, random_state=12)  # , verbose=logging.DEBUG)
        trainers[i] = trainer
        pds[i], dics[i] = trainer.verbose_dic(observed_sequences, lengths)
        dic_values.append(trainer.dic(observed_sequences, lengths))
        assert pds[i] == pds_mix[i]
        assert dics[i] == dics_mix[i]

    for i, m in trainers.items():
        print(i, pds[i].mean(),  pds_mix[i].mean(), dics[i].mean(), dics_mix[i].mean())

    assert np.argmin(dic_values) == 3, dic_values


def test_dic2():

    random_state = sklearn.utils.check_random_state(21)
    train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = for_tests.get_smyth_data(random_state)
    trainers_mix = {}
    dics_mix = {}
    pds_mix = {}
    dic_values_mix = {}

    for k in range(1, 4):
        for n in range(1, 4):
            trainer = GaussianVariationalMHMM.GaussianVariationalMHMM(n_mixture_components=k, n_components=n, n_iterations=1000, n_inits=4, random_state=12)  # , verbose=logging.DEBUG)
            trainer.fit(train_observations, train_lengths)
            trainers_mix[(k, n)] = trainer
            pds_mix[(k, n)], dics_mix[(k, n)] = trainer.verbose_dic(train_observations, train_lengths)
            dic_values_mix[(k,n)] = trainer.dic(train_observations, train_lengths)
            print(k, n, pds_mix[(k, n)].sum(axis=0), dic_values_mix[(k,n)], trainer.score(test_observations, test_lengths))
    dic_values_mix = pd.Series(dic_values_mix)
    print(dic_values_mix)
    print(dic_values_mix.idxmin())
    assert dic_values_mix.idxmin() == (2, 2)


def test_smyth_extended():
    for impl in ["scaling", "log"]:
        random_state = sklearn.utils.check_random_state(1983)
        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = for_tests.get_smyth_extended(random_state)
        mixture = GaussianVariationalMHMM.GaussianVariationalMHMM(
            n_mixture_components=4,
            n_components=3,
            n_iterations=500,
            implementation=impl,
            n_inits=N_INITS,
            random_state=random_state,
        )

        mixture.fit(train_observations, train_lengths)
        predicted = mixture.predict(test_observations, test_lengths)
        print("predicted", predicted)
        cluster_report(test_labels, predicted)
        for i in range(mixture.n_mixture_components):
            print("cluster", i)
            print(mixture.mixture_weights_normalized_[i])
            print(pd.DataFrame(mixture.A_normalized_[i]))
            print(pd.DataFrame({"means": mixture.means_posterior_[i], "variances": mixture.variances_posterior_[i]}))
        print(pd.crosstab(test_labels, predicted))
        print(sklearn.metrics.normalized_mutual_info_score(test_labels, predicted))
        assert sklearn.metrics.normalized_mutual_info_score(test_labels, predicted) >= 0.9657722409004419


if __name__ == "__main__":
    test_normals_simple()
