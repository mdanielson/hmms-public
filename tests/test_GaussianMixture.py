# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import pytest
import sklearn.metrics
import sklearn.mixture
import sklearn.utils

from hmm.mixtures import GaussianMixtureModel
from hmm.utilities import normal


def test_worksheet():

    data = []
    random_state = sklearn.utils.check_random_state(12)

    normal1 = normal.Normal(5, 2, random_state=random_state)
    normal2 = normal.Normal(10, 2, random_state=random_state)

    for i in range(100):
        data.append(normal1.rvs())
        data.append(normal2.rvs())
    data = np.asarray(data)
    mm = GaussianMixtureModel.GaussianMixtureModel(2, random_state=random_state)
    mm.fit(data[:, None])
    print(mm.means_)
    print(mm.variances_)
    print(mm.weights_)
    print(mm.lower_bounds_)
    sorting = np.argsort(mm.means_, axis=0)
    print(sorting)
    assert mm.means_[sorting[0]] == pytest.approx(4.70715808, 1e-5)
    assert mm.means_[sorting[1]] == pytest.approx(9.7715329, 1e-5)
    assert mm.variances_[sorting[0]] == pytest.approx(2.24139827, 1e-5)
    assert mm.variances_[sorting[1]] == pytest.approx(2.14451393, 1e-5)
    assert mm.weights_[sorting[0]] == pytest.approx(0.505, 1e-4)
    assert mm.weights_[sorting[1]] == pytest.approx(0.495, 1e-4)

    print(len(mm.lower_bounds_))
    # members = [0, 1] * 100
    # memberships = mm.predict(data[:, None])
    # print(memberships)
    # print(members)
    # members[25] = 0
    # members[84] = 1
    # members[108] = 1
    # members[110] = 0
    # members[136] = 0
    # print(len(members))
    # print(len(memberships))
    # for i in range(200):
    #     assert np.all(memberships[i] == members[i]) , (i, memberships[i], members[i])
    #assert mm.score_samples(data[:, None]).sum() == pytest.approx(mm.lower_bounds_[-1], 1e-6)
    #assert mm.score(data[:, None]) == pytest.approx(-2.9205311685516326, 1e-6)
    #assert model_selection.bayesian_information_criterion(mm, data[:, None]) == pytest.approx(65.31837865003814, 1e-6)
    #assert model_selection.akaike_information_criterion(mm) == pytest.approx(64.410623371056, 1e-6)

    mm = GaussianMixtureModel.GaussianMixtureModel(1, random_state=random_state)
    mm.fit(data[:, None])

    print(mm.means_)
    print(mm.weights_)
    print(mm.lower_bounds_)
    assert mm.weights_[0] == 1
    assert mm.means_[0] == pytest.approx(data.mean(), 1e-5)
    assert mm.variances_[0] == pytest.approx(data.var(), 1e-5)

    print(mm.bic(data[:, None]))
    print(mm.aic(data[:, None]))

    data = mm.sample(100, random_state=4343)
    assert data.mean() == pytest.approx(6.82968134463064, 1e-2), data.mean()


def test_going_to_zero():

    random_state = sklearn.utils.check_random_state(12)
    ans = [0] * 100 + [1] * 120 + [2] * 200
    data1 = [int(i) for i in random_state.normal(10, 5, 100)]
    data2 = [5] * 100 + [6]*10 + [7]*10
    data3 = [0] * 200
    data = np.concatenate([data1, data2, data3]).astype(float)
    data2d = data[:, None]

    model = GaussianMixtureModel.GaussianMixtureModel(n_components=3, random_state=random_state)
    model.fit(data2d)
    print(model.means_)

    new = sklearn.mixture.GaussianMixture(n_components=3, random_state=random_state)
    new.fit(data2d)
    print(new.means_)
    sorting = np.argsort(model.means_, axis=0)
    print(sorting)
    assert model.means_[sorting[0]] == pytest.approx(0, 1e-6)
    assert model.means_[sorting[1]] == pytest.approx(5, 1e-6)
    assert model.means_[sorting[2]] == pytest.approx(8.66929007, 1e-5)
    assert model.variances_[sorting[0]] == 1e-6
    assert model.variances_[sorting[1]] == 1e-6
    assert model.variances_[sorting[2]] == pytest.approx(23.33476635)

    assert model.score(data2d) == pytest.approx(2.3954148216326683, 1e-6)
    assert model.aic(data2d) == pytest.approx(-1996.1484501714413, 1e-6)
    assert model.bic(data2d) == pytest.approx(-1963.826412481222, 1e-6)
    predicted = model.predict(data2d)
    assert sklearn.metrics.mutual_info_score(ans, predicted) == pytest.approx(0.8710427676855432, 1e-6)

    print(model.means_)
    print(model.variances_)
    print(new.means_)
    print(new.covariances_)
    print(model.score(data2d))
    print(new.score(data2d))
    print(model.aic(data2d))
    print(new.aic(data2d))
    print(model.bic(data2d))
    print(new.bic(data2d))


def test_multivariate():
    random_state = sklearn.utils.check_random_state(12)
    data1 = random_state.multivariate_normal([-1, 1], [[1, 0], [0, 1]], 500)
    data2 = random_state.multivariate_normal([-2, 2], [[1, 1], [1, 1]], 500)
    data3 = random_state.multivariate_normal([4, 0], [[5, 1], [1, 5]], 500)
    data = np.concatenate([data1, data2, data3]).astype(np.float64)

    model = GaussianMixtureModel.GaussianMixtureModel(n_components=3, random_state=random_state)
    model.fit(data)
    print(model.lower_bounds_)
    for i in range(model.n_components):
        print(i)
        print(model.means_[i])
        print(model.variances_[i])
        print()


def test_multivariate2():
    random_state = sklearn.utils.check_random_state(12)
    data1 = random_state.multivariate_normal([-1, 1], [[1, 0], [0, 1]], 50)
    data2 = random_state.multivariate_normal([-2, 2], [[1, 1], [1, 1]], 50)
    data3 = random_state.multivariate_normal([4, 0], [[5, 1], [1, 5]], 50)
    data = np.concatenate([data1, data2, data3]).astype(np.float64)

    model = GaussianMixtureModel.GaussianMixtureModel(n_components=3, n_iterations=0, random_state=random_state)
    model.fit(data)
    print(model.lower_bounds_)
    for i in range(model.n_components):
        print(i)
        print(model.means_[i])
        print(model.variances_[i])
        print()


if __name__ == "__main__":
    test_worksheet()
    test_going_to_zero()
    #test_multivariate()
