# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import numpy as np
import pandas as pd
import pytest
import sklearn.utils

from hmm import PoissonHMM, PoissonVariationalHMM
from hmm.mixtures import PoissonMixtureModel, VariationalPoissonMixtureModel

def test_simple():

    rs = sklearn.utils.check_random_state(123324)
    rates_ = np.asarray([5, 10, 15])
    pi = np.asarray([.4, .3, .3])
    A = np.asarray([[.4, .3, .3],
                    [.2, .5, .3],
                    [.2, .2, .6]])
    model = PoissonHMM.PoissonHMM(init_pi=False, init_A=False, init_emissions=False)
    model.pi_ = pi
    model.A_ = A
    model.rates_ = rates_

    observations, states, lengths = model.sample(10, 1000, random_state=rs)
    mixture = PoissonMixtureModel.PoissonMixtureModel(n_components=3, n_iterations=100, random_state=rs)
    mixture.fit(observations.ravel()[:, None])
    new = PoissonHMM.PoissonHMM(n_components=3, n_iterations=1000, random_state=rs)
    new.fit(observations, lengths)
    check_pi = np.asarray([0.497448839907874, 0.13024339821988754, 0.37230776187223835])
    check_A = np.asarray([[0.5025083142054838, 0.17467942440371234, 0.3228122613908039], [0.256101580964683, 0.4042511761404539, 0.33964724289486314], [0.19954380642984074, 0.20183409416115067, 0.5986220994090086]])
    check_rates = np.asarray([9.597844598020842, 4.877594657285684, 14.761398868323433])
    print(new.pi_.tolist())
    print(new.A_.tolist())
    print(new.rates_.tolist())
    check(
        check_pi, new.pi_,
        check_A, new.A_,
        check_rates, new.rates_
    )
    #assert sorted(new.rates_) == pytest.approx(sorted([9.597844598020842, 4.877594657285684, 14.761398868323433]), abs=1e-3)
    new4 = PoissonHMM.PoissonHMM(n_components=4, n_iterations=1000, random_state=rs)
    new4.fit(observations, lengths)
    print(new4.pi_.tolist())
    print(new4.A_.tolist())
    print(new4.rates_.tolist())
    check_pi = np.asarray([0.1282912049181092, 0.5054797398138375, 0.36622905526805316, 2.973878546975322e-39])
    check_A = np.asarray([[0.4069135325406443, 0.25256257350261446, 0.1905541648821798, 0.1499697290745614], [0.15246657130733215, 0.4649010879304283, 0.37943284453313625, 0.003199496229103405], [0.28263686754300893, 0.2059553785809267, 0.2689482692039999, 0.24245948467206457], [0.06870804450458838, 0.17883368065527883, 0.4749712752781258, 0.2774869995620069]])
    check_rates = np.asarray([4.878085142689413, 9.456549867642517, 14.183592931982918, 15.511135487061388])
    check(
        check_pi, new4.pi_,
        check_A, new4.A_,
        check_rates, new4.rates_,
        abs_tol=.1
    )
    variational = PoissonVariationalHMM.PoissonVariationalHMM(n_components=3, n_iterations=1000, random_state=rs)
    variational.fit(observations, lengths)
    print(variational.pi_posterior_.tolist())
    print(variational.A_posterior_.tolist())
    print(variational.rates_posterior_.tolist())
    print(variational.b_posterior_.tolist())
    check_pi = np.asarray([3.9305829881371057, 1.5152245929519572, 5.554192418910979])
    check_A = np.asarray([[2706.5151766601675, 917.3757752125583, 891.3665574546744], [829.9526012741281, 988.7690552882677, 619.0308555820758], [980.1870352734068, 531.4584417750709, 1528.3445014796803]])
    check_rates = np.asarray([14.754599131875962, 4.891217861987989, 9.606297796273797])
    check_bs = np.asarray([4520.252062862508, 2438.785163535524, 3043.9627736020007])
    check(
        check_pi, variational.pi_posterior_,
        check_A, variational.A_posterior_,
        check_rates, variational.rates_posterior_,
        abs_tol=1e-1
    )
    reestimated = PoissonVariationalHMM.PoissonVariationalHMM.reestimate_from_sequences(observations, states, lengths)
    print(reestimated.rates_posterior_.tolist())
    rates_ = [4.98671875, 9.919874804381847, 14.928621908127209]
    assert reestimated.rates_posterior_ == pytest.approx(rates_, 1e-6)


def check(pi, check_pi, A, check_A, rates, check_rates, bs=None, check_bs=None, abs_tol=1e-3):
    sort = np.argsort(pi)
    check_sort = np.argsort(check_pi)
    for i,j in zip(check_sort, sort):
        assert check_pi[i] == pytest.approx(pi[j], abs=abs_tol)
        print(check_A[i][check_sort])
        print(A[j][sort])
        assert check_A[i][check_sort] == pytest.approx(A[j][sort], abs=abs_tol)
        assert check_rates[i] == pytest.approx(rates[j], abs=abs_tol)
        if check_bs is not None and bs is not None:
            print(bs[sort])
            print(check_bs[check_sort])
            assert check_bs[check_sort].ravel() == pytest.approx(bs[sort].ravel(), abs=abs_tol)



if __name__ == "__main__":
    test_simple()
