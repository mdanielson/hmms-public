# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import pandas as pd
import pytest
import sklearn.utils

from hmm.mixtures import PoissonMixtureModel

def test_worksheet():

    actual_memberships = np.asarray([0, 1, 0, 1, 1, 0, 1, 1, 1, 1])
    data = {
            "D1": 1,
            "D2": 12,
            "D3": 3,
            "D4": 11,
            "D5": 15,
            "D6": 2,
            "D7": 9,
            "D8": 17,
            "D9": 20,
            "D10": 16
    }
    data = pd.Series(data)

    mm = PoissonMixtureModel.PoissonMixtureModel(2, init_emissions=False, init_weights=False, random_state=12)
    mm.rates_ = np.asarray([7, 8])
    mm.weights_ = np.asarray([.4, .6])
    mm.fit(data.values[:, None])
    print(mm.rates_)
    print(mm.weights_)
    print(mm.lower_bounds_)
    assert mm.rates_[0] == pytest.approx(2.00335509626, 1e-5)
    assert mm.rates_[1] == pytest.approx(14.27974374, 1e-5)
    assert mm.weights_[0] == pytest.approx(.29975, 1e-4)
    assert mm.weights_[1] == pytest.approx(.700258, 1e-4)

    memberships = mm.predict(data.values[:, None])
    print(memberships)
    print(mm.predict_proba(data.values[:, None]))
    print(len(mm.lower_bounds_))
    assert np.all(memberships == actual_memberships)
    assert mm.score_samples(data.values[:, None]).sum() == pytest.approx(mm.lower_bounds_[-1], 1e-6)
    assert mm.score(data.values[:, None]) == pytest.approx(-2.9205311685516326, 1e-6)
    assert mm.bic(data.values[:, None]) == pytest.approx(65.31837865003814, 1e-6)
    assert mm.aic(data.values[:, None]) == pytest.approx(64.410623371056, 1e-6)

    mm = PoissonMixtureModel.PoissonMixtureModel(1, random_state=12)
    mm.fit(data.values[:, None])

    print(mm.rates_)
    print(mm.weights_)
    print(mm.lower_bounds_)
    assert mm.weights_[0] == 1
    assert mm.rates_[0] == pytest.approx(data.values.mean(), 1e-5)

    print(mm.bic(data.values[:, None]))
    print(mm.aic(data.values[:, None]))

    data = mm.sample(100)
    print(data)
    assert data.mean() == pytest.approx(10.74, 1e-2)


    rs = sklearn.utils.check_random_state(234)
    data = {
        "D1": 1,
        "D2": 12,
        "D3": 3,
        "D4": 11,
        "D5": 15,
        "D6": 2,
        "D7": 9,
        "D8": 17,
        "D9": 20,
        "D10": 16
    }
    data = pd.Series(data)


if __name__ == "__main__":
    test_worksheet()
