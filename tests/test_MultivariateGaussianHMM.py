# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import hmmlearn.hmm
import numpy as np
import pytest
import scipy.stats
import sklearn.utils

from hmm import MultivariateGaussianHMM, distance


def test_normals_simple():
    hidden_states = np.asarray([0]*100 + [1] * 100)
    random_state = sklearn.utils.check_random_state(1)
    observed = np.concatenate([random_state.normal(5, np.sqrt(5), size=100), random_state.normal(10, 1, size=100)])
    observed = observed[:, None]
    new = MultivariateGaussianHMM.MultivariateGaussianHMM.reestimate_from_sequences(observed, hidden_states, [200])
    print(new.pi_)
    print(new.A_)
    print(new.means_)
    print(new.covariances_)
    assert new.pi_[0] == 1
    assert new.pi_[1] == 0
    assert new.A_[0, 0] == pytest.approx(0.99, 1e-6)
    assert new.A_[1, 1] == pytest.approx(1, 1e-6)
    assert new.means_[0] == pytest.approx(5.13546738, 1e-6)
    assert new.means_[1] == pytest.approx(10.15279478, 1e-6)
    assert new.covariances_[0][0] == pytest.approx(3.9570784, 1e-6)
    assert new.covariances_[1][0] == pytest.approx(0.87738822, 1e-6)

    trainer = new._new_trainer()
    assert trainer.emissions.density_for(0, np.asarray([5.])) == pytest.approx(0.20008553505858093, 1e-6)
    assert trainer.emissions.log_density_for(0, np.asarray([5.])) == pytest.approx(np.log(0.20008553505858093), 1e-6)


def test_normals_not_simple():
    hidden_states = np.asarray(
        [0]*100 + [1] * 100 + [1]*100 + [0] * 100,
    )

    random_state = sklearn.utils.check_random_state(1)
    observed = np.concatenate([
        np.concatenate([
            random_state.multivariate_normal([0, 4], [[2, 0], [0, 2]], size=100),
            random_state.multivariate_normal([2, 2], [[1, 2], [2, 1]], size=100),
        ]
        ),
        np.concatenate([
            random_state.multivariate_normal([2, 2], [[1, 2], [2, 1]], size=100),
            random_state.multivariate_normal([0, 4], [[2, 1], [1, 2]], size=100),
        ]
        )
    ]
    )
    lengths = np.asarray([200, 200])
    new = MultivariateGaussianHMM.MultivariateGaussianHMM.reestimate_from_sequences(observed, hidden_states, lengths)
    print(new.pi_)
    print(new.A_)
    print(new.means_)
    print(new.covariances_)
    assert new.pi_[0] == .5
    assert new.pi_[1] == .5
    assert new.A_[0, 0] == pytest.approx(0.9949748, 1e-6)
    assert new.A_[1, 1] == pytest.approx(0.9949748, 1e-6)
    assert new.means_[0] == pytest.approx([0.23102236, 3.99547887], 1e-6)
    assert new.means_[1] == pytest.approx([2.12027811, 1.95180708], 1e-6)
    covar = [[[1.65106487, 0.4633164],
              [0.4633164,  2.06417026]],

             [[2.25585878, 1.0025886],
              [1.0025886,  1.98238376]]]

    assert new.covariances_[0][0] == pytest.approx(covar[0][0], 1e-6)
    assert new.covariances_[0][1] == pytest.approx(covar[0][1], 1e-6)
    assert new.covariances_[1][0] == pytest.approx(covar[1][0], 1e-6)
    assert new.covariances_[1][1] == pytest.approx(covar[1][1], 1e-6)


def test_normals_two_state_one_dimension():

    for impl in ["scaling", "log"]:
        print(impl)
        hidden_states = np.asarray([0]*100 + [1] * 100)
        random_state = sklearn.utils.check_random_state(1)
        observed = np.concatenate([random_state.normal(5, np.sqrt(5), size=100), random_state.normal(10, 1, size=100)])
        observed = observed[:, None]
        lengths = np.asarray([200])
        new = MultivariateGaussianHMM.MultivariateGaussianHMM.reestimate_from_sequences(observed, hidden_states, lengths)
        print(new.pi_)
        print(new.A_)
        print(new.means_)
        print(new.covariances_)
        assert new.pi_[0] == 1
        assert new.pi_[1] == 0
        assert new.A_[0, 0] == pytest.approx(0.99, 1e-6)
        assert new.A_[1, 1] == pytest.approx(1, 1e-6)
        assert new.means_[0] == pytest.approx(5.13546738, 1e-6)
        assert new.means_[1] == pytest.approx(10.15279478, 1e-6)
        assert new.covariances_[0][0] == pytest.approx(3.9570784, 1e-6)
        assert new.covariances_[1][0] == pytest.approx(0.87738822, 1e-6)

        observed_sequences, hidden_sequences, lengths = new.sample(1000, 50, random_state=random_state)
        print("Mean of data")
        print(np.mean(observed_sequences.ravel()))

        trainer = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=2,  n_iterations=10, implementation=impl, n_inits=1, verbose=logging.DEBUG, random_state=random_state)
        trainer.fit(observed_sequences, lengths)
        print(trainer.pi_.tolist())
        print(trainer.A_.tolist())
        print(trainer.means_.tolist())
        print(trainer.covariances_.tolist())
        check_pi = [2.5657288586593945e-09, 0.9999999974342711]
        check_A = [[0.9999988146990052, 1.1853009948540221e-06], [0.009295753092426489, 0.9907042469075735]]
        check_means = [[10.162760245706744], [5.139510149953236]]
        check_covariances = [[[0.8593276631070011]], [[3.94980893569169]]]
        check(
            trainer.pi_, check_pi,
            trainer.A_, check_A,
            trainer.means_, check_means,
            trainer.covariances_, check_covariances
        )


def test_normals_two_state_full_covariance():
    hidden_states = np.asarray([0]*100 + [1] * 100 +  [1]*100 + [0] * 100)

    for impl in ["scaling", "log"]:
        print(impl)
        random_state = sklearn.utils.check_random_state(1)
        observed = np.concatenate(
            [
                np.concatenate([
                    random_state.multivariate_normal([0, 4], [[2, 0], [0, 2]], size=100),
                    random_state.multivariate_normal([2, 2], [[2, 1], [1, 2]], size=100),
                ]),
                np.concatenate([
                    random_state.multivariate_normal([2, 2], [[2, 2], [2, 2]], size=100),
                    random_state.multivariate_normal([0, 4], [[2, 0], [0, 2]], size=100),
                ])
            ]
        )
        print(observed.shape)
        lengths = np.asarray([200, 200])
        new = MultivariateGaussianHMM.MultivariateGaussianHMM.reestimate_from_sequences(observed, hidden_states, lengths)
        print("pi")
        print(new.pi_.tolist())
        print("A")
        print(new.A_.tolist())
        print("means")
        print(new.means_.tolist())
        print("covars")
        print(new.covariances_.tolist())
        assert new.pi_[0] == .5
        assert new.pi_[1] == .5
        assert new.A_[0, 0] == pytest.approx(0.9949748, 1e-6)
        assert new.A_[1, 1] == pytest.approx(0.9949748, 1e-6)
        assert new.means_[0] == pytest.approx([0.11661011, 3.92472611], 1e-6)
        assert new.means_[1] == pytest.approx([2.0112638, 2.05756857], 1e-6)
        covars = [[[1.86039179, -0.25860461],
                   [-0.25860461,  1.80915919]],
                  [[1.95876983,  1.54369133],
                   [1.54369133,  2.15056654]]]
        assert new.covariances_[0][0] == pytest.approx(covars[0][0], 1e-6)
        assert new.covariances_[0][1] == pytest.approx(covars[0][1], 1e-6)
        assert new.covariances_[1][0] == pytest.approx(covars[1][0], 1e-6)
        assert new.covariances_[1][1] == pytest.approx(covars[1][1], 1e-6)

        observed_sequences, hidden_sequences, lengths = new.sample(1000, 50, random_state=random_state)
        print("Fitting")
        trainer = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=2,  n_iterations=50, covariance_type="full", implementation=impl, verbose=logging.DEBUG, random_state=random_state, tol=1e-6)
        trainer.fit(observed_sequences, lengths)

        print("pi")
        print(trainer.pi_.tolist())
        print("A")
        print(trainer.A_.tolist())
        print("means")
        print(trainer.means_.tolist())
        print("covars")
        print(trainer.covariances_.tolist())

        check_pi = [0.4835930655445203, 0.5164069344554797]
        check_A = [[0.9951672061889895, 0.004832793811010489], [0.005866341445005132, 0.9941336585549949]]
        check_means = [[0.10963439623377572, 3.928650441554377], [2.0042087608632886, 2.0612117833533814]]
        check_covariances = [[[1.8467239182384985, -0.25963536163394857], [-0.2596353616339487, 1.8172258030651123]], [[1.9389772351202084, 1.543375647798909], [1.543375647798909, 2.1596941216238648]]]
        check(
            trainer.pi_, check_pi,
            trainer.A_, check_A,
            trainer.means_, check_means,
            trainer.covariances_, check_covariances
        )


def test_normals_two_state_tied_covariance():
    hidden_states = np.asarray([0]*100 + [1] * 100 + [1]*100 + [0] * 100)

    for impl in ["log", "scaling", "log"]:
        print(impl)
        random_state = sklearn.utils.check_random_state(1)
        covariance = [[2, 1], [1, 2]]
        data1 = random_state.multivariate_normal([0, 4], covariance, size=100)
        data2 = random_state.multivariate_normal([2, 4], covariance, size=100)
        print(np.cov(data1.T))
        print(np.cov(data2.T))
        observed = np.concatenate([
            np.concatenate([
                data1, data2
            ]
            ),
            np.concatenate([
                data2,
                data1
            ]
            )
        ]
        )
        lengths = np.asarray([200, 200])
        new = MultivariateGaussianHMM.MultivariateGaussianHMM.reestimate_from_sequences(observed, hidden_states, lengths, "tied")
        print("pi")
        print(new.pi_.tolist())
        print("A")
        print(new.A_.tolist())
        print("means")
        print(new.means_.tolist())
        print("covars")
        print(new.covariances_.tolist())
        assert new.pi_[0] == .5
        assert new.pi_[1] == .5
        assert new.A_[0, 0] == pytest.approx(0.9949748, 1e-6)
        assert new.A_[1, 1] == pytest.approx(0.9949748, 1e-6)
        check_means = np.asarray([[-0.26820626, 3.71301607],
                       [2.04680696,  4.13941649]])
        check_covars = np.asarray([[1.6851681455923992, 0.9210576856975996], [0.9210576856975996, 2.0707351953012942]])
        assert new.means_ == pytest.approx(check_means, 1e-6)
        assert new.covariances_ == pytest.approx(check_covars, 1e-6)

        observed_sequences, hidden_sequences, lengths = new.sample(1000, 50, random_state=random_state)
        print("Fitting")
        trainer = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=2,  n_iterations=50, covariance_type="tied", implementation=impl, verbose=logging.DEBUG, random_state=random_state)
        trainer.fit(observed_sequences, lengths)
        print("pi")
        print(trainer.pi_.tolist())
        print("A")
        print(trainer.A_.tolist())
        print("means")
        print(trainer.means_.tolist())
        print("covars")
        print(trainer.covariances_.tolist())

        check_pi = np.asarray([0.498702476173293, 0.5012975238267071])
        check_A = np.asarray([[0.9950786962331084, 0.004921303766891584], [0.006016569396023093, 0.9939834306039769]])
        check_means = np.asarray([[-0.2760868899053676, 3.7154625816618085], [2.0430376356622473, 4.134413678099696]])
        check_covariances = np.asarray([[1.6715765230314519, 0.9188696734588012], [0.9188696734588006, 2.0693312760213742]])

        check(
            trainer.pi_, check_pi,
            trainer.A_, check_A,
            trainer.means_, check_means,
            trainer.covariances_, check_covariances
        )


def test_normals_two_state_diagonal_covariance():
    hidden_states = np.asarray([0]*500 + [1] * 500 + [1]*500 + [0] * 500)

    for impl in ["scaling", "log"]:
        random_state = sklearn.utils.check_random_state(1)
        observed = np.concatenate([
            np.concatenate([
                random_state.multivariate_normal([0, 4], [[3, 0], [0, 2]], size=500),
                random_state.multivariate_normal([2, 2], [[1, 0], [0, 2]], size=500),
            ]
            ),
            np.concatenate([
                random_state.multivariate_normal([2, 2], [[1, 0], [0, 2]], size=500),
                random_state.multivariate_normal([0, 4], [[3, 0], [0, 2]], size=500),
            ]
            )
        ]
        )
        lengths = np.asarray([1000, 1000])
        new = MultivariateGaussianHMM.MultivariateGaussianHMM.reestimate_from_sequences(observed, hidden_states, lengths, covariance_type="diagonal")
        print("pi")
        print(new.pi_.tolist())
        print("A")
        print(new.A_.tolist())
        print("means")
        print(new.means_.tolist())
        print("covars")
        print(new.covariances_.tolist())
        assert new.pi_[0] == .5
        assert new.pi_[1] == .5
        assert new.A_[0, 0] == pytest.approx(0.998998998998999, 1e-6)
        assert new.A_[1, 1] == pytest.approx(0.998998998998999, 1e-6)
        assert new.means_[0] == pytest.approx([0.04696561, 4.02163123], 1e-6)
        assert new.means_[1] == pytest.approx([2.00285728, 2.00325211], 1e-6)

        covars = [[[2.93970135, 0.],
                   [0., 2.00513239]],
                  [[0.97124665, 0.],
                   [0., 2.07402978]]]

        assert new.covariances_[0][0] == pytest.approx(covars[0][0], 1e-6)
        assert new.covariances_[0][1] == pytest.approx(covars[0][1], 1e-6)
        assert new.covariances_[1][0] == pytest.approx(covars[1][0], 1e-6)
        assert new.covariances_[1][1] == pytest.approx(covars[1][1], 1e-6)

        observed_sequences, hidden_sequences, lengths = new.sample(100, 50, random_state=random_state)
        print(observed_sequences.shape)

        print("Fitting")
        trainer = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=2,  n_iterations=500, covariance_type="diagonal", implementation=impl, verbose=logging.DEBUG, random_state=random_state)
        print(observed.shape)
        trainer.fit(observed_sequences, lengths)
        states = trainer.transform(observed_sequences, lengths)
        print("pi")
        print(trainer.pi_.tolist())
        print("A")
        print(trainer.A_.tolist())
        print("means")
        print(trainer.means_.tolist())
        print("covars")
        print(trainer.covariances_.tolist())
        # pi doesn't seem right
        check_pi = [0.50941036, 0.49058964]
        check_A = [[9.99201901e-01, 7.98099281e-04],
                  [1.82498963e-03, 9.98175010e-01]]
        check_means = [[1.98820824, 1.98814958],
                      [0.03158776, 4.01720438]]
        check_covariances = [[[0.98746766, 0.],
                              [0.,         2.04128111]],

                             [[3.05476244, 0.],
                              [0.,         1.98175566]]]

        check(
            trainer.pi_, check_pi,
            trainer.A_, check_A,
            trainer.means_, check_means,
            trainer.covariances_, check_covariances
        )


def test_normals_spherical_covariance():
    hidden_states = np.asarray([0]*500 + [1] * 500 + [1]*500 + [0] * 500)

    for impl in ["scaling", "log"]:
        random_state = sklearn.utils.check_random_state(1)
        observed = np.concatenate([
            np.concatenate([
                random_state.multivariate_normal([0, 4], [[3, 0], [2, 0]], size=500),
                random_state.multivariate_normal([2, 2], [[1, 0], [0, 2]], size=500),
            ]
            ),
            np.concatenate([
                random_state.multivariate_normal([2, 2], [[1, 0], [1, 0]], size=500),
                random_state.multivariate_normal([0, 4], [[3, 0], [0, 1]], size=500),
            ]
            )
        ]
        )
        lengths = np.asarray([1000, 1000])
        new = MultivariateGaussianHMM.MultivariateGaussianHMM.reestimate_from_sequences(observed, hidden_states, lengths, covariance_type="spherical")
        print("pi")
        print(new.pi_.tolist())
        print("A")
        print(new.A_.tolist())
        print("means")
        print(new.means_.tolist())
        print("covars")
        print(new.covariances_.tolist())
        means = [[-0.03793322050726366, 3.9998655520929556], [2.0276406609148165, 2.022796264492357]]
        assert new.pi_[0] == .5
        assert new.pi_[1] == .5
        assert new.A_[0, 0] == pytest.approx(0.998998998998999, 1e-6)
        assert new.A_[1, 1] == pytest.approx(0.998998998998999, 1e-6)
        assert new.means_[0] == pytest.approx(means[0], 1e-6)
        assert new.means_[1] == pytest.approx(means[1], 1e-6)

        covars = [[[1.8735696880608985, 0.0], [0.0, 1.8735696880608985]], [[1.1520759571030834, 0.0], [0.0, 1.1520759571030834]]]

        assert new.covariances_[0][0] == pytest.approx(covars[0][0], 1e-6)
        assert new.covariances_[0][1] == pytest.approx(covars[0][1], 1e-6)
        assert new.covariances_[1][0] == pytest.approx(covars[1][0], 1e-6)
        assert new.covariances_[1][1] == pytest.approx(covars[1][1], 1e-6)

        observed_sequences, hidden_sequences, lengths = new.sample(100, 50, random_state=random_state)
        print(observed_sequences.shape)

        print("Fitting")
        trainer = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=2,  n_iterations=500, covariance_type="spherical", implementation=impl, verbose=logging.DEBUG, random_state=random_state)
        print(observed.shape)
        trainer.fit(observed_sequences, lengths)
        states = trainer.transform(observed_sequences, lengths)
        print("pi")
        print(trainer.pi_.tolist())
        print("A")
        print(trainer.A_.tolist())
        print("means")
        print(trainer.means_.tolist())
        print("covars")
        print(trainer.covariances_.tolist())
        # pi doesn't seem right
        check_pi = [0.5048500360307523, 0.4951499639692477]
        check_A = [[0.999345761725938, 0.0006542382740619368], [0.0018341724209543675, 0.9981658275790457]]
        check_means = [[2.014081569890744, 2.009012756649338], [-0.04905118029411965, 3.993781183806832]]
        check_covariances = [[[1.1534036057159531, 0.0], [0.0, 1.1534036057159531]], [[1.9008182414110666, 0.0], [0.0, 1.9008182414110666]]]

        check(
            trainer.pi_, check_pi,
            trainer.A_, check_A,
            trainer.means_, check_means,
            trainer.covariances_, check_covariances
        )


def check(pi, check_pi, A, check_A, means, check_means, variances, check_variances, abs_tol=1e-5):
    pi_sort = np.argsort(pi)
    check_sort = np.argsort(check_pi)

    pi = np.asarray(pi)[pi_sort]
    check_pi = np.asarray(check_pi)[check_sort]
    assert pi == pytest.approx(check_pi, abs=abs_tol)
    means = np.asarray(means)[pi_sort]
    check_means = np.asarray(check_means)[check_sort]
    assert check_means == pytest.approx(means, abs=abs_tol)
    variances = np.asarray(variances)[pi_sort]
    check_variances = np.asarray(check_variances)[check_sort]
    assert check_variances == pytest.approx(variances, abs=abs_tol)
    for i,j  in zip(pi_sort, check_sort):
        a_i = np.asarray(A)[i]
        a_j = np.asarray(check_A)[j]
        assert a_i[pi_sort] == pytest.approx(a_j[check_sort], abs=abs_tol)


def test_normals_three_state():

    random_state = sklearn.utils.check_random_state(2)
    pi = [.5, .1, .4]
    A = [
        [.4, .2, .4],
        [.2, .3, .5],
        [.1, .1, .8],
    ]
    means = [[-1], [1.], [4], ]
    variances = [[[1]], [[1.]], [[5.]]]

    gaus = MultivariateGaussianHMM.MultivariateGaussianHMM(init_pi=None, init_A=None, init_emissions=None, random_state=random_state)
    gaus.pi_ = pi
    gaus.A_ = A
    gaus.means_ = means
    gaus.covariances_ = variances

    observed_sequences, hidden_sequences, lengths = gaus.sample(1000, 50, random_state=random_state)

    new = MultivariateGaussianHMM.MultivariateGaussianHMM.reestimate_from_sequences(observed_sequences, hidden_sequences, lengths)
    print(new.pi_)
    print(new.A_)
    print(new.means_)
    print(new.covariances_)
    check_pi = [0.485, 0.109, 0.406]
    check_A =  [[0.40582775, 0.19680414, 0.39736811],
                [0.19724314, 0.28648572, 0.51627114],
                [0.10044242, 0.1010403,  0.79851728]]
    check_means = [[-1.00828069],  [0.99627508],  [3.97768968]]
    check_variances = [[[1.01063096]], [[0.99120944]], [[4.9917699]]]
    check(
        new.pi_, check_pi,
        new.A_, check_A,
        new.means_, check_means,
        new.covariances_, check_variances)
    print(np.ravel(observed_sequences).min())
    print(np.ravel(observed_sequences).max())
    print(np.ravel(observed_sequences).max())
    trainer = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=3, n_iterations=1000, verbose=logging.DEBUG, random_state=random_state)
    trainer.fit(observed_sequences, lengths)
    print(trainer.pi_)
    print(trainer.A_)
    print(trainer.means_)
    print(trainer.covariances_)
    check_pi = [0.49195748,  0.40179942, 0.1062431 ]
    check_A = [[0.40691507,  0.39009713, 0.20298779],
                [0.0997485,  0.80888952, 0.09136199],
                [0.21219239, 0.51062215, 0.27718545]]

    check_means = [[-1.002635  ],
                   [ 3.94867273],
                   [ 1.01375715]]

    check_variances = [[[0.99261344]],
                        [[5.06176496]],
                        [[0.8903289 ]]]
    check(
        trainer.pi_, check_pi,
        trainer.A_, check_A,
        trainer.means_, check_means,
        trainer.covariances_, check_variances,
    abs_tol=1e-3)


if __name__ == "__main__":
    #test_normals_simple()
    #test_normals_not_simple()
    #test_normals_two_state_one_dimension()
    #test_normals_two_state_full_covariance()
    #test_normals_two_state_tied_covariance()
    #test_normals_two_state_diagonal_covariance()
    test_normals_three_state()
    #test_normals_spherical_covariance()
