import sklearn.utils

from hmm import GaussianHMM, PoissonHMM
from hmm.datasets.real import load_earthquakes, load_old_faithful_waiting


def test_confidence_intervals_poisson():
    rs = sklearn.utils.check_random_state(234234)
    earthquakes = load_earthquakes()
    model = PoissonHMM.PoissonHMM(n_components=2, n_iterations=500, n_inits=10, random_state=rs)
    model.fit(earthquakes.data, earthquakes.lengths)
    print(model)
    confidence_data = model.compute_confidence_intervals(n_samples=1, length=earthquakes.data.shape[0], n_iterations=500, ci=90)
    print(confidence_data)


def test_confidence_intervals_gaussian():
    rs = sklearn.utils.check_random_state(234234)
    faithful = load_old_faithful_waiting()
    model = GaussianHMM.GaussianHMM(n_components=2, n_iterations=500, n_inits=10, random_state=rs)
    model.fit(faithful.data, faithful.lengths)
    print(model)
    confidence_data = model.compute_confidence_intervals(n_samples=1, length=faithful.data.shape[0], n_iterations=500, ci=90)
    print(confidence_data)


if __name__ == "__main__":
    test_confidence_intervals_gaussian()
