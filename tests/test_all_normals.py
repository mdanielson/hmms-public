# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
=============================
Demonstration of Gaussian HMM
=============================
"""
import numpy as np
import numpy.testing

import pandas as pd
pd.options.display.float_format = "{:.7f}".format
from hmm import GaussianHMM, MultivariateGaussianHMM, GMMHMM
from hmm.datasets.real import load_old_faithful_waiting, load_old_faithful_duration

from sklearn.mixture import BayesianGaussianMixture

def test_same_answers():
    waiting_times = load_old_faithful_waiting()

    models = {}
    data_2d = waiting_times.data

    model = GaussianHMM.GaussianHMM(n_components=3, n_iterations=30, verbose=False, n_inits=1, random_state=32)
    model.fit(waiting_times.data, waiting_times.lengths)

    print(pd.Series(model.pi_))
    print(pd.DataFrame(model.A_))
    print("means")
    print(pd.Series(model.means_))
    print("variances")
    print(model.variances_)

    mmodel = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=3, n_iterations=30, verbose=False, n_inits=1, random_state=32)
    mmodel.fit(waiting_times.data, waiting_times.lengths)
    print(pd.Series(mmodel.pi_))
    print(pd.DataFrame(mmodel.A_))
    print("means")
    print(pd.DataFrame(mmodel.means_))
    print("variances")
    print(mmodel.covariances_.ravel())

    gmm = GMMHMM.GMMHMM(n_components=3, mixture_n_components=1, n_iterations=30, verbose=False, n_inits=1, random_state=32)
    gmm.fit(waiting_times.data, waiting_times.lengths)
    print(pd.Series(gmm.pi_))
    print(pd.DataFrame(gmm.A_))
    print("means")
    print(pd.Series(gmm.mixture_means_.ravel()))
    print("variances")
    print(gmm.mixture_covariances_)

    numpy.testing.assert_array_almost_equal(model.pi_.ravel(),        mmodel.pi_.ravel())
    numpy.testing.assert_array_almost_equal(model.A_.ravel(),         mmodel.A_.ravel())
    numpy.testing.assert_array_almost_equal(model.means_.ravel(),     mmodel.means_.ravel())
    numpy.testing.assert_array_almost_equal(model.variances_.ravel(), mmodel.covariances_.ravel(), decimal=4)
    # Not yet
    if False:
        numpy.testing.assert_array_almost_equal(model.pi_.ravel(),        gmm.pi_.ravel())
        numpy.testing.assert_array_almost_equal(model.A_.ravel(),         gmm.A_.ravel())
        numpy.testing.assert_array_almost_equal(model.means_.ravel(),     gmm.mixture_means_.ravel())
        numpy.testing.assert_array_almost_equal(model.variances_.ravel(), gmm.mixture_covariances_.ravel())
if __name__ == "__main__":
    test_same_answers()
