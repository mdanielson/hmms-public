import numpy as np
import sklearn.utils

from hmm import util
from hmm.datasets import for_tests



def test_partition_sequences():
    random_state = sklearn.utils.check_random_state(23)
    train_data, train_lengths, train_labels, train_hidden_states = for_tests.get_categorical_beal(random_state, num_each=5, min_length=15, max_length=39, unique_hidden_states=False)
    groups = {}
    for label in set(train_labels):
        groups[label] = np.where(train_labels == label)[0]
    grouped_data, grouped_lengths = util.partition_sequences(train_data, train_lengths, partition_indices=groups)

    for key, data in grouped_data.items():
        if key == 0:
            for sequence in util.iter_sequences(data, grouped_lengths[key]):
                sequence = sequence.ravel()
                if sequence[0] == 0:
                    assert np.all(sequence[:3] == [0, 1, 2])
                elif sequence[0] == 1:
                    assert np.all(sequence[:3] == [1, 2, 0])
                elif sequence[0] == 2:
                    assert np.all(sequence[:3] == [2, 0, 1])
                else:
                    assert False, sequence
        elif key == 1:
            for sequence in util.iter_sequences(data, grouped_lengths[key]):
                sequence = sequence.ravel()
                if sequence[0] == 0:
                    assert np.all(sequence[:3] == [0, 2, 1])
                elif sequence[0] == 1:
                    assert np.all(sequence[:3] == [1, 0, 2])
                elif sequence[0] == 2:
                    assert np.all(sequence[:3] == [2, 1, 0])
                else:
                    assert False, sequence

        elif key == 2:
            for sequence in util.iter_sequences(data, grouped_lengths[key]):
                assert len(set(sequence.ravel())) == 2


if __name__ == "__main__":
    test_partition_sequences()
