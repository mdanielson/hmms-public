import pytest
import sklearn.utils
import pandas as pd
import logging

from hmm import CategoricalVariationalHMM, CategoricalVariationalMHMM, CategoricalVariationalBlockHMM
from hmm.datasets.for_tests import get_categorical_beal

def test_single_hmm_with_mixture():

    random_state = sklearn.utils.check_random_state(123)
    for impl in ["scaling", "log"]:
        for n_components in range(2, 12):
            observations, lengths, labels, _ = get_categorical_beal(random_state)
            rand = 42
            single = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=n_components, n_features=3, n_iterations=500, n_inits=1, n_jobs=-1, random_state=rand, verbose=logging.DEBUG)
            single.fit(observations, lengths)
            print("pi")
            print(pd.DataFrame(single.pi_normalized_))
            print("A")
            print(pd.DataFrame(single.A_normalized_))
            print("B")
            print(pd.DataFrame(single.B_normalized_))
            print(single.lower_bound_[-10:])

            rand = 42
            mixture = CategoricalVariationalMHMM.CategoricalVariationalMHMM(n_mixture_components=1, n_components=n_components, n_features=3, n_iterations=500, n_inits=1, n_jobs=-1, random_state=rand, verbose=logging.DEBUG)
            mixture.fit(observations, lengths)
            print(mixture.mixture_weights_normalized_)
            for i in range(mixture.n_mixture_components):
                print("pi")
                print(pd.DataFrame(mixture.pi_normalized_[i]))
                print("A")
                print(pd.DataFrame(mixture.A_normalized_[i]))
                print("B")
                print(pd.DataFrame(mixture.B_normalized_[i]))
            print(mixture.lower_bound_[-10:])

            for component in range(mixture.n_components):
                print(single.B_posterior_[component])
                print(mixture.B_posterior_[0, component])
                assert single.pi_normalized_[component] == pytest.approx(mixture.pi_normalized_[0, component])
                assert single.A_normalized_[component] == pytest.approx(mixture.A_normalized_[0, component])
                assert single.A_posterior_[component] == pytest.approx(mixture.A_posterior_[0, component])
                assert single.B_normalized_[component] == pytest.approx(mixture.B_normalized_[0, component])
                assert single.B_posterior_[component] == pytest.approx(mixture.B_posterior_[0, component])
                assert single.lower_bound_ == pytest.approx(mixture.lower_bound_)


if __name__ == "__main__":
    test_single_hmm_with_mixture()
