# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import contextlib
import os.path
import shutil
import tempfile

import pytest
import sklearn.utils

from hmm import GaussianHMM, distance
from hmm.datasets.models import \
        get_rabiner_juang_categorical_distance_models


@contextlib.contextmanager
def delete_temp(suffix=""):
    """
    Efficient use of Temp dirs during tests
    """
    temp_dir = tempfile.mkdtemp(suffix)
    shutil.rmtree(temp_dir)
    os.makedirs(temp_dir)
    print("Created", temp_dir)
    yield temp_dir
    print("deleting", temp_dir)
    shutil.rmtree(temp_dir)


def test_simple_distance():
    random_state = sklearn.utils.check_random_state(43)
    pi = [.5, .5]
    A = [
        [.1, .9],
        [.5, .5],
    ]
    means = [-1, 1]
    variances = [2, 4]
    means2 = [-2, 2]

    gaus = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, random_state=random_state)
    gaus.pi_ = pi
    gaus.A_ = A
    gaus.means_ = means
    gaus.variances_ = variances
    observed_sequences, hidden_sequences, lengths = gaus.sample(100, 10, random_state=random_state)

    gaus2 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None, random_state=random_state)
    gaus2.pi_ = pi
    gaus2.A_ = A
    gaus2.means_ = means2
    gaus2.variances_ = variances

    reestimated = GaussianHMM.GaussianHMM.reestimate_from_sequences(observed_sequences, hidden_sequences, lengths, random_state=random_state)
    print(reestimated.pi_)
    print(reestimated.A_)
    print(reestimated.means_)
    print(reestimated.variances_)

    learned = GaussianHMM.GaussianHMM(n_components=2,  n_iterations=200, verbose=False, random_state=random_state)
    learned.fit(observed_sequences, lengths)

    print(learned.pi_)
    print(learned.A_)
    print(learned.means_)
    print(learned.variances_)
    # Avoid exact distance chacks due to models having the sames states but in different orders.  This then
    # alters how sampling from those models occurs.  For example, we may deterministically sample from state (1)
    # but state(1) has different semantics in each model
    reestimated_distance = distance.symmetric_model_distance(reestimated, learned, random_state=random_state)
    learned_distance = distance.symmetric_model_distance(gaus, learned, random_state=random_state)
    similar_distance = distance.symmetric_model_distance(gaus, gaus2, random_state=random_state)
    print(reestimated_distance)
    print(learned_distance)
    print(similar_distance)
    assert reestimated_distance <= 0.003
    assert learned_distance <= 0.004
    assert similar_distance >= 0.1
    with delete_temp() as tmp:
        fig = distance.run_model_distance_tests(gaus, gaus2, n=300, random_state=random_state)
        name = os.path.join(tmp, "model_distances_gaussian.png")
        fig.savefig(name)
        assert os.path.exists(name)


def test_categorical():
    random_state = sklearn.utils.check_random_state(43)
    m_0, m_1 = get_rabiner_juang_categorical_distance_models()
    print(distance.symmetric_model_distance(m_0, m_1, n=200, random_state=random_state))
    fig = distance.run_model_distance_tests(m_0, m_1, n=300, random_state=random_state)
    with delete_temp() as tmp:
        name = os.path.join(tmp, "model_distances_categorical.png")
        fig.savefig(name)
        assert os.path.exists(name)


if __name__ == "__main__":
    test_simple_distance()
    #test_categorical()
