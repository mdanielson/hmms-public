# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import json
import logging

import numpy as np
import pytest
import sklearn.metrics
import sklearn.mixture
import sklearn.utils

from hmm import GMMHMM


def test_normals_simple():
    hidden_states = np.asarray([0]*500 + [1] * 500)
    random_state = sklearn.utils.check_random_state(1)
    observed_1 = np.concatenate([random_state.normal(5, 1, size=400), random_state.normal(10, 2, size=100)])
    random_state.shuffle(observed_1)
    observed_2 = np.concatenate([random_state.normal(0, 2, size=250), random_state.normal(-5, 1, size=250)])
    random_state.shuffle(observed_2)
    observed = np.concatenate([observed_1, observed_2])
    observed_2d = observed[:, None]
    lengths = np.asarray([1000])

    new = GMMHMM.GMMHMM.reestimate_from_sequences(observed_2d, hidden_states, lengths, 2, "full", random_state=random_state)
    print("pi")
    print(new.pi_)
    print("A")
    print(new.A_)
    print("weights")
    print(new.mixture_weights_)
    print("means")
    print(new.mixture_means_)
    print("covariances")
    print(new.mixture_covariances_)

    assert new.pi_[0] == 1
    assert new.pi_[1] == 0
    assert new.A_[0, 0] == pytest.approx(0.998, 1e-6)
    assert new.A_[1, 1] == pytest.approx(1, 1e-6)
    assert new.mixture_weights_[0][0] == pytest.approx(0.81157128, 1e-6)
    assert new.mixture_weights_[0][1] == pytest.approx(0.18842872, 1e-6)
    assert new.mixture_weights_[1][0] == pytest.approx(0.52232355, 1e-6)
    assert new.mixture_weights_[1][1] == pytest.approx(0.47767645, 1e-6)
    assert new.mixture_means_[0][0] == pytest.approx(5.06654703, 1e-5)
    assert new.mixture_means_[0][1] == pytest.approx(10.37231655, 1e-5)
    assert new.mixture_means_[1][0] == pytest.approx(-4.90515675, 1e-5)
    assert new.mixture_means_[1][1] == pytest.approx(0.26108044, 1e-5)
    assert new.mixture_covariances_[0][0][0] == pytest.approx(0.96789897, 1e-6)
    assert new.mixture_covariances_[0][1][0] == pytest.approx(3.47933485, 1e-6)
    assert new.mixture_covariances_[1][0][0] == pytest.approx(1.08455119, 1e-6)
    assert new.mixture_covariances_[1][1][0] == pytest.approx(3.18310979, 1e-6)

    new_observations, new_hidden_states, lengths = new.sample(1, 5000)
    em_model = GMMHMM.GMMHMM(n_components=2, mixture_n_components=2, verbose=logging.DEBUG, n_iterations=100, random_state=random_state)
    em_model.fit(new_observations, lengths)
    print("pi")
    print(em_model.pi_.tolist())
    print("A")
    print(em_model.A_.tolist())
    print("weights")
    print(em_model.mixture_weights_.tolist())
    print("means")
    print(em_model.mixture_means_.tolist())
    print("covariances")
    print(em_model.mixture_covariances_.tolist())
    print(em_model.transform(new_observations, lengths))
    # Compare models by sorting based on pi, and then mixture weights
    states_sorting = np.argsort(em_model.pi_)
    assert em_model.pi_[states_sorting[0]] == 0
    assert em_model.pi_[states_sorting[1]] == 1
    assert em_model.A_[states_sorting[0], states_sorting[0]] == pytest.approx(1, 1e-6)
    assert em_model.A_[states_sorting[1], states_sorting[1]] == pytest.approx(0.9994810568473824, 1e-6)
    weights = np.asarray([[0.4896403367681322, 0.5103596632318678], [0.8231575236788361, 0.17684247632116384]])
    means = np.asarray([[[0.22419199170521192], [-4.942223891393787]], [[5.053910233765724], [10.56102699788437]]])
    covariances = np.asarray([[[[3.2279351985388467]], [[1.1515712532167506]]], [[[0.984478041475505]], [[3.304820693542679]]]])
    for i, j in zip(states_sorting, range(len(states_sorting))):
        trained_sorting = np.argsort(em_model.mixture_means_[j], axis=0).ravel()
        actual_sorting = np.argsort(means[j], axis=0).ravel()
        assert em_model.mixture_means_[i][trained_sorting] == pytest.approx(means[j][actual_sorting], abs=1e-2)
        assert em_model.mixture_weights_[i][trained_sorting] == pytest.approx(weights[j][actual_sorting], abs=1e-2)
        assert em_model.mixture_covariances_[i][trained_sorting] == pytest.approx(covariances[j][actual_sorting], abs=1e-2)

    print(sklearn.metrics.classification_report(em_model.transform(new_observations, lengths).ravel(), new_hidden_states.ravel()))
    score = sklearn.metrics.v_measure_score(em_model.transform(new_observations, lengths).ravel(), new_hidden_states.ravel())
    print(score)
    assert score == pytest.approx(1)

def test_normals_three_state_three_components_full_simple():
    for impl in ["scaling", "log"]:
        hidden_states = []
        observations = []
        state = 0
        random_state = sklearn.utils.check_random_state(1)
        for i in range(500):

            if i > 0 and i % 5 == 0:
                state += 1
                state = state % 3
            hidden_states.append(state)
            rand = random_state.rand()
            if state == 0:
                if rand < .4:
                    observations.append(
                        random_state.multivariate_normal([1, 2], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([1, 5], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([1, 8], [[2, 1], [1, 2]])
                    )
            elif state == 1:
                if rand < .4:
                    observations.append(
                        random_state.multivariate_normal([2, 1], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([5, 1], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([8, 1], [[2, 1], [1, 2]])
                    )

            elif state == 2:
                if rand < .2:
                    observations.append(
                        random_state.multivariate_normal([-5, 5], [[2, 1], [1, 2]])
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([-5, 2], [[2, 1], [1, 2]])
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([-5, -5], [[2, 1], [1, 2]])
                    )
        observations = np.asarray(observations)
        hidden_states = np.asarray(hidden_states)
        lengths = [len(hidden_states)]
        print(np.cov(observations.T))
        new = GMMHMM.GMMHMM.reestimate_from_sequences(observations, hidden_states, lengths, 3, "full", random_state=random_state)
        print(new.mixture_weights_)
        print(new.mixture_means_)
        print(new.mixture_covariances_)

        em_gmm = GMMHMM.GMMHMM(n_components=3, mixture_n_components=3, random_state=random_state, n_iterations=5000, n_inits=1, n_jobs=1, verbose=logging.DEBUG, implementation=impl)
        em_gmm.fit(observations, lengths)
        unpacked_em_gmm = GMMHMM.GMMHMM.from_dict(json.loads(json.dumps(em_gmm.to_dict())))
        print("pi")
        print(unpacked_em_gmm.pi_)
        print("A")
        print(unpacked_em_gmm.A_)
        print("weights")
        print(unpacked_em_gmm.mixture_weights_)
        print("means")
        print(unpacked_em_gmm.mixture_means_)
        print("covariances")
        print(unpacked_em_gmm.mixture_covariances_)
        print(unpacked_em_gmm.transform(observations, lengths))
        score = sklearn.metrics.v_measure_score(unpacked_em_gmm.transform(observations, lengths).ravel(), hidden_states.ravel())
        print(score)
        assert score == pytest.approx(0.9478424072361311, abs=1e-2)


def test_normals_three_state_three_components_full_hard():
    for impl in ["log", "scaling"]:
        hidden_states = []
        observations = []
        state = 0
        random_state = sklearn.utils.check_random_state(1)
        for state in [0, 1, 2, 2]:
            print(state)
            for i in range(1250):
                if i > 5 and i % 5 == 0:
                    state += 1
                    state = state % 3
                hidden_states.append(state)
                rand = random_state.rand()
                if state == 0:
                    if rand < .4:
                        observations.append(
                            random_state.multivariate_normal([0, 4], [[2, 1], [1, 2]])
                        )
                    else:
                        observations.append(
                            random_state.multivariate_normal([2, 2], [[2, 1], [1, 2]])
                        )
                elif state == 1:
                    if rand < .5:
                        observations.append(
                            random_state.multivariate_normal([0, -5], [[2, 1], [1, 2]])
                        )
                    else:
                        observations.append(
                            random_state.multivariate_normal([4, 4], [[2, 1], [1, 2]])
                        )

                elif state == 2:
                    if rand < .2:
                        observations.append(
                            random_state.multivariate_normal([5, 5], [[2, 1], [1, 2]])
                        )
                    else:
                        observations.append(
                            random_state.multivariate_normal([2, -2], [[2, 1], [1, 2]])
                        )

        observations = np.vstack(observations)
        hidden_states = np.asarray(hidden_states)
        lengths = np.asarray([1250]*4)
        new = GMMHMM.GMMHMM.reestimate_from_sequences(observations, hidden_states, lengths, 3, "full", random_state=random_state)
        print(new.mixture_weights_)
        print(new.mixture_means_)
        print(new.mixture_covariances_)

        em_gmm = GMMHMM.GMMHMM(n_components=3, mixture_n_components=2, random_state=random_state, n_iterations=5000, n_inits=1, n_jobs=1, verbose=logging.INFO, implementation=impl)
        em_gmm.fit(observations, lengths)

        unpacked_em_gmm = GMMHMM.GMMHMM.from_dict(json.loads(json.dumps(em_gmm.to_dict())))
        print(unpacked_em_gmm.pi_.tolist())
        print(unpacked_em_gmm.A_.tolist())
        print(unpacked_em_gmm.mixture_weights_.tolist())
        print(unpacked_em_gmm.mixture_means_.tolist())
        print(unpacked_em_gmm.mixture_covariances_.tolist())
        print(unpacked_em_gmm.transform(observations, lengths))
        pi = np.asarray([0.3014143037353588, 0.5012497730946054, 0.19733592317003584])
        their_pi_sort = np.argsort(pi)
        our_pi_sort = np.argsort(unpacked_em_gmm.pi_)
        assert unpacked_em_gmm.pi_[our_pi_sort] == pytest.approx(pi[their_pi_sort], 1e-2)
        A = np.asarray([[0.31514624, 0.58021015, 0.10464361],
              [0.26303577, 0.59702071, 0.13994352],
              [0.12064186, 0.1288038,  0.75055434]])
        their_A = A[their_pi_sort][:, their_pi_sort]
        our_A = unpacked_em_gmm.A_[our_pi_sort][:, our_pi_sort]
        assert our_A[-1] == pytest.approx(their_A[-1], abs=1e-2)
        weights = np.asarray([[0.5779484302936881, 0.42205156970631175], [0.6829632495924809, 0.3170367504075191], [0.6137512097748881, 0.3862487902251119]])

        for theirs, ours in zip(their_pi_sort, our_pi_sort):
            assert sorted(unpacked_em_gmm.mixture_weights_[ours]) == pytest.approx(sorted(weights[theirs]), abs=1e-2)


def test_normals_three_state_three_components_tied():
    for impl in ["scaling", "log"]:
        hidden_states = []
        observations = []
        state = 0
        random_state = sklearn.utils.check_random_state(1)
        for i in range(1000):

            if i > 0 and i % 5 == 0:
                state += 1
                state = state % 3
            hidden_states.append(state)
            rand = random_state.rand()
            if state == 0:
                covariance = [[2, 1], [1, 2]]
                if rand < .4:
                    observations.append(
                        random_state.multivariate_normal([1, 2], covariance)
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([1, 5], covariance)
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([1, 8], covariance)
                    )
            elif state == 1:
                covariance = [[1, 2], [2, 1]]
                if rand < .4:
                    observations.append(
                        random_state.multivariate_normal([5, 1], covariance)
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([5, 4], covariance)
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([5, 9], covariance)
                    )

            elif state == 2:
                covariance = [[1, 0], [0, 1]]
                if rand < .2:
                    observations.append(
                        random_state.multivariate_normal([-5, 5], covariance)
                    )
                elif rand < .8:
                    observations.append(
                        random_state.multivariate_normal([-5, 2], covariance)
                    )
                else:
                    observations.append(
                        random_state.multivariate_normal([-5, -5], covariance)
                    )
        observations = np.asarray(observations)
        hidden_states = np.asarray(hidden_states)
        lengths = np.asarray([len(hidden_states)])
        print(observations.shape)
        new = GMMHMM.GMMHMM.reestimate_from_sequences(observations, hidden_states, lengths, 3, "tied", random_state=random_state)
        print("mixture_weights")
        print(new.mixture_weights_)
        print("mixture_means")
        print(new.mixture_means_)
        print("mixture_covariances")
        print(new.mixture_covariances_)

        em_gmm = GMMHMM.GMMHMM(init_pi="uniform", init_A="uniform", n_components=3, mixture_n_components=3, random_state=random_state, mixture_covariance_type="tied", n_iterations=5000, n_inits=1, n_jobs=1, verbose=logging.INFO, implementation=impl)
        em_gmm.fit(observations, lengths)

        unpacked_em_gmm = GMMHMM.GMMHMM.from_dict(json.loads(json.dumps(em_gmm.to_dict())))
        print("pi")
        print(unpacked_em_gmm.pi_)
        print("A")
        print(unpacked_em_gmm.A_)
        print("weights")
        print(unpacked_em_gmm.mixture_weights_)
        print("means")
        print(unpacked_em_gmm.mixture_means_)
        print("covariances")
        print(unpacked_em_gmm.mixture_covariances_)
        new.verbose = logging.DEBUG
        print(new.transform(observations, lengths))
        score = sklearn.metrics.v_measure_score(unpacked_em_gmm.transform(observations, lengths).ravel(), hidden_states.ravel())
        print(score)
        assert score == pytest.approx(0.9566034572595805, 1e-2)


if __name__ == "__main__":
    #test_normals_simple()
    #test_normals_three_state_three_components_full_simple()
    #test_normals_three_state_three_components_tied()
    test_normals_three_state_three_components_full_hard()
