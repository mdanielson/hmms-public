# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import numpy as np
import pandas as pd
import pytest
import sklearn.utils

from hmm import CategoricalHMM, model_selection, wrapped_hmm
from hmm._hmm import new_categorical

from hmm.datasets import for_tests


def test_log_indiana():
    """
    Computations from:
        http://www.indiana.edu/~iulg/moss/hmmcalculations.pdf
    """
    pi = [.85, .15]
    B = [[.4, .6],
         [.5, .5]
         ]
    A = [[.3, .7],
         [.1, .9]
         ]
    pi = np.asarray(pi)
    B = np.asarray(B)
    A = np.asarray(A)
    example = new_categorical(pi, A, B, prefer_scaling=False)
    # A = 0
    # B = 1
    # [ A B B A] - > [0, 1, 1, 0]
    # [ B A B ] - > [1, 0, 1]

    observations = np.asarray([[0], [1], [1], [0]], float)
    lengths = np.asarray([4])
    alpha = example.log_alpha_pass(observations, lengths)
    ans_paper = [[0.34, 0.075],
                 [0.06570, 0.15275],
                 [0.020991, 0.0917325],
                 [0.00618822, 0.04862647],
                 ]
    assert alpha.shape[0] == 4
    assert alpha.shape[1] == 2
    for i in range(alpha.shape[0]):
        scaled = alpha[i, :]
        assert np.exp(scaled) == pytest.approx(ans_paper[i], 1e-5)

    beta = example.log_beta_pass(observations, lengths)
    print("beta")
    print(np.exp(beta))
    answer_b = [[0.133143, 0.127281],
                [0.2561,   0.2487],
                [0.47,     0.49],
                [1.,       1.]]
    for i in reversed(range(beta.shape[0])):
        for j in range(beta.shape[1]):
            assert np.exp(beta[i, j]) == pytest.approx(answer_b[i][j], 1e-5)

    digamma, digamma_1 = example.log_compute_digammas(alpha, beta, observations, lengths)
    digamma = np.exp(digamma)
    print("digamma")
    print(digamma)
    print(digamma_1)

    example = new_categorical(pi, A, B, prefer_scaling=False, allowed_to_use_log=True)
    example.train(observations, lengths, 1)
    print(np.asarray(example.pi))
    print(np.asarray(example.A))
    print(np.asarray(example.emissions))
    new_A = [[0.33005013, 0.66994987],
             [0.09871229, 0.90128771]]

    for i in range(example.A.shape[-2]):
        assert example.A[i] == pytest.approx(new_A[i], 1e-5)

    new_B = [[0.6584505, 0.3415495],
             [0.41224849, 0.58775151]]
    for i in range(example.emissions.B.shape[-2]):
        assert example.emissions.B[i] == pytest.approx(new_B[i], 1e-5)

    result = example.loglik(observations, lengths)
    print(result)
    assert result[0] == pytest.approx(-2.56387792)


def test_indiana():
    """
    Computations from:
        http://www.indiana.edu/~iulg/moss/hmmcalculations.pdf
    """
    pi = [.85, .15]
    B = [[.4, .6],
         [.5, .5]
         ]
    A = [[.3, .7],
         [.1, .9]
         ]
    pi = np.asarray(pi)
    B = np.asarray(B)
    A = np.asarray(A)
    example = new_categorical(pi, A, B, prefer_scaling=True)
    # A = 0
    # B = 1
    # [ A B B A] - > [0, 1, 1, 0]
    # [ B A B ] - > [1, 0, 1]

    observations = np.asarray([[0], [1], [1], [0], ], float)
    lengths = np.asarray([4])
    print(observations.shape)
    print(observations)
    c, alpha = example.scaling_alpha_pass(observations, lengths)
    alpha = np.asarray(alpha)
    c = np.asarray(c)
    print("alpha")
    print(alpha)
    print("c")
    print(c)
    scaled_c = [
        [0.34,  0.075],
        [0.0657,  0.15275],
        [0.020991,  0.0917325],
        [0.00618822, 0.04862647]
    ]
    ans_paper = [[0.34, 0.08],
                 [0.06600, 0.15500],
                 [0.02118, 0.09285],
                 [0.00625, 0.04919]
                 ]
    cc = 1
    for i in range(alpha.shape[0]):
        cc *= c[i]
        scaled = alpha[i, :] / cc
        print(i)
        print(scaled)
        print(ans_paper[i])
        for j in range(alpha.shape[1]):
            assert scaled[j] == pytest.approx(scaled_c[i][j], 1e-5)

    beta = example.scaling_beta_pass(observations, lengths, c)
    beta = np.asarray(beta)
    print("beta")
    print(beta)
    cc = 1
    answer_b = [[0.133143, 0.127281],
                [0.2561,   0.2487],
                [0.47,     0.49],
                [1.,       1.]]
    for i in reversed(range(beta.shape[0])):
        cc *= c[i]
        scaled = beta[i, :] / cc
        print(i)
        print(scaled)
        for j in range(beta.shape[1]):
            assert scaled[j] == pytest.approx(answer_b[i][j], 1e-5)

    digamma, digamma_1 = example.scaling_compute_digammas(alpha, beta, observations, lengths)
    digamma = np.asarray(digamma)
    print("digamma")
    print(digamma)
    print(np.asarray(digamma_1))

    example.train(observations, lengths, 1)
    print(np.asarray(example.pi))
    print(np.asarray(example.A))
    print(np.asarray(example.emissions))
    new_A = [[0.33005013, 0.66994987],
             [0.09871229, 0.90128771]]

    for i in range(example.A.shape[-2]):
        assert example.A[i] == pytest.approx(new_A[i], 1e-5)
    new_B = [[0.6584505, 0.3415495],
             [0.41224849, 0.58775151]]
    for i in range(example.emissions.B.shape[-2]):
        assert example.emissions.B[i] == pytest.approx(new_B[i], 1e-5)
    result = example.loglik(observations, lengths)
    print(result)
    assert result[0] == pytest.approx(-2.56387792)


def test_three_real_states():

    start_probability = {"q1": .3, "q2": .3, "q3": .3, "qf": 0}  # , "qf":0}
    transition_probability = {
        "q1": {
            "q1": .2,
            "q2": .2,
            "q3": .2,
            "qf": .4,
        },
        "q2": {
            "q1": .2,
            "q2": .2,
            "q3": .2,
            "qf": .4,
        },
        "q3": {
            "q1": .2,
            "q2": .2,
            "q3": .2,
            "qf": .4,
        },
        "qf": {
            "q1": 0,
            "q2": 0,
            "q3": 0,
            "qf": 1
        }
    }
    emission_probability = {
        "q1": {
            "a": .2,
            "b": .4,
            "c": .2,
            "d": 0,
        },
        "q2": {
            "a": .4,
            "b": .2,
            "c": .2,
            "d": 0,
        },
        "q3": {
            "a": .2,
            "b": .2,
            "c": .4,
            "d": 0,
        },
        "qf": {
            "a": 0,
            "b": 0,
            "c": 0,
            "d": 1,
        },

    }

    wrapped = wrapped_hmm.WrappedHmm(
        start_probability,
        transition_probability,
        emission_probability,
        log_level=logging.DEBUG,
    )
    print(wrapped._hmm)
    wrapped.train([
        ["a", "b", "a", "c", "d"],
        ["c", "b", "a", "c", "d"]
    ], iterations=10)
    print(wrapped.pi)
    print(wrapped.A)
    print(wrapped.B)

    assert wrapped.A["qf"]["q1"] == 0
    assert wrapped.A["qf"]["q2"] == 0
    assert wrapped.A["qf"]["qf"] == 1

    assert wrapped.B["qf"]["a"] == 0
    assert wrapped.B["qf"]["b"] == 0
    assert wrapped.B["qf"]["c"] == 0
    assert wrapped.B["qf"]["d"] == 1


def test_mark_jan2():

    start_probability = {"q1": .6, "q2": .4, "qf": 0}  # , "qf":0}
    transition_probability = {
        "q1": {
            "q1": .3,
            "q2": .3,
            "qf": .4,
        },
        "q2": {
            "q1": .3,
            "q2": .3,
            "qf": .4,
        },
        "qf": {
            "q1": 0,
            "q2": 0,
            "qf": 1
        }
    }
    emission_probability = {
        "q1": {
            "a": .5,
            "b": .5,
            "c": 0,
        },
        "q2": {
            "a": .5,
            "b": .5,
            "c": 0,
        },
        "qf": {
            "a": 0,
            "b": 0,
            "c": 1,
        },

    }
    for impl in ["scaling", "log"]:
        print(impl)
        wrapped = wrapped_hmm.WrappedHmm(
            start_probability,
            transition_probability,
            emission_probability,
            log_level=logging.DEBUG,
            implementation=impl,
        )
        wrapped.train(
            [
                ["a", "b", "c"],
                ["a", "b", "c"]
            ],
            iterations=4)
        print(wrapped.pi)
        print(wrapped.A)
        print(wrapped.B)
        assert wrapped.pi["q1"] == pytest.approx(0.9974493866252675, 1e-5)
        assert wrapped.pi["q2"] == pytest.approx(0.002550613374732519, 1e-5)
        assert wrapped.A["q1"]["q1"] == pytest.approx(0.006789486325377364, 1e-5)
        assert wrapped.A["q1"]["q2"] == pytest.approx(0.9864036657117915, 1e-5)
        assert wrapped.A["q1"]["qf"] == pytest.approx(0.00680684796283113, 1e-5)

        assert wrapped.A["q2"]["q1"] == pytest.approx(1.751108116556169e-05, 1e-5)
        assert wrapped.A["q2"]["q2"] == pytest.approx(0.002544079746905848, 1e-5)
        assert wrapped.A["q2"]["qf"] == pytest.approx(0.9974384091719286, 1e-5)

        assert wrapped.A["qf"]["q1"] == 0
        assert wrapped.A["qf"]["q2"] == 0
        assert wrapped.A["qf"]["qf"] == 1

        assert wrapped.B["q1"]["a"] == pytest.approx(0.9931931520371688, 1e-5)
        assert wrapped.B["q1"]["b"] == pytest.approx(0.00680684796283113, 1e-5)
        assert wrapped.B["q1"]["c"] == 0
        assert wrapped.B["q2"]["a"] == pytest.approx(0.0025615908280714095, 1e-5)
        assert wrapped.B["q2"]["b"] == pytest.approx(0.9974384091719286, 1e-5)
        assert wrapped.B["q2"]["c"] == 0

        assert wrapped.B["qf"]["a"] == 0
        assert wrapped.B["qf"]["b"] == 0
        assert wrapped.B["qf"]["c"] == 1


def test_mark_jan():

    start_probability = {"q1": .6, "q2": .4, "qf": 0}  # , "qf":0}
    transition_probability = {
        "q1": {
            "q1": .3,
            "q2": .3,
            "qf": .4,
        },
        "q2": {
            "q1": .3,
            "q2": .3,
            "qf": .4,
        },
        "qf": {
            "q1": 0,
            "q2": 0,
            "qf": 1
        }
    }
    emission_probability = {
        "q1": {
            "a": .5,
            "b": .5,
            "c": 0,
        },
        "q2": {
            "a": .5,
            "b": .5,
            "c": 0,
        },
        "qf": {
            "a": 0,
            "b": 0,
            "c": 1,
        },

    }
    for impl in ["scaling", "log"]:
        wrapped = wrapped_hmm.WrappedHmm(
            start_probability,
            transition_probability,
            emission_probability,
            log_level=logging.DEBUG,
        )
        wrapped.train([["a", "b", "c"]], iterations=4)
        print(wrapped.pi)
        print(wrapped.A)
        print(wrapped.B)
        assert wrapped.pi["q1"] == pytest.approx(0.9974493866252675, 1e-5), impl
        assert wrapped.pi["q2"] == pytest.approx(0.002550613374732519, 1e-5), impl
        assert wrapped.A["q1"]["q1"] == pytest.approx(0.006789486325377364, 1e-5), impl
        assert wrapped.A["q1"]["q2"] == pytest.approx(0.9864036657117915, 1e-5), impl
        assert wrapped.A["q1"]["qf"] == pytest.approx(0.00680684796283113, 1e-5), impl

        assert wrapped.A["q2"]["q1"] == pytest.approx(1.751108116556169e-05, 1e-5), impl
        assert wrapped.A["q2"]["q2"] == pytest.approx(0.002544079746905848, 1e-5), impl
        assert wrapped.A["q2"]["qf"] == pytest.approx(0.9974384091719286, 1e-5), impl

        assert wrapped.A["qf"]["q1"] == 0, impl
        assert wrapped.A["qf"]["q2"] == 0, impl
        assert wrapped.A["qf"]["qf"] == 1, impl

        assert wrapped.B["q1"]["a"] == pytest.approx(0.9931931520371688, 1e-5), impl
        assert wrapped.B["q1"]["b"] == pytest.approx(0.00680684796283113, 1e-5), impl
        assert wrapped.B["q1"]["c"] == 0, impl
        assert wrapped.B["q2"]["a"] == pytest.approx(0.0025615908280714095, 1e-5), impl
        assert wrapped.B["q2"]["b"] == pytest.approx(0.9974384091719286, 1e-5), impl
        assert wrapped.B["q2"]["c"] == 0, impl

        assert wrapped.B["qf"]["a"] == 0, impl
        assert wrapped.B["qf"]["b"] == 0, impl
        assert wrapped.B["qf"]["c"] == 1, impl


def test_diagonal_initialization():
    random_state = sklearn.utils.check_random_state(342)
    A = [
        [.5, .25, 0, .25],
        [.25, .5, 0, .25],
        [.10, .25, .5, .15],
        [.10, .25, .15, .5]
    ]

    B = [
        [.5, 0, 0, .5],
        [.1, .5, .1, .3],
        [.1, .2, .4, .3],
        [0, .5, .5, .0],
    ]

    generator = CategoricalHMM.CategoricalHMM(n_components=4, init_pi=None, init_A=None, init_emissions=None, random_state=random_state, verbose=logging.ERROR)
    generator.pi_ = [.25, .25, .25, .25]
    generator.A_ = A
    generator.B_ = B

    observed, hidden, lengths = generator.sample(100, 100, random_state=random_state)
    print(observed)
    print(hidden)
    reestimated = CategoricalHMM.CategoricalHMM.reestimate_from_sequences(observed, hidden, lengths)
    print("pi")
    print(pd.DataFrame(reestimated.pi_))
    assert reestimated.pi_[0] == .29
    assert reestimated.pi_[1] == .14
    assert reestimated.pi_[2] == .26
    assert reestimated.pi_[3] == .31
    print("A")
    print(pd.DataFrame(reestimated.A_))
    print("B")
    print(pd.DataFrame(reestimated.B_))

    model = CategoricalHMM.CategoricalHMM(n_components=4, init_A="diagonal", init_emissions="uniform", random_state=random_state, n_iterations=500, verbose=False)
    model.fit(observed, lengths)
    print(model.loglikelihoods_[-4:])
    print("pi")
    print(pd.DataFrame(model.pi_))
    print("A")
    print(pd.DataFrame(model.A_))
    print("B")
    print(pd.DataFrame(model.B_))

    probs = model.score_samples(observed, lengths).tolist()
    print("perplexity")
    perp = model_selection.perplexity(model, observed, lengths)
    print(sum(perp))
    assert sum(perp) == pytest.approx(378.98101856729335, 1e-6)


def gen_smoothing_data():
    training_data = [
        [0, 0, ] * 20 + [1] + [0]*20 + [5] + [0] * 20
    ]

    testing_data = [
        [2, 3, 7, 5] * 40,
        [0, 1, 5, 0] * 40
    ]
    training_data = np.asarray(training_data)
    training_data = training_data.ravel()[:, None]
    training_lengths = [len(training_data)]
    testing_lengths = [len(s) for s in testing_data]
    testing_data = np.concatenate(testing_data)[:, None]
    return training_data, training_lengths, testing_data, testing_lengths


def test_simple_no_smoothing():
    training_data, training_lengths, testing_data, testing_lengths = gen_smoothing_data()

    model = CategoricalHMM.CategoricalHMM(n_components=10, n_iterations=40,  n_features=8, random_state=32, verbose=logging.ERROR)
    model.fit(training_data, training_lengths)
    viterbid = model.transform(training_data, training_lengths)
    print(viterbid)
    print("pi")
    print(pd.DataFrame(model.pi_))
    print("A")
    print(pd.DataFrame(model.A_))
    print("B")
    print(pd.DataFrame(model.B_))

    probs = model.score_samples(testing_data, testing_lengths).tolist()
    print(probs)
    print(model.transform(testing_data, testing_lengths).tolist())
    # First Sequence will be unlikely
    # Second Sequence will be likely
    assert np.isinf(probs[0])

    print("perplexity")
    print(model_selection.perplexity(model, testing_data, testing_lengths))


def test_simple_with_smoothing_1():
    training_data, training_lengths, testing_data, testing_lengths = gen_smoothing_data()

    model = CategoricalHMM.CategoricalHMM(n_components=10, n_iterations=40,  n_features=8, smoothing="laplace", random_state=32, verbose=logging.ERROR)
    model.fit(training_data, training_lengths)
    viterbid = model.transform(training_data, training_lengths)
    #print("pi")
    #print(pd.DataFrame(model.pi_))
    #print("A")
    #print(pd.DataFrame(model.A_))
    #print("B")
    #print(pd.DataFrame(model.B_))

    probs = model.score_samples(testing_data, testing_lengths).tolist()
    #print(probs)
    #print(model.transform(testing_data, testing_lengths).tolist())
    assert not np.isinf(probs[0])
    print("perplexity")
    print(model_selection.perplexity(model, testing_data, testing_lengths))


def test_simple_with_smoothing_2():
    training_data, training_lengths, testing_data, testing_lengths = gen_smoothing_data()

    model = CategoricalHMM.CategoricalHMM(n_components=10, n_iterations=40,  n_features=8, smoothing="AddK", smoothing_params=(0.7,), random_state=32, verbose=logging.ERROR)
    model.fit(training_data, training_lengths)
    viterbid = model.transform(training_data, training_lengths)
    print("pi")
    print(pd.DataFrame(model.pi_))
    print("A")
    print(pd.DataFrame(model.A_))
    print("B")
    print(pd.DataFrame(model.B_))

    probs = model.score_samples(testing_data, testing_lengths).tolist()
    #print(probs)
    #print(model.transform(testing_data, testing_lengths).tolist())
    assert not np.isinf(probs[0])
    print("perplexity")
    print(model_selection.perplexity(model, testing_data, testing_lengths))

def test_simple_with_smoothing_3():
    rs = sklearn.utils.check_random_state(32)
    training_data, training_lengths, _, _ = for_tests.get_categorical_beal(rs, unique_hidden_states=True, max_length=39)
    testing_data, testing_lengths, _, _ = for_tests.get_categorical_beal(rs, unique_hidden_states=True, max_length=39)

    model = CategoricalHMM.CategoricalHMM(n_components=10, n_iterations=1000,  n_features=10, smoothing="laplace", random_state=32, verbose=logging.ERROR)
    model.fit(training_data, training_lengths)
    print(model.loglikelihoods_)
    pd.options.display.float_format = "{:.3f}".format
    viterbid = model.transform(testing_data, testing_lengths)
    print("pi")
    print(pd.DataFrame(model.pi_))
    print("A")
    print(pd.DataFrame(model.A_))
    print("B")
    print(pd.DataFrame(model.B_))

    probs = model.score_samples(testing_data, testing_lengths).tolist()
    #print(probs)
    #print(model.transform(testing_data, testing_lengths).tolist())
    assert not np.isinf(probs[0])
    print("perplexity")
    print(model_selection.perplexity(model, testing_data, testing_lengths))

if __name__ == "__main__":
    test_log_indiana()
    # test_indiana()
    # test_bound()
    # test_simple_no_smoothing()
    # test_simple_with_smoothing_1()
    # test_simple_with_smoothing_2()
    # test_simple_with_smoothing_3()
    # test_diagonal_initialization()
    # test_mark_jan()
    # test_three_real_states()
    # test_viterbi()
    # test_bound()
    # test_wikipedia()
    # test_speech_and_language_processing()
    # test_revealing()
    # test_longer()
