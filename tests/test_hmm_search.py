# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import pandas as pd
import pytest
import sklearn.utils
import sklearn.model_selection

from hmm import CategoricalHMM, GaussianHMM
from hmm import model_selection, util
from hmm.datasets.for_tests import get_categorical_beal, get_gaussian_mcgrory_and_titterington_sequences


def test_em_gauss():
    for criterion in ["tll"]:
        rs = sklearn.utils.check_random_state(1)
        data, _, lengths = get_gaussian_mcgrory_and_titterington_sequences(n_samples=20, sample_size=50, random_state=rs)
        trainer = GaussianHMM.GaussianHMM(n_components=4, n_iterations=1000, random_state=rs, n_inits=1)
        params = {
            "n_components": np.arange(1,8),
            "n_inits": [1]

        }
        searcher = model_selection.HMMSearchCV(
            trainer,
            param_grid=params,
            cv=sklearn.model_selection.KFold(2, shuffle=True, random_state=rs),
            scoring=model_selection.EM_SCORERS,
            refit=criterion,
            n_jobs=-1,
        )
        searcher.fit(data, lengths)
        print(pd.DataFrame(searcher.scores_).T)
        print(pd.DataFrame(searcher.scores_).T[["mean_test_tll", "mean_train_tll"]])
        if criterion is None:
            assert searcher.best_model_ is None
        else:
            assert searcher.best_model_.n_components == 4, (criterion, searcher.best_model_.n_components)
    for criterion in ["aic", "bic", "caic", None]:
        rs = sklearn.utils.check_random_state(2)
        data, _, lengths = get_gaussian_mcgrory_and_titterington_sequences(n_samples=20, sample_size=50, random_state=rs)
        trainer = GaussianHMM.GaussianHMM(n_components=4, n_iterations=1000, random_state=rs, n_inits=1)
        params = {
            "n_components": np.arange(1,8),
            "n_inits": [1]

        }
        searcher = model_selection.HMMSearch(
            trainer,
            param_grid=params,
            scoring=model_selection.EM_SCORERS,
            refit=criterion,
            n_jobs=-1,
        )
        searcher.fit(data, lengths)
        print(pd.DataFrame(searcher.scores_).T)
        print(pd.DataFrame(searcher.scores_).T[["aic", "bic", "caic"]])
        if criterion is None:
            assert searcher.best_model_ is None
        else:
            assert searcher.best_model_.n_components == 4, (criterion, searcher.best_model_.n_components)

def test_em_categorical():
    for criterion in model_selection.EM_SCORERS.keys():
        rs = sklearn.utils.check_random_state(2011)
        data, lengths, _, _ = get_categorical_beal(rs)
        test_data, test_lengths, _, _= get_categorical_beal(rs, num_each=14, min_length=39, max_length=40)
        test_data, test_lengths = util.shuffle_sequences(test_data, test_lengths, random_state=rs)
        data = np.concatenate([data, test_data])
        lengths = np.concatenate([lengths, test_lengths])
        trainer = CategoricalHMM.CategoricalHMM(
            random_state=rs,
        )
        params = {
            "n_components": np.arange(1, 10),
            "n_iterations":[500]
        }
        if criterion == "tll":
            searcher = model_selection.HMMSearchCV(
                trainer,
                param_grid=params,
                cv=sklearn.model_selection.RepeatedKFold(2, 5, random_state=rs),
                scoring=model_selection.EM_SCORERS,
                refit=criterion,
                n_jobs=-1,
                verbose=True
            )
            searcher.fit(data, lengths)
            print(pd.DataFrame(searcher.scores_).T)
            assert searcher.best_model_.n_components == 7, (criterion, searcher.best_model_.n_components)
            assert len(searcher.models_) == 9, len(searcher.models_)
            for ml in searcher.models_:
                assert len(ml) == 10, len(ml)

        else:
            searcher = model_selection.HMMSearch(
                trainer.set_params(n_inits=4),
                param_grid=params,
                scoring=model_selection.EM_SCORERS,
                refit=criterion,
                n_jobs=-1,
                verbose=True
            )
            searcher.fit(data, lengths)
            for ll in searcher.best_model_.explored_loglikelihoods_:
                print(ll[0], ll[-1])
            print(pd.DataFrame(searcher.scores_).T)
            assert searcher.best_model_.n_components == 7, (criterion, searcher.best_model_.n_components)
            assert len(searcher.models_) == 9, len(searcher.models_)

if __name__ == "__main__":
    test_em_gauss()
    test_em_categorical()
