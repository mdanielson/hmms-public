# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging

import numpy as np
import pandas as pd
import pytest
import sklearn.metrics
import sklearn.utils

from hmm import (GaussianHMM, GaussianMHMM)
from hmm.datasets import for_tests, models
from hmm.util import sample_and_split
from hmm.scoring import cluster_report


def test_gaussian_three():
    for impl in ["scaling", "log"]:
        random_state = sklearn.utils.check_random_state(1983)
        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = for_tests.three_cluster_gaussian(random_state)
        mixture = GaussianMHMM.GaussianMHMM(n_mixture_components=3, n_components=2, n_iterations=500, implementation=impl, random_state=random_state, verbose=logging.INFO)
        mixture.fit(train_observations, train_lengths)
        predicted = mixture.predict(test_observations, test_lengths)
        cluster_report(test_labels, predicted)
        print(pd.crosstab(test_labels, predicted))
        assert sklearn.metrics.normalized_mutual_info_score(test_labels, predicted) == pytest.approx(0.9772499851985815, 1e-3), impl


def test_smyth_extended():
    for impl in ["scaling", "log"]:
        random_state = sklearn.utils.check_random_state(1983)
        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = for_tests.get_smyth_extended(random_state)
        mixture = GaussianMHMM.GaussianMHMM(n_mixture_components=3, n_components=2, n_iterations=500, implementation=impl, random_state=random_state)
        mixture.fit(train_observations, train_lengths)
        predicted = mixture.predict(test_observations, test_lengths)
        cluster_report(test_labels, predicted)
        print(pd.crosstab(test_labels, predicted))
        assert sklearn.metrics.normalized_mutual_info_score(test_labels, predicted) >= 0.96


def test_gaussian_smyth2():
    for impl in ["log", "scaling"]:
        random_state = sklearn.utils.check_random_state(1983)
        model_1, model_2 = models.get_smyth_clustering_models(random_state)
        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = sample_and_split([model_1, model_2, ], [100, 260, ], random_state=random_state)
        mixture = GaussianMHMM.GaussianMHMM(n_mixture_components=2, n_components=2, n_iterations=100, implementation=impl, random_state=random_state, verbose=logging.DEBUG)
        mixture.fit(train_observations, train_lengths)
        predicted = mixture.predict(test_observations, test_lengths)
        print(predicted)
        print(test_labels)
        cluster_report(test_labels, predicted)
        print(pd.crosstab(test_labels, predicted))
        assert sklearn.metrics.normalized_mutual_info_score(test_labels, predicted) == pytest.approx(0.9231486338759094, 1e-3), impl
