import numpy as np
import pandas as pd
import sklearn.utils
import sklearn.model_selection
from hmm import CategoricalVariationalHMM, GaussianVariationalHMM
from hmm import model_selection
from hmm.datasets.for_tests import get_categorical_beal, get_gaussian_mcgrory_and_titterington_sequences


def test_selection_categorical():
    for criterion in ["lb", "tll"]:
        rs = sklearn.utils.check_random_state(9)
        data, lengths, _, _ = get_categorical_beal(rs, num_each=7, min_length=39, max_length=40)

        trainer = CategoricalVariationalHMM.CategoricalVariationalHMM(
            random_state=rs,
        )
        params = {
            "n_components": np.arange(1, 10),
            "n_inits": [10],
            "n_iterations":[500]
        }
        searcher = model_selection.HMMSearch(
            trainer,
            param_grid=params,
            scoring=model_selection.VARIATIONAL_SCORERS,
            refit=criterion,
            n_jobs=-1,
            verbose=True
        )
        searcher.fit(data, lengths)
        print(pd.DataFrame(searcher.scores_).T)
        print(pd.DataFrame(searcher.best_model_.A_posterior_))
        for scores in searcher.scores_.values():
            assert "mean_test_DIC" not in scores
        print(searcher.best_model_)
        # This is a pathalogical test case - not enough sample data to find the correct model
        assert searcher.best_model_.n_components == 7, (criterion, searcher.best_model_.n_components)
        assert len(searcher.models_) == 9


def test_selection_gaussian():
    for criterion in ["tll"]:
        rs = sklearn.utils.check_random_state(2)
        data, _, lengths = get_gaussian_mcgrory_and_titterington_sequences(n_samples=20, sample_size=50, random_state=rs)
        trainer = GaussianVariationalHMM.GaussianVariationalHMM(n_components=4, n_iterations=1000, random_state=rs, n_inits=1, n_jobs=1)
        params = {
            "n_components": np.arange(1,8),
            "n_inits": [1]

        }
        searcher = model_selection.HMMSearchCV(
            trainer,
            param_grid=params,
            cv=sklearn.model_selection.RepeatedKFold(2, 2, random_state=rs),
            scoring=model_selection.GAUSSIAN_VARIATIONAL_SCORERS,
            refit=criterion,
            n_jobs=-1,
            verbose=True,
        )
        searcher.fit(data, lengths)
        print(pd.DataFrame(searcher.scores_))
        for scores in searcher.scores_.values():
            assert "mean_test_DIC" in scores
        assert searcher.best_model_.n_components == 4, (criterion, searcher.best_model_.n_components)

        assert len(searcher.models_) == 7, len(searcher.models_)
    for criterion in ["DIC", "lb"]:
        rs = sklearn.utils.check_random_state(2)
        data, _, lengths = get_gaussian_mcgrory_and_titterington_sequences(n_samples=20, sample_size=50, random_state=rs)
        trainer = GaussianVariationalHMM.GaussianVariationalHMM(n_components=4, n_iterations=1000, random_state=rs, n_inits=1, n_jobs=1)
        params = {
            "n_components": np.arange(1,8),
            "n_inits": [10]

        }
        searcher = model_selection.HMMSearch(
            trainer,
            param_grid=params,
            scoring=model_selection.GAUSSIAN_VARIATIONAL_SCORERS,
            refit=criterion,
            n_jobs=-1,
            verbose=True,
        )
        searcher.fit(data, lengths)
        print(pd.DataFrame(searcher.scores_).T)
        for scores in searcher.scores_.values():
            assert "DIC" in scores
        assert searcher.best_model_.n_components == 4, (criterion, searcher.best_model_.n_components)

        assert len(searcher.models_) == 7, len(searcher.models_)



if __name__ == "__main__":
    test_selection_gaussian()
    #test_selection_categorical()
