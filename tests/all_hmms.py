

from hmm import CategoricalHMM, CategoricalVariationalHMM, GaussianVariationalHMM, MultivariateGaussianHMM, MultivariateGaussianVariationalHMM, PoissonHMM, PoissonVariationalHMM

from hmm.datasets import models

# Sample models for testing
_em_cat = CategoricalHMM.CategoricalHMM(init_pi=None, init_A=None, init_emissions=None)
_em_cat.pi_ = [.26, .25]
_em_cat.A_ = [[.7, .3], [.3, .7]]
_em_cat.B_ = [[0, .4, .6, 0], [.2, .1, 0, .7]]

_vi_cat = CategoricalVariationalHMM.CategoricalVariationalHMM()
_vi_cat.pi_prior_ = [1, 1]
_vi_cat.A_prior_ = [[1, 1], [1, 1]]
_vi_cat.B_prior_ = [[1, 1, 1,1], [1,1,1,1]]
_vi_cat.pi_posterior_ = [25, 25]
_vi_cat.A_posterior_ = [[700, 300], [300, 700]]
_vi_cat.B_posterior_ = [[0, 400, 600, 0], [200, 100, 0, 700]]

_em_gauss = models.get_smyth_clustering_models()[0]

_em_poisson = PoissonHMM.PoissonHMM(init_pi=None, init_A=None, init_emissions=None)
_em_poisson.pi_ = [.26, .25]
_em_poisson.A_ = [[.7, .3], [.3, .7]]
_em_poisson.rates_ = [5, 10]

_vi_poisson = PoissonVariationalHMM.PoissonVariationalHMM()
_vi_poisson.pi_prior_ = [1, 1]
_vi_poisson.A_prior_ = [[1, 1], [1, 1]]
_vi_poisson.pi_posterior_ = [25, 25]
_vi_poisson.A_posterior_ = [[700, 300], [300, 700]]
_vi_poisson.rates_prior_ = [8, 8]
_vi_poisson.rates_posterior_ = [5, 10]
_vi_poisson.a_prior_ = [8, 8]
_vi_poisson.b_prior_ = [1, 1]
_vi_poisson.a_posterior_ = [110, 259]
_vi_poisson.b_posterior_ = [25, 25]

_em_mv_gauss = MultivariateGaussianHMM.MultivariateGaussianHMM(init_pi=None, init_A=None, init_emissions=None, covariance_type="full")
_em_mv_gauss.pi_ = [.26, .25]
_em_mv_gauss.A_ = [[.7, .3], [.3, .7]]
_em_mv_gauss.means_ = [[0, 5], [0, 10]]
_em_mv_gauss.covariances_ = [[[2, 1], [1, 2]], [[1, 2], [2, 1]]]

_vi_gauss = GaussianVariationalHMM.GaussianVariationalHMM()
_vi_gauss.pi_prior_ = [1, 1]
_vi_gauss.A_prior_ = [[1, 1], [1, 1]]
_vi_gauss.means_prior_ = [2.5, 2.5]
_vi_gauss.beta_prior_ = [1, 1]
_vi_gauss.a_prior_ = [1, 1]
_vi_gauss.b_prior_ = [1e-3, 1e-3]
_vi_gauss.pi_posterior_ = [25, 25]
_vi_gauss.A_posterior_ = [[700, 300], [300, 700]]
_vi_gauss.means_posterior_ = [0, 5]
_vi_gauss.beta_posterior_ = [ 150, 150]
_vi_gauss.a_posterior_ = [150, 150]
_vi_gauss.b_posterior_ = [10, 10]

_vi_mv_gauss_full = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(covariance_type="full")
_vi_mv_gauss_full.pi_prior_ = [1, 1]
_vi_mv_gauss_full.A_prior_ = [[1, 1], [1, 1]]
_vi_mv_gauss_full.B_prior_ = [[1, 1, 1,1], [1,1,1,1]]
_vi_mv_gauss_full.pi_posterior_ = [25, 25]
_vi_mv_gauss_full.A_posterior_ = [[700, 300], [300, 700]]
_vi_mv_gauss_full.B_posterior_ = [[0, 400, 600, 0], [200, 100, 0, 700]]
_vi_mv_gauss_full.means_prior_ = [[2.5, 0], [2.5, 0]]
_vi_mv_gauss_full.means_posterior_ = [[5, 0], [0, 0]]
_vi_mv_gauss_full.beta_prior_ = [1, 1]
_vi_mv_gauss_full.beta_posterior_ = [150, 150]
_vi_mv_gauss_full.degrees_of_freedom_prior_ = [1, 1]
_vi_mv_gauss_full.degrees_of_freedom_posterior_= [10, 10]
_vi_mv_gauss_full.scale_prior_ = [[[1e-3, 2e-3], [2e-3, 1e-3]],[[1e-3, 2e-3], [2e-3, 1e-3]]]
_vi_mv_gauss_full.scale_posterior_ = [[[10, 1], [1, 10]],[[10, 1], [1, 10]]]

_vi_mv_gauss_tied = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(covariance_type="tied")
_vi_mv_gauss_tied.pi_prior_ = [1, 1]
_vi_mv_gauss_tied.A_prior_ = [[1, 1], [1, 1]]
_vi_mv_gauss_tied.B_prior_ = [[1, 1, 1,1], [1,1,1,1]]
_vi_mv_gauss_tied.pi_posterior_ = [25, 25]
_vi_mv_gauss_tied.A_posterior_ = [[700, 300], [300, 700]]
_vi_mv_gauss_tied.B_posterior_ = [[0, 400, 600, 0], [200, 100, 0, 700]]
_vi_mv_gauss_tied.means_prior_ = [[2.5, 0], [2.5, 0]]
_vi_mv_gauss_tied.means_posterior_ = [[5, 0], [0, 0]]
_vi_mv_gauss_tied.beta_prior_ = [1, 1]
_vi_mv_gauss_tied.beta_posterior_ = [150, 150]
_vi_mv_gauss_tied.degrees_of_freedom_prior_ = 1
_vi_mv_gauss_tied.degrees_of_freedom_posterior_= 100
_vi_mv_gauss_tied.scale_prior_ = [[1e-3, 2e-3], [2e-3, 1e-3]]
_vi_mv_gauss_tied.scale_posterior_ = [[10, 1], [1, 10]]

_vi_mv_gauss_diagonal = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(covariance_type="diagonal")
_vi_mv_gauss_diagonal.pi_prior_ = [1, 1]
_vi_mv_gauss_diagonal.A_prior_ = [[1, 1], [1, 1]]
_vi_mv_gauss_diagonal.B_prior_ = [[1, 1, 1,1], [1,1,1,1]]
_vi_mv_gauss_diagonal.pi_posterior_ = [25, 25]
_vi_mv_gauss_diagonal.A_posterior_ = [[700, 300], [300, 700]]
_vi_mv_gauss_diagonal.B_posterior_ = [[0, 400, 600, 0], [200, 100, 0, 700]]
_vi_mv_gauss_diagonal.means_prior_ = [[2.5, 0], [2.5, 0]]
_vi_mv_gauss_diagonal.means_posterior_ = [[5, 0], [0, 0]]
_vi_mv_gauss_diagonal.beta_prior_ = [1, 1]
_vi_mv_gauss_diagonal.beta_posterior_ = [150, 150]
_vi_mv_gauss_diagonal.degrees_of_freedom_prior_ = [1, 1]
_vi_mv_gauss_diagonal.degrees_of_freedom_posterior_= [100, 100]
_vi_mv_gauss_diagonal.scale_prior_ = [[[1e-3, 0], [0, 1e-3]],[[1e-3, 0], [0, 1e-3]]]
_vi_mv_gauss_diagonal.scale_posterior_ = [[[200, 0], [0, 100]],[[100, 0], [0, 200]]]

_vi_mv_gauss_spherical = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(covariance_type="spherical")
_vi_mv_gauss_spherical.pi_prior_ = [1, 1]
_vi_mv_gauss_spherical.A_prior_ = [[1, 1], [1, 1]]
_vi_mv_gauss_spherical.B_prior_ = [[1, 1, 1,1], [1,1,1,1]]
_vi_mv_gauss_spherical.pi_posterior_ = [25, 25]
_vi_mv_gauss_spherical.A_posterior_ = [[700, 300], [300, 700]]
_vi_mv_gauss_spherical.B_posterior_ = [[0, 400, 600, 0], [200, 100, 0, 700]]
_vi_mv_gauss_spherical.means_prior_ = [[2.5, 0], [2.5, 0]]
_vi_mv_gauss_spherical.means_posterior_ = [[5, 0], [0, 0]]
_vi_mv_gauss_spherical.beta_prior_ = [1, 1]
_vi_mv_gauss_spherical.beta_posterior_ = [150, 150]
_vi_mv_gauss_spherical.degrees_of_freedom_prior_ = [1, 1]
_vi_mv_gauss_spherical.degrees_of_freedom_posterior_= [200, 200]
_vi_mv_gauss_spherical.scale_prior_ = [[[1e-3, 0], [0, 1e-3]],[[1e-3, 0], [0, 1e-3]]]
_vi_mv_gauss_spherical.scale_posterior_ = [[[100, 0], [0, 100]],[[100, 0], [0, 100]]]
SAMPLE_MODELS = {
     "CategoricalHMM": _em_cat,
     "CategoricalVariationalHMM": _vi_cat,
     "GaussianHMM": _em_gauss,
     "GaussianVariationalHMM": _vi_gauss,
     "PoissonHMM": _em_poisson,
     "PoissonVariationalHMM": _vi_poisson,
     "MultivariateGaussianHMM": _em_mv_gauss,
     "MultivariateGaussianVariationalHMM_full": _vi_mv_gauss_full,
     "MultivariateGaussianVariationalHMM_tied": _vi_mv_gauss_tied,
     "MultivariateGaussianVariationalHMM_diagonal": _vi_mv_gauss_diagonal,
     "MultivariateGaussianVariationalHMM_spherical": _vi_mv_gauss_spherical,
}
