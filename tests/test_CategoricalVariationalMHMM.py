# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import base64
import logging
import pickle

import numpy as np
import pandas as pd
import pytest
import sklearn.metrics
import sklearn.utils

from hmm import (CategoricalHMM, CategoricalVariationalBlockHMM,
                 CategoricalVariationalMHMM, GaussianHMM,
                 GaussianVariationalHMM,
                 GaussianVariationalMHMM)
from hmm.datasets import for_tests
from hmm.util import sample_and_split
from hmm.scoring import cluster_report


N_INITS=10


def test_convergence():

    random_state = sklearn.utils.check_random_state(1143)
    rs1 = sklearn.utils.check_random_state(None)
    for impl in ["scaling", "log"]:
        observations, lengths, labels, _ = for_tests.get_categorical_beal(random_state)
        print(observations.shape)
        for n_mixture_components in range(2, 5):
            for n_components in range(2, 5):
                try:
                    rs2 = rs1.get_state()
                    mixture = CategoricalVariationalMHMM.CategoricalVariationalMHMM(n_mixture_components=n_mixture_components, n_components=n_components, n_iterations=500, n_inits=N_INITS, n_jobs=1, random_state=rs1, verbose=logging.INFO)
                    mixture.fit(observations, lengths)
                except AssertionError as err:
                    print(err)
                    state = pickle.dumps(rs2)
                    print("n_mixture_components={}".format(n_mixture_components))
                    print("n_components={}".format(n_components))
                    print("Random state was")
                    print(base64.b64encode(state))
                    raise
                for i in range(mixture.n_mixture_components):
                    print("pi")
                    print(pd.DataFrame(mixture.pi_normalized_[i]))
                    print("A")
                    print(pd.DataFrame(mixture.A_normalized_[i]))
                    print("B")
                    print(pd.DataFrame(mixture.B_normalized_[i]))
                print("lb", n_mixture_components, n_components, mixture.lower_bound_[-10:])



def test_categorical_beal():

    for impl in ["scaling", "log"]:
        random_state = sklearn.utils.check_random_state(1143)
        observations, lengths, labels, _ = for_tests.get_categorical_beal(random_state)
        mixture = CategoricalVariationalMHMM.CategoricalVariationalMHMM(
            n_mixture_components=3,
            n_components=3,
            n_features=3,
            n_iterations=500,
            n_inits=10,
            n_jobs=1,
            tol=1e-9,
            random_state=random_state,
        )
        mixture.fit(observations, lengths)
        mixture_predicted = mixture.predict_log_proba(observations, lengths)
        assert mixture_predicted.shape == (len(lengths), 3)
        print(mixture_predicted)
        mixture_scores = mixture.score_samples(observations, lengths)
        assert mixture_scores.shape == (len(lengths), )
        print(mixture_scores)

        for mixture_component in range(mixture.n_mixture_components):
            print("Component {}: {}".format(mixture_component, mixture.mixture_weights_normalized_[mixture_component]))
            print("pi")
            print(mixture.pi_normalized_[mixture_component])
            print(mixture.pi_posterior_[mixture_component])
            print("A")
            print(mixture.A_normalized_[mixture_component])
            print(mixture.A_posterior_[mixture_component])
            print("B")
            print(mixture.B_normalized_[mixture_component])
            print(mixture.B_posterior_[mixture_component])

        predicted = mixture.predict(observations, lengths)
        print(predicted)
        print(pd.crosstab(predicted, labels))
        assert sklearn.metrics.normalized_mutual_info_score(predicted, labels) == 1, impl
        #print("dic: ", mixture.dic(observations))
        print("score: ", mixture.score_samples(observations, lengths).sum())
        print("LB: ", mixture.lower_bound_[-1])


def test_music_analysis():
    rs = sklearn.utils.check_random_state(1983)
    models = for_tests.get_music_analysis(rs)
    observations = []
    labels = []
    lengths = []
    for m in models:
        o, h, l = m.sample(50, 30, random_state=rs)
        observations.extend(o)
        lengths.extend(l)

    mixture = CategoricalVariationalMHMM.CategoricalVariationalMHMM(3, 3, n_iterations=500, n_inits=N_INITS, n_jobs=1, random_state=rs, verbose=logging.INFO)
    mixture.fit(observations, lengths)
    for i in range(mixture.n_mixture_components):
        print("pi")
        print(pd.DataFrame(mixture.pi_normalized_[i]))
        print("A")
        print(pd.DataFrame(mixture.A_normalized_[i]))
        print("B")
        print(pd.DataFrame(mixture.B_normalized_[i]))
        print("lb", mixture.n_mixture_components, mixture.n_components, mixture.lower_bound_[-10:])

if __name__ == "__main__":
    test_music_analysis()
