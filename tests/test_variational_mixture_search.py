import numpy as np
import pandas as pd
import sklearn.utils
from hmm import CategoricalVariationalMHMM, GaussianVariationalMHMM
from hmm import model_selection
from hmm.datasets.for_tests import get_smyth_data, get_categorical_beal


def test_selection_categorical():
    for criterion in ["lb"]:
        rs = sklearn.utils.check_random_state(422)
        data, lengths, _, _ = get_categorical_beal(rs, num_each=14, max_length=39)
        trainer = CategoricalVariationalMHMM.CategoricalVariationalMHMM(n_iterations=500, random_state=rs)
        param_grid = {
            "n_mixture_components": [1, 2, 3, 4],
            "n_components": [1, 2, 3, 4, 5, 6, 7],
            "n_inits": [5],
            "pi_prior": [1]
        }
        searcher = model_selection.HMMSearch(
            trainer,
            param_grid=param_grid,
            scoring=model_selection.VARIATIONAL_SCORERS,
            refit=criterion
        )
        searcher.fit(data, lengths)

        frame = pd.DataFrame(searcher.scores_).T
        print(frame[[c for c in frame.columns if c.startswith("para")] + ["tll", "lb"]].sort_values("lb"))
        for scores in searcher.scores_.values():
            assert "DIC" not in scores
        assert searcher.best_model_.n_components == 3, (criterion, searcher.best_model_.n_components)
        assert searcher.best_model_.n_mixture_components == 3, (criterion, searcher.best_model_.n_mixture_components)


def test_selection_gaussian():
    for criterion in ["DIC", "lb", ]:
        rs = sklearn.utils.check_random_state(5)
        train_observations, test_observations, train_labels, test_labels, train_lengths, test_lengths = get_smyth_data(rs)
        train_observations = np.concatenate([train_observations, test_observations])
        train_lengths = np.concatenate([train_lengths, train_lengths])
        param_grid = {
            "n_mixture_components": [1, 2, 3],
            "n_components": [1, 2, 3],
            "n_inits": [4],
            "n_iterations": [100]
        }

        trainer = GaussianVariationalMHMM.GaussianVariationalMHMM(random_state=rs)
        searcher = model_selection.HMMSearch(
            trainer,
            param_grid=param_grid,
            scoring=model_selection.GAUSSIAN_VARIATIONAL_SCORERS,
            refit=criterion,
            n_jobs=-1
        )
        searcher.fit(train_observations, train_lengths)
        frame = pd.DataFrame(searcher.scores_).T
        print(frame.columns)
        cols = [col for col in frame.columns if col.startswith("param")]
        print(frame[["lb", "tll", "DIC"] + cols])
        for scores in searcher.scores_.values():
            assert "DIC" in scores
        assert searcher.best_model_.n_components == 2, (criterion, searcher.best_model_.n_components)
        assert searcher.best_model_.n_mixture_components == 2, (criterion, searcher.best_model_.n_mixture_components)


if __name__ == "__main__":
    test_selection_gaussian()
    test_selection_categorical()
