# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging
import requests
import collections
import pathlib

import sklearn.metrics
import sklearn.utils
import pandas as pd


from hmm import GaussianHMM, GaussianVariationalHMM, plots

# https://github.com/LuisM78/HMMs-for-occupancy-detection/
TRAINING_DATA = "https://raw.githubusercontent.com/LuisM78/HMMs-for-occupancy-detection/master/datatraining.txt"
TEST_DATA = "https://raw.githubusercontent.com/LuisM78/HMMs-for-occupancy-detection/master/datatest.txt"
TEST2_DATA = "https://raw.githubusercontent.com/LuisM78/HMMs-for-occupancy-detection/master/datatest2.txt"
DATA_DIR = "HMMs-for-occupancy-detection"

def main():
    datafiles = {
        "training": TRAINING_DATA,
        "test1" : TEST_DATA,
        "test2": TEST2_DATA
    }
    mapped_datafiles = {}

    for key, dataset in datafiles.items():

        filename = pathlib.Path(DATA_DIR).joinpath(dataset.split("/")[-1])
        mapped_datafiles[key] = filename
        dirname = filename.parent
        if not dirname.exists():
            dirname.mkdir(parents=True)
        if not filename.exists():
            want = requests.get(dataset)
            with open(filename, mode="w") as fd:
                fd.write(want.text)
        else:
            print("already downloaded: {}".format(filename))

    training = pd.read_csv(mapped_datafiles["training"], index_col=0, parse_dates=["date"]).set_index("date")
    co2 = training["CO2"]
    occupancy = training["Occupancy"]
    # 5 minute rolling averages
    rolling_5min_co2 = co2.rolling(5).mean().dropna()
    rolling_5min_occupancy = occupancy.rolling(5).mean().dropna()

    rolling_5min_co2_diff = rolling_5min_co2.diff().dropna()
    rolling_5min_occupancy = rolling_5min_occupancy[1:].astype(int)
    print(rolling_5min_co2_diff.shape)
    print(rolling_5min_occupancy.shape)

    # Minor data munging
    rolling_5min_co2_diff_copy = rolling_5min_co2_diff.copy()
    #rolling_5min_co2_diff_copy[rolling_5min_co2_diff_copy < -100] = -10
    #rolling_5min_co2_diff_copy[rolling_5min_co2_diff_copy > 100] = 10

    # correct shape
    data = rolling_5min_co2_diff_copy[:, None]
    print(data.shape)
    random_state = sklearn.utils.check_random_state(1234)
    try:
        gauss2state = GaussianHMM.GaussianHMM(n_components=2, n_inits=4, verbose=logging.DEBUG, implementation="scaling", random_state=random_state, allowed_to_use_log=False)
        gauss2state.fit(data, [data.shape[0]])
        learned_states = gauss2state.transform(data, [data.shape[0]])
        raise VaueError("oops")
    except AssertionError:
        pass

    gauss2state = GaussianHMM.GaussianHMM(n_components=2, n_inits=4, verbose=logging.ERROR, n_iterations=500, implementation="scaling", random_state=random_state, allowed_to_use_log=True)
    gauss2state.fit(data, [data.shape[0]])
    print(gauss2state.means_)
    learned_states = gauss2state.transform(data, [data.shape[0]]).ravel()
    print(collections.Counter(learned_states.ravel()))
    print(sklearn.metrics.classification_report(rolling_5min_occupancy, learned_states))
    print("Accuracy: {:.4f}".format(sklearn.metrics.accuracy_score(rolling_5min_occupancy, learned_states)))
    print(sklearn.metrics.mutual_info_score(rolling_5min_occupancy, learned_states))

    variational = GaussianVariationalHMM.GaussianVariationalHMM(n_components=2, n_inits=4, n_iterations=500, verbose=logging.ERROR, random_state=random_state)
    variational.fit(data, [data.shape[0]])
    print(variational.means_posterior_)
    learned_states = variational.transform(data, [data.shape[0]]).ravel()
    print(collections.Counter(learned_states.ravel()))
    print(sklearn.metrics.classification_report(rolling_5min_occupancy, learned_states))
    print("Accuracy: {:.4f}".format(sklearn.metrics.accuracy_score(rolling_5min_occupancy, learned_states)))
    print(sklearn.metrics.mutual_info_score(rolling_5min_occupancy, learned_states))

    for i in range(1, 10):
        variational = GaussianVariationalHMM.GaussianVariationalHMM(n_components=i, n_inits=4, n_iterations=500, random_state=random_state)
        variational.fit(data, [data.shape[0]])
        print(i, variational.lower_bound_[-1])


if __name__ == "__main__":

    main()
