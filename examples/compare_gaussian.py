# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
========================
Comparison with hmmlearn
========================
"""
import logging
import time
import os

import sklearn.utils
import numpy as np

from hmm.datasets import synthetic
from hmm import GaussianHMM, GaussianVariationalHMM
import hmmlearn.hmm

def generate_data():

    random_state = sklearn.utils.check_random_state(2)
    model = synthetic.get_gaussian_mcgrory_and_titterington_model(random_state)
    sequences, hidden_sequences, lengths = model.sample(1, 500, random_state=random_state)

    return sequences, lengths


def test_gaussianv():
    print("cython-veriational")
    random_state = 3
    sequence, lengths = generate_data()
    for impl in ["scaling"]:
        trainer = GaussianVariationalHMM.GaussianVariationalHMM(n_components=5, n_iterations=500, verbose=False, random_state=random_state, implementation=impl)
        start = time.time()
        trainer.fit(sequence, lengths)
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, "cython"))
        print("means", trainer.means_posterior_)
        print("variances", trainer.variances_posterior_)
        print("SCORE")
        print(trainer.score(sequence, lengths))



def test_gaussian():
    print("cython-em")
    random_state = 3
    sequence, lengths = generate_data()
    for impl in ["scaling"]:
        trainer = GaussianHMM.GaussianHMM(n_components=5, n_iterations=500, verbose=False, random_state=random_state, implementation=impl)
        start = time.time()
        trainer.fit(sequence, lengths)
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, "cython"))
        print("means", trainer.means_)
        print("variances", trainer.variances_)
        print("SCORE")
        print(trainer.score(sequence, lengths))

def test_hmmlearn():
    print("hmmlearn")
    random_state = 3
    sequence, lengths = generate_data()
    trainer = hmmlearn.hmm.GaussianHMM(n_components=5, n_iter=500, random_state=random_state, verbose=False)
    start = time.time()
    trainer.fit(sequence, lengths)
    end = time.time()
    print("Training Took {} seconds {}".format(end - start, "hmmlearn"))
    print("means", trainer.means_.ravel())
    print("variances",trainer.covars_.ravel())
    print("SCORE")
    print(trainer.score(sequence, lengths))


if __name__ == "__main__":
    test_gaussian()
    test_gaussianv()
    test_hmmlearn()
