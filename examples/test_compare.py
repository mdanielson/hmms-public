# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
========================
Comparison with hmmlearn
========================
"""
import re
import time
import string
import os

from nltk.corpus import brown
import numpy as np
from hmm import CategoricalHMM
import hmmlearn.hmm


def setup():
    print("SETUP!")


def teardown():
    print("TEAR DOWN!")


def get_inits():
    pi = np.asarray([0.51316, .48684])
    pi /= pi.sum()

    A = np.asarray([
        [0.47468, 0.52532],
        [0.51656, 0.48344],
    ])
    for i in range(A.shape[0]):
        A[i, :] /= A[i, :].sum()
    B = np.asarray([
        [0.03735, 0.03909],
        [0.03408, 0.03537],
        [0.03455, 0.03537],
        [0.03828, 0.03909],
        [0.03782, 0.03583],
        [0.03922, 0.03630],
        [0.03688, 0.04048],
        [0.03408, 0.03537],
        [0.03875, 0.03816],
        [0.04062, 0.03909],
        [0.03735, 0.03490],
        [0.03968, 0.03723],
        [0.03548, 0.03537],
        [0.03735, 0.03909],
        [0.04062, 0.03397],
        [0.03595, 0.03397],
        [0.03641, 0.03816],
        [0.03408, 0.03676],
        [0.04062, 0.04048],
        [0.03548, 0.03443],
        [0.03922, 0.03537],
        [0.04062, 0.03955],
        [0.03455, 0.03816],
        [0.03595, 0.03723],
        [0.03408, 0.03769],
        [0.03408, 0.03955],
        [0.03688, 0.03397],
    ]).T

    for i in range(A.shape[0]):
        B[i, :] /= B[i, :].sum()
    return pi, A, B


def get_words(how_many):
    chars = " ".join(clean(w) for w in brown.words())
    mapping = {}
    rmapping = {}
    for i, item in enumerate(string.ascii_lowercase + " "):
        mapping[item] = i
        rmapping[i] = item

    sequence = np.asarray([mapping[c] for c in chars[:how_many]])
    return sequence, rmapping


def clean(w):
    keep = [c for c in w.lower() if c in string.ascii_lowercase or c == " "]
    return "".join(keep)


def test_cython_small():
    sequence, rmapping = get_words(10000)
    # print(sequence)
    print("I RAN!")
    print((len(sequence)))
    pi, A, B = get_inits()
    model = CategoricalHMM.CategoricalHMM(n_components=A.shape[0], init_pi=None, init_A=None, init_emissions=None, n_iterations=100)
    model.pi_ = pi
    model.A_ = A
    model.B_ = B
    sequence = sequence[None, :]
    run_10000(model, sequence, rmapping, "cython-ish")
    print(np.asarray(model.pi_))
    print(np.asarray(model.A_))
    print(model.score(sequence))


def run_10000(model, sequence, mapping, tag, iterations=100):
    print(sequence)
    start = time.time()
    model.fit(sequence)
    end = time.time()
    print("Training Took {} seconds {}".format(end - start, tag))
    for idx, letter in mapping.items():
        print("{} {} {} {}".format(idx, letter, model.B_[0, idx], model.B_[1, idx]))


def test_hmmlearn():
    pi, A, B = get_inits()
    multinomial = hmmlearn.hmm.MultinomialHMM(
        n_components=2,
        n_iter=100,
        tol=1e-10,
        init_params="",
        verbose=True
    )
    multinomial.emissionprob_ = B
    multinomial.transmat_ = A
    multinomial.startprob_ = pi
    # ).T
    print(multinomial)
    sequence, rmapping = get_words(10000)
    sequence = sequence[None, :]
    print(sequence)
    start = time.time()
    multinomial.fit(sequence)
    end = time.time()
    print("Training Took {} seconds {}".format(end - start, "hmmlearn"))

    print(multinomial.score(sequence))
    print(multinomial.startprob_)
    print(multinomial.transmat_)
    for idx, letter in sorted(rmapping.items()):
        print(idx, letter, multinomial.emissionprob_[0, idx], multinomial.emissionprob_[1, idx])

if __name__ == "__main__":
    test_hmmlearn()
    test_cython_small()
