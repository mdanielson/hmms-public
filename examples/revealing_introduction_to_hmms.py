# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import numpy as np
import pytest

from hmm._hmm import new_categorical, viterbi


def test_revealing_introduction():
    """
    Tests extracted from:
        http://www.cs.sjsu.edu/~stamp/RUA/HMM.pdf (October 17, 2018)
    """
    A = np.asarray([
        [
            .7, .3
        ],
        [
            .4, .6
        ]
    ])
    B = np.asarray([
        [
            .1, .4, .5,
        ],
        [
            .7, .2, .1,
        ]
    ])

    pi = np.asarray([
        0, 1.
    ])
    #
    # problem (1)
    trainer = new_categorical(pi, A, B, impl="scaling")

    X = np.asarray(
        [
            [1.], [0.], [2.]
        ]
    )

    lengths = np.asarray([3])
    c, alpha = trainer.scaling_alpha_pass(X, lengths)
    c = np.asarray(c)
    alpha = np.asarray(alpha)
    cc = 1
    ans_paper = [
        [
            0, .2
        ],
        [
            .008, .084,
        ],
        [
            .0196, .00528
        ],
    ]

    for i in range(X.shape[0]):
        cc *= c[i]
        scaled = alpha[i, :] / cc
        for j in range(2):
            assert scaled[j] == pytest.approx(ans_paper[i][j], 1e-5)
        print(i, scaled)
    beta = trainer.scaling_beta_pass(X, lengths, c)
    beta = np.asarray(beta)
    # TODO
    print("beta")

    #
    # Dynamic Programming Search
    trainer = new_categorical(pi, A, B, impl="scaling")

    X_probs = np.zeros((3, 2))
    for i in range(X.shape[0]):
        for j in range(A.shape[0]):
            X_probs[i, j] = trainer.pdf(j, X[i])

    # Verify Probabilities
    pointers, probabilities = viterbi.single_viterbi_probability(trainer._pi, trainer._A, X_probs)
    probabilities = np.asarray(np.exp(probabilities))
    pointers = np.asarray(pointers)

    ans = np.asarray(
        [
            [0, .2],
            [.008, .084],
            [.0168, .00504]
        ]
    )
    for i in range(3):
        for t in range(2):
            assert probabilities[i, t] == pytest.approx(ans[i, t], 1e-3)

    # Verify path
    path = trainer.viterbi(X, lengths)
    assert path.tolist() == [1, 1, 0]

    # Probability of all possible sequences sums to 1
    sequences = []
    lengths = []
    for i in range(3):
        for j in range(3):
            for k in range(3):
                sequences.extend([[i], [j], [k]])
                lengths.append(3)
    #sequences = [[0,0,0],[1, 0, 2]]
    sequences = np.asarray(sequences, dtype=float)
    lengths = np.asarray(lengths, dtype=int)
    # print(sequences)
    trainer = new_categorical(pi, A, B, impl="scaling")
    c, alpha = trainer.scaling_alpha_pass(sequences, lengths)
    c = np.asarray(c)

    alpha = np.asarray(alpha)

    sums = []
    seq_start = 0
    for i, l in enumerate(lengths):
        seq_end = seq_start + l
        sequence = sequences[seq_start:seq_end]
        cs = c[seq_start:seq_end]
        alphas = alpha[seq_start:seq_end]
        cc = 1
        for j in range(len(sequence)):
            cc *= cs[j]
            alphas[j, :] /= cc
        sums.append(alphas[-1, :].sum())
        seq_start = seq_end
    assert sum(sums) == pytest.approx(1.0)

if __name__ == "__main__":
    test_revealing_introduction()
