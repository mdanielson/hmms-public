# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause

from hmm.mixtures import GaussianMixtureModel
from hmm.datasets.real import load_old_faithful_waiting
from hmm.model_selection import make_model_summary

from hmm.utilities import normal

def parse_interval(interval):
    interval = interval.strip(" ")
    hours, minutes, seconds  = interval.split(":")
    #assert int(seconds) == 0, seconds
    return float(hours) * 60 + float(minutes) + round(float(seconds) / 60)


def main():

    waiting_times = load_old_faithful_waiting()

    models = {}
    data_2d = waiting_times.data[:, None]

    for i in range(1,5):
        models[i] = GaussianMixtureModel.GaussianMixtureModel(n_components=i)
        models[i].fit(data_2d)


    summary = make_model_summary(models, data_2d)
    print(summary)


if __name__ == "__main__":
    main()
