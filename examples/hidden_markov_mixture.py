# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging
import copy
import numpy as np
import pandas as pd
import scipy.special
import sklearn.utils
import sklearn.model_selection
import sklearn.preprocessing

from hmm import CategoricalHMM, HMMCluster, CategoricalMHMM, CategoricalVariationalMHMM
from hmm._hmm import forward_backward
from hmm._hmm.em import new_mixture_categorical
from hmm._hmm.variational import new_mixture_categorical as new_mixture_categorical_variational

from hmm.datasets import synthetic, for_tests


def sample_and_split(models, n_samples, sample_lengths=60, random_state=None):

    observations = []
    labels = []
    for i, (m, n) in enumerate(zip(models, n_samples)):
        labels += [i] * n
        observed, _ = m.sample(n , sample_lengths, random_state=random_state)
        observations += observed.tolist()
    observations = np.asarray(observations)

    train_idx, test_idx = sklearn.model_selection.train_test_split(np.arange(observations.shape[0]), test_size=.4, stratify=labels, random_state=random_state)

    test_labels = np.asarray([labels[i] for i in test_idx])
    train_labels = np.asarray([labels[i] for i in train_idx])

    test_observations = observations[test_idx]
    training_observations = observations[train_idx]
    return training_observations, test_observations, train_labels, test_labels

def main2():
    random_state = sklearn.utils.check_random_state(2342)
    data, labels = synthetic.get_categorical_beal(random_state)
    mixture = CategoricalMHMM.CategoricalMHMM(
        n_mixture_components=4,
        n_components=4,
        n_iterations=3000,
        random_state=random_state,
        n_inits=4,
        n_jobs=4,
        verbose=False,
        tol=1e-20,
        smoothing=("k", 1e-6)
    )
    mixture.fit(data)
    for i in range(mixture.n_components):
        print("=="*10)
        print(i)
        print(pd.Series(mixture.pi_[i]))
        print(pd.DataFrame(mixture.A_[i]))
        print(pd.DataFrame(mixture.B_[i]))



    mixture = CategoricalVariationalMHMM.CategoricalVariationalMHMM(
        n_mixture_components=4,
        n_components=4,
        n_iterations=3000,
        random_state=random_state,
        n_inits=4,
        n_jobs=4,
        verbose=False,
        tol=1e-20
    )
    data, labels = synthetic.get_categorical_beal(random_state)
    mixture.fit(data)
    for i in range(mixture.n_components):
        print("=="*10)
        print(i)
        print(pd.Series(mixture.pi_posterior_[i]))
        print(pd.DataFrame(mixture.A_posterior_[i]))
        print(pd.DataFrame(mixture.B_posterior_[i]))

def main():
    random_state = 234
    train_observations, test_observations, train_labels, test_labels = for_tests.two_cluster_categorical_clustering(random_state)

    print(train_observations)
    print(train_labels)
    mixture = CategoricalMHMM.CategoricalMHMM(n_mixture_components=3, n_components=3, n_features=3, n_iterations=1000, verbose=False, random_state=copy.deepcopy(random_state), implementation="scaling")
    mixture.fit(train_observations)
    train_predicted = mixture.predict(train_observations)
    print(pd.crosstab(train_labels, train_predicted))
    #test_predicted = mixture.predict(test_observations)
    #print(pd.crosstab(test_labels, test_predicted))
    #print(mixture.transform(train_observations))

    #mixture = VariationalMixtureHMM(n_mixture_components=3, n_components=3, n_features=3, n_iterations=100, random_state=copy.deepcopy(random_state))
    #mixture.fit(train_observations)
    #train_predicted = mixture.predict(train_observations)
    #print(train_predicted[0])
    #print(pd.crosstab(train_labels, train_predicted))
    #mixture = CategoricalVariationalMHMM.CategoricalVariationalMHMM(n_mixture_components=3, n_components=3, A_prior=A_prior, B_prior=B_prior, n_features=4, n_iterations=1000, random_state=copy.deepcopy(random_state))
    mixture = CategoricalVariationalMHMM.CategoricalVariationalMHMM(n_mixture_components=3, n_components=3, n_features=3, n_iterations=1000, random_state=copy.deepcopy(random_state), verbose=True, tol=1e-9)
    mixture.fit(train_observations)
    print(pd.DataFrame(mixture.A_posterior_[0]))
    print(pd.DataFrame(mixture.A_posterior_[1]))
    print(pd.DataFrame(mixture.A_posterior_[2]))
    #for mixture_component in range(mixture.n_mixture_components):
    #    print("Component {}: {}".format(mixture_component, mixture.mixture_weights_normalized_[mixture_component]))
    #    print(mixture.pi_normalized_[mixture_component])
    #    print(mixture.A_normalized_[mixture_component])
    #    print(mixture.B_normalized_[mixture_component])

    test_predicted = mixture.predict(test_observations)
    print(pd.crosstab(test_labels, test_predicted))


    #distance = HMMCluster.HMMDistanceCluster(n_mixture_components=2, n_iterations_1=100, n_iterations_2=500, hmm_impl=CategoricalHMM.CategoricalHMM(n_components=2, n_features=3))
    #distance.fit(train_observations)
    #train_predicted = distance.predict(train_observations)
    #print(pd.crosstab(train_labels, train_predicted))
    #test_predicted = distance.predict(test_observations)
    #print(pd.crosstab(test_labels, test_predicted))

class VariationalMixtureHMM(sklearn.base.BaseEstimator, sklearn.base.ClusterMixin):

    def __init__(self, n_mixture_components=2, n_components=2, n_features=3, n_iterations=10, tol=1e-6, random_state=None):
        self.n_mixture_components = n_mixture_components
        self.n_components = n_components
        self.n_features = n_features
        self.n_iterations = n_iterations
        self.tol = tol
        self.random_state = random_state

    def fit(self, X):
        rs = sklearn.utils.check_random_state(self.random_state)
        pi_ = np.full((self.n_mixture_components, self.n_components), 1., dtype=float)
        A_ = np.full((self.n_mixture_components, self.n_components, self.n_components), 1./self.n_components, dtype=float)
        B_ = np.full((self.n_mixture_components, self.n_components, self.n_features), 1./self.n_features, dtype=float)
        pi_posterior = np.zeros_like(pi_)
        A_posterior = np.zeros_like(A_)
        B_posterior = np.zeros_like(B_)
        #B_posterior = B_.copy() * X.shape[0] * X.shape[1]
        mixture_weights = np.full(self.n_mixture_components, 1./self.n_mixture_components)
        mixture_weights_posterior = rs.dirichlet(mixture_weights) * X.shape[0] + mixture_weights
        for i in range(self.n_mixture_components):
            pi_posterior[i] = mixture_weights[i] * rs.dirichlet(pi_[i]) * X.shape[0]
            for j in range(self.n_components):
                A_posterior[i, j] = mixture_weights[i] * rs.dirichlet(A_[i, j]) * X.shape[0] * X.shape[1]
                B_posterior[i, j] = mixture_weights[i] * rs.dirichlet(B_[i, j]) * X.shape[1] * X.shape[1]
        pi_posterior += pi_
        B_posterior += B_
        A_posterior += A_


        #pi_ = np.full((self.n_mixture_components, self.n_components), 1/self.n_components, dtype=float)
        #A_ = np.full((self.n_mixture_components, self.n_components, self.n_components), 1/self.n_components, dtype=float)
        #pi_posterior = pi_.copy() * X.shape[0]
        #A_posterior = A_.copy() * X.shape[0] * X.shape[1]
        #seed = np.array([100] *self.n_components)
        #for i in range(self.n_mixture_components):
        #    for j in range(self.n_components):
        #        A_[i, j] = rs.dirichlet(seed)
        #B_ = np.zeros((self.n_mixture_components, self.n_components, self.n_features))
        #seed = np.array([100] *self.n_features)
        #for i in range(self.n_mixture_components):
        #    for j in range(self.n_components):
        #        B_[i, j] = rs.dirichlet(seed)
        #B_posterior = B_.copy() * X.shape[0] * X.shape[1]
        #mixture_weights = np.full(self.n_mixture_components, 1/self.n_mixture_components)
        #mixture_weights_posterior = mixture_weights * X.shape[0]

        print("START")
        print(pi_)
        print(A_)
        print(B_)
        trainer = new_mixture_categorical_variational(
            mixture_weights,
            mixture_weights_posterior,
            pi_,
            pi_posterior,
            A_,
            A_posterior,
            B_,
            B_posterior,
            impl="scaling",
            log_level=logging.DEBUG
        )
        trainer.train(X[:, :, None].astype(float), self.n_iterations, tol=1e-6)
        self.trainer_ = trainer
        for cluster in range(self.n_mixture_components):
            print("Cluster {}: {}".format(cluster, trainer.mixture_weights[cluster]))
            print(pd.DataFrame(np.asarray(trainer._pi_normalized[cluster])))
            print(pd.DataFrame(np.asarray(trainer._A_normalized[cluster])))
            print(pd.DataFrame(np.asarray(trainer.emissions[cluster]._B_normalized)))
            print()

    def predict(self, X):

        probs = self.trainer_.loglik(X[:, :, None].astype(float))
        print(probs[:50])
        return probs.argmax(axis=1)

    def fit2(self, X):
        rs = sklearn.utils.check_random_state(self.random_state)
        pi_ = np.full((self.n_mixture_components, self.n_components), 1/self.n_components, dtype=float)
        A_ = np.full((self.n_mixture_components, self.n_components, self.n_components), 1/self.n_components, dtype=float)
        seed = np.array([100] *self.n_components)
        for i in range(self.n_mixture_components):
            for j in range(self.n_components):
                A_[i, j] = rs.dirichlet(seed)
        B_ = np.zeros((self.n_mixture_components, self.n_components, self.n_features), dtype=float)
        seed = np.array([100] *self.n_features)
        for i in range(self.n_mixture_components):
            for j in range(self.n_components):
                B_[i, j] = rs.dirichlet(seed)
        mixture_weights = np.full(self.n_mixture_components, 1/self.n_mixture_components)
        print("START")
        print(pi_)
        print(A_)
        print(B_)
        predictions = None
        ll = new_ll = None
        for iteration in range(self.n_iterations):
            membership_log_probs = np.zeros((X.shape[0], self.n_mixture_components))
            next_pi_ = np.zeros_like(pi_)
            next_A_ = np.zeros_like(A_)
            next_B_ = np.zeros_like(B_)

            #print(A_[0])
            #print(B_[0])
            for x in range(X.shape[0]):
                sequence = X[x]
                alpha = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components), dtype=float)
                beta = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components), dtype=float)
                digamma = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components, self.n_components), dtype=float)
                digamma_1 = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components), dtype=float)
                for cluster in range(self.n_mixture_components):
                    observation_log_probabilities = self.observation_probabilities(np.log(B_[cluster]), sequence)
                    forward_backward.wrapped_log_alpha_pass(
                        observation_log_probabilities,
                        np.log(pi_[cluster]),
                        np.log(A_[cluster]),
                        alpha[cluster],
                    )
                    log_prob = scipy.special.logsumexp(alpha[cluster][-1, :])
                    membership_log_probs[x, cluster] = np.log(mixture_weights[cluster]) + log_prob

                    forward_backward.wrapped_log_beta_pass(observation_log_probabilities, np.log(A_[cluster]), beta[cluster])
                    forward_backward.wrapped_log_compute_digammas(np.log(A_[cluster]), alpha[cluster], beta[cluster], observation_log_probabilities, digamma[cluster], digamma_1[cluster])

                cluster_weights = np.exp(membership_log_probs[x] - scipy.special.logsumexp(membership_log_probs[x]))

                for cluster in range(self.n_mixture_components):
                    for state in range(self.n_components):
                        next_pi_[cluster, state] += cluster_weights[cluster] * digamma_1[cluster, 0, state]
                    for state1 in range(self.n_components):
                        for state2 in range(self.n_components):
                            for t in range(sequence.shape[0] - 1):
                                next_A_[cluster, state1, state2] += cluster_weights[cluster] * digamma[cluster, t, state1, state2]

                    for state in range(self.n_components):
                        for t in range(sequence.shape[0]):
                            item = sequence[t]
                            next_B_[cluster, state, item] += cluster_weights[cluster] * digamma_1[cluster, t, state]
            new_ll = np.sum(membership_log_probs)
            normed_membership_probs = np.exp(membership_log_probs - scipy.special.logsumexp(membership_log_probs, axis=1)[:, None])
            mixture_weights = normed_membership_probs.sum(axis=0) / normed_membership_probs.shape[0]
            new_predictions = normed_membership_probs.argmax(axis=1)
            print(iteration, new_ll)
            if ll is not None and abs(new_ll - ll) < self.tol:
                print("converged after {} iterations".format(iteration))
                break
            ll = new_ll
            for cluster in range(self.n_mixture_components):
                next_pi_[cluster] = next_pi_[cluster] / next_pi_[cluster].sum()
                next_A_[cluster] = sklearn.preprocessing.normalize(next_A_[cluster], axis=1, norm="l1")
                next_B_[cluster] = sklearn.preprocessing.normalize(next_B_[cluster], axis=1, norm="l1")

            pi_ = next_pi_
            A_ = next_A_
            B_ = next_B_
        for cluster in range(self.n_mixture_components):
            print("Cluster {}: {}".format(cluster, mixture_weights[cluster]))
            print(pi_[cluster])
            print(A_[cluster])
            print(B_[cluster])
            print()
        self.mixture_weights_ = mixture_weights
        self.pi_ = pi_
        self.A_ = A_
        self.B_ = B_

    def observation_probabilities(self, B, sequence):
        probs = np.zeros((sequence.shape[0], B.shape[0], ), dtype=float)
        for t in range(sequence.shape[0]):
            for state in range(B.shape[0]):
                probs[t, state] = B[state, sequence[t]]
        return probs

    def predict2(self, X):
        membership_log_probs = np.zeros((X.shape[0], self.n_mixture_components))
        for cluster in range(self.n_mixture_components):
            for x in range(X.shape[0]):
                sequence = X[x]
                alpha = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components), dtype=float)
                for cluster in range(self.n_mixture_components):
                    observation_log_probabilities = self.observation_probabilities(np.log(self.B_[cluster]), sequence)
                    forward_backward.wrapped_log_alpha_pass(
                        observation_log_probabilities,
                        np.log(self.pi_[cluster]),
                        np.log(self.A_[cluster]),
                        alpha[cluster],
                    )
                    log_prob = scipy.special.logsumexp(alpha[cluster][-1, :])
                    membership_log_probs[x, cluster] = np.log(self.mixture_weights_[cluster]) + log_prob
        return membership_log_probs.argmax(axis=1)


class MixtureHMM(sklearn.base.BaseEstimator, sklearn.base.ClusterMixin):

    def __init__(self, n_mixture_components=2, n_components=2, n_features=3, n_iterations=10, tol=1e-6, random_state=None):
        self.n_mixture_components = n_mixture_components
        self.n_components = n_components
        self.n_features = n_features
        self.n_iterations = n_iterations
        self.tol = tol
        self.random_state = random_state

    def fit(self, X):
        rs = sklearn.utils.check_random_state(self.random_state)
        pi_ = np.full((self.n_mixture_components, self.n_components), 1/self.n_components, dtype=float)
        A_ = np.full((self.n_mixture_components, self.n_components, self.n_components), 1/self.n_components, dtype=float)
        seed = np.array([100] *self.n_components)
        for i in range(self.n_mixture_components):
            for j in range(self.n_components):
                A_[i, j] = rs.dirichlet(seed)
        B_ = np.zeros((self.n_mixture_components, self.n_components, self.n_features), dtype=float)
        seed = np.array([100] *self.n_features)
        for i in range(self.n_mixture_components):
            for j in range(self.n_components):
                B_[i, j] = rs.dirichlet(seed)
        mixture_weights = np.full(self.n_mixture_components, 1/self.n_mixture_components)

        print("START")
        print(pi_)
        print(A_)
        print(B_)
        trainer = new_mixture_categorical(mixture_weights, pi_, A_, B_, impl="scaling")
        trainer.train(X[:, :, None].astype(float), self.n_iterations, tol=1e-6)
        self.trainer_ = trainer
        for cluster in range(self.n_mixture_components):
            print("Cluster {}: {}".format(cluster, trainer.mixture_weights[cluster]))
            print(trainer.pi[cluster])
            print(trainer.A[cluster])
            print(trainer.emissions[cluster].B)
            print()

    def predict(self, X):

        probs = self.trainer_.loglik(X[:, :, None].astype(float))
        return probs.argmax(axis=1)

    def fit2(self, X):
        rs = sklearn.utils.check_random_state(self.random_state)
        pi_ = np.full((self.n_mixture_components, self.n_components), 1/self.n_components, dtype=float)
        A_ = np.full((self.n_mixture_components, self.n_components, self.n_components), 1/self.n_components, dtype=float)
        seed = np.array([100] *self.n_components)
        for i in range(self.n_mixture_components):
            for j in range(self.n_components):
                A_[i, j] = rs.dirichlet(seed)
        B_ = np.zeros((self.n_mixture_components, self.n_components, self.n_features), dtype=float)
        seed = np.array([100] *self.n_features)
        for i in range(self.n_mixture_components):
            for j in range(self.n_components):
                B_[i, j] = rs.dirichlet(seed)
        mixture_weights = np.full(self.n_mixture_components, 1/self.n_mixture_components)
        print("START")
        print(pi_)
        print(A_)
        print(B_)
        predictions = None
        ll = new_ll = None
        for iteration in range(self.n_iterations):
            membership_log_probs = np.zeros((X.shape[0], self.n_mixture_components))
            next_pi_ = np.zeros_like(pi_)
            next_A_ = np.zeros_like(A_)
            next_B_ = np.zeros_like(B_)

            #print(A_[0])
            #print(B_[0])
            for x in range(X.shape[0]):
                sequence = X[x]
                alpha = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components), dtype=float)
                beta = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components), dtype=float)
                digamma = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components, self.n_components), dtype=float)
                digamma_1 = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components), dtype=float)
                for cluster in range(self.n_mixture_components):
                    observation_log_probabilities = self.observation_probabilities(np.log(B_[cluster]), sequence)
                    forward_backward.wrapped_log_alpha_pass(
                        observation_log_probabilities,
                        np.log(pi_[cluster]),
                        np.log(A_[cluster]),
                        alpha[cluster],
                    )
                    log_prob = scipy.special.logsumexp(alpha[cluster][-1, :])
                    membership_log_probs[x, cluster] = np.log(mixture_weights[cluster]) + log_prob

                    forward_backward.wrapped_log_beta_pass(observation_log_probabilities, np.log(A_[cluster]), beta[cluster])
                    forward_backward.wrapped_log_compute_digammas(np.log(A_[cluster]), alpha[cluster], beta[cluster], observation_log_probabilities, digamma[cluster], digamma_1[cluster])

                cluster_weights = np.exp(membership_log_probs[x] - scipy.special.logsumexp(membership_log_probs[x]))

                for cluster in range(self.n_mixture_components):
                    for state in range(self.n_components):
                        next_pi_[cluster, state] += cluster_weights[cluster] * digamma_1[cluster, 0, state]
                    for state1 in range(self.n_components):
                        for state2 in range(self.n_components):
                            for t in range(sequence.shape[0] - 1):
                                next_A_[cluster, state1, state2] += cluster_weights[cluster] * digamma[cluster, t, state1, state2]

                    for state in range(self.n_components):
                        for t in range(sequence.shape[0]):
                            item = sequence[t]
                            next_B_[cluster, state, item] += cluster_weights[cluster] * digamma_1[cluster, t, state]
            new_ll = np.sum(membership_log_probs)
            normed_membership_probs = np.exp(membership_log_probs - scipy.special.logsumexp(membership_log_probs, axis=1)[:, None])
            mixture_weights = normed_membership_probs.sum(axis=0) / normed_membership_probs.shape[0]
            new_predictions = normed_membership_probs.argmax(axis=1)
            print(iteration, new_ll)
            if ll is not None and abs(new_ll - ll) < self.tol:
                print("converged after {} iterations".format(iteration))
                break
            ll = new_ll
            for cluster in range(self.n_mixture_components):
                next_pi_[cluster] = next_pi_[cluster] / next_pi_[cluster].sum()
                next_A_[cluster] = sklearn.preprocessing.normalize(next_A_[cluster], axis=1, norm="l1")
                next_B_[cluster] = sklearn.preprocessing.normalize(next_B_[cluster], axis=1, norm="l1")

            pi_ = next_pi_
            A_ = next_A_
            B_ = next_B_
        for cluster in range(self.n_mixture_components):
            print("Cluster {}: {}".format(cluster, mixture_weights[cluster]))
            print(pi_[cluster])
            print(A_[cluster])
            print(B_[cluster])
            print()
        self.mixture_weights_ = mixture_weights
        self.pi_ = pi_
        self.A_ = A_
        self.B_ = B_

    def observation_probabilities(self, B, sequence):
        probs = np.zeros((sequence.shape[0], B.shape[0], ), dtype=float)
        for t in range(sequence.shape[0]):
            for state in range(B.shape[0]):
                probs[t, state] = B[state, sequence[t]]
        return probs

    def predict2(self, X):
        membership_log_probs = np.zeros((X.shape[0], self.n_mixture_components))
        for cluster in range(self.n_mixture_components):
            for x in range(X.shape[0]):
                sequence = X[x]
                alpha = np.zeros((self.n_mixture_components, sequence.shape[0], self.n_components), dtype=float)
                for cluster in range(self.n_mixture_components):
                    observation_log_probabilities = self.observation_probabilities(np.log(self.B_[cluster]), sequence)
                    forward_backward.wrapped_log_alpha_pass(
                        observation_log_probabilities,
                        np.log(self.pi_[cluster]),
                        np.log(self.A_[cluster]),
                        alpha[cluster],
                    )
                    log_prob = scipy.special.logsumexp(alpha[cluster][-1, :])
                    membership_log_probs[x, cluster] = np.log(self.mixture_weights_[cluster]) + log_prob
        return membership_log_probs.argmax(axis=1)

if __name__ == "__main__":
    main2()
