# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import argparse
import collections
import copy
import re
import time
import string
import os
import pandas as pd
import scipy.special
import sklearn.base
from nltk.corpus import brown
import numpy as np
import pytest


from hmm import CategoricalHMM, CategoricalVariationalHMM, GaussianHMM, GaussianVariationalHMM, GMMHMM, GMMVariationalHMM, MultivariateGaussianHMM, MultivariateGaussianVariationalHMM
import hmm.datasets.models

import hmmlearn.hmm
import ssm
import pomegranate


def clean(w):
    keep = [c for c in w.lower() if c in string.ascii_lowercase or c == " "]
    return "".join(keep)

def get_words(how_many):
    chars = " ".join(clean(w) for w in brown.words())
    mapping = {}
    rmapping = {}
    for i, item in enumerate(string.ascii_lowercase + " "):
        mapping[item] = i
        rmapping[i] = item
        print(i, rmapping[i])

    sequence = np.asarray([mapping[c] for c in chars[:how_many]])
    return sequence, rmapping


def run_pomegranate_categorical(model, sequence, lengths, rmapping, tag, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = model.copy()
        start = time.time()
        lls = cloned.fit(sequence, max_iterations=100, multiple_check_input=False)
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, tag))
        elapsed.append(end-start)
        print(cloned.dense_transition_matrix())
    for i in range(len(rmapping)):
        print("|{}| {:.5f} {:.5f}".format(
            rmapping[i], cloned.states[0].distribution.probability(i), cloned.states[1].distribution.probability(i),
        )
        )


    return cloned, np.asarray(elapsed)

def run_ssm_categorical(model, sequence, lengths, rmapping, tag, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = copy.deepcopy(model)
        sequence = sequence.ravel()[:, None]
        start = time.time()
        lls = cloned.fit(sequence, num_iters=100, initialize=False)
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, tag))
        elapsed.append(end-start)
        print(np.exp(cloned.init_state_distn.log_pi0))
        print(np.exp(cloned.transitions.params))
    emissions = np.exp(cloned.observations.params)
    for i in range(emissions.shape[-1]):
        print("|{}| {} {}".format(rmapping[i], emissions[0, 0, i], emissions[1, 0, i]))

    return cloned, np.asarray(elapsed)


def run_hmmlearn_categorical(model, sequence, lengths, rmapping, tag, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = sklearn.base.clone(model)
        cloned.startprob_ = model.startprob_.copy()
        cloned.transmat_ = model.transmat_.copy()
        cloned.emissionprob_ = model.emissionprob_.copy()
        print(sequence.shape)
        print(sequence.ravel().reshape(1,-1).shape)
        start = time.time()
        cloned.fit(sequence.ravel().reshape(1,-1))
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, tag))
        elapsed.append(end-start)
        assert cloned.n_iter == cloned.monitor_.iter, cloned.monitor_.iter
    print(cloned.startprob_)
    print(cloned.transmat_)
    for i in range(cloned.emissionprob_.shape[1]):
        print("|{}| {} {}".format(rmapping[i], cloned.emissionprob_[0, i], cloned.emissionprob_[1, i]))

    if hasattr(cloned,"B_posterior_"):
        for i in range(cloned.B_posterior_.shape[1]):
            print("|{}| {} {}".format(rmapping[i], cloned.B_posterior_[0, i], cloned.B_posterior_[1, i]))

    return cloned, np.asarray(elapsed)


def run_our_library_categorical(model, sequence, lengths, rmapping, tag, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = sklearn.base.clone(model)
        if hasattr(cloned, "init_pi") and cloned.init_pi is None:
            cloned.pi_ = model.pi_.copy()
            cloned.A_ = model.A_.copy()
            cloned.B_ = model.B_.copy()

        start = time.time()
        cloned.fit(sequence, lengths)
        end = time.time()
        elapsed.append(end-start)
        iterations = None
        if hasattr(cloned, "loglikelihoods_"):
            iterations = len(cloned.loglikelihoods_)
        elif hasattr(cloned, "lower_bound_"):
            iterations = len(cloned.lower_bound_)
        print("Training Took {} seconds {}".format(end - start, tag))
        assert cloned.n_iterations == iterations, (cloned.n_iterations, iterations)
    if hasattr(cloned, "pi_posterior_"):
        print(cloned.pi_posterior_)
    else:
        print(cloned.pi_)
    if hasattr(cloned, "A_posterior_"):
        print(cloned.pi_posterior_)
    else:
        print(cloned.A_)
    if hasattr(cloned,"B_posterior_"):
        for i in range(cloned.B_posterior_.shape[1]):
            print("|{}| {} {}".format(rmapping[i], cloned.B_posterior_[0, i], cloned.B_posterior_[1, i]))
    else:
        for i in range(cloned.B_.shape[1]):
            print("|{}| {} {}".format(rmapping[i], cloned.B_[0, i], cloned.B_[1, i]))
    return cloned, np.asarray(elapsed)

def test_categorical(args):
    runtimes = collections.defaultdict(dict)
    n_iterations = 100
    sequence, rmapping = get_words(50000)

    default_pi = np.asarray([0.51316, .48684])
    default_A = np.asarray([
        [0.47468, 0.52532],
        [0.51656, 0.48344],
    ])
    default_B = np.asarray([
            [0.03735, 0.03909],
            [0.03408, 0.03537],
            [0.03455, 0.03537],
            [0.03828, 0.03909],
            [0.03782, 0.03583],
            [0.03922, 0.03630],
            [0.03688, 0.04048],
            [0.03408, 0.03537],
            [0.03875, 0.03816],
            [0.04062, 0.03909],
            [0.03735, 0.03490],
            [0.03968, 0.03723],
            [0.03548, 0.03537],
            [0.03735, 0.03909],
            [0.04062, 0.03397],
            [0.03595, 0.03397],
            [0.03641, 0.03816],
            [0.03408, 0.03676],
            [0.04062, 0.04048],
            [0.03548, 0.03443],
            [0.03922, 0.03537],
            [0.04062, 0.03955],
            [0.03455, 0.03816],
            [0.03595, 0.03723],
            [0.03408, 0.03769],
            [0.03408, 0.03955],
            [0.03688, 0.03397],
        ]).T
    model = CategoricalHMM.CategoricalHMM(n_components=2, tol=1e-6, n_iterations=n_iterations, init_pi=None, init_A=None, init_emissions=None, implementation="scaling", random_state=sklearn.utils.check_random_state(42))
    model.pi_ = default_pi
    model.A_ = default_A
    model.B_ = default_B
    sequence2 = sequence.reshape(-1, 1)
    lengths = np.asarray([len(sequence2)])
    model, elapsed = run_our_library_categorical(model, sequence2, lengths, rmapping, "cython-scaling", args.repeat)
    runtimes["Categorical|EM|scaling"]["mean"] = elapsed.mean()
    runtimes["Categorical|EM|scaling"]["std"] = elapsed.std()

    model = CategoricalHMM.CategoricalHMM(n_components=2, n_iterations=n_iterations, init_pi=None, init_A=None, init_emissions=None, implementation="log", random_state=sklearn.utils.check_random_state(42))
    model.pi_ = default_pi
    model.A_ = default_A
    model.B_ = default_B
    model, elapsed = run_our_library_categorical(model, sequence2, lengths, rmapping, "cython-log", args.repeat)
    runtimes["Categorical|EM|log"]["mean"] = elapsed.mean()
    runtimes["Categorical|EM|log"]["std"] = elapsed.std()

    model = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=2, n_iterations=n_iterations, B_prior=1, implementation="scaling", random_state=sklearn.utils.check_random_state(42))
    model, elapsed = run_our_library_categorical(model, sequence2, lengths, rmapping, "cython-scaling", args.repeat)
    runtimes["Categorical|Variational|scaling"]["mean"] = elapsed.mean()
    runtimes["Categorical|Variational|scaling"]["std"] = elapsed.std()

    model = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=2, n_iterations=n_iterations, B_prior=1, implementation="log", random_state=sklearn.utils.check_random_state(42))
    model, elapsed = run_our_library_categorical(model, sequence2, lengths, rmapping, "cython-log", args.repeat)
    runtimes["Categorical|Variational|log"]["mean"] = elapsed.mean()
    runtimes["Categorical|Variational|log"]["std"] = elapsed.std()

    model = hmmlearn.hmm.MultinomialHMM(n_components=2, tol=1e-6, init_params="", n_iter=n_iterations, random_state=sklearn.utils.check_random_state(42))
    model.startprob_ = default_pi
    model.transmat_ = default_A
    model.emissionprob_ = default_B
    model, elapsed = run_hmmlearn_categorical(model, sequence2, lengths, rmapping, "hmmlearn", args.repeat)
    runtimes["Categorical|EM|hmmlearn"]["mean"] = elapsed.mean()
    runtimes["Categorical|EM|hmmlearn"]["std"] = elapsed.std()

    model = ssm.HMM(K=2, D=1, observations="categorical", observation_kwargs={"C":len(rmapping)})
    model.init_state_distn.log_pi0 = (np.log(default_pi),)
    model.transitions.params = (np.log(default_A),)
    model.observations.params = np.log(default_B)[:, None, :]
    model, elapsed = run_ssm_categorical(model, sequence2, lengths, rmapping, "ssm-em", args.repeat)
    runtimes["Categorical|EM|ssm"]["mean"] = elapsed.mean()
    runtimes["Categorical|EM|ssm"]["std"] = elapsed.std()

    model = pomegranate.hmm.HiddenMarkovModel.from_matrix(
        transition_probabilities=default_A,
        starts=default_pi,
        distributions=[
            pomegranate.distributions.DiscreteDistribution(dict(zip(rmapping.keys(), default_B[0]))),
            pomegranate.distributions.DiscreteDistribution(dict(zip(rmapping.keys(), default_B[1]))),
        ]
    )
    model, elapsed = run_pomegranate_categorical(model, [sequence2.ravel()], lengths, rmapping, "pomegrante-em", args.repeat)
    runtimes["Categorical|EM|pomegrante"]["mean"] = elapsed.mean()
    runtimes["Categorical|EM|pomegrante"]["std"] = elapsed.std()
    frame = pd.DataFrame(runtimes).T
    frame["n_iterations"] = n_iterations
    frame.index.name = "configuration"
    return frame


def run_our_library_gaussian(model, sequence, lengths, tag, check, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = sklearn.base.clone(model)

        start = time.time()
        cloned.fit(sequence, lengths)
        end = time.time()
        iterations = None
        if hasattr(cloned, "loglikelihoods_"):
            iterations = len(cloned.loglikelihoods_)
        elif hasattr(cloned, "lower_bound_"):
            iterations = len(cloned.lower_bound_)
        print("Training Took {} seconds {}".format(end - start, tag))
        assert cloned.n_iterations == iterations, iterations

        elapsed.append(end-start)
    if hasattr(cloned, "pi_posterior_"):
        print(cloned.pi_posterior_)
    else:
        print(cloned.pi_)
    if hasattr(cloned, "A_posterior_"):
        print(cloned.A_posterior_)
    else:
        print(cloned.A_)
    if hasattr(cloned, "means_"):
        print(cloned.means_)
        print(cloned.variances_)
    else:
        print(cloned.means_posterior_)
        if hasattr(cloned, "variances_posterior_"):
            print(cloned.variances_posterior_)
        else:
            print(cloned.covariances_posterior_)
    return cloned, np.asarray(elapsed)

def run_hmmlearn_gaussian(model, sequence, lengths, tag, check,  repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = sklearn.base.clone(model)

        start = time.time()
        cloned.fit(sequence, lengths)
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, tag))
        elapsed.append(end-start)
        assert cloned.n_iter == cloned.monitor_.iter, cloned.monitor_.iter
    print(cloned.startprob_)
    print(cloned.transmat_)
    print(cloned.means_)
    print(cloned.covars_)
    return cloned, np.asarray(elapsed)


def run_ssm_gaussian(model, sequence, lengths, tag, check, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = copy.deepcopy(model)
        #sequence = sequence.ravel()[:, None]
        start = time.time()
        lls = cloned.fit(sequence, num_iters=100, initialize=False)
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, tag))
        elapsed.append(end-start)
        assert len(lls) == 101, len(lls)
        print(np.exp(cloned.init_state_distn.log_pi0))
        print(np.exp(cloned.transitions.params))
        print(cloned.observations.params)
    return cloned, np.asarray(elapsed)


def run_pomegranate_gaussian(model, sequence, lengths, rmapping, tag, repeat=10, distribution=None):
    elapsed = []
    sequence = [sequence.T[0]]
    for i in range(repeat):
        start = time.time()
        model, history = pomegranate.hmm.HiddenMarkovModel.from_samples(
            distribution,
            n_components=4,
            X=sequence,
            max_iterations=100,
            return_history=True,
            multiple_check_input=False
        )
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, tag))
        elapsed.append(end-start)
        assert len(history.log_probabilities) == 100, (history.log_probabilities, len(history.log_probabilities))
        print(model.dense_transition_matrix())
        print(model.states)


    return model, np.asarray(elapsed)


def test_gaussian(args):
    runtimes = collections.defaultdict(dict)
    n_iterations = 100

    pi = np.asarray([0, 0, 0, 1])
    A = np.asarray([
        [.2, .2, .3, .3],
        [.3, .2, .2, .3],
        [.2, .3, .3, .2],
        [.3, .3, .2, .2],
    ])
    means = np.asarray([
        -1.5,
        0,
        1.5,
        3
    ])
    variances =([
        .5,
        .5,
        .5,
        .5
    ])

    sampler = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None)
    sampler.pi_ = pi
    sampler.A_ = A
    sampler.means_ = means
    sampler.variances_ = variances

    observed, hidden, lengths = sampler.sample(1, 50000)

    model = GaussianHMM.GaussianHMM(n_components=sampler.pi_.shape[0], n_iterations=n_iterations, implementation="scaling", tol=1e-6, allowed_to_use_log=False)
    model, elapsed = run_our_library_gaussian(model, observed, lengths, "cython-scaling", False, args.repeat)
    runtimes["Gaussian|EM|scaling"]["mean"] = elapsed.mean()
    runtimes["Gaussian|EM|scaling"]["std"] = elapsed.std()

    model = GaussianHMM.GaussianHMM(n_components=sampler.pi_.shape[0], n_iterations=n_iterations, implementation="log", tol=1e-6, allowed_to_use_log=True)
    model, elapsed = run_our_library_gaussian(model, observed, lengths, "cython-log", False, args.repeat)
    runtimes["Gaussian|EM|log"]["mean"] = elapsed.mean()
    runtimes["Gaussian|EM|log"]["std"] = elapsed.std()

    model = GaussianVariationalHMM.GaussianVariationalHMM(n_components=sampler.pi_.shape[0], n_iterations=n_iterations, implementation="scaling", tol=1e-6)
    model, elapsed = run_our_library_gaussian(model, observed, lengths, "cython-scaling", False, args.repeat)
    runtimes["Gaussian|Variational|scaling"]["mean"] = elapsed.mean()
    runtimes["Gaussian|Variational|scaling"]["std"] = elapsed.std()

    model = GaussianVariationalHMM.GaussianVariationalHMM(n_components=sampler.pi_.shape[0], n_iterations=n_iterations, implementation="log", tol=1e-6)
    model, elapsed = run_our_library_gaussian(model, observed, lengths, "cython-log", False, args.repeat)
    runtimes["Gaussian|Variational|log"]["mean"] = elapsed.mean()
    runtimes["Gaussian|Variational|log"]["std"] = elapsed.std()

    model = hmmlearn.hmm.GaussianHMM(n_components=sampler.pi_.shape[0], n_iter=n_iterations, covariance_type="full", tol=1e-6)
    model, elapsed = run_hmmlearn_gaussian(model, observed, lengths, "hmmlearn", False, args.repeat)
    runtimes["Gaussian|EM|hmmlearn"]["mean"] = elapsed.mean()
    runtimes["Gaussian|EM|hmmlearn"]["std"] = elapsed.std()
    model = ssm.HMM(K=4, D=1, observations="gaussian")
    model, elapsed = run_ssm_gaussian(model, observed, lengths, "ssm-em", False, args.repeat)
    runtimes["Gaussian|EM|ssm"]["mean"] = elapsed.mean()
    runtimes["Gaussian|EM|ssm"]["std"] = elapsed.std()

    model, elapsed = run_pomegranate_gaussian(None, observed, lengths, "ssm-em", False, args.repeat, distribution=pomegranate.distributions.NormalDistribution)
    runtimes["Gaussian|EM|pomegranate"]["mean"] = elapsed.mean()
    runtimes["Gaussian|EM|pomegranate"]["std"] = elapsed.std()


    frame = pd.DataFrame(runtimes).T
    frame["n_iterations"] = n_iterations
    frame.index.name = "configuration"
    return frame


def draw_one(weights, means, variances, random_state):
    weights = np.cumsum(weights)
    sample = rs.rand()
    for i in range(weights.shape[0]):
        if sample <= weights[i]:
            return random_state.normal(means[i], np.sqrt(variances[i]))
    else:
        assert False

def run_our_library_multivariate_gaussian(model, sequence, lengths, tag, check, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = sklearn.base.clone(model)

        start = time.time()
        cloned.fit(sequence, lengths)
        end = time.time()
        iterations = None
        if hasattr(cloned, "loglikelihoods_"):
            iterations = len(cloned.loglikelihoods_)
        elif hasattr(cloned, "lower_bound_"):
            iterations = len(cloned.lower_bound_)
        print("Training Took {} seconds {}".format(end - start, tag))
        assert cloned.n_iterations == iterations
        elapsed.append(end-start)
    print(cloned.pi_)
    if hasattr(cloned, "pi_posterior_"):
        print(cloned.pi_posterior_)
    print(cloned.A_)
    if hasattr(cloned, "A_posterior_"):
        print(cloned.pi_posterior_)
    if hasattr(cloned, "means_"):
        print(cloned.means_)
        print(cloned.covariances_)
    else:
        print(cloned.means_posterior_)
        print(cloned.variances_posterior_)
    return cloned, np.asarray(elapsed)



def test_multivariate_gaussian(args):
    runtimes = collections.defaultdict(dict)
    n_iterations = 100
    pi = np.asarray([0, 0, 0, 1])
    A = np.asarray([
        [.2, .2, .3, .3],
        [.3, .2, .2, .3],
        [.2, .3, .3, .2],
        [.3, .3, .2, .2],
    ])
    means = np.asarray([
        [-1.5, 0],
        [0,  0],
        [1.5, 0],
        [3, 0]
    ])
    variances = np.asarray([
        [[.5, 0],
         [0, .5]],
        [[.5, 0],
         [0, 0.5]],

        [[.5, 0],
         [0, .5]],
        [[0.5, 0],
         [0, 0.5]],
    ])

    sampler = MultivariateGaussianHMM.MultivariateGaussianHMM(init_pi=None, init_A=None, init_emissions=None)
    sampler.pi_ = pi
    sampler.A_ = A
    sampler.means_ = means
    sampler.covariances_ = variances

    observed, hidden, lengths = sampler.sample(1, 50000)

    model = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=4, n_iterations=n_iterations, implementation="scaling", tol=1e-6, allowed_to_use_log=True)
    model, elapsed = run_our_library_multivariate_gaussian(model, observed, lengths , "cython-scaling", False, args.repeat)
    runtimes["MultivariateGaussian|EM|scaling"]["mean"] = elapsed.mean()
    runtimes["MultivariateGaussian|EM|scaling"]["std"] = elapsed.std()

    model = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=4, n_iterations=n_iterations, implementation="log", tol=1e-6, allowed_to_use_log=True)
    model, elapsed = run_our_library_multivariate_gaussian(model, observed, lengths , "cython-log", False, args.repeat)
    runtimes["MultivariateGaussian|EM|log"]["mean"] = elapsed.mean()
    runtimes["MultivariateGaussian|EM|log"]["std"] = elapsed.std()

    model = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=4, n_iterations=n_iterations, implementation="scaling", tol=1e-6)
    model, elapsed = run_our_library_gaussian(model, observed, lengths , "cython-scaling", False, args.repeat)
    runtimes["MultivariateGaussian|Variational|scaling"]["mean"] = elapsed.mean()
    runtimes["MultivariateGaussian|Variational|scaling"]["std"] = elapsed.std()

    model = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=4, n_iterations=n_iterations, implementation="log", tol=1e-6)
    model, elapsed = run_our_library_gaussian(model, observed, lengths , "cython-log", False, args.repeat)
    runtimes["MultivariateGaussian|Variational|log"]["mean"] = elapsed.mean()
    runtimes["MultivariateGaussian|Variational|log"]["std"] = elapsed.std()

    model = hmmlearn.hmm.GaussianHMM(n_components=4, n_iter=n_iterations, covariance_type="full", tol=1e-6)
    model, elapsed = run_hmmlearn_gaussian(model, observed, lengths, "hmmlearn", False, args.repeat)
    runtimes["MultivariateGaussian|EM|hmmlearn"]["mean"] = elapsed.mean()
    runtimes["MultivariateGaussian|EM|hmmlearn"]["std"] = elapsed.std()

    model = ssm.HMM(K=4, D=2, observations="gaussian")
    model, elapsed = run_ssm_gaussian(model, observed, lengths, "ssm-em", False, args.repeat)
    runtimes["MultivariateGaussian|EM|ssm"]["mean"] = elapsed.mean()
    runtimes["MultivariateGaussian|EM|ssm"]["std"] = elapsed.std()
    # model, elapsed = run_pomegranate_gaussian(None, observed, lengths, "ssm-em", False, args.repeat, distribution=pomegranate.distributions.MultivariateGaussianDistribution)
    # runtimes["MultivariateGaussian|EM|pomegranate"]["mean"] = elapsed.mean()
    # runtimes["MultivariateGaussian|EM|pomegranate"]["std"] = elapsed.std()


    frame = pd.DataFrame(runtimes).T
    frame["n_iterations"] = n_iterations
    frame.index.name = "configuration"
    return frame


def generate_gmm_data():
    observations = []
    rs = sklearn.utils.check_random_state(43232)
    pi = [.25, .25, .25, .25]
    A = [
        [.2, .2, .3, .3],
        [.3, .2, .2, .3],
        [.2, .3, .3, .2],
        [.3, .3, .2, .2],
    ]
    weights = [
        [.25, .5, .25],
        [.5, .25, .25],
        [.5, .25, .25],
        [.25, .25, .5],
    ]
    means = [
    [[-10], [-12],[-9]],
    [[-5], [-4], [-3]],
    [[-1.5], [0], [1.5]],
    [[5], [7], [9]],
    ]

    variances = [
        [[[.25]], [[.25]], [[.25]]],
        [[[.25]], [[.25]], [[.25]]],
        [[[.25]], [[.25]], [[.25]]],
        [[[.25]], [[.25]], [[.25]]],
    ]
    generator = GMMHMM.GMMHMM(
        init_pi=None,
        init_A=None,
        init_emissions=None,
        n_components=4,
        mixture_n_components=3)
    generator.pi_ = pi
    generator.A_ = A
    generator.mixture_means_ = means
    generator.mixture_weights_ = weights
    generator.mixture_covariances_ = variances

    return generator.sample(10, 5000, random_state=rs)

def test_gaussian_mixture(args):

    n_iterations = 100
    runtimes = collections.defaultdict(dict)
    observed, hidden, lengths = generate_gmm_data()
    print(observed.shape)
    print(observed[:10])
    model = GMMHMM.GMMHMM(n_components=4, mixture_n_components=3, n_iterations=n_iterations, implementation="scaling", tol=1e-6, verbose=False, random_state=sklearn.utils.check_random_state(32))
    model, elapsed = run_our_library_gmm(model, observed, lengths, "cython-scaling", False, args.repeat)
    runtimes["GaussianMixture|EM|scaling"]["mean"] = elapsed.mean()
    runtimes["GaussianMixture|EM|scaling"]["std"] = elapsed.std()

    model = GMMHMM.GMMHMM(n_components=4, mixture_n_components=3, n_iterations=n_iterations, implementation="log", tol=1e-6, random_state=sklearn.utils.check_random_state(32))
    model, elapsed = run_our_library_gmm(model, observed, lengths, "cython-log", False, args.repeat)
    runtimes["GaussianMixture|EM|log"]["mean"] = elapsed.mean()
    runtimes["GaussianMixture|EM|log"]["std"] = elapsed.std()

    model = GMMVariationalHMM.GMMVariationalHMM(n_components=4, mixture_n_components=3, n_iterations=n_iterations, implementation="scaling", tol=1e-6, random_state=sklearn.utils.check_random_state(32))
    model, elapsed = run_our_library_gmm(model, observed, lengths , "cython-scaling", False, args.repeat)
    runtimes["GaussianMixture|Variational|scaling"]["mean"] = elapsed.mean()
    runtimes["GaussianMixture|Variational|scaling"]["std"] = elapsed.std()

    model = GMMVariationalHMM.GMMVariationalHMM(n_components=4, mixture_n_components=3, n_iterations=n_iterations, implementation="log", tol=1e-6, random_state=sklearn.utils.check_random_state(32))
    model, elapsed = run_our_library_gmm(model, observed, lengths , "cython-log", False, args.repeat)
    runtimes["GaussianMixture|Variational|log"]["mean"] = elapsed.mean()
    runtimes["GaussianMixture|Variational|log"]["std"] = elapsed.std()

    model = hmmlearn.hmm.GMMHMM(n_components=4, n_mix=3, n_iter=n_iterations, covariance_type="full", tol=1e-18, verbose=False, random_state=sklearn.utils.check_random_state(32))
    model, elapsed = run_hmmlearn_gmm(model, observed, lengths, "hmmlearn", False, args.repeat)
    runtimes["GaussianMixture|EM|hmmlearn"]["mean"] = elapsed.mean()
    runtimes["GaussianMixture|EM|hmmlearn"]["std"] = elapsed.std()
    frame = pd.DataFrame(runtimes).T
    frame["n_iterations"] = n_iterations
    frame.index.name = "configuration"
    return frame

def run_our_library_gmm(model, sequence, lengths, tag, check, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = sklearn.base.clone(model)
        start = time.time()
        cloned.fit(sequence, lengths)
        end = time.time()
        iterations = None
        if hasattr(cloned, "loglikelihoods_"):
            iterations = len(cloned.loglikelihoods_)
        elif hasattr(cloned, "lower_bound_"):
            iterations = len(cloned.lower_bound_)
        print("Training Took {} seconds {}, {}".format(end - start, tag, iterations))
        assert cloned.n_iterations == iterations
        elapsed.append(end-start)
    if hasattr(cloned, "pi_posterior_"):
        print(cloned.pi_posterior_)
        print(cloned.pi_posterior_)
        print(cloned.mixture_weights_posterior_)
        print(cloned.mixture_means_posterior_)
        print(cloned.mixture_covariances_posterior_)
    else:
        print(cloned.pi_)
        print(cloned.A_)
        print(cloned.mixture_weights_)
        print(cloned.mixture_means_)
        print(cloned.mixture_covariances_)
    return cloned, np.asarray(elapsed)

def run_hmmlearn_gmm(model, sequences, lengths, rmapping, tag, repeat=10):
    elapsed = []
    for i in range(repeat):
        cloned = sklearn.base.clone(model)
        start = time.time()
        cloned.fit(sequences, lengths)
        end = time.time()
        print("Training Took {} seconds {}".format(end - start, tag))
        elapsed.append(end-start)
        assert cloned.n_iter == cloned.monitor_.iter, cloned.monitor_.iter
    print(cloned.monitor_.history)
    print(cloned.startprob_)
    print(cloned.transmat_)
    print(cloned.means_)
    print(cloned.covars_)
    return cloned, np.asarray(elapsed)




def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--all", action="store_true")
    parser.add_argument("--categorical", action="store_true")
    parser.add_argument("--gaussian", action="store_true")
    parser.add_argument("--multivariate-gaussian", action="store_true")
    parser.add_argument("--gaussian-mixture", action="store_true")
    parser.add_argument("--repeat", type=int, default=10)

    args = parser.parse_args()
    if args.all:
        args.categorical = True
        args.gaussian = True
        args.multivariate_gaussian = True
        args.gaussian_mixture = True

    if args.categorical:
        frame = test_categorical(args)
        frame["n_repeat"] = args.repeat
        frame.to_csv("categorical.benchmark.csv")
    if args.gaussian:
        frame = test_gaussian(args)
        frame["n_repeat"] = args.repeat
        frame.to_csv("gaussian.benchmark.csv")
    if args.multivariate_gaussian:
        frame = test_multivariate_gaussian(args)
        frame["n_repeat"] = args.repeat
        frame.to_csv("multivariate_gaussian.benchmark.csv")
    if args.gaussian_mixture:
        frame = test_gaussian_mixture(args)
        frame["n_repeat"] = args.repeat
        frame.to_csv("gaussian_mixture.benchmark.csv")


if __name__ == "__main__":
    main()
