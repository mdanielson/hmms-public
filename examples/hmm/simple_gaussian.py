# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
=============================
Demonstration of Gaussian HMM
=============================
"""
import pandas as pd

from hmm import GaussianHMM
from hmm.datasets.real import load_old_faithful_waiting
from hmm.model_selection import make_model_summary

def main():
    waiting_times = load_old_faithful_waiting()

    models = {}
    data_2d = waiting_times.data[None, :]
    for i in range(1,5):
        models[i] = GaussianHMM.GaussianHMM(n_components=i, n_iterations=100, n_inits=10, verbose=False)
        models[i].fit(data_2d)
        print(i)
        print(models[i].means_.ravel())
        print(models[i].variances_.ravel())


    summary = make_model_summary(models, data_2d)
    print(summary)

    #confidences = models[3].compute_confidence_intervals(n_samples=1, length=data_2d.shape[1], n_iterations=200)
    #for key, stuff in confidences.items():
    #    print(key)
    #    print(pd.DataFrame(stuff))

if __name__ == "__main__":
    main()
