# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
=============================
Demonstration of Gaussian HMM
=============================
"""
import numpy as np
import pandas as pd
pd.options.display.float_format = "{:.3f}".format
from hmm import GaussianVariationalHMM, MultivariateGaussianVariationalHMM
from hmm.datasets.real import load_old_faithful_waiting, load_old_faithful_duration

from sklearn.mixture import BayesianGaussianMixture
def main():
    waiting_times = load_old_faithful_waiting()

    models = {}
    data_2d = waiting_times.data

    mixed = BayesianGaussianMixture(n_components=5, max_iter=500)
    mixed.fit(data_2d)
    print("prior-weight concentration", mixed.degrees_of_freedom_prior_)
    print("prior-weight concentration", mixed.covariance_prior_)
    print("weights", mixed.weight_concentration_)
    print("means", mixed.means_)
    print("covarainces", mixed.covariances_)
    print(np.var(data_2d.ravel()))
    model = GaussianVariationalHMM.GaussianVariationalHMM(n_components=3, n_iterations=30, verbose=False, n_inits=1, random_state=32)
    model.fit(waiting_times.data, waiting_times.lengths)

    print(pd.Series(model.pi_posterior_))
    print(pd.DataFrame(model.A_posterior_))
    print(pd.DataFrame(model.A_normalized_))
    print("means")
    print(pd.Series(model.means_posterior_))
    print("variances")
    print(pd.Series(model.variances_posterior_))
    print("stddev")
    print(pd.Series(np.sqrt(model.variances_posterior_)))
    print("gamma")
    print(pd.Series(model.gamma_posterior_))
    print("delta")
    print(pd.Series(model.delta_posterior_))
    print(data_2d.shape)

    mmodel = MultivariateGaussianVariationalHMM.MultivariateGaussianVariationalHMM(n_components=3, n_iterations=30, verbose=False, n_inits=1, random_state=32)
    mmodel.fit(waiting_times.data, waiting_times.lengths)
    print(pd.Series(mmodel.pi_posterior_))
    print(pd.DataFrame(mmodel.A_posterior_))
    print(pd.DataFrame(mmodel.A_normalized_))
    print("means")
    print(pd.DataFrame(mmodel.means_posterior_))
    print("variances")
    print(mmodel.covariances_posterior_)

if __name__ == "__main__":
    main()
