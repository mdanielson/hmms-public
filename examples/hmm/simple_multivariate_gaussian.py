# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
============================
Demonstration of Poisson HMM
============================
"""
import pandas as pd

from hmm import MultivariateGaussianHMM
from hmm.datasets.real import load_old_faithful_waiting, load_old_faithful_duration
from hmm.model_selection import make_model_summary

def main():
    waiting_times = load_old_faithful_waiting()
    durations = load_old_faithful_duration()

    models = {}
    data = pd.DataFrame(
        {
            "waiting": waiting_times.data,
            "duration": durations.data
        }
    )
    data_3d = data.values[None, :]
    for i in range(1, 6):
        models[i] = MultivariateGaussianHMM.MultivariateGaussianHMM(n_components=i, n_inits=5, verbose=False)
        models[i].fit(data_3d)


    summary = make_model_summary(models, data_3d)
    print(summary)
    # TODO:
    # confidences = models[3].compute_confidence_intervals(n_samples=1, length=data_3d.shape[1], n_iterations=200)
    # for key, stuff in confidences.items():
    #     print(key)
    #     print(pd.DataFrame(stuff))

if __name__ == "__main__":
    main()
