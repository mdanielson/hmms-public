# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
import logging
import numpy as np
import pandas as pd

import sklearn.model_selection

from hmm import GaussianVariationalHMM, GaussianHMM
from hmm import HMMCluster


def sample_and_split(models, n_samples = 100, sample_lengths=200):

    observations = []
    labels = []

    for i, m in enumerate(models):
        labels += [i] * n_samples
        observed, _ = m.sample(n_samples, sample_lengths)
        observations += observed.tolist()
    observations = np.asarray(observations)

    train_idx, test_idx = sklearn.model_selection.train_test_split(np.arange(observations.shape[0]), test_size=.7, stratify=labels)

    test_labels = np.asarray([labels[i] for i in test_idx])
    train_labels = np.asarray([labels[i] for i in train_idx])

    test_observations = observations[test_idx]
    training_observations = observations[train_idx]
    return training_observations, test_observations, train_labels, test_labels


def setup_data():
    pi_1 = [.5, .25, .25]
    A_1 = [
            [.6, .2, .2],
            [.2, .6, .2],
            [.2, .2, .6]
    ]
    means_1 = [11, 5, 8]
    vars_1 = np.sqrt([.25, .25, .25])

    pi_2 = np.full((3), 1/3)
    A_2 = [
            [.2, .4, .4],
            [.4, .2, .4],
            [.4, .4, .2]
    ]
    means_2 = means_1
    vars_2 = vars_1

    model_1 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None)
    model_1.pi_ = pi_1
    model_1.A_ = A_1
    model_1.means_ = means_1
    model_1.variances_ = vars_1
    model_2 = GaussianHMM.GaussianHMM(init_pi=None, init_A=None, init_emissions=None)
    model_2.pi_ = pi_2
    model_2.A_ = A_2
    model_2.means_ = means_2
    model_2.variances_ = vars_2

    return sample_and_split([model_1, model_2], 40, 100)

def main():

    train_observations, test_observations, train_labels, test_labels = setup_data()
    print(train_observations.shape)
    clusterer = HMMCluster.HMMDistanceCluster(
        n_clusters=2,
        n_jobs=1,
        n_iterations_1=200,
        n_iterations_2=500,
        hmm_impl=GaussianVariationalHMM.GaussianVariationalHMM(
            n_components=3,
            n_jobs=1,
            tol=1e-6,
            n_inits=1
        ),
        log_level=logging.INFO,
        cluster_linkage="complete"
    )

    clusterer.fit(train_observations)
    predicted_test = clusterer.predict(test_observations)
    ct = pd.crosstab(test_labels, predicted_test)
    print(ct)
if __name__ == "__main__":
    main()
