# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
=============================
Finding Vowels and Consonants
=============================
"""
import argparse
import re
import time
import string
import os

from nltk.corpus import brown
import numpy as np
import pytest

from hmm import CategoricalHMM, CategoricalVariationalHMM


def setup():
    print("SETUP!")


def teardown():
    print("TEAR DOWN!")


def get_words(how_many):
    chars = " ".join(clean(w) for w in brown.words())
    mapping = {}
    rmapping = {}
    for i, item in enumerate(string.ascii_lowercase + " "):
        mapping[item] = i
        rmapping[i] = item
        print(i, rmapping[i])

    sequence = np.asarray([mapping[c] for c in chars[:how_many]])
    return sequence, rmapping


def get_model(args):
    if args.hmm_implementation == "variational":
        model = CategoricalVariationalHMM.CategoricalVariationalHMM(n_components=args.n_components, B_prior=1, verbose=True)
    elif args.hmm_implementation == "em":
        if args.reproduce:
            model = CategoricalHMM.CategoricalHMM(n_components=2, init_pi=None, init_A=None, init_emissions=None, implementation=args.numeric_implementation)
            model.pi_ = np.asarray([0.51316, .48684])
            model.A_ = np.asarray([
                    [0.47468, 0.52532],
                    [0.51656, 0.48344],
                ])
            model.B_ = np.asarray([
                    [0.03735, 0.03909],
                    [0.03408, 0.03537],
                    [0.03455, 0.03537],
                    [0.03828, 0.03909],
                    [0.03782, 0.03583],
                    [0.03922, 0.03630],
                    [0.03688, 0.04048],
                    [0.03408, 0.03537],
                    [0.03875, 0.03816],
                    [0.04062, 0.03909],
                    [0.03735, 0.03490],
                    [0.03968, 0.03723],
                    [0.03548, 0.03537],
                    [0.03735, 0.03909],
                    [0.04062, 0.03397],
                    [0.03595, 0.03397],
                    [0.03641, 0.03816],
                    [0.03408, 0.03676],
                    [0.04062, 0.04048],
                    [0.03548, 0.03443],
                    [0.03922, 0.03537],
                    [0.04062, 0.03955],
                    [0.03455, 0.03816],
                    [0.03595, 0.03723],
                    [0.03408, 0.03769],
                    [0.03408, 0.03955],
                    [0.03688, 0.03397],
                ]).T
        else:
            model = CategoricalHMM.CategoricalHMM(n_components=2, implementation=args.numeric_implementation)

    else:
        assert False, args
    return model


def clean(w):
    keep = [c for c in w.lower() if c in string.ascii_lowercase or c == " "]
    return "".join(keep)


def test_cython_small(args):
    sequence, rmapping = get_words(10000)
    # print(sequence)
    print("I RAN!")
    print((len(sequence)))

    model = get_model(args)
    sequence = sequence.reshape(1, sequence.shape[0]//1)
    run_model(model, sequence, rmapping, "cython-ish")
    print(model.pi_)
    print(model.A_)
    if args.reproduce and args.hmm_implementation == "em":
        assert model.pi_[0] == pytest.approx(2.81304922e-05, 1e-5)
        assert model.pi_[1] == pytest.approx(9.99971870e-01, 1e-5)
        check_A = [[0.21923583, 0.78076417],
                   [0.71224039, 0.28775961]]
        for i in range(model.A_.shape[0]):
            for j in range(model.A_.shape[1]):
                assert check_A[i][j] == pytest.approx(model.A_[i, j], 1e-6)
    model = get_model(args)

    sequence = sequence.reshape(4, sequence.shape[1]//4)
    run_model(model, sequence, rmapping, "cython-ish")
    print(model.pi_)
    print(model.A_)
    if args.reproduce and args.hmm_implementation == "em":
        assert model.pi_[0] == pytest.approx(0.22733858, 1e-5)
        assert model.pi_[1] == pytest.approx(0.77266142, 1e-5)
        check_A = [[0.21911191, 0.78088809],
                   [0.71275036, 0.28724964]]
        for i in range(model.A_.shape[0]):
            for j in range(model.A_.shape[1]):
                assert check_A[i][j] == pytest.approx(model.A_[i, j], 1e-6)


def test_cython_500000(args):
    sequence, rmapping = get_words(500000)
    # print(sequence)

    model = get_model(args)

    sequence2 = sequence.reshape(1, sequence.shape[0]//1)
    run_model(model, sequence2, rmapping, "cython-ish")
    if args.reproduce and args.hmm_implementation == "em":
        assert model.pi_[0] == pytest.approx(2.18313022e-10, 1e-10)
        assert model.pi_[1] == pytest.approx(1, 1e-8)
        check_A = [[0.21375057, 0.78624943],
                   [0.68957699, 0.31042301]]
        for i in range(model.A_.shape[0]):
            for j in range(model.A_.shape[1]):
                assert check_A[i][j] == pytest.approx(model.A_[i, j], 1e-5)

    model = get_model(args)

    sequence2 = sequence.reshape(4, sequence.shape[0]//4)
    run_model(model, sequence2, rmapping, "cython-ish")
    print(model.pi_)
    print(model.A_)
    if args.reproduce and args.hmm_implementation == "em":
        assert model.pi_[0] == pytest.approx(0.2878129878915469, 1e-5)
        assert model.pi_[1] == pytest.approx(0.7121870121084529, 1e-5)
        check_A = [[0.21375595, 0.78624405],
                   [0.68959022, 0.31040978]]
        for i in range(model.A_.shape[0]):
            for j in range(model.A_.shape[1]):
                assert check_A[i][j] == pytest.approx(model.A_[i, j], 1e-5)


def run_model(model, sequence, rmapping, tag):
    start = time.time()
    model.fit(sequence)
    end = time.time()
    print("Training Took {} seconds {}".format(end - start, tag))
    print(model.pi_)
    if hasattr(model, "pi_counts_"):
        print(model.pi_counts_)
    print(model.A_)
    for i in range(model.B_.shape[1]):
        print("|{}| {} {}".format(rmapping[i], model.B_[0, i], model.B_[1, i]))



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--numeric-implementation", choices=["scaling", "log"], required=True)
    parser.add_argument("--hmm-implementation", choices=["em", "variational"], required=True)
    parser.add_argument("--reproduce", action="store_true")
    parser.add_argument("--n_components", type=int, default=2)
    args = parser.parse_args()

    test_cython_small(args)
    test_cython_500000(args)
