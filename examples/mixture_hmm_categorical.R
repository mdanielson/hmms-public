library(seqHMM)
set.seed(1)

onlyfair <- sample(0:1, size = 100, replace = TRUE)

# 100/100 matrix

#A1 <- matrix(1,1,1)
#B1 <- matrix(c(0.5,0.5),1,2)
p1 <- c(0.5, 0.5)
A1 <- matrix(c(0.8,0.2,0.2,0.8),2,2)
B1 <- matrix(c(0.5, 0.2, 0, 0.8, 0.5, 0.0 ), 2,3)

A2 <- matrix(c(0.4,0.6,0.6, 0.4),2,2)

obs1 = simulate_hmm(100, c(0.2, 0.8), A1, B1, 100)
obs2 = simulate_hmm(200, c(0.5, 0.5), A2, B1, 100)


all_observations = rbind(obs1$observations, obs2$observations)
transition1 = matrix(c(0.5, 0.5, 0.5, 0.5), 2, 2)
transition2 = matrix(c(0.5, 0.5, 0.5, 0.5), 2, 2)

emission1 = matrix(runif(6), 2, 3)
emission2 = matrix(runif(6), 2, 3)
emission1 = emission1/rowSums(emission1)
emission2 = emission2/rowSums(emission2)

hmm <- build_mhmm(observations = seqdef(all_observations, states=c("one","two","three")), 
                  n_states=2, 
                  initial_probs = list(c(0.5, 0.5),c(0.5,0.5)),
                  transition_probs = list(transition1, transition2),
                  emission_probs = list(emission1, emission2),
                  formula=NULL)


fit <- fit_model(hmm, control_em=c(print_level=1))
#fit$model
summary(fit$model)

plot(fit$model)
