# Author: Matthew Danielson <mgd5@st-andrews.ac.uk>
# License: BSD 2 clause
"""
========================
Comparison with hmmlearn
========================
"""
import logging
import time
import os

import sklearn.utils
import sklearn.metrics
import sklearn.model_selection
import numpy as np
import pandas as pd

from hmm import GaussianHMM, HMMClassifier
import hmmlearn.hmm


def test_gaussian():
    print("cython")
    frame = pd.read_csv("sample_data.csv")
    cols = [c for c in frame.columns if c.startswith("col")]

    X = frame[cols]
    y = frame["y"]
    print(X.shape)
    train_X, test_X, train_y, test_y = sklearn.model_selection.train_test_split(
        X, y,
        stratify=y,
    )
    chmm = HMMClassifier.HMMClassifier(
        estimators={
            0:GaussianHMM.GaussianHMM(n_components=4, n_inits=1, n_iterations=500, verbose=True, implementation="scaling"),
            1:GaussianHMM.GaussianHMM(n_components=4, n_inits=1, n_iterations=500, verbose=True, implementation="scaling")
        }
    )
    chmm.fit(train_X, train_y)

    test_predicted = chmm.predict(test_X)
    test_probs = chmm.predict_proba(test_X)
    print(sklearn.metrics.roc_auc_score(test_y, test_probs[:, 1]))
    print(sklearn.metrics.confusion_matrix(test_y, test_predicted))
    print(sklearn.metrics.classification_report(test_y, test_predicted))

def test_hmmlearn():
    print("hmmlearn")
    random_state = 3
    sequence = generate_data()
    shape = sequence.shape
    sequence = sequence.ravel()
    sequence = sequence[:, None]
    lengths = [shape[0]] * shape[1]
    trainer = hmmlearn.hmm.GaussianHMM(n_components=3, n_iter=100, random_state=random_state, verbose=True)
    start = time.time()
    trainer.fit(sequence)
    end = time.time()
    print("Training Took {} seconds {}".format(end - start, "hmmlearn"))
    print(trainer.startprob_)
    print(trainer.transmat_)
    print(trainer.means_)
    print(trainer.covars_)
    print("SCORE")
    print(trainer.score(sequence))


if __name__ == "__main__":
    test_gaussian()
